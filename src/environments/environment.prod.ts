const buildinfo = require('../../.buildinfo.json');

let revisionInfo = '';
if (buildinfo.gitBranch) {
  revisionInfo =
    buildinfo.gitBranch +
    (buildinfo.gitRevision ? '@' + buildinfo.gitRevision : '');
}

export const buildInfo =
  buildinfo.npmPackageVersion +
  (revisionInfo ? ' - ' + revisionInfo : '') +
  ' - ' +
  buildinfo.timestamp;

export const version = buildinfo.npmPackageVersion + ' ' + buildinfo.timestamp;

export const environment = {
  production: true,
  mobile: false,
  buildInfo,
  version,
};
