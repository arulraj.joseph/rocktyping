import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WellCustomDataComponent } from './well-custom-data.component';

describe('WellCustomDataComponent', () => {
  let component: WellCustomDataComponent;
  let fixture: ComponentFixture<WellCustomDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WellCustomDataComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WellCustomDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
