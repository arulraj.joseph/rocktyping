import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IWellData } from '../../shared/entities';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'gng-well-custom-data',
  templateUrl: './well-custom-data.component.html',
  styleUrls: ['./well-custom-data.component.scss']
})
export class WellCustomDataComponent implements OnInit {
  @Output()
  dataFilled = new EventEmitter<IWellData>();

  form = new FormGroup({
    wellName: new FormControl(''),
    wellNo: new FormControl(''),
    UWI: new FormControl(''),
    country: new FormControl(''),
    state: new FormControl(''),
    reservoir: new FormControl(''),
    field: new FormControl('')
  });

  constructor() {}

  ngOnInit() {}

  submitData() {
    const pseudoWell: IWellData = {
      wellNo: this.form.get('wellNo').value,
      wellName: this.form.get('wellName').value,
      apiUwiNo: this.form.get('UWI').value,
      countyName: this.form.get('country').value,
      stateName: this.form.get('state').value,
      reservoir: this.form.get('reservoir').value,
      fieldName: this.form.get('field').value,

      isCustomMetadata: true
    };

    this.dataFilled.emit(pseudoWell);
  }
}
