import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WellSearchAdvancedComponent } from './well-search-advanced.component';

describe('WellSearchAdvancedComponent', () => {
  let component: WellSearchAdvancedComponent;
  let fixture: ComponentFixture<WellSearchAdvancedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [WellSearchAdvancedComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WellSearchAdvancedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
