import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IWellData } from '../../shared/entities';
import { FormGroup, FormControl } from '@angular/forms';
import { WellSearchService } from '../../shared/services/well-search.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'gng-well-search-advanced',
  templateUrl: './well-search-advanced.component.html',
  styleUrls: ['./well-search-advanced.component.scss']
})
export class WellSearchAdvancedComponent implements OnInit {
  readonly SnackbarDuration = 3000;
  readonly DisplayedColumns: string[] = [
    'wellName',
    'wellNo',
    'apiUwiNo',
    'countryName',
    'stateName',
    'reservoir',
    'fieldName'
  ];

  @Output()
  public wellSelected = new EventEmitter<IWellData>();

  loading = 0;
  wellSearchResult: IWellData[] = [];

  searchForm = new FormGroup({
    wellName: new FormControl(''),
    wellNo: new FormControl(''),
    UWI: new FormControl(''),
    country: new FormControl(''),
    state: new FormControl(''),
    reservoir: new FormControl(''),
    field: new FormControl('')
  });

  constructor(private snackBar: MatSnackBar, private wellSearchService: WellSearchService) {}

  ngOnInit() {}

  search(): void {
    this.loading++;

    this.wellSearchService
      .searchWells({
        wellName: this.searchForm.get('wellName').value as string,
        wellNo: this.searchForm.get('wellNo').value as string,
        apiUwiNo: this.searchForm.get('UWI').value as string,
        countryName: this.searchForm.get('country').value,
        stateName: this.searchForm.get('state').value as string,
        reservour: this.searchForm.get('reservoir').value as string,
        fieldName: this.searchForm.get('field').value as string,

        pageNo: 1,
        pageSize: 250
      })
      .subscribe(
        (data: IWellData[]) => {
          this.wellSearchResult = data;
          this.loading--;
        },
        () => {
          this.loading--;
          this.wellSearchResult = [];

          this.snackBar.open('ANORM search failed, please try again later', 'Ok', {
            duration: this.SnackbarDuration,
            panelClass: 'snackbar--error'
          });
        }
      );
  }

  selectWell(well: IWellData): void {
    this.wellSelected.emit(well);
  }
}
