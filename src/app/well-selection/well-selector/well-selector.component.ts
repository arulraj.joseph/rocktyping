import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { IWellData } from '../../shared/entities/well-data';
import { CurrentWellService } from '../../shared/services/current-well.service';
import { WellSearchAdvancedComponent } from '../well-search-advanced/well-search-advanced.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'gng-well-selector',
  templateUrl: './well-selector.component.html',
  styleUrls: ['./well-selector.component.scss']
})
export class WellSelectorComponent implements OnInit {
  @Output() onWellSelected: EventEmitter<IWellData> = new EventEmitter();

  selectedWell: IWellData;

  advancedSearchEnabled = false;
  changeWell = false;

  constructor(public dialog: MatDialog, private currentWellService: CurrentWellService) {}

  ngOnInit() {
    this.currentWellService.currentWell.subscribe((well: IWellData) => (this.selectedWell = well));
  }

  wellSelected(well: IWellData) {
    this.currentWellService.selectWell(well);

    this.onWellSelected.emit(well);

    this.advancedSearchEnabled = false;
    this.changeWell = false;
  }

  openAdvancedSearchDialog(): void {
    const dialogRef = this.dialog.open(WellSearchAdvancedComponent, {});

    dialogRef.afterClosed().subscribe((result: IWellData) => {});
  }

  toggleAdvancedSearch(): void {
    this.advancedSearchEnabled = !this.advancedSearchEnabled;
  }

  enableWellSelection(): void {
    this.changeWell = true;
  }
}
