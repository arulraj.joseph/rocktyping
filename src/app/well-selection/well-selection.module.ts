import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WellSelectorComponent } from './well-selector/well-selector.component';
import { WellSearchBasicComponent } from './well-search-basic/well-search-basic.component';
import { CommonModule } from '@angular/common';
import {
  MatInputModule,
  MatAutocompleteModule,
  MatProgressSpinnerModule,
  MatCardModule,
  MatButtonModule,
  MatTableModule
} from '@angular/material';
import { WellInfoComponent } from './well-info/well-info.component';
import { WellSearchAdvancedComponent } from './well-search-advanced/well-search-advanced.component';
import { WellCustomDataComponent } from './well-custom-data/well-custom-data.component';

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatProgressSpinnerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    WellSearchBasicComponent,
    WellSelectorComponent,
    WellInfoComponent,
    WellSearchAdvancedComponent,
    WellCustomDataComponent
  ],
  providers: [],
  exports: [WellSelectorComponent],
  entryComponents: [WellSelectorComponent]
})
export class WellSelectionModule {}
