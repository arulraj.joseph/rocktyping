import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { distinctUntilChanged, debounce } from 'rxjs/operators';
import { Subscription, Subject, interval } from 'rxjs';

import { IWellData } from '../../shared/entities';
import { WellSearchService } from '../../shared/services/well-search.service';

@Component({
  selector: 'gng-well-search-basic',
  templateUrl: './well-search-basic.component.html',
  styleUrls: ['./well-search-basic.component.scss']
})
export class WellSearchBasicComponent {
  private readonly DebounceTime = 300;

  @Output()
  public wellSelected = new EventEmitter<IWellData>();

  form = new FormGroup({
    query: new FormControl()
  });

  wellsOptions: IWellData[];
  wellsSubscription: Subscription;
  wellsSearchChanged: Subject<string> = new Subject<string>();

  loading = 0;

  constructor(private wellSearchService: WellSearchService) {
    this.wellsSearchChanged
      .pipe(
        debounce(() => interval(this.DebounceTime)),
        distinctUntilChanged()
      )
      .subscribe((query) => {
        this.searchWells(query);
      });
  }

  searchWells(query: string): void {
    this.loading++;
    this.wellsSubscription = this.wellSearchService.searchWells(query).subscribe(
      (data) => {
        this.wellsOptions = data;
        this.loading--;
      },
      () => this.loading--
    );
  }

  wellSearchQueryChanged(): void {
    this.wellsOptions = [];

    if (this.wellsSubscription) {
      this.wellsSubscription.unsubscribe();

      if (this.loading > 0) {
        this.loading--;
      }
    }

    this.wellsSearchChanged.next(this.form.get('query').value);
  }

  onWellSelected(well: IWellData) {
    this.form.reset();

    this.wellSelected.emit(well);
  }
}
