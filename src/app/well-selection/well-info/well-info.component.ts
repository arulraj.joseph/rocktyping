import { Component, Input } from '@angular/core';

import { IWellData } from '../../shared/entities';

@Component({
  selector: 'gng-well-info',
  templateUrl: './well-info.component.html',
  styleUrls: ['./well-info.component.scss']
})
export class WellInfoComponent {
  @Input()
  wellData: IWellData;

  constructor() {}
}
