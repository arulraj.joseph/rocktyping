import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IUnit } from '../models';
import { copyToClipboard } from '../utils/copy-to-clipboard';
import { StringHelper } from '../utils/beautifier';

@Component({
  selector: 'uc-conversion-box',
  templateUrl: './conversion-box.component.html',
  styleUrls: ['./conversion-box.component.scss']
})
export class ConversionBoxComponent {
  @Input() value: string;
  @Input() currentUnit: IUnit;
  @Input() units: IUnit[] = [];
  @Output() valueChanged = new EventEmitter<string>();
  @Output() selectedUnit = new EventEmitter<IUnit>();

  isExpanded: boolean = false;
  isCopied: boolean = false;
  searchQuery: string = '';

  constructor() {}

  setCurrentUnit(unit: IUnit): void {
    this.selectedUnit.emit(unit);
    this.isExpanded = false;
  }

  changeValue(event): void {
    const parsedValue = event.target.value;
    this.valueChanged.emit(parsedValue);
  }

  copyToClipboard(valueInput) {
    copyToClipboard(valueInput);
    this.isCopied = true;
  }

  beautifyName(name: string): string {
    return StringHelper.beautifyName(name);
  }

  openUnitsPanel(): void {
    this.isExpanded = true;
    this.searchQuery = '';
  }

  getInputFontSize(): string {
    if (!this.value || !this.value.length) {
      return '100%';
    }

    const fontPercentage = 100 - 5 * Math.max(0, this.value.length - 13);

    return Math.max(50, fontPercentage) + '%';
  }
}
