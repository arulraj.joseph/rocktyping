import { TestBed } from '@angular/core/testing';

import { UnitsConverterService } from './units-converter.service';

describe('UnitsConverterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UnitsConverterService = TestBed.get(UnitsConverterService);
    expect(service).toBeTruthy();
  });
});
