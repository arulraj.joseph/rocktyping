import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { IUnitsGroup } from '../models';
import { UnitsConverterService } from '../units-converter.service';
import { ConverterAddFormComponent } from '../converter-add-form/converter-add-form.component';
import { ConverterAddFormulaComponent } from '../converter-add-formula/converter-add-formula.component';
import { StringHelper } from '../utils/beautifier';
import { UnitListComponent } from '../unit-list/unit-list.component';

@Component({
  selector: 'uc-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  groupedUnits: IUnitsGroup[];
  addForm: FormGroup;
  formulas: string[];

  constructor(private unitsConverterService: UnitsConverterService, public dialog: MatDialog) {}

  ngOnInit() {
    this.unitsConverterService
      .getAllUnits()
      .subscribe((data) => (this.groupedUnits = data || []), () => (this.groupedUnits = []));
  }

  beautifyName(name: string) {
    return StringHelper.beautifyName(name);
  }

  openConverterAddForm(): void {
    const oldAddForm = this.addForm || null;
    const dialogRef = this.dialog.open(ConverterAddFormComponent, {
      data: oldAddForm,
      panelClass: 'add-form',
      closeOnNavigation: true
    });

    dialogRef.afterClosed().subscribe((form?: FormGroup) => {
      this.addForm = form;

      if (!!form) {
        this.openConverterFormulaForm(this.addForm.value.units);
      }
    });
  }

  openConverterFormulaForm(units: string[]): void {
    const dialogRef = this.dialog.open(ConverterAddFormulaComponent, {
      data: units,
      panelClass: 'add-form',
      closeOnNavigation: true
    });

    dialogRef.afterClosed().subscribe((formulasForm: string[]) => {
      this.formulas = formulasForm;

      if (!!formulasForm) {
        this.openConverterAddForm();
      } else {
        this.addForm = null;
      }
    });
  }

  openUnitListForm() {
    this.dialog.open(UnitListComponent, {
      data: this.groupedUnits,
      closeOnNavigation: true,
      height: '500px'
    });
  }
}
