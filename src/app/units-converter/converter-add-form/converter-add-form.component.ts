import { Component, Output, EventEmitter, OnInit, Inject } from '@angular/core';
import { MatRadioChange, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators, FormArray, FormGroup } from '@angular/forms';
import { deleteEmptyArrayFields } from '../utils/delete-empty-array-fields';

@Component({
  selector: 'uc-converter-add-form',
  templateUrl: './converter-add-form.component.html',
  styleUrls: ['./converter-add-form.component.scss']
})
export class ConverterAddFormComponent implements OnInit {
  @Output() change: EventEmitter<MatRadioChange>;
  addForm = this.fb.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    units: this.fb.array([this.fb.control('', Validators.required)])
  });
  minUnitsAmount: number = 5;

  constructor(
    public dialogRef: MatDialogRef<ConverterAddFormComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: FormGroup
  ) {}

  get units(): FormArray {
    return this.addForm.get('units') as FormArray;
  }

  addUnits() {
    this.units.push(this.fb.control(null));
  }

  ngOnInit() {
    if (!!this.data) {
      this.addForm = this.data;
    } else {
      for (let i = 1; i < this.minUnitsAmount; i++) {
        this.addUnits();
      }
    }
  }

  nextForm() {
    const units = deleteEmptyArrayFields(this.addForm.value.units);

    return units.length < 2 ? false : this.dialogRef.close(this.addForm);
  }

  closeForm() {
    this.dialogRef.close(null);
  }
}
