import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IConvertRequestData, IConvertResponseData, IUnitsGroup, IUnit } from './models';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UnitsConverterService {
  constructor(private httpClient: HttpClient) {}

  convertUnits(data: IConvertRequestData): Observable<IConvertResponseData> {
    return this.httpClient.post<IConvertResponseData>('{UNIT_CONVERTER_SERVICE}/services/convert', data);
  }

  getAllUnits(): Observable<IUnitsGroup[]> {
    return this.httpClient.get<{ results: IUnitsGroup[] }>('{UNIT_CONVERTER_SERVICE}/services/unit/all').pipe(
      map((data) => {
        const result = (data.results || []).filter((x) => x.isConversion).sort(this.sortByName);

        result.forEach((group) => {
          group.units = group.units.filter((unit) => unit.isConversion).sort(this.sortByName);
        });

        return result;
      })
    );
  }

  sortByName(a: IUnitsGroup | IUnit, b: IUnitsGroup | IUnit) {
    const firstName = a.displayName || a.name;
    const secondName = b.displayName || b.name;

    return firstName < secondName ? -1 : firstName > secondName ? 1 : 0;
  }
}
