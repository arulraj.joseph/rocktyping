export interface IUnit {
  isConversion: boolean;
  name: string;
  displayName: string;
  formula: string;
  groups: string[];
  symbol: string;
  aliases: string[];
}
export interface IUnitsGroup {
  name: string;
  displayName: string;
  description: string;
  units: IUnit[];
  isConversion: boolean;
}

export interface IConvertRequestData {
  from: string;
  to: string;
  amount: number[];
}
export interface IConvertResponseData {
  from: string;
  to: string;
  amount: number[];
  result: number[];
  created_at: string;
}
