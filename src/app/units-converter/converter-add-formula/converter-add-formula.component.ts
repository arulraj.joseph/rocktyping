import { Component, Output, EventEmitter, Inject } from '@angular/core';
import { MatRadioChange, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'uc-converter-add-formula',
  templateUrl: './converter-add-formula.component.html',
  styleUrls: ['./converter-add-formula.component.scss']
})
export class ConverterAddFormulaComponent {
  @Output() change: EventEmitter<MatRadioChange>;
  formulaForm = this.fb.group({
    formulas: this.fb.array([this.fb.control('')])
  });

  constructor(
    public dialogRef: MatDialogRef<ConverterAddFormulaComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}

  get units(): FormArray {
    return this.formulaForm.get('units') as FormArray;
  }

  prevForm() {
    this.dialogRef.close(this.formulaForm.value.formulas);
  }

  closeForm() {
    this.dialogRef.close(null);
  }
}
