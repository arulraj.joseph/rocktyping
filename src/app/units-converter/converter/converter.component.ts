import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, of, merge } from 'rxjs';
import { map, switchMap, first } from 'rxjs/operators';
import { IUnit, IUnitsGroup } from '../models';
import { UnitsConverterService } from '../units-converter.service';
import { StringHelper } from '../utils/beautifier';

@Component({
  selector: 'uc-converter',
  templateUrl: './converter.component.html',
  styleUrls: ['./converter.component.scss']
})
export class ConverterComponent implements OnInit {
  @Input() group: IUnitsGroup;
  leftUnit$: BehaviorSubject<IUnit>;
  rightUnit$: BehaviorSubject<IUnit>;
  leftUnitAmountFromUserInput$: BehaviorSubject<string> = new BehaviorSubject('0');
  rightUnitAmountFromUserInput$: BehaviorSubject<string> = new BehaviorSubject('0');
  rightResultAmount$: Observable<string>;
  leftResultAmount$: Observable<string>;
  constructor(private unitsConverterService: UnitsConverterService) {}

  ngOnInit() {
    this.leftUnit$ = new BehaviorSubject(this.group.units[0]);
    this.rightUnit$ = new BehaviorSubject(this.group.units[0]);

    this.rightResultAmount$ = merge(
      this.rightUnitAmountFromUserInput$,
      this.convertUnit(this.leftUnit$, this.rightUnit$, this.leftUnitAmountFromUserInput$)
    );

    this.leftResultAmount$ = merge(
      this.leftUnitAmountFromUserInput$,
      this.rightUnitAmountFromUserInput$.pipe(
        switchMap(() =>
          this.convertUnit(this.rightUnit$, this.leftUnit$, this.rightUnitAmountFromUserInput$).pipe(first())
        )
      )
    );
  }

  changeLeftUnit(unit: IUnit) {
    this.leftUnit$.next(unit);
  }
  changeRightUnit(unit: IUnit) {
    this.rightUnit$.next(unit);
  }
  changeLeftUnitAmount(amount: string) {
    this.leftUnitAmountFromUserInput$.next(amount);
  }
  changeRightUnitAmount(amount: string) {
    this.rightUnitAmountFromUserInput$.next(amount);
  }

  private convertUnit(from$: Observable<IUnit>, to$: Observable<IUnit>, amount$: Observable<string>) {
    return combineLatest(from$, to$, amount$).pipe(
      switchMap(([from, to, rawAmount]) => {
        const parsedAmount = parseFloat(rawAmount);
        const amount = isNaN(parsedAmount) ? 0 : parsedAmount;

        if (from.name === to.name) {
          return of({ result: [amount] });
        }

        return this.unitsConverterService.convertUnits({
          from: from.name,
          to: to.name,
          amount: [amount]
        });
      }),
      map((data) => data.result[0] + '')
    );
  }

  beautifyName(name: string) {
    return StringHelper.beautifyName(name);
  }
}
