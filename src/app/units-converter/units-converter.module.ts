import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatExpansionModule,
  MatInputModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';
import { ConversionBoxComponent } from './conversion-box/conversion-box.component';
import { PageComponent } from './page/page.component';
import { ConverterComponent } from './converter/converter.component';
import { ConverterAddFormComponent } from './converter-add-form/converter-add-form.component';
import { ConverterAddFormulaComponent } from './converter-add-formula/converter-add-formula.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { UnitListComponent } from './unit-list/unit-list.component';
import { UnitsFilterPipe } from './utils';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatExpansionModule,
    MatInputModule,
    MatTabsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    SharedModule
  ],
  declarations: [
    ConversionBoxComponent,
    PageComponent,
    ConverterComponent,
    ConverterAddFormComponent,
    ConverterAddFormulaComponent,
    UnitListComponent,
    UnitsFilterPipe
  ],
  entryComponents: [ConverterAddFormComponent, ConverterAddFormulaComponent, UnitListComponent]
})
export class UnitsConverterModule {}
