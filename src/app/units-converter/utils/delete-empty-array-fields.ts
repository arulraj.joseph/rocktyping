export const deleteEmptyArrayFields = (array: string[]): string[] => {
  return array.filter((field) => !!field);
};
