export class StringHelper {
  public static beautifyName(str: string): string {
    if (!str) {
      return '';
    }

    return str
      .replace(/[_\-]/g, ' ')
      .split(' ')
      .map((x) => this.toTitleCase(x))
      .join(' ');
  }

  public static toTitleCase(str: string) {
    if (!str) {
      return '';
    }

    return str.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
  }
}
