export const copyToClipboard = (valueInput): void => {
  valueInput.select();
  document.execCommand('copy');
  valueInput.blur();
};
