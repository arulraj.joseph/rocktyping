import { Pipe, PipeTransform } from '@angular/core';
import { IUnit } from '../models';

@Pipe({
  name: 'unitsFilter',
  pure: true
})
export class UnitsFilterPipe implements PipeTransform {
  transform(items: IUnit[], term: string): IUnit[] {
    if (!term) {
      return items;
    }

    term = term.toLowerCase().trim();

    return items
      .filter((x) => !!x)
      .filter((item) => {
        const displayName = (item.displayName || item.name || '').trim().toLowerCase();

        return displayName.indexOf(term) >= 0;
      });
  }
}
