import { UnitsConverterModule } from './units-converter.module';

describe('UnitsConverterModule', () => {
  let unitsConverterModule: UnitsConverterModule;

  beforeEach(() => {
    unitsConverterModule = new UnitsConverterModule();
  });

  it('should create an instance', () => {
    expect(unitsConverterModule).toBeTruthy();
  });
});
