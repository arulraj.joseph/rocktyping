import { AuthGuard } from '../routing/auth.guard';
import { PageComponent } from './page/page.component';

export const UNITS_CONVERTER_ROUTES = [
  {
    path: '',
    component: PageComponent,
    canActivate: [AuthGuard]
  }
];
