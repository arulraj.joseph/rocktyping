import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IUnitsGroup } from '../models';
import { StringHelper } from '../utils/beautifier';

@Component({
  selector: 'uc-unit-list',
  templateUrl: './unit-list.component.html',
  styleUrls: ['./unit-list.component.scss']
})
export class UnitListComponent {
  constructor(public dialogRef: MatDialogRef<UnitListComponent>, @Inject(MAT_DIALOG_DATA) public data: IUnitsGroup[]) {}

  beautifyName(name: string): string {
    return StringHelper.beautifyName(name);
  }
}
