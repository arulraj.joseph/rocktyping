/**
 * contains the App Constants
 */
export class AppConstants {
  // the cts data columns
  public static CTSDataColumns = [
    {
      label: 'STAGES'
    },
    {
      label: 'FROM (ft)'
    },
    {
      label: 'TO (ft)'
    },
    {
      label: '(MIN/STAND)'
    },
    {
      label: '(ft/h)'
    },
    {
      label: '# STANDS'
    },
    {
      label: 'TIME (hr)'
    }
  ];

  // the data values abbreviated
  public static DataValuesAbbreviated = [
    {
      text: 'Sample ID'
    },
    {
      text: 'P',
      tooltip: 'Reservoir pressure'
    },
    {
      text: 'T',
      tooltip: 'Reservoir temperature'
    },
    {
      text: 'SGA',
      tooltip: 'Oil gravity'
    },
    {
      text: 'SGG',
      tooltip: 'Specific gas gravity'
    },
    {
      text: 'Pb',
      tooltip: 'Oil bubble-point pressure'
    },
    {
      text: 'Rs',
      tooltip: 'Solution GOR'
    },
    {
      text: 'Rsb',
      tooltip: 'Solution GOR at P=Pb'
    },
    {
      text: 'TDS',
      tooltip: 'Water salinity'
    },
    {
      text: 'y_H2S',
      tooltip: 'H2S mole fraction'
    },
    {
      text: 'y_CO2',
      tooltip: 'CO2 mole fraction'
    },
    {
      text: 'y_N2',
      tooltip: 'N2 mole fraction'
    }
  ];
}
