import { NgModule } from '@angular/core';

import { WellSelectionModule } from '../well-selection/well-selection.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [SharedModule, WellSelectionModule],
  declarations: [DashboardComponent],
  entryComponents: [DashboardComponent]
})
export class DashboardModule {}
