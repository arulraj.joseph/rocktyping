import { AfterViewInit, Component, OnInit, OnDestroy } from '@angular/core';
import { Menu } from '@apc-ng/theme-material';
import { navItems } from './routing/routing.module';
import { AppPropertiesService } from '@apc-ng/config';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { ProgressBarService } from './core/services/progress-bar/progress-bar.service';

@Component({
  selector: 'gng-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit, OnInit, OnDestroy {
  menuItems: Menu[] = [];
  /**
   * Flag to show/hide spinner
   *
   * @memberof ProgressBarService
   */
  showProgressBar = false;
  /**
   * Subscription variable for progress bar
   *
   * @memberof ProgressBarService
   */

  progressBarSubscription: Subscription;

  constructor(
    private configService: AppPropertiesService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private progressBarSvc: ProgressBarService
  ) {
    this.matIconRegistry.addSvgIcon(
      `icon-calculator`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/images/plus-minus.svg')
    );

    this.progressBarSubscription = this.progressBarSvc.progressBarObservable.subscribe((flag) => {
      this.showProgressBar = flag;
    });
  }

  ngOnInit() {
    this.configService.getAppProperties().subscribe(
      (appConfig) => {
        if (!appConfig || !appConfig.app || !appConfig.app.modules || !appConfig.app.modules.length) {
          return navItems;
        }

        const enabledModules = (appConfig.app.modules as string[]).map((x) => x.trim().toLowerCase());

        this.menuItems = navItems.filter((nav) => this.filterNavItems(nav, enabledModules));
      },
      () => (this.menuItems = navItems)
    );
  }

  ngAfterViewInit() {}

  private filterNavItems(nav: Menu, enabledModules: string[]): boolean {
    return !!enabledModules.find((m) => {
      if (!nav.data || typeof nav.data.code !== 'string') {
        return false;
      }

      return m === nav.data.code.trim().toLowerCase();
    });
  }

  ngOnDestroy() {
    this.progressBarSubscription.unsubscribe();
  }
}
