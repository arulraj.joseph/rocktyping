import { Routes } from '@angular/router';
import { AuthGuard } from '../routing/auth.guard';
import { CalculatorComponent } from './pages/calculator/calculator/calculator.component';

export const OVERBURDEN_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'calculator',
    pathMatch: 'full'
  },
  {
    path: 'calculator',
    component: CalculatorComponent,
    canActivate: [AuthGuard]
  }
];
