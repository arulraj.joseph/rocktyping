import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import {
  MatBadgeModule,
  MatButtonModule,
  MatChipsModule,
  MatDialogModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatInputModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';
import { FileDropModule } from 'ngx-file-drop';

import { CalculatorComponent } from './pages/calculator/calculator/calculator.component';
import { DensityChartComponent } from './pages/calculator/density-chart/density-chart.component';
import { LineChartComponent } from './pages/calculator/line-chart/line-chart.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PerfectScrollbarModule,
    CommonModule,
    MatBadgeModule,
    MatButtonModule,
    MatChipsModule,
    MatDialogModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatInputModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    FileDropModule
  ],
  declarations: [CalculatorComponent, LineChartComponent, DensityChartComponent],
  exports: [
    MatIconModule,
    MatSelectModule,
    FileDropModule,
    MatButtonModule,
    MatTableModule,
    PerfectScrollbarModule,
    LineChartComponent,
    DensityChartComponent
  ]
})
export class OverburdenModule {}
