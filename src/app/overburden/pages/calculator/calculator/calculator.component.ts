import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { FileSystemFileEntry, UploadEvent, UploadFile } from 'ngx-file-drop';
import { OverburdenService } from '../../../../core/services/overburden/overburden.service';
import { ToastService } from '../../../../core/services/toast/toast.service';

@Component({
  selector: 'overburden-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
  c1 = null;
  c2 = null;
  detailView = false;
  manual = false;
  selectedData = false;
  fileName = '';
  displayedColumns = ['from', 'to', 'density', 'action'];
  dataNoSource: any = [];
  dataSource: any = [];
  optionsUnits = [{ label: 'Feet', id: 'feet' }, { label: 'Inch', id: 'inch' }, { label: 'CM', id: 'cm' }];
  totalDepthError = false;

  optionsRecentlyOpen = [
    { label: 'Overburden-27-june-2018-13-25.csv', id: '1' },
    { label: 'Overburden-27-june-2018-11-25.csv', id: '2' },
    { label: 'Overburden-26-june-2018-9-10.csv', id: '3' },
    { label: 'Overburden-26-june-2018-11-00.csv', id: '4' },
    { label: 'Overburden-25-june-2018-15-30.csv', id: '5' },
    { label: 'Overburden-25-june-2018-14-20.csv', id: '6' }
  ];
  dataInputs = {
    units: 'feet',
    airGap: '',
    avgWaterDensity: '',
    waterDepth: '',
    totalVerticalDepth: ''
  };

  noDataInput: any = {
    from: '',
    to: '',
    density: ''
  };

  dataInput: any = {
    from: '',
    to: '',
    density: ''
  };

  constructor(private dataService: OverburdenService, private toast: ToastService) {}

  ngOnInit() {
    this.dataService.getWellFileDetails().subscribe((data: any) => {
      this.c1 = data.c1;
      this.c2 = data.c2;
    });
  }

  public dropped(event: UploadEvent) {
    const droppedFile: UploadFile = event.files[0];

    // Is it a file?
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        this.dataService.readCsvFile(file).subscribe((dataSource: any) => {
          const hasDepthRangeGaps = dataSource.some((d, i) => {
            if (dataSource[i - 1] && d.from !== dataSource[i - 1].to) {
              return (d.invalid = true);
            }
          });

          if (hasDepthRangeGaps) {
            this.toast.error(`There should be no gaps between Depth Ranges. Please correct the highlighted errors.`);
          }

          this.dataSource = [...dataSource];
          this.dataNoSource = [];

          this.fileName = file.name;
          this.selectedData = true;
        });
      });
    }
  }

  addManually() {
    this.dataSource = [];
    this.dataNoSource = [];
    this.selectedData = true;
    this.manual = true;
  }

  openSelectedFile(item) {
    if (item) {
      if (item.value) {
        const file: any = _.find(this.optionsRecentlyOpen, { id: item.value });
        this.fileName = file.label;
      }
      this.selectedData = true;
      this.manual = false;
      this.dataService.getWellFileDetails().subscribe((data: any) => {
        this.dataNoSource = data.noData;
        this.dataSource = data.data;
      });
    }
  }

  addData() {
    if (this.dataInput.to === '' || this.dataInput.from === '' || this.dataInput.density === '') {
      return false;
    }

    const last = this.dataSource[this.dataSource.length - 1];
    if (last && this.dataInput.from !== last.to) {
      this.toast.error('There should be no gaps between Depth Ranges');
      return;
    }

    this.dataSource = [...this.dataSource, { ...this.dataInput, id: _.random(100, 1000) }];

    this.dataInput = {
      from: this.dataInput.to,
      to: '',
      density: ''
    };
  }

  removeData(item) {
    const index = _.findIndex(this.dataSource, { id: item.id });
    this.dataSource.splice(index, 1);
    this.dataSource = [...this.dataSource];
  }

  editData(item) {
    item.edit = true;
  }

  saveData(item) {
    const index = _.findIndex(this.dataSource, { id: item.id });
    const prev = this.dataSource[index - 1];
    item.invalid = prev && item.from !== prev.to;

    if (item.invalid) {
      this.toast.error(`There should be no gaps between Depth Ranges. Please correct the highlighted errors.`);
    }

    item.edit = false;
  }

  backToImport() {
    this.manual = false;
    this.selectedData = false;
  }

  calculate() {
    this.detailView = true;
  }

  checkTotalVerticalDept() {
    const { totalVerticalDepth, waterDepth, airGap } = this.dataInputs;

    this.totalDepthError = Number(totalVerticalDepth) < Number(waterDepth) + Number(airGap);
  }
}
