import { Component, ElementRef, HostListener, Input, OnChanges, ViewChild } from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'overburden-app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnChanges {
  @Input() data = [];
  @Input() xAxisLabel = 'Pressure (ppg)';
  @Input() yAxisMax = 20;
  @Input() yAxisLabel = 'Depth (feet)';
  @ViewChild('svgChart') svgChart: ElementRef;
  tooltip = null;

  constructor(private _el: ElementRef) {}

  ngOnChanges(): void {
    this.resizeChart();
  }

  /**
   * handles the resize chart
   */
  resizeChart() {
    const widthContent = this._el.nativeElement.offsetWidth;
    if (this.data && this.data.length > 0 && widthContent > 0) {
      this.drawChart();
    }
  }

  /**
   * renders a new svg chart in the specified div using the data passed in
   */
  drawChart() {
    const dataset = [...this.data];
    const widthContent = this._el.nativeElement.offsetWidth;
    const heightContent = this._el.nativeElement.offsetHeight - 62;
    // remove svg
    d3.select(this.svgChart.nativeElement.querySelector('svg')).remove();
    d3.select(this.svgChart.nativeElement.querySelector('div')).remove();
    this.tooltip = d3
      .select(this.svgChart.nativeElement)
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);
    // setup dimensions based on size of container element
    const margin = { top: 88, right: 45, bottom: 67, left: 86 };
    const width = widthContent - margin.left - margin.right;
    const height = heightContent - margin.top - margin.bottom;
    const svg = d3
      .select(this.svgChart.nativeElement)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    const n = this.yAxisMax;

    const xScale = d3
      .scaleLinear()
      .domain([0, n - 1]) // input
      .range([0, width]); // output

    const yScale = d3
      .scaleLinear()
      .domain([15000, 0]) // input
      .range([height, 0]); // output

    const line: any = d3
      .line()
      .x((d: any) => {
        return xScale(d.x);
      })
      .y((d: any) => {
        return yScale(d.y);
      })
      .curve(d3.curveMonotoneX);

    const bAxis = d3.axisBottom(xScale).ticks(4);
    svg
      .append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + height + ')')
      .call(bAxis);

    const lAxis = d3.axisLeft(yScale).ticks(4);
    svg
      .append('g')
      .attr('class', 'y axis')
      .call(lAxis);

    svg
      .append('path')
      .datum(dataset)
      .attr('class', 'line')
      .attr('d', line);

    svg
      .selectAll('.dot')
      .data(dataset)
      .enter()
      .append('circle')
      .attr('class', 'dot')
      .attr('cx', (d: any) => {
        return xScale(d.x);
      })
      .attr('cy', (d: any) => {
        return yScale(d.y);
      })
      .attr('r', 5)
      .on('mouseover', (d: any) => {
        this.tooltip
          .transition()
          .duration(200)
          .style('opacity', 0.9);
        this.tooltip
          .html(
            '<div class="arrow-box"><div>Depth <span>' +
              d.y +
              '<small>ft</small></span></div>' +
              '<div>σV <span>' +
              d['σV'] +
              '<small>PSI</small></span></div>' +
              '<div>σH <span>' +
              d['σH'] +
              '<small>PSI</small></span></div>' +
              '<div>σh <span>' +
              d['σh'] +
              '<small>PSI</small></span></div>' +
              '<div>Pp <span>' +
              d['Pp'] +
              '<small>PSI</small></span></div>' +
              '<div>σE <span>' +
              d['σE'] +
              '<small></small></span></div>' +
              '<div>σm <span>' +
              d['σm'] +
              '<small></small></span></div></div> '
          )
          .style('left', d3.event.pageX - margin.left - 180 + 'px')
          .style('top', d3.event.pageY - margin.top - 80 + 'px');
      })
      .on('mouseout', () => {
        this.tooltip
          .transition()
          .duration(500)
          .style('opacity', 0);
      });

    svg
      .append('g')
      .attr('class', 'grid')
      .attr('transform', 'translate(0,' + height + ')')
      .call(bAxis.tickSize(-height).tickFormat(() => ''));

    svg
      .append('g')
      .attr('class', 'grid')
      .call(lAxis.tickSize(-width).tickFormat(() => ''));

    svg
      .append('text')
      .attr('class', 'label')
      .attr('transform', 'translate(' + (width - 5) + ' ,' + (height - 10) + ')')
      .style('text-anchor', 'end')
      .text(this.xAxisLabel);

    svg
      .append('text')
      .attr('class', 'label')
      .attr('transform', 'translate(' + 0 + ' ,' + -10 + ')')
      .style('text-anchor', 'start')
      .text(this.yAxisLabel);
  }

  /**
   * handles the windows resize
   * @param event the event
   */
  @HostListener('window:resize', ['$event']) onResize(event) {
    this.resizeChart();
  }
}
