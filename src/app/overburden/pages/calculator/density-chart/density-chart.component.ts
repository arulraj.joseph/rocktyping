import { Component, ElementRef, HostListener, Input, OnChanges, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import * as _ from 'lodash';

@Component({
  selector: 'overburden-density-chart',
  templateUrl: './density-chart.component.html',
  styleUrls: ['./density-chart.component.scss']
})
export class DensityChartComponent implements OnChanges {
  @Input() data = [];
  @Input() noData = [];
  @Input() xAxisLabel = 'Pressure (ppg)';
  @Input() yAxisMax = 20;
  @Input() yAxisLabel = 'Depth (feet)';
  @ViewChild('svgChart') svgChart: ElementRef;
  tooltip = null;

  constructor(private _el: ElementRef) {}

  ngOnChanges(): void {
    if (this.data && this.data.length > 0) {
      this.drawChart();
    }
  }

  /**
   * handles the resize chart
   */
  resizeChart() {
    const widthContent = this._el.nativeElement.offsetWidth;
    if (this.data && this.data.length > 0 && widthContent > 0) {
      this.drawChart();
    }
  }

  /**
   * renders a new svg chart in the specified div using the data passed in
   */
  drawChart() {
    const margin = { top: 90, right: 10, bottom: 30, left: 10 };
    const width = 377 - margin.left - margin.right; // Use the window's width
    const height = 550 - margin.top - margin.bottom; // Use the window's height

    const n = 5000;

    const xScale = d3
      .scaleLinear()
      .domain([0, n - 1]) // input
      .range([0, height / 2]); // output

    const yScale = d3
      .scaleLinear()
      .domain([0, 15]) // input
      .range([width / 2, width]); // output

    const line = d3
      .line()
      .y((d: any) => {
        return xScale(d.x);
      }) // set the x values for the line generator
      .x((d: any) => {
        return yScale(d.y);
      }) // set the y values for the line generator
      .curve(d3.curveMonotoneY); // apply smoothing to the line

    const datasetData = [];
    _.each(this.data, (item) => {
      datasetData.push({ x: item.from - 5000, y: item.density });
    });
    const datasetNoData = [];
    _.each(this.noData, (item) => {
      datasetNoData.push({ x: item.from, y: item.density });
    });

    d3.select(this.svgChart.nativeElement.querySelector('svg')).remove();
    d3.select(this.svgChart.nativeElement.querySelector('div')).remove();
    this.tooltip = d3
      .select(this.svgChart.nativeElement)
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);
    const svg = d3
      .select(this.svgChart.nativeElement)
      .append('svg')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

    const types = [{ name: 'noData', offset: 0 }, { name: 'data', offset: 0 }];
    _.each(types, (type) => {
      let count = 0;
      let last = 0;
      while (count < this[type.name].length - 2) {
        if (this[type.name][count].to !== this[type.name][count + 1].from) {
          svg
            .append('rect')
            .attr('class', 'bar')
            .attr('x', width / 2)
            .attr('y', (height / 2) * (this[type.name][last + 1].from / 5000))
            .attr('height', (height / 2) * ((this[type.name][count].to - this[type.name][last + 1].from) / 5000))
            .attr('width', 10)
            .attr('transform', 'translate(' + -5 + ', ' + type.offset + ')');
          last = count;
        }
        count++;
      }
      if (last && last !== this[type.name].length - 2) {
        svg
          .append('rect')
          .attr('class', 'bar')
          .attr('x', width / 2)
          .attr('y', (height / 2) * (this[type.name][last + 1].from / 5000))
          .attr('height', (height / 2) * ((this[type.name][count].to - this[type.name][last + 1].from) / 5000))
          .attr('width', 10)
          .attr('transform', 'translate(' + -5 + ', ' + type.offset + ')');
      }
    });

    svg
      .append('line')
      .attr('x1', 0)
      .attr('x2', 80)
      .attr('y1', 0)
      .attr('y2', 0)
      .attr('stroke', '#018FFF')
      .attr('stroke-width', 2)
      .attr('transform', 'translate(' + (width / 2 - 40) + ', ' + -53 + ')');

    svg
      .append('path')
      .datum(datasetNoData)
      .attr('class', 'line')
      .attr('d', line);

    svg
      .append('path')
      .datum(datasetData)
      .attr('class', 'line')
      .attr('d', line)
      .attr('transform', 'translate(0, ' + height / 2 + ')');

    _.each(types, (type) => {
      let count = 0;
      let last = 0;
      while (count < this[type.name].length - 2) {
        if (this[type.name][count].to !== this[type.name][count + 1].from) {
          svg
            .append('rect')
            .attr('class', 'bar-clear')
            .attr('x', 0)
            .attr('y', (height / 2) * (this[type.name][count].to / 5000))
            .attr('height', (height / 2) * ((this[type.name][count + 1].from - this[type.name][count].to) / 5000))
            .attr('width', width)
            .attr('transform', 'translate(' + 0 + ', ' + type.offset + ')');
          last = count;
        }
        count++;
      }
    });

    svg
      .append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + height + ')')
      .call(d3.axisBottom(yScale).ticks(4));

    svg
      .append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(' + width / 2 + ', ' + height / 2 + ')')
      .call(d3.axisLeft(xScale));

    svg
      .append('g')
      .attr('class', 'x axis middle')
      .attr('transform', 'translate(' + -(width / 2) + ',' + height + ')')
      .call(d3.axisBottom(yScale));

    // No data
    svg
      .append('g')
      .attr('class', 'y axis')
      .attr('transform', 'translate(' + width / 2 + ', 0)')
      .call(d3.axisLeft(xScale));

    svg
      .append('g')
      .attr('class', 'x axis middle')
      .attr('transform', 'translate(0,' + height / 2 + ')')
      .call(d3.axisBottom(yScale));
    svg
      .append('g')
      .attr('class', 'x axis middle')
      .attr('transform', 'translate(' + -(width / 2) + ',' + height / 2 + ')')
      .call(d3.axisBottom(yScale));
    /* start of top section */
    svg
      .append('image')
      .attr('xlink:href', '/assets/images/water.svg')
      .attr('width', 357)
      .attr('height', 37)
      .attr('transform', 'translate(0 ,' + -47 + ')');

    svg
      .append('image')
      .attr('xlink:href', '/assets/images/sea-bottom.svg')
      .attr('width', 357)
      .attr('height', 37);

    svg
      .append('image')
      .attr('xlink:href', '/assets/images/well.svg')
      .attr('width', 17)
      .attr('height', 34)
      .attr('transform', 'translate(' + (width / 2 - 9) + ' ,' + -90 + ')');

    svg
      .append('g')
      .attr('class', 'x axis middle')
      .attr('transform', 'translate(0, 0)')
      .call(d3.axisTop(yScale));

    svg
      .append('g')
      .attr('class', 'x axis middle')
      .attr('transform', 'translate(' + -(width / 2) + ', 0)')
      .call(d3.axisBottom(yScale));

    svg
      .append('text')
      .attr('x', width - 70)
      .attr('y', -5)
      .attr('class', 'normal-text')
      .text('Sea bottom');
    svg
      .append('text')
      .attr('x', width - 70)
      .attr('y', height + 30)
      .attr('class', 'normal-text')
      .text('Density');

    svg
      .append('text')
      .attr('x', width / 2 - 30)
      .attr('y', +15)
      .attr('class', 'normal-text')
      .text('0ft');

    svg
      .append('text')
      .attr('x', width / 2 - 50)
      .attr('y', height / 2 + 15)
      .attr('class', 'normal-text')
      .text('5,000ft');

    svg
      .append('text')
      .attr('x', width / 2 - 60)
      .attr('y', height + 15)
      .attr('class', 'normal-text')
      .text('10,000ft');

    svg
      .append('text')
      .attr('x', 0)
      .attr('y', -65)
      .attr('class', 'bold-text')
      .text('Air');

    svg
      .append('text')
      .attr('x', 0)
      .attr('y', -15)
      .attr('class', 'bold-text')
      .text('Water');

    svg
      .append('text')
      .attr('x', 0)
      .attr('y', height / 4)
      .attr('class', 'bold-text')
      .text('No Data');

    svg
      .append('text')
      .attr('x', 0)
      .attr('y', (height / 4) * 3)
      .attr('class', 'bold-text')
      .text('Data');

    /* end of top section */

    _.each(types, (type) => {
      let count = 0;
      while (count < this[type.name].length - 1) {
        const rectCover = svg
          .append('rect')
          .attr('class', 'tooltip-bar')
          .attr('x', 0)
          .attr('y', (height / 2) * (this[type.name][count].from / 5000))
          .attr('height', (height / 2) * ((this[type.name][count].to - this[type.name][count].from) / 5000))
          .attr('width', width)
          .attr('transform', 'translate(' + 0 + ', ' + type.offset + ')');
        rectCover
          .on('mouseover', (d: any) => {
            this.tooltip
              .transition()
              .duration(200)
              .style('opacity', 1);
            this.tooltip
              .html(
                '<div class="arrow-box"><div><h5>Depth Range (ft)</h5>' +
                  '<span><h5>From</h5>' +
                  this[type.name][count].from +
                  '</span>' +
                  '<span><h5>To</h5>' +
                  this[type.name][count].to +
                  '</span>' +
                  '<span><h5>Density</h5>' +
                  this[type.name][count].density +
                  '</span>' +
                  '</div></div> '
              )
              .style('left', d3.event.pageX - 300 + 'px')
              .style('top', d3.event.pageY - margin.top - 50 + 'px');
          })
          .on('mouseout', () => {
            this.tooltip
              .transition()
              .duration(500)
              .style('opacity', 0);
          });
        count++;
      }
    });
  }

  /**
   * handles the windows resize
   * @param event the event
   */
  @HostListener('window:resize', ['$event']) onResize(event) {
    this.resizeChart();
  }
}
