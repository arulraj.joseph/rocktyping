import { Component, Input, OnInit } from '@angular/core';
import { ILineChartSeries } from '../line-chart/line-chart.component';
import { ICalculatedData } from '../models';

@Component({
  selector: 'bc-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.scss']
})
export class ChartsComponent implements OnInit {
  options = {
    yAxis: {
      type: 'logarithmic',
      title: {
        text: ''
      }
    }
  };
  pcModelVsPcPciChartSeries: ILineChartSeries[];
  pcModelChartSeries: ILineChartSeries[];
  @Input() set data(data: ICalculatedData) {
    this.pcModelVsPcPciChartSeries = this.getPcModelVsPcPciChartSeries(data);
    this.pcModelChartSeries = this.getPcModelChartSeries(data);
  }

  constructor() {}

  ngOnInit() {}

  getPcModelVsPcPciChartSeries(data: ICalculatedData): ILineChartSeries[] {
    const pcModel = data.input.water_saturation.map((item, index) => [item, data.output.pc_model[index]]);
    const pcPsi = data.input.water_saturation.map((item, index) => [item, data.input.pc_psi[index]]);
    return [
      { name: 'Pc_Model', data: pcModel },
      {
        name: 'Pc_Psi',
        data: pcPsi
      }
    ];
  }
  getPcModelChartSeries(data: ICalculatedData): ILineChartSeries[] {
    const pcPsi = data.input.water_saturation.map((item, index) => [item, data.input.pc_psi[index]]);
    return [{ name: 'Pc_PSI', data: pcPsi }];
  }
}
