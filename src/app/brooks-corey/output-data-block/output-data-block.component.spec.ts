import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputDataBlockComponent } from './output-data-block.component';

describe('OutputDataBlockComponent', () => {
  let component: OutputDataBlockComponent;
  let fixture: ComponentFixture<OutputDataBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OutputDataBlockComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputDataBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
