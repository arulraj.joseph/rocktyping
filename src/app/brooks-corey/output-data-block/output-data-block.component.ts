import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { first } from 'rxjs/operators';
import { BrooksCoreyService } from '../brooks-corey.service';
import { ICalculatedData, ISample } from '../models';

@Component({
  selector: 'bc-output-data-block',
  templateUrl: './output-data-block.component.html',
  styleUrls: ['./output-data-block.component.scss']
})
export class OutputDataBlockComponent implements OnInit {
  sourceData: ICalculatedData;
  editedSample: ISample;
  loading: boolean = false;
  @Input() set data(data: ICalculatedData) {
    this.sourceData = data;
  }
  @Output() recalculated = new EventEmitter<ICalculatedData>();
  constructor(private brooksCoreyService: BrooksCoreyService) {}

  ngOnInit() {}
  rawDataItemChanged(item: ISample) {
    this.editedSample = item;
  }
  recalculate(): void {
    if (this.editedSample) {
      this.loading = true;
      this.brooksCoreyService
        .recalculate(this.editedSample)
        .pipe(first())
        .subscribe((data) => {
          this.loading = false;
          this.recalculated.emit(data);
        });
    }
  }
}
