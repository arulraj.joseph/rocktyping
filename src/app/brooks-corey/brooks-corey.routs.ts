import { AuthGuard } from '../routing/auth.guard';
import { InputDataPageComponent } from './input-data-page/input-data-page.component';
import { OutputDataPageComponent } from './output-data-page/output-data-page.component';
import { UploadFilePageComponent } from './upload-file-page/upload-file-page.component';

export const BROOKS_COREY_ROUTES = [
  {
    path: '',
    redirectTo: 'upload',
    pathMatch: 'full'
  },
  {
    path: 'upload',
    component: UploadFilePageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'input',
    component: InputDataPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'output',
    component: OutputDataPageComponent,
    canActivate: [AuthGuard]
  }
];
