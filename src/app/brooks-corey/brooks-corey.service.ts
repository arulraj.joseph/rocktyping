import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ICalculatedData, IRawData, ISample } from './models';

@Injectable({
  providedIn: 'root'
})
export class BrooksCoreyService {
  private rawDataSource: BehaviorSubject<IRawData> = new BehaviorSubject(null);
  private calculatedDataSource: BehaviorSubject<ICalculatedData[]> = new BehaviorSubject(null);

  rawData$ = this.rawDataSource.asObservable();
  calculatedData$ = this.calculatedDataSource.asObservable();

  constructor(private httpClient: HttpClient) {}

  setRawData = (data: IRawData): void => {
    this.rawDataSource.next(data);
  };

  setCalculatedData = (data: ICalculatedData[]): void => {
    this.calculatedDataSource.next(data);
  };

  uploadFile = (file: File): Observable<IRawData> => {
    const body = new FormData();
    body.append('file', file);
    return this.httpClient
      .post<IRawData>(`{BROOKS_COREY_SERVICE}/services/wse/file/upload`, body)
      .pipe(tap(this.setRawData));
  };

  calculate = (data: IRawData): Observable<ICalculatedData[]> =>
    this.httpClient
      .post<ICalculatedData[]>(`{BROOKS_COREY_SERVICE}/services/wse/calculation/all/samples`, data)
      .pipe(tap(this.setCalculatedData));

  recalculate = (data: ISample): Observable<ICalculatedData> =>
    this.httpClient.post<ICalculatedData>(`{BROOKS_COREY_SERVICE}/services/wse/calculation/recalculate/one`, data);

  save = (data: ICalculatedData[]): Observable<ICalculatedData[]> =>
    this.httpClient.post<ICalculatedData[]>(`{BROOKS_COREY_SERVICE}/services/wse/calculation/save/all`, data);

  exportAlgorithmResult = (data: ICalculatedData[]): Observable<Blob> =>
    this.httpClient.post(`{BROOKS_COREY_SERVICE}/services/wse/file/export`, data, {
      observe: 'body',
      responseType: 'blob'
    });
}
