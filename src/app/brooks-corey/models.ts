export interface IPagedResult<T> {
  currentPage: number;
  perPage: number;
  total: number;
  totalPages: number;
  results: T[];
}
export interface ISample {
  name: string;
  depth: number;
  kilnk: number;
  porosity: number;
  delta_rho: number;
  ambient_pc_ratio: number;
  sigma: number;
  water_saturation: number[];
  pc_psi: number[];
}
export interface IOutputData {
  rqi: number;
  swirr: number;
  pe: number;
  lambda_v: number;
  pc_model: number[];
  h_above_fwl: number[];
  j_function: number[];
}
export interface IRawData {
  name: string;
  samples: ISample[];
}
export interface ICalculatedData {
  id: string;
  input: ISample;
  output: IOutputData;
  created_at: string;
  updated_at: string;
}
