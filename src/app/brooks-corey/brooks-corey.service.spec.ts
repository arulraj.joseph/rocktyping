import { TestBed } from '@angular/core/testing';

import { BrooksCoreyService } from './brooks-corey.service';

describe('BrooksCoreyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrooksCoreyService = TestBed.get(BrooksCoreyService);
    expect(service).toBeTruthy();
  });
});
