const sample = {
  name: 'dsfsd',
  depth: 1999,
  kilnk: 2.27,
  porosity: 0.1508,
  water_saturation: [1, 1, 1, 1, 0.93, 0.807, 0.682, 0.54, 0.452, 0.385, 0.341, 0.285],
  delta_rho: 50,
  ambient_pc_ratio: 0.694,
  sigma: 45,
  pc_psi: [0, 0.5, 1, 2, 4, 8, 15, 35, 70, 120, 200, 400]
};
const output = {
  rqi: 0.1218265388587059,
  swirr: 0.18082957521967777,
  pe: 5.035321249653781,
  lambda_v: 1.6,
  pc_model: [
    5.035321249653781,
    5.035321249653781,
    5.035321249653781,
    5.035321249653781,
    5.808941559742935,
    7.739589454845299,
    11.052214655731833,
    18.83410975317133,
    29.528289621151632,
    46.49834274592658,
    68.56370574679349,
    136.4692830577808
  ],
  h_above_fwl: [
    10.064197288108005,
    10.064197288108005,
    10.064197288108005,
    10.064197288108005,
    11.6104476742894,
    15.469272235188395,
    22.090282476704328,
    37.644111845858596,
    59.018783031588185,
    92.93716761313837,
    137.03964995023108,
    272.76388543324765
  ],
  j_function: [
    0.06525966652549178,
    0.06525966652549178,
    0.06525966652549178,
    0.06525966652549178,
    0.07528607813870057,
    0.1003080045592216,
    0.14324087918934764,
    0.24409718086626234,
    0.3826977938849468,
    0.6026360963157678,
    0.8886115405437818,
    1.7686934878159115
  ],
  charts: [
    {
      id: '5d272e103ada912bfc09a321',
      name: '[Sw, PcModel], [Sw, Capillary Pressure]'
    },
    {
      id: '5d272e103ada912bfc09a324',
      name: '[Sw, log(PcModel)]'
    },
    {
      id: '5d272e103ada912bfc09a327',
      name: '[Sw, PcModel], [Swn, Capillary Pressure], [Sw, Capillary Pressure]'
    },
    {
      id: '5d272e103ada912bfc09a32a',
      name: '[Sw, Lambda]'
    }
  ]
};
export const mockData = {
  id: '5d272e103ada912bfc09a32b',
  name: 'data-1562848784',
  depth: 44,
  input: sample,
  output,
  created_at: '2019-07-11T15:39:44.491000',
  updated_at: '2019-07-11T15:39:44.491000'
};

export const mockSamples = [
  { ...sample, name: 'test1', depth: 1000 },
  { ...sample, name: 'test2', depth: 900 },
  { ...sample, name: 'test3', depth: 8000 },
  { ...sample, name: 'test4', depth: 700 },
  { ...sample, name: 'test5', depth: 600 },
  { ...sample, name: 'test6', depth: 500 }
];
export const mockCalculatedData = [
  { ...mockData, id: Math.random().toString(), input: mockSamples[0] },
  { ...mockData, id: Math.random().toString(), input: mockSamples[1] },
  { ...mockData, id: Math.random().toString(), input: mockSamples[2] },
  { ...mockData, id: Math.random().toString(), input: mockSamples[3] },
  { ...mockData, id: Math.random().toString(), input: mockSamples[4] },
  { ...mockData, id: Math.random().toString(), input: mockSamples[5] }
];

export const mockRawData = {
  name: 'test.xls',
  samples: mockSamples
};
