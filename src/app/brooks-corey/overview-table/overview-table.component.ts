import { Component, Input } from '@angular/core';
import { finalize, first } from 'rxjs/operators';
import { DownloadService } from '../../shared/services/download.service';
import { BrooksCoreyService } from '../brooks-corey.service';
import { ICalculatedData } from '../models';

@Component({
  selector: 'bc-overview-table',
  templateUrl: './overview-table.component.html',
  styleUrls: ['./overview-table.component.scss']
})
export class OverviewTableComponent {
  calculatedData: ICalculatedData[];
  tableData: Array<{ [key: string]: any }>;
  @Input() set data(data: ICalculatedData[]) {
    this.tableData = this.getTableData(data);
    this.calculatedData = data;
  }

  tableColumnDefs = [
    { headerName: 'Depth', field: 'depth' },
    { headerName: 'RQI', field: 'rqi' },
    { headerName: 'Swirr', field: 'swirr' },
    { headerName: 'Pe', field: 'pe' },
    { headerName: 'Lambda', field: 'lambda_v' }
  ];

  loading: boolean;

  constructor(private brooksCoreyService: BrooksCoreyService, private downloadService: DownloadService) {}

  private getTableData(data: ICalculatedData[]): Array<{ [key: string]: any }> {
    return data.map((item) => ({ depth: item.input.depth, ...item.output }));
  }
  exportFile() {
    event.preventDefault();
    event.stopPropagation();
    this.loading = true;
    const fileName = `algorithm-result.xlsx`;
    this.brooksCoreyService
      .exportAlgorithmResult(this.calculatedData)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((data) => {
        this.downloadService.downloadFile(data, fileName);
      });
  }
  onGridReady(params) {
    params.api.sizeColumnsToFit();
  }
}
