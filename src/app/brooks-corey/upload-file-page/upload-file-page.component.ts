import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { first } from 'rxjs/internal/operators/first';
import { finalize, switchMap } from 'rxjs/operators';
import { BrooksCoreyService } from '../brooks-corey.service';

@Component({
  selector: 'bc-upload-file-page',
  templateUrl: './upload-file-page.component.html',
  styleUrls: ['./upload-file-page.component.scss']
})
export class UploadFilePageComponent implements OnInit {
  loading: boolean;

  constructor(private brooksCoreyService: BrooksCoreyService, private router: Router) {}

  ngOnInit() {}
  public upload(file: File) {
    this.loading = true;
    this.brooksCoreyService
      .uploadFile(file)
      .pipe(
        first(),
        switchMap(() => from(this.router.navigateByUrl('/brooks-corey/input'))),

        finalize(() => (this.loading = false))
      )
      .subscribe();
  }
}
