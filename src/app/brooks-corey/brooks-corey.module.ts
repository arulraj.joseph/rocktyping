import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCardModule, MatExpansionModule, MatIconModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { HighchartsChartModule } from 'highcharts-angular';
import { SharedModule } from '../shared/shared.module';
import { UploadFilePageComponent } from './upload-file-page/upload-file-page.component';
import { InputDataPageComponent } from './input-data-page/input-data-page.component';
import { AgGridModule } from 'ag-grid-angular';
import { ChartsComponent } from './charts/charts.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { OutputTablesComponent } from './output-tables/output-tables.component';
import { InputTablesComponent } from './input-tables/input-tables.component';
import { OutputDataPageComponent } from './output-data-page/output-data-page.component';
import { OutputDataBlockComponent } from './output-data-block/output-data-block.component';
import { FileReselectBlockComponent } from './file-reselect-block/file-reselect-block.component';
import { OverviewTableComponent } from './overview-table/overview-table.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    AgGridModule.withComponents(null),
    HighchartsChartModule,
    MatCardModule,
    MatButtonModule,
    MatExpansionModule,
    MatIconModule
  ],
  declarations: [
    UploadFilePageComponent,
    InputDataPageComponent,
    ChartsComponent,
    LineChartComponent,
    OutputTablesComponent,
    InputTablesComponent,
    OutputDataPageComponent,
    OutputDataBlockComponent,
    FileReselectBlockComponent,
    OverviewTableComponent
  ]
})
export class BrooksCoreyModule {}
