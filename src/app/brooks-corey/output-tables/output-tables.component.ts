import { Component, Input, OnInit } from '@angular/core';
import { ICalculatedData } from '../models';

@Component({
  selector: 'bc-output-tables',
  templateUrl: './output-tables.component.html',
  styleUrls: ['./output-tables.component.scss']
})
export class OutputTablesComponent implements OnInit {
  calculatedData: Array<{ pc_model: number; h_above_fwl: number; j_function: number }>;
  generalData: Array<{ [key: string]: any }>;
  @Input() set data(data: ICalculatedData) {
    this.generalData = this.getGeneralData(data);
    this.calculatedData = this.getCalculatedData(data);
  }
  outputTableColumnDefs = [
    { headerName: 'Pc_Model, psi', field: 'pc_model', suppressMenu: true },
    { headerName: 'H above FWL, feet', field: 'h_above_fwl', suppressMenu: true },
    { headerName: 'J-function', field: 'j_function', suppressMenu: true }
  ];
  mixedTableColumnDefs = [
    { headerName: 'RQI', field: 'rqi', suppressMenu: true },
    { headerName: 'Swirr', field: 'swirr', suppressMenu: true },
    { headerName: 'Pe', field: 'pe' },
    { headerName: 'Lambda', field: 'lambda_v', suppressMenu: true }
  ];

  constructor() {}

  ngOnInit() {}

  onCellValueChanged(params) {
    console.log('Callback onCellValueChanged:', params);
  }
  getCalculatedData(data: ICalculatedData): Array<{ pc_model: number; h_above_fwl: number; j_function: number }> {
    return data.output.pc_model.map((item, index) => ({
      pc_model: item,
      h_above_fwl: data.output.h_above_fwl[index],
      j_function: data.output.j_function[index]
    }));
  }
  getGeneralData(data: ICalculatedData): Array<{ [key: string]: any }> {
    const {
      output: { rqi, swirr, pe, lambda_v }
    } = data;
    return [{ rqi, swirr, pe, lambda_v }];
  }
  onGridReady(params) {
    params.api.sizeColumnsToFit();
  }
}
