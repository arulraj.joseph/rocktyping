import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputTablesComponent } from './output-tables.component';

describe('OutputTablesComponent', () => {
  let component: OutputTablesComponent;
  let fixture: ComponentFixture<OutputTablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OutputTablesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
