import { Component, Input, OnInit } from '@angular/core';
import { chartColors } from '../../utils/charts.helper';
import * as Highcharts from 'highcharts';
export interface ILineChartSeries {
  name: string;
  data: number[][];
}

@Component({
  selector: 'bc-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit {
  chart;
  Highcharts = Highcharts;
  chartConstructor = 'chart';

  chartOptions: Highcharts.Options = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'line'
    },
    plotOptions: {
      line: {
        marker: {
          enabled: true
        }
      },
      series: {
        label: {
          connectorAllowed: false
        }
      }
    },

    yAxis: {
      title: {
        text: ''
      }
    },
    title: {
      text: null
    },
    series: [],
    colors: Object.keys(chartColors).map((x) => chartColors[x]),
    credits: {
      enabled: false
    }
  };

  updateFlag = false;
  oneToOneFlag = true;
  runOutsideAngularFlag = true;

  @Input()
  set title(title: string) {
    this.setChartTitle(title);
  }
  @Input()
  set options(options) {
    this.chartOptions = {
      ...this.chartOptions,
      ...options
    };
  }

  @Input()
  set series(series: ILineChartSeries[]) {
    this.chartOptions = {
      ...this.chartOptions,
      series
    };
  }
  constructor() {}

  ngOnInit(): void {}

  setChartTitle(title: string) {
    this.chartOptions.title.text = title;
  }

  chartCallback = (chart) => (this.chart = chart);
}
