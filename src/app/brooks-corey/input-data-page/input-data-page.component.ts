import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { finalize, first, switchMap } from 'rxjs/operators';
import { BrooksCoreyService } from '../brooks-corey.service';
import { IRawData, ISample } from '../models';

@Component({
  selector: 'bc-input-data-page',
  templateUrl: './input-data-page.component.html',
  styleUrls: ['./input-data-page.component.scss']
})
export class InputDataPageComponent implements OnInit {
  loading: boolean = false;
  rawDataList: IRawData;
  editedDataList: IRawData;

  constructor(private brooksCoreyService: BrooksCoreyService, private router: Router) {}

  ngOnInit() {
    this.brooksCoreyService.rawData$.subscribe((data) => {
      if (data) {
        this.rawDataList = data;
        this.editedDataList = { ...data, samples: [...data.samples] };
      } else {
        this.router.navigateByUrl('brooks-corey').then();
      }
    });
  }
  rawDataItemChanged(item: ISample, index: number) {
    this.editedDataList.samples[index] = item;
  }
  onSubmit(): void {
    this.loading = true;
    this.brooksCoreyService
      .calculate(this.editedDataList)
      .pipe(
        first(),
        switchMap(() => from(this.router.navigateByUrl('brooks-corey/output'))),
        finalize(() => (this.loading = false))
      )
      .subscribe();
  }
}
