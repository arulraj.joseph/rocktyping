import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { finalize, first, switchMap } from 'rxjs/operators';
import { BrooksCoreyService } from '../brooks-corey.service';
import { ICalculatedData } from '../models';

@Component({
  selector: 'bc-output-data-page',
  templateUrl: './output-data-page.component.html',
  styleUrls: ['./output-data-page.component.scss']
})
export class OutputDataPageComponent implements OnInit {
  loading: boolean = false;
  calculatedData: ICalculatedData[];

  constructor(public brooksCoreyService: BrooksCoreyService, private router: Router) {}

  ngOnInit() {
    this.brooksCoreyService.calculatedData$.subscribe((data) => {
      if (data) {
        this.calculatedData = data;
      } else {
        this.router.navigateByUrl('brooks-corey').then();
      }
    });
  }

  trackByFn(index, item) {
    return index;
  }

  sampleRecalculated(data: ICalculatedData, index) {
    this.calculatedData[index] = data;
  }
  saveCalculation() {
    this.loading = true;
    this.brooksCoreyService
      .save(this.calculatedData)
      .pipe(
        first(),
        switchMap(() => from(this.router.navigateByUrl('brooks-corey'))),
        finalize(() => (this.loading = false))
      )
      .subscribe();
  }
}
