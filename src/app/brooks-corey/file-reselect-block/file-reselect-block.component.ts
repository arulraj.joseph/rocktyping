import { Component, Input } from '@angular/core';

@Component({
  selector: 'bc-file-reselect-block',
  templateUrl: './file-reselect-block.component.html',
  styleUrls: ['./file-reselect-block.component.scss']
})
export class FileReselectBlockComponent {
  @Input() fileName: string;
  constructor() {}
}
