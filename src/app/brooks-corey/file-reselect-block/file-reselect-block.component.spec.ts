import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileReselectBlockComponent } from './file-reselect-block.component';

describe('FileReselectBlockComponent', () => {
  let component: FileReselectBlockComponent;
  let fixture: ComponentFixture<FileReselectBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileReselectBlockComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileReselectBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
