import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputTablesComponent } from './input-tables.component';

describe('InputTablesComponent', () => {
  let component: InputTablesComponent;
  let fixture: ComponentFixture<InputTablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InputTablesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
