import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NumericEditorComponent } from '../../shared/components/numeric-editor/numeric-editor.component';
import { ISample } from '../models';

@Component({
  selector: 'bc-input-tables',
  templateUrl: './input-tables.component.html',
  styleUrls: ['./input-tables.component.scss']
})
export class InputTablesComponent implements OnInit {
  waterSaturationData: Array<{ water_saturation: number; pc_psi: number }>;
  generalData: Array<{ [key: string]: any }>;
  editedData: ISample;
  frameworkComponents = {
    numericEditor: NumericEditorComponent
  };
  @Input() set data(data: ISample) {
    this.generalData = [data];
    this.waterSaturationData = this.getWaterSaturationData(data);
    this.editedData = JSON.parse(JSON.stringify(data));
  }
  @Output() dataChanged = new EventEmitter<ISample>();
  inputTableColumnDefs = [
    {
      headerName: 'SW',
      field: 'water_saturation',
      editable: true,
      type: 'numericColumn',
      filter: 'agNumberColumnFilter',
      suppressMenu: true,
      cellEditor: 'numericEditor'
    },
    {
      headerName: 'PC, pci',
      field: 'pc_psi',
      editable: true,
      type: 'numericColumn',
      filter: 'agNumberColumnFilter',
      suppressMenu: true,
      cellEditor: 'numericEditor'
    }
  ];
  generalTableColumnDefs = [
    { headerName: 'Sample', field: 'name', suppressMenu: true },
    { headerName: 'Depth, m', field: 'depth', suppressMenu: true, cellEditor: 'numericEditor' },
    {
      headerName: 'Klink, md',
      field: 'kilnk',
      editable: true,
      type: 'numericColumn',
      suppressMenu: true,
      cellEditor: 'numericEditor'
    },
    {
      headerName: 'Porosity, md',
      field: 'porosity',
      editable: true,
      type: 'numericColumn',
      suppressMenu: true,
      cellEditor: 'numericEditor'
    },
    {
      headerName: 'Delta Rho,lb/ft^3',
      field: 'delta_rho',
      editable: true,
      type: 'numericColumn',
      suppressMenu: true,
      cellEditor: 'numericEditor'
    },
    {
      headerName: 'Pc@Ambient to PC, Res 45/72',
      field: 'ambient_pc_ratio',
      editable: true,
      type: 'numericColumn',
      suppressMenu: true,
      cellEditor: 'numericEditor'
    },
    {
      headerName: 'Sigma@Recodition, dynes/cm',
      field: 'sigma',
      editable: true,
      type: 'numericColumn',
      suppressMenu: true,
      cellEditor: 'numericEditor'
    }
  ];

  constructor() {}

  ngOnInit() {}

  waterSaturationDataChanged({ oldValue, newValue, column: { colId } }) {
    if (oldValue !== newValue) {
      const oldValueIndex = this.editedData[colId].indexOf(oldValue);
      this.editedData[colId][oldValueIndex] = newValue;
      this.dataChanged.next(this.editedData);
    }
  }
  generalDataChanged({ oldValue, newValue, column: { colId } }) {
    if (oldValue !== newValue) {
      this.editedData[colId] = newValue;
      this.dataChanged.next(this.editedData);
    }
  }
  getWaterSaturationData(data: ISample): Array<{ water_saturation: number; pc_psi: number }> {
    return data.water_saturation.map((item, index) => ({
      water_saturation: item,
      pc_psi: data.pc_psi[index]
    }));
  }
  onGridReady(params) {
    params.api.sizeColumnsToFit();
  }
}
