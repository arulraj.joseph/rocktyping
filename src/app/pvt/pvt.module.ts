import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatRadioModule,
  MatTooltipModule,
  MatSelectModule,
  MatButtonToggleModule
} from '@angular/material';
import { SharedModule } from '../shared/shared.module';

import { BlackOilPropertiesComponent } from './pages/pvt/black-oil-properties/black-oil-properties.component';
import { RowBlackOilPropertiesComponent } from './pages/pvt/row-black-oil-properties/row-black-oil-properties.component';
import { TableBlackOilPropertiesComponent } from './pages/pvt/table-black-oil-properties/table-black-oil-properties.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatRadioModule,
    MatTooltipModule,
    MatSelectModule,
    SharedModule,
    MatButtonToggleModule
  ],
  declarations: [BlackOilPropertiesComponent, RowBlackOilPropertiesComponent, TableBlackOilPropertiesComponent],
  exports: [BlackOilPropertiesComponent, RowBlackOilPropertiesComponent, TableBlackOilPropertiesComponent]
})
export class PvtModule {}
