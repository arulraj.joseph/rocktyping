import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowBlackOilPropertiesComponent } from './row-black-oil-properties.component';

describe('RowBlackOilPropertiesComponent', () => {
  let component: RowBlackOilPropertiesComponent;
  let fixture: ComponentFixture<RowBlackOilPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RowBlackOilPropertiesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowBlackOilPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
