import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppConstants } from '../../../../app.constants';

@Component({
  selector: 'pvt-row-black-oil-properties',
  templateUrl: './row-black-oil-properties.component.html',
  styleUrls: ['./row-black-oil-properties.component.scss']
})
export class RowBlackOilPropertiesComponent {
  public listValueAbbreviated = AppConstants.DataValuesAbbreviated;
  @Input() item: FormGroup;
  @Input() submitted: boolean;
  @Input() index: number;
  @Input() hideRemove: boolean;
  @Output() callback = new EventEmitter();

  /**
   * handles the change input sample id
   * @param event the event
   */
  changeInputSampleId(event) {
    if (event.trim() === '') {
      event = null;
      this.callback.emit({ event: 'change', showBtn: false });
    } else {
      this.callback.emit({ event: 'change', showBtn: true });
    }
  }

  /**
   * delete row
   */
  delete() {
    this.callback.emit({ event: 'delete', index: this.index });
  }
}
