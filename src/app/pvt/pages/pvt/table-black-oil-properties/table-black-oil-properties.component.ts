import { Component, Input, ViewChild, ElementRef, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'pvt-table-black-oil-properties',
  templateUrl: './table-black-oil-properties.component.html',
  styleUrls: ['./table-black-oil-properties.component.scss']
})
export class TableBlackOilPropertiesComponent implements OnChanges {
  @Input() data: any[] = [];
  @ViewChild('tableContent') tableContent: ElementRef;
  @Output() callback = new EventEmitter();

  ngOnChanges() {
    this.callback.emit({ event: 'export', el: this.tableContent.nativeElement });
  }
}
