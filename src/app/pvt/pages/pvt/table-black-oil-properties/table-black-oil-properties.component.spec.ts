import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableBlackOilPropertiesComponent } from './table-black-oil-properties.component';

describe('TableBlackOilPropertiesComponent', () => {
  let component: TableBlackOilPropertiesComponent;
  let fixture: ComponentFixture<TableBlackOilPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableBlackOilPropertiesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableBlackOilPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
