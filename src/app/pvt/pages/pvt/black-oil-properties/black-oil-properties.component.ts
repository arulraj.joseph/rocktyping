import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { finalize } from 'rxjs/operators';
import { CalculatorsService } from '../../../../core//services/calculators/calculators.service';
import { DropdownComponent } from '../../../../shared/components/dropdown/dropdown.component';
import { AppConstants } from '../../../../app.constants';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'pvt-black-oil-properties',
  templateUrl: './black-oil-properties.component.html',
  styleUrls: ['./black-oil-properties.component.scss']
})
export class BlackOilPropertiesComponent implements OnInit {
  public optionsWellName: any[] = [];
  public selectedWellName = null;
  public dataTableCalculators: any[];
  public showTableCalculator = false;
  public showBtnCalculator = false;
  public querySearch = '';
  public isLoading = false;
  public form: FormGroup;
  public submitted = false;
  public isUpdate = false;
  public selectedData = null;
  public listValueAbbreviated = AppConstants.DataValuesAbbreviated;
  public tableContent: any;
  public txtUnit = 'ft';
  @Output() callback = new EventEmitter();
  @ViewChild(DropdownComponent) dropdownComponent: DropdownComponent;
  constructor(
    private calculatorsService: CalculatorsService,
    private fb: FormBuilder /* ,
    private progressBar: ProgressBarService */
  ) {}

  /**
   * on init
   */
  ngOnInit() {
    this.form = this.fb.group({
      data: this.fb.array([])
    });
  }

  /**
   * gets the array data
   */
  get arrayData() {
    return this.form.get('data') as FormArray;
  }

  /**
   * handles callback dropdown
   * @param result the result
   */
  callbackDropdown(result) {
    if (result.event === 'search') {
      if (result.query.trim() !== '') {
        this.selectedData = null;
      }
      this.isUpdate = result.query.trim() === '' ? true : false;
      this.querySearch = result.query;
      this.fetch();
    } else if (result.event === 'select') {
      this.showTableCalculator = false;
      this.showBtnCalculator = false;
      this.isUpdate = true;
      this.isLoading = true;
      this.calculatorsService
        .searchPVTData(result.item.name)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe((res) => {
          this.selectedData = res.items[0];
          this.clearFormArray(this.arrayData);
          res.items[0].input.forEach((element) => {
            this.arrayData.push(
              this.fb.group({
                inputs: this.fb.group({
                  sampleId: [element.sampleNumber, Validators.required],
                  p: [element.reservoirPressure, Validators.required],
                  t: [element.reservoirTemperature, Validators.required],
                  sga: [element.oilGravity, Validators.required],
                  sgg: [element.specificGasGravity, Validators.required],
                  pb: [element.oilBPPressure, Validators.required],
                  rs: [element.solutionGOR, Validators.required],
                  rsb: [element.solutionGORPb, Validators.required],
                  tds: [element.waterSalinity, Validators.required],
                  yH2S: [element.h2sMoleFraction, Validators.required],
                  yCO2: [element.co2MoleFraction, Validators.required],
                  yN2: [element.n2MoleFraction, Validators.required]
                })
              })
            );
          });
          this.dataTableCalculators = res.items[0].output;

          if (this.form.invalid) {
            return;
          }
          this.showBtnCalculator = true;
          this.callback.emit({ showBtn: this.showBtnCalculator });
        });
    }
  }

  /**
   * clear form array
   * @param formArray the form array
   */
  clearFormArray(formArray: FormArray) {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  /**
   * handles callback back row black oil properties
   * @param event the event
   */
  callbackRowBlackOilProperties(result) {
    if (result.event === 'change') {
      if (this.form.invalid) {
        return;
      }
      this.showBtnCalculator = result.showBtn;
      this.callback.emit({ showBtn: this.showBtnCalculator });
    } else if (result.event === 'delete') {
      this.arrayData.removeAt(result.index);
    }
  }

  /**
   * add sample id
   */
  addSampleID() {
    this.arrayData.push(
      this.fb.group({
        inputs: this.fb.group({
          sampleId: [null, Validators.required],
          p: [0, Validators.required],
          t: [0, Validators.required],
          sga: [0, Validators.required],
          sgg: [0, Validators.required],
          pb: [0, Validators.required],
          rs: [0, Validators.required],
          rsb: [0, Validators.required],
          tds: [0, Validators.required],
          yH2S: [0, Validators.required],
          yCO2: [0, Validators.required],
          yN2: [0, Validators.required]
        })
      })
    );
  }

  /**
   * show calculator
   */
  showCalculator() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.dataTableCalculators = [];
    this.showTableCalculator = true;
    const data = _.cloneDeep(this.form.value);
    const requestData = [];
    _.each(data.data, (element) => {
      const elementData = {
        sampleNumber: element.inputs.sampleId,
        reservoirPressure: element.inputs.p,
        reservoirTemperature: element.inputs.t,
        oilGravity: element.inputs.sga,
        specificGasGravity: element.inputs.sgg,
        oilBPPressure: element.inputs.pb,
        solutionGOR: element.inputs.rs,
        solutionGORPb: element.inputs.rsb,
        waterSalinity: element.inputs.tds,
        h2sMoleFraction: element.inputs.yH2S,
        co2MoleFraction: element.inputs.yCO2,
        n2MoleFraction: element.inputs.yN2
      };
      requestData.push(elementData);
    });
    const body = {
      input: requestData,
      name: this.isUpdate ? this.selectedData.name : this.dropdownComponent.selectOption
    };
    this.isLoading = true;

    // if its already selected well then update
    if (this.isUpdate) {
      this.calculatorsService
        .updatePVT(this.selectedData.id, body)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe((res: any) => {
          this.dataTableCalculators = res.output;
        });
    } else {
      this.calculatorsService
        .createPVT(body)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe((res: any) => {
          this.dataTableCalculators = res.output;
        });
    }
  }

  /**
   * change value unit conversion from feet to meter & vice-versa
   * @param event event change value
   */
  changeValueFeetToMeter(event) {
    this.txtUnit = event.value;
    if (this.form.value.data.length > 0 && this.dataTableCalculators.length > 0) {
      const amount = [];
      this.form.value.data.forEach((element) => {
        amount.push(parseFloat(element.inputs.p ? element.inputs.p : 0));
        amount.push(parseFloat(element.inputs.t ? element.inputs.t : 0));
        amount.push(parseFloat(element.inputs.sga ? element.inputs.sga : 0));
        amount.push(parseFloat(element.inputs.sgg ? element.inputs.sgg : 0));
        amount.push(parseFloat(element.inputs.pb ? element.inputs.pb : 0));
        amount.push(parseFloat(element.inputs.rs ? element.inputs.rs : 0));
        amount.push(parseFloat(element.inputs.rsb ? element.inputs.rsb : 0));
        amount.push(parseFloat(element.inputs.tds ? element.inputs.tds : 0));
        amount.push(parseFloat(element.inputs.yH2S ? element.inputs.yH2S : 0));
        amount.push(parseFloat(element.inputs.yCO2 ? element.inputs.yCO2 : 0));
        amount.push(parseFloat(element.inputs.yN2 ? element.inputs.yN2 : 0));
      });

      this.dataTableCalculators.forEach((element) => {
        amount.push(parseFloat(element.zFactor ? element.zFactor : 0));
        amount.push(parseFloat(element.oilDensity ? element.oilDensity : 0));
        amount.push(parseFloat(element.oilCompressibility ? element.oilCompressibility : 0));
        amount.push(parseFloat(element.oilViscosity ? element.oilViscosity : 0));
        amount.push(parseFloat(element.gor ? element.gor : 0));
        amount.push(parseFloat(element.bo ? element.bo : 0));
        amount.push(parseFloat(element.bw ? element.bw : 0));
        amount.push(parseFloat(element.gwr ? element.gwr : 0));
        amount.push(parseFloat(element.waterDensity ? element.waterDensity : 0));
        amount.push(parseFloat(element.waterCompressibility ? element.waterCompressibility : 0));
        amount.push(parseFloat(element.waterViscosity ? element.waterViscosity : 0));
        amount.push(parseFloat(element.gasViscosity ? element.gasViscosity : 0));
      });
      const data = {
        from: event.value === 'm' ? 'feet' : 'meter',
        to: event.value === 'ft' ? 'feet' : 'meter',
        amount
      };
      this.isLoading = true;
      this.calculatorsService
        .convertUnit(data)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe((res) => {
          this.arrayData.controls.forEach((element: any, index) => {
            element.controls.inputs.controls.p.setValue(res.result[0 * index]);
            element.controls.inputs.controls.t.setValue(res.result[1 * index]);
            element.controls.inputs.controls.sga.setValue(res.result[2 * index]);
            element.controls.inputs.controls.sgg.setValue(res.result[3 * index]);
            element.controls.inputs.controls.pb.setValue(res.result[4 * index]);
            element.controls.inputs.controls.rs.setValue(res.result[5 * index]);
            element.controls.inputs.controls.rsb.setValue(res.result[6 * index]);
            element.controls.inputs.controls.tds.setValue(res.result[7 * index]);
            element.controls.inputs.controls.yH2S.setValue(res.result[8 * index]);
            element.controls.inputs.controls.yCO2.setValue(res.result[9 * index]);
            element.controls.inputs.controls.yN2.setValue(res.result[10 * index]);
          });
          this.dataTableCalculators.forEach((element, index) => {
            element.zFactor = res.result[11 * index];
            element.oilDensity = res.result[12 * index];
            element.oilCompressibility = res.result[13 * index];
            element.oilViscosity = res.result[14 * index];
            element.gor = res.result[15 * index];
            element.bo = res.result[16 * index];
            element.bw = res.result[17 * index];
            element.gwr = res.result[18 * index];
            element.waterDensity = res.result[19 * index];
            element.waterCompressibility = res.result[20 * index];
            element.waterViscosity = res.result[21 * index];
            element.gasViscosity = res.result[22 * index];
          });
        });
    }
  }

  /**
   * export file to csv
   */
  downloadCsv() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Black Oil Properties',
      useBom: true,
      noDownload: false,
      headers: [
        'Sample ID',
        'Reservoir pressure',
        'Reservoir temperature',
        'Oil gravity',
        'Specific gas gravity',
        'Oil bubble-point pressure',
        'Solution GOR',
        'Solution GOR at P=Pb',
        'Water salinity',
        'H2S mole fraction',
        'CO2 mole fraction',
        'N2 mole fraction',
        'z-factor',
        'Oil Density',
        'Oil Compressibility',
        'Oil Viscosity',
        'GOR',
        'Bo',
        'Bw',
        'GWR',
        'Water Density',
        'Water Compressibility',
        'Water Viscosity',
        'Gas Viscosity'
      ]
    };
    const array = [];
    this.dataTableCalculators.forEach((element: any, index: number) => {
      const data: any = {};
      data.sampleId = element.sampleNumber;
      data.p = this.form.value.data[index].inputs.p;
      data.pb = this.form.value.data[index].inputs.pb;
      data.rs = this.form.value.data[index].inputs.rs;
      data.rsb = this.form.value.data[index].inputs.rsb;
      data.sga = this.form.value.data[index].inputs.sga;
      data.sgg = this.form.value.data[index].inputs.sgg;
      data.t = this.form.value.data[index].inputs.t;
      data.tds = this.form.value.data[index].inputs.tds;
      data.yCO2 = this.form.value.data[index].inputs.yCO2;
      data.yH2S = this.form.value.data[index].inputs.yH2S;
      data.yN2 = this.form.value.data[index].inputs.yN2;
      data.zFactor = element.zFactor;
      data.density = element.oilDensity;
      data.oilCompressibility = element.oilCompressibility;
      data.oilViscosity = element.oilViscosity;
      data.gor = element.gor;
      data.bo = element.bo;
      data.bw = element.bw;
      data.gwr = element.gwr;
      data.waterDensity = element.waterDensity;
      data.waterCompressibility = element.waterCompressibility;
      data.viscosity = element.waterViscosity;
      data.gasViscosity = element.gasViscosity;
      array.push(data);
    });
    const exportFile = new ngxCsv(array, 'Calculator-Black-Oil-Properties', options);
    return exportFile;
  }

  /**
   * callback table
   * @param result result table
   */
  callbackTable(result) {
    this.tableContent = result.el;
  }

  /**
   * handles the pdf download click
   */
  downloadPdf() {
    const doc = new jsPDF('l', 'pt', 'letter');

    // the titles
    const colInput = [
      { title: 'Sample ID' },
      { title: 'P' },
      { title: 'T' },
      { title: 'SGA' },
      { title: 'SGG' },
      { title: 'PB' },
      { title: 'Rs' },
      { title: 'Rsb' },
      { title: 'TDS' },
      { title: 'y_H2S' },
      { title: 'y_CO2' },
      { title: 'y_N2' }
    ];
    const rowsInput = [];

    const colOutput = [
      { title: 'Sample ID' },
      { title: 'z-factor' },
      { title: 'Oil Density' },
      { title: 'Oil Compressibility' },
      { title: 'Oil Viscosity' },
      { title: 'GOR' },
      { title: 'Bo' },
      { title: 'Bw' },
      { title: 'GWR' },
      { title: 'Water Density' },
      { title: 'Water Compressibility' },
      { title: 'Water Viscosity' },
      { title: 'Gas Viscosity' }
    ];
    const rowsOutput = [];

    this.form.value.data.forEach((element) => {
      const dataArray = [];
      dataArray.push(element.inputs.sampleId);
      dataArray.push(element.inputs.p);
      dataArray.push(element.inputs.t);
      dataArray.push(element.inputs.sga);
      dataArray.push(element.inputs.sgg);
      dataArray.push(element.inputs.pb);
      dataArray.push(element.inputs.rs);
      dataArray.push(element.inputs.rsb);
      dataArray.push(element.inputs.tds);
      dataArray.push(element.inputs.yH2S);
      dataArray.push(element.inputs.yCO2);
      dataArray.push(element.inputs.yN2);
      rowsInput.push(dataArray);
    });

    this.dataTableCalculators.forEach((element) => {
      const dataArray = [];
      dataArray.push(element.sampleNumber);
      dataArray.push(element.zFactor);
      dataArray.push(element.oilDensity.toFixed(2));
      dataArray.push(element.oilCompressibility);
      dataArray.push(element.oilViscosity.toFixed(2));
      dataArray.push(element.gor.toFixed(2));
      dataArray.push(element.bo.toFixed(2));
      dataArray.push(element.bw.toFixed(2));
      dataArray.push(element.gwr.toFixed(2));
      dataArray.push(element.waterDensity.toFixed(2));
      dataArray.push(element.waterCompressibility);
      dataArray.push(element.waterViscosity.toFixed(2));
      dataArray.push(element.gasViscosity.toFixed(2));
      rowsOutput.push(dataArray);
    });

    doc.autoTable(colInput, rowsInput);
    doc.autoTable(colOutput, rowsOutput);
    // doc.fromHTML(this.tableContent);
    doc.save(this.getDownloadFileName());
  }

  /**
   * gets the download file name
   * @returns the file name
   */
  getDownloadFileName() {
    const date = new Date();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? `0${month}` : month;
    return `${this.dropdownComponent.selectOption.trim()}${month}${date.getDate()}`;
  }

  /**
   * fetches the well data
   */
  fetch() {
    this.isLoading = true;
    this.calculatorsService
      .getWellLookupData(this.dropdownComponent.selectOption.trim())
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((res) => {
        this.optionsWellName = _.map(res, (i) => {
          return {
            name: i
          };
        });
      });
  }
}
