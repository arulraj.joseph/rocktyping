import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { KlinkenbergRoutes } from '../../core/models';

@Component({
  selector: 'klinkenberg-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  correctionLink: string = KlinkenbergRoutes.Correction;
  samplesLink: string = KlinkenbergRoutes.Samples;

  constructor(private router: Router) {}

  ngOnInit() {}

  get route(): string {
    switch (this.router.url) {
      case KlinkenbergRoutes.Correction: {
        return 'correction';
      }
      /* falls through */
      case KlinkenbergRoutes.Results: {
        return 'results';
      }
      /* falls through */
      case KlinkenbergRoutes.Samples: {
        return 'samples';
      }
    }
  }
}
