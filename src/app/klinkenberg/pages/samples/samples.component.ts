import { Component } from '@angular/core';
import { Router } from '@angular/router';

import * as agGridParamaters from '../../../utils/ag-grid-parameters';
import { KlinkenbergRoutes, IKlinkenbergSampleInput } from '../../core/models';
import { ResultsService } from '../../core/services/results/results.service';
import { GridOptions } from 'ag-grid-community';

const displayedColumns = [
  { headerName: 'Sample #', field: 'sampleNumber', width: 150, editable: true, menuTabs: [], lockPosition: true },
  {
    headerName: 'Depth (ft)',
    field: 'depth',
    width: 150,
    editable: true,
    menuTabs: [],
    lockPosition: true,
    valueParser: agGridParamaters.numberParser,
    valueFormatter: agGridParamaters.getTwoNumbersAfterComma,
    type: 'numericColumn'
  },
  {
    headerName: 'K air (mD)',
    field: 'airPermeability',
    width: 150,
    editable: true,
    menuTabs: [],
    lockPosition: true,
    valueParser: agGridParamaters.numberParser,
    valueFormatter: agGridParamaters.getTwoNumbersAfterComma,
    type: 'numericColumn'
  },
  {
    headerName: 'P mean (abs atm)',
    field: 'pMean',
    width: 150,
    editable: true,
    menuTabs: [],
    lockPosition: true,
    valueParser: agGridParamaters.numberParser,
    valueFormatter: agGridParamaters.getTwoNumbersAfterComma,
    type: 'numericColumn'
  }
];

@Component({
  selector: 'klinkenberg-samples',
  templateUrl: './samples.component.html',
  styleUrls: ['./samples.component.scss']
})
export class SamplesComponent {
  resultsLink: string = KlinkenbergRoutes.Results;
  emptyRow: IKlinkenbergSampleInput = {
    sampleNumber: '',
    depth: undefined,
    airPermeability: undefined,
    pMean: undefined
  };

  minRowsAmount = 20;

  displayedColumns = displayedColumns;

  dataSource: IKlinkenbergSampleInput[] = this.resultsService.jobData;
  gridOptions: GridOptions;
  samplesGrid;

  constructor(private router: Router, private resultsService: ResultsService) {
    this.clearTable();

    this.gridOptions = {
      rowData: this.dataSource,
      columnDefs: this.displayedColumns,
      getContextMenuItems: this.menuOptions,
      context: {
        this: this
      },
      stopEditingWhenGridLosesFocus: true,
      enableRangeSelection: true
    };
  }

  menuOptions(params) {
    return [
      'copy',
      'paste',
      'separator',
      {
        name: 'Add row',
        action() {
          const component = params.context.this;

          component.dataSource.splice(params.node.childIndex + 1, 0, { ...component.emptyRow });
          params.api.setRowData(component.dataSource);
        }
      },
      {
        name: 'Delete row',
        action() {
          const component = params.context.this;

          component.dataSource.splice(params.node.childIndex, 1);
          component.clearTable();
          params.api.setRowData(component.dataSource);
        }
      }
    ];
  }

  setEntryData(): void {
    this.resultsService.setEntryData(this.dataSource);

    if (this.resultsService.hasJob) {
      this.router.navigateByUrl(KlinkenbergRoutes.Results);
    }
  }

  clearTable(): void {
    const currentLength = this.minRowsAmount - this.dataSource.length;

    for (let i = 0; i < currentLength; i++) {
      this.dataSource.push({ ...this.emptyRow });
    }
  }

  addRow(): void {
    this.dataSource.push({ ...this.emptyRow });
  }

  removeAllData() {
    this.dataSource = [];
    this.clearTable();
    this.samplesGrid.api.setRowData(this.dataSource);
  }

  onResultsGridReady(params) {
    this.samplesGrid = params;
  }
}
