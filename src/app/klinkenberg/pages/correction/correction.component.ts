import { Component } from '@angular/core';
import { copyToClipboard } from '../../utils/copy-to-clipboard';
import { CorrectionService } from '../../core/services/correction/correction.service';

@Component({
  selector: 'klinkenberg-correction',
  templateUrl: './correction.component.html',
  styleUrls: ['./correction.component.scss']
})
export class CorrectionComponent {
  isCopied: boolean = false;
  isPmean: boolean = false;
  klinkenbergValue: number = 0;
  permeabilityValue: number = 0;
  minPermeabilityValue: number = 0;
  maxPermeabilityValue: number = 200;
  noPmeanStatus: string = 'No Pmean available';
  pmeanStatus: string = 'Pmean available';
  pmeanValue: number = 0;
  minPmeanValue: number = 0;
  maxPmeanValue: number = 10;

  constructor(private correctionService: CorrectionService) {}

  changeKlinkenberg(): void {
    if (!this.permeabilityValue) {
      this.klinkenbergValue = 0;
      return;
    }

    this.correctionService.getCorrectionValue(this.permeabilityValue, this.pmeanValue).subscribe((data) => {
      if (this.pmeanValue) {
        this.klinkenbergValue = data.klinkenbergPermeabilityP;
        return;
      }

      this.klinkenbergValue = data.klinkenbergPermeabilityCalc;
    });
  }

  changePmean(event) {
    let currentValue = event.target.value;

    if (currentValue === '') {
      return;
    }

    currentValue = Number(currentValue);
    this.pmeanValue = parseFloat(currentValue.toFixed(3));

    if (currentValue < this.minPmeanValue) {
      this.pmeanValue = this.minPmeanValue;
    }

    if (currentValue > this.maxPmeanValue) {
      this.pmeanValue = this.maxPmeanValue;
    }

    this.changeKlinkenberg();
  }

  changePermeability(event) {
    let currentValue = event.target.value;

    if (currentValue === '') {
      return;
    }

    currentValue = Number(currentValue);
    this.permeabilityValue = parseFloat(currentValue.toFixed(3));

    if (currentValue < this.minPermeabilityValue) {
      this.permeabilityValue = this.minPermeabilityValue;
    }

    if (currentValue > this.maxPermeabilityValue) {
      this.permeabilityValue = this.maxPermeabilityValue;
    }

    this.changeKlinkenberg();
  }

  copyToClipboard(value: number): void {
    copyToClipboard(value);
    this.isCopied = true;
  }

  onToogleButton(value: boolean): void {
    this.isPmean = value;

    if (!this.isPmean) {
      this.pmeanValue = 0;
      this.changeKlinkenberg();
    }
  }

  resetAllValue() {
    this.permeabilityValue = 0;
    this.pmeanValue = 0;
    this.changeKlinkenberg();
  }
}
