import { Component, OnInit, Input } from '@angular/core';
import * as agGridParamaters from '../../../../utils/ag-grid-parameters';
import { IKlinkenbergSampleOutput } from '../../../core/models';

const resultsColumns = [
  {
    headerName: 'B air (atm)',
    field: 'bAir',
    width: 120,
    editable: false,
    menuTabs: [],
    lockPosition: true,
    valueParser: agGridParamaters.numberParser,
    valueFormatter: agGridParamaters.getSixNumbersAfterComma,
    type: 'numericColumn'
  },
  {
    headerName: 'Klinkenberg Permeability (mD)',
    field: 'klinkenbergPermeabilityP',
    width: 140,
    editable: false,
    menuTabs: [],
    lockPosition: true,
    valueParser: agGridParamaters.numberParser,
    valueFormatter: agGridParamaters.getTwoNumbersAfterComma,
    type: 'numericColumn'
  },
  {
    headerName: '*Klinkenberg Calculated (mD)',
    field: 'klinkenbergPermeabilityCalc',
    width: 140,
    editable: false,
    menuTabs: [],
    lockPosition: true,
    valueParser: agGridParamaters.numberParser,
    valueFormatter: agGridParamaters.getTwoNumbersAfterComma,
    type: 'numericColumn',
    cellClass: ['highlight-yellow']
  }
];

@Component({
  selector: 'klinkenberg-table-output',
  templateUrl: './table-output.component.html',
  styleUrls: ['./table-output.component.scss']
})
export class TableOutputComponent implements OnInit {
  @Input()
  data: IKlinkenbergSampleOutput[] = [];

  resultsGrid;
  columns = resultsColumns;

  constructor() {}

  ngOnInit() {}

  onResultsGridReady(params) {
    this.resultsGrid = params;
  }

  menuOptions(): string[] {
    return ['copy', 'paste'];
  }
}
