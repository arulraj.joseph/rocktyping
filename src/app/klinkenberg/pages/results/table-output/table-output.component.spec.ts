import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableOutputComponent } from './table-output.component';

describe('TableOutputComponent', () => {
  let component: TableOutputComponent;
  let fixture: ComponentFixture<TableOutputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableOutputComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
