import { Component, OnInit, Input } from '@angular/core';
import * as agGridParamaters from '../../../../utils/ag-grid-parameters';

import { IKlinkenbergSampleInput } from '../../../core/models';

const inputDataColumns = [
  {
    headerName: 'Sample #',
    field: 'sampleNumber',
    width: 100,
    editable: false,
    menuTabs: [],
    lockPosition: true
  },
  {
    headerName: 'Depth (ft)',
    field: 'depth',
    width: 100,
    editable: false,
    menuTabs: [],
    lockPosition: true,
    valueParser: agGridParamaters.numberParser,
    valueFormatter: agGridParamaters.getTwoNumbersAfterComma,
    type: 'numericColumn'
  },
  {
    headerName: 'K air (mD)',
    field: 'airPermeability',
    width: 100,
    editable: false,
    menuTabs: [],
    lockPosition: true,
    valueParser: agGridParamaters.numberParser,
    valueFormatter: agGridParamaters.getTwoNumbersAfterComma,
    type: 'numericColumn'
  },
  {
    headerName: 'P mean (abs atm)',
    field: 'pMean',
    width: 110,
    editable: false,
    menuTabs: [],
    lockPosition: true,
    valueParser: agGridParamaters.numberParser,
    valueFormatter: agGridParamaters.getTwoNumbersAfterComma,
    type: 'numericColumn'
  }
];

@Component({
  selector: 'klinkenberg-table-input',
  templateUrl: './table-input.component.html',
  styleUrls: ['./table-input.component.scss']
})
export class TableInputComponent implements OnInit {
  @Input()
  data: IKlinkenbergSampleInput[] = [];

  entryDataGrid;
  columns = inputDataColumns;

  constructor() {}

  ngOnInit() {}

  onEntryDataGridReady(params) {
    this.entryDataGrid = params;
  }

  menuOptions(): string[] {
    return ['copy', 'paste'];
  }
}
