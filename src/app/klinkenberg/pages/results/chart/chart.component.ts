import { Component, OnInit, Input } from '@angular/core';

import * as Highcharts from 'highcharts';

const options = {
  chart: {
    type: 'scatter',
    zoomType: 'xy'
  },
  title: {
    text: ''
  },
  xAxis: {
    title: {
      text: 'Air Permeability (mD)'
    }
  },
  yAxis: {
    title: {
      text: 'Klinkenberg Permeability (mD)'
    }
  },
  series: [],
  credits: {
    enabled: false
  }
};

export interface IKlinkenbergChartData {
  airPeremability: number;
  klinkenbergPermeability: number;
}

@Component({
  selector: 'klinkenberg-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  chart;
  Highcharts = Highcharts;
  chartConstructor = 'chart';
  chartOptions = options;
  updateFlag = false;
  oneToOneFlag = true;

  @Input()
  set title(title: string) {
    this.setChartTitle(title);
  }

  @Input()
  set data(data: IKlinkenbergChartData[]) {
    this.setChartOptions(data);
  }

  constructor() {}

  ngOnInit() {}

  setChartOptions(data: IKlinkenbergChartData[]) {
    const airPeremabilityValues = data.map((x) => x.airPeremability);
    const klinkenbergPermeabilityValues = data.map((x) => x.klinkenbergPermeability);

    const maxLine = Math.max(...airPeremabilityValues, ...klinkenbergPermeabilityValues);

    this.chartOptions = {
      ...this.chartOptions,
      series: [
        {
          type: 'scatter',
          name: 'Data',
          data: data.map((x) => [x.airPeremability, x.klinkenbergPermeability])
        },
        {
          type: 'line',
          name: 'Line',
          data: [[0, 0], [maxLine, maxLine]],
          marker: {
            enabled: false
          },
          states: {
            hover: {
              lineWidth: 0
            }
          },
          enableMouseTracking: false
        }
      ]
    };
  }

  setChartTitle(title: string) {
    this.chartOptions.title.text = title;
  }

  chartCallback = (chart) => (this.chart = chart);
}
