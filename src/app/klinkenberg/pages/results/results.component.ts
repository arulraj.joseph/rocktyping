import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { KlinkenbergRoutes, IKlinkenbergJob } from '../../core/models';
import { ResultsService } from '../../core/services/results/results.service';
import { IKlinkenbergChartData } from './chart/chart.component';

@Component({
  selector: 'klinkenberg-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {
  samplesLink: string = KlinkenbergRoutes.Samples;

  chartData: IKlinkenbergChartData[] = [];
  jobData: IKlinkenbergJob;

  constructor(private router: Router, private resultsService: ResultsService) {}

  ngOnInit() {
    if (!this.resultsService.hasJob) {
      this.router.navigateByUrl(KlinkenbergRoutes.Samples);
      return;
    }

    this.resultsService.getCorrectionJob().subscribe((data: IKlinkenbergJob) => {
      this.jobData = data;

      const chartData = [];

      for (let i = 0; i < data.output.length; i++) {
        chartData.push({
          airPeremability: data.input[i].airPermeability,
          klinkenbergPermeability: data.output[i].klinkenbergPermeabilityP
        });
      }

      this.chartData = chartData;
    });
  }

  editJobData() {
    this.resultsService.jobData = this.jobData.input;
    this.router.navigateByUrl(KlinkenbergRoutes.Samples);
  }

  exportJobData() {
    this.resultsService.downloadCorrectionJob();
  }

  returnToInput() {
    this.router.navigateByUrl(KlinkenbergRoutes.Samples);
  }

  saveJobData() {
    this.resultsService.saveCorrectionJob().subscribe(() => {
      this.router.navigateByUrl(KlinkenbergRoutes.Correction);
    });
  }
}
