export enum KlinkenbergRoutes {
  Correction = '/klinkenberg/correction',
  Samples = '/klinkenberg/samples',
  Results = '/klinkenberg/results'
}
