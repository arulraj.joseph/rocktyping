export const copyToClipboard = (value: number): void => {
  const copyValue = document.createElement('textarea');

  copyValue.value = String(value);
  document.body.appendChild(copyValue);
  copyValue.select();
  document.execCommand('copy');
  document.body.removeChild(copyValue);
};
