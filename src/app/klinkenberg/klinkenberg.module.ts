import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatInputModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatTableModule,
  MatTooltipModule,
  MatIconModule,
  MatProgressSpinnerModule
} from '@angular/material';

import { CorrectionComponent } from './pages/correction/correction.component';
import { KlinkenbergRoutingModule } from './klinkenberg.routs';
import { MainComponent } from './pages/main/main.component';
import { SamplesComponent } from './pages/samples/samples.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AgGridModule } from 'ag-grid-angular';
import { ResultsComponent } from './pages/results/results.component';
import { SharedModule } from '../shared/shared.module';
import { HighchartsChartModule } from 'highcharts-angular';
import { ChartComponent } from './pages/results/chart/chart.component';
import { TableInputComponent } from './pages/results/table-input/table-input.component';
import { TableOutputComponent } from './pages/results/table-output/table-output.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HighchartsChartModule,
    KlinkenbergRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatSliderModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatTableModule,
    MatTooltipModule,
    PerfectScrollbarModule,
    AgGridModule.withComponents([]),
    SharedModule
  ],
  declarations: [
    CorrectionComponent,
    MainComponent,
    SamplesComponent,
    ResultsComponent,
    ChartComponent,
    TableInputComponent,
    TableOutputComponent
  ]
})
export class KlinkenbergModule {}
