import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthGuard } from '../routing/auth.guard';
import { CorrectionComponent } from './pages/correction/correction.component';
import { MainComponent } from './pages/main/main.component';
import { SamplesComponent } from './pages/samples/samples.component';
import { ResultsComponent } from './pages/results/results.component';

export const KLINKENBERG_ROUTES = [
  {
    path: '',
    redirectTo: 'correction',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'correction',
        component: CorrectionComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'samples',
        component: SamplesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'results',
        component: ResultsComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  exports: [RouterModule]
})
export class KlinkenbergRoutingModule {}
