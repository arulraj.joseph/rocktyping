export enum KlinkenbergRoutes {
  Correction = '/klinkenberg/correction',
  Samples = '/klinkenberg/samples',
  Results = '/klinkenberg/results'
}

export interface IKlinkenbergSampleInput {
  sampleNumber: string;
  depth: number;
  pMean?: number;
  airPermeability: number;
}

export interface IKlinkenbergSampleOutput {
  bAir: number;
  klinkenbergPermeabilityCalc: number;
  klinkenbergPermeabilityP: number;
  chart?: number;
}

export interface IKlinkenbergInput {
  name: string;
  input: IKlinkenbergSampleInput[];
}

export interface IKlinkenbergJob {
  createdAt: string;
  name: string;
  input: IKlinkenbergSampleInput[];
  output: IKlinkenbergSampleOutput[];
}

export interface IKlinkenbergResponse {
  createdAt: string;
  name: string;
  input: IKlinkenbergSampleInput[];
  output: {
    bAir: number[];
    klinkenbergPermeabilityCalc: number[];
    klinkenbergPermeabilityP: number[];
  };
  createdBy?: string;
  createdDate?: string;
  id?: string;
  lastModifiedBy?: string;
  lastModifiedDate?: string;
  uwi?: string;
}
