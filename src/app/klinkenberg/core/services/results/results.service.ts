import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  IKlinkenbergInput,
  IKlinkenbergSampleInput,
  IKlinkenbergJob,
  IKlinkenbergSampleOutput,
  IKlinkenbergResponse
} from '../../models';
import { DownloadService } from '../../../../shared/services/download.service';

@Injectable({
  providedIn: 'root'
})
export class ResultsService {
  get hasJob(): boolean {
    return this.entryDataSource.input && this.entryDataSource.input.length > 0;
  }

  get jobData(): IKlinkenbergSampleInput[] {
    return this.tableData;
  }

  set jobData(data: IKlinkenbergSampleInput[]) {
    this.tableData = data;
  }

  entryDataSource: IKlinkenbergInput = {
    input: [],
    name: 'data-{{$timestamp}}'
  };

  allJobData: IKlinkenbergResponse;
  tableData: IKlinkenbergSampleInput[] = [];

  constructor(private httpClient: HttpClient, private downloadService: DownloadService) {}

  getCorrectionJob(newData?: IKlinkenbergSampleInput[]): Observable<IKlinkenbergJob> {
    this.entryDataSource.input = newData || this.entryDataSource.input;

    return this.httpClient
      .post<IKlinkenbergResponse>('{KLINKENBERG_SERVICE}/services/klinkenberg/calculation/list', this.entryDataSource)
      .pipe(
        map((data) => {
          const outputResult: IKlinkenbergSampleOutput[] = [];

          this.allJobData = data;

          for (let i = 0; i < data.output.bAir.length; i++) {
            outputResult.push({
              bAir: data.output.bAir[i],
              klinkenbergPermeabilityCalc: data.output.klinkenbergPermeabilityCalc[i],
              klinkenbergPermeabilityP: data.output.klinkenbergPermeabilityP[i]
            });
          }

          return {
            name: data.name,
            createdAt: data.createdAt,
            input: data.input,
            output: outputResult
          };
        })
      );
  }

  exportCorrectionJob(): Observable<Blob> {
    return this.httpClient.post('{KLINKENBERG_SERVICE}/services/klinkenberg/calculation/export', this.allJobData, {
      responseType: 'blob'
    });
  }

  downloadCorrectionJob() {
    this.exportCorrectionJob().subscribe((data) => {
      this.downloadService.downloadFile(data, `${this.allJobData.uwi}.xlsx`);
    });
  }

  saveCorrectionJob(): Observable<IKlinkenbergResponse> {
    return this.httpClient.post<IKlinkenbergResponse>(
      '{KLINKENBERG_SERVICE}/services/klinkenberg/calculation/save',
      this.allJobData
    );
  }

  setEntryData(newData: IKlinkenbergSampleInput[]) {
    this.entryDataSource.input = newData.filter((row) => !!row.airPermeability && !!row.depth);
  }
}
