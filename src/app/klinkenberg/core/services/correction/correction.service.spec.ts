/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CorrectionService } from './correction.service';

describe('Service: Correction', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CorrectionService]
    });
  });

  it('should ...', inject([CorrectionService], (service: CorrectionService) => {
    expect(service).toBeTruthy();
  }));
});
