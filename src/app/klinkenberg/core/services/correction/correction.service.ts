import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { IKlinkenbergInput, IKlinkenbergResponse, IKlinkenbergSampleOutput } from '../../models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CorrectionService {
  constructor(private httpClient: HttpClient) {}

  getCorrectionValue(permeability: number, pmean: number): Observable<IKlinkenbergSampleOutput> {
    if (!pmean) {
      pmean = null;
    }

    const params: IKlinkenbergInput = {
      input: [
        {
          airPermeability: permeability,
          depth: 0.0,
          sampleNumber: '',
          pMean: pmean
        }
      ],
      name: 'data-{{$timestamp}}'
    };

    return this.httpClient
      .post<IKlinkenbergResponse>('{KLINKENBERG_SERVICE}/services/klinkenberg/calculation/single', params)
      .pipe(
        map((data) => {
          if (data.output && data.output.klinkenbergPermeabilityCalc.length > 0) {
            return {
              klinkenbergPermeabilityP: data.output.klinkenbergPermeabilityP[0],
              klinkenbergPermeabilityCalc: data.output.klinkenbergPermeabilityCalc[0],
              bAir: data.output.bAir[0]
            };
          }

          return null;
        })
      );
  }
}
