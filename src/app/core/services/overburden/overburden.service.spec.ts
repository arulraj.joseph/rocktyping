import { TestBed } from '@angular/core/testing';

import { OverburdenService } from './overburden.service';

describe('OverburdenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OverburdenService = TestBed.get(OverburdenService);
    expect(service).toBeTruthy();
  });
});
