import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs/index';
import { WellsDetails } from '../../../xrd/core/models';

@Injectable({
  providedIn: 'root'
})
export class OverburdenService {
  private url = 'assets/data';

  constructor(private httpClient: HttpClient) {}

  public getWellFileDetails(): Observable<any> {
    //  TODO: change mock data to real service
    return this.httpClient.get<WellsDetails[]>(`/${this.url}/WellFile.json`);
  }

  /**
   * Read data from an uploaded csv file
   * @param file
   */
  readCsvFile(file) {
    return from(
      new Promise((resolve, reject) => {
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
          const [cols, ...data] = e.target.result.split('\n').map((d) => d.split(',').map((s) => s.trim()));

          const mappedData = data.map((row) =>
            row.reduce((p, c, i) => {
              if (cols[i]) {
                p[cols[i]] = c;
              }

              return p;
            }, {})
          );

          resolve(mappedData);
        };

        reader.onerror = (e) => reject(e);
        reader.readAsBinaryString(file);
      })
    );
  }
}
