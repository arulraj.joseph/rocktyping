import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICalculationResult, ICreateCalculationData, IUnitConverted, IPVTSearchData } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class CalculatorsService {
  private url = 'assets/data';
  constructor(private httpClient: HttpClient) {}

  /**
   * gets the well data of BlackOilProperties
   * @param searchText the search text
   * @returns The observable for the HTTP request.
   */
  public getWellLookupData = (searchText: string = ''): Observable<string[]> => {
    return this.httpClient.get<string[]>(`{PVT_SERVICE}/services/pvt/lookups/wells?name=${searchText}`);
  };

  /**
   * gets the pvt data according to search criteria
   * @param name the name
   * @returns The observable for the HTTP request.
   */
  public searchPVTData(name) {
    return this.httpClient.get<IPVTSearchData>(`{PVT_SERVICE}/services/pvt/data?\
    name=${name}&sortBy=updatedAt&page=1&perPage=1`);
  }

  /**
   * creates the PVT
   * @param data the data
   * @returns The observable for the HTTP request.
   */
  public createPVT(data) {
    return this.httpClient.post(`{PVT_SERVICE}/services/pvt/data`, data);
  }

  /**
   * updates the PVT
   * @param id the id
   * @param data the data
   * @returns The observable for the HTTP request.
   */
  public updatePVT(id, data) {
    return this.httpClient.put(`{PVT_SERVICE}/services/pvt/data/${id}`, data);
  }

  public getAllIncrementBy = (): Observable<any[]> => {
    //  TODO: change mock data to real service
    return this.httpClient.get<any[]>(`/${this.url}/IncrementBy.json`);
  };

  /**
   * converts the unit
   * @param data the request data
   * @returns The observable for the HTTP request.
   */
  public convertUnit(data) {
    return this.httpClient.post<IUnitConverted>(`{UNIT_CONVERTER_ALGO}/unit-converter/convert`, data);
  }

  /**
   * gets the well data
   * @param name the name
   * @returns The observable for the HTTP request.
   */
  public getWellData(name: string) {
    const perPage = 2;
    if (name) {
      return this.httpClient.get<string[]>(
        `{NCS_SERVICE}/services/ncs/calculation/list?\
          name=${name}&page=1&perPage=${perPage}&sortBy=updatedAt&sortOrder=desc`
      );
    } else {
      return this.httpClient.get<string[]>(
        `{NCS_SERVICE}/services/ncs/calculation/list?\
          page=1&perPage=${perPage}&sortBy=updatedAt&sortOrder=desc`
      );
    }
  }

  /**
   * creates the calculation
   * @param body the body
   * @returns The observable for the HTTP request.
   */
  public createCalculation(body: ICreateCalculationData) {
    return this.httpClient.post<ICalculationResult>(`{NCS_SERVICE}/services/ncs/calculation/create`, body);
  }

  /**
   * updates the calculation
   * @param id the id
   * @param body the body
   * @returns The observable for the HTTP request.
   */
  public updateCalculation(id, body: ICreateCalculationData) {
    return this.httpClient.put<ICalculationResult>(`{NCS_SERVICE}/services/ncs/calculation/save/${id}`, body);
  }
}
