import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { map, share, takeUntil } from 'rxjs/operators';
import { IBrpData, ISearchResult } from '../../models';

const UPLOAD_TIME_MOCK = 1500;
const UPLOAD_UPDATE_INTERVAL = 150;

@Injectable({
  providedIn: 'root'
})
export class TemplatesService {
  private filters: any;

  private data$: Observable<any>;

  constructor(private http: HttpClient) {}

  // POST: Create BRP data (by las file)
  uploadFile = (file: string) => {
    const data = new FormData();
    data.append('file', file);
    data.append('name', `Well-${new Date().valueOf()}`);
    return this.http.post(`{BRP_SERVICE}/services/brp/data`, data);
  };

  // GET: Search BRP data
  public getFiles = (): Observable<ISearchResult> => {
    return this.http.get<ISearchResult>(`{BRP_SERVICE}/services/brp/data`);
  };

  // PUT: Update BRP data (by json): Reject toggle
  public toggleReject = (id: string, body: IBrpData) => {
    return this.http.put(`{BRP_SERVICE}/services/brp/data/${id}`, body);
  };

  applyFilters(filters) {
    this.filters = filters;
  }

  getFilters() {
    return this.filters || {};
  }

  getAllData() {
    return this.data$;
  }

  // GET: Export BRP data
  export2LAS(id: string) {
    return this.http.get(`{BRP_SERVICE}/services/brp/data/export/${id}`, { responseType: 'text' });
  }

  // GET: Get BRP data
  readFileData(id: string) {
    return this.http.get<IBrpData>(`{BRP_SERVICE}/services/brp/data/${id}`);
  }

  uploadFileTimer() {
    const start = Date.now();
    const end = start + UPLOAD_TIME_MOCK;

    const timer$ = timer(0, UPLOAD_UPDATE_INTERVAL)
      .pipe(takeUntil(timer(UPLOAD_TIME_MOCK + UPLOAD_UPDATE_INTERVAL)))
      .pipe(map(() => Math.min(1, 1 - (end - Date.now()) / UPLOAD_TIME_MOCK)))
      .pipe(share());

    timer$.subscribe(() => {}, () => {}, () => {});

    return timer$;
  }
}
