import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xls';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {
  constructor() {}

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    console.log(json);
    const workbook: XLSX.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xls', type: 'array' });
    // const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  /**
   * Update input sample from excel data
   * @param excelData excel data in string
   * @param inputSample inputSample object
   * @param index index of sheet, this is optional
   */
  updateInputSampleWithExcelData(excelData: string, inputSample, index = 0) {
    const wBook: XLSX.WorkBook = XLSX.read(excelData, { type: 'array' });
    console.log('wBook', wBook);
    const wsname: string = wBook.SheetNames[index];
    const wSheet: XLSX.WorkSheet = wBook.Sheets[wsname];
    const sheetCellRange = XLSX.utils.decode_range(wSheet['!ref']);
    const origExcelData: any = XLSX.utils.sheet_to_json(wSheet, {
      header: 1,
      range: wSheet['!ref'],
      raw: true
    });

    let refExcelData = origExcelData;
    const excelTransformNum = [];
    for (let idx = 0; idx <= sheetCellRange.e.c; idx++) {
      excelTransformNum[idx] = this.transform(idx);
    }
    refExcelData = refExcelData.map((x) => {
      x.unshift('#');
      return x;
    });
    excelTransformNum.unshift('order');
    const excelDataEncodeToJson = refExcelData.map((item) =>
      item.reduce((obj, val, i) => {
        obj[excelTransformNum[i]] = val;
        return obj;
      }, {})
    );
    this.convertExcelToJson(excelDataEncodeToJson, inputSample);
  }

  transform(value): any {
    return (value >= 26 ? this.transform(value / 26 - 1) : '') + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[value % 26];
  }

  /**
   * Convert data from excel to json and show on ui
   * @param excelDataEncodeToJson Data object
   * @param inputSample inputSample object
   */
  convertExcelToJson(excelDataEncodeToJson, inputSample) {
    console.log('convertExcelToJson', excelDataEncodeToJson);
    inputSample.inputs.sampleProperties.sampleNumber = excelDataEncodeToJson[0].E;
    inputSample.inputs.sampleProperties.depth = excelDataEncodeToJson[1].E;
    inputSample.inputs.sampleProperties.proposity = excelDataEncodeToJson[2].E;
    inputSample.inputs.sampleProperties.airPermeability = excelDataEncodeToJson[3].E;
    inputSample.inputs.sampleProperties.grainDensity = excelDataEncodeToJson[4].E;
    inputSample.inputs.sampleProperties.saturation = excelDataEncodeToJson[5].E;
    inputSample.inputs.sampleProperties.confiningStress = excelDataEncodeToJson[6].E;
    inputSample.inputs.sampleProperties.saturantResistivity = excelDataEncodeToJson[7].E;
    inputSample.inputs.sampleProperties.porosityExponentM = excelDataEncodeToJson[8].E;
    inputSample.inputs.sampleProperties.porosityExponentMIndex = excelDataEncodeToJson[9].E;
    inputSample.inputs.sampleProperties.interceptY = excelDataEncodeToJson[10].E;
    inputSample.inputs.sampleProperties.resRw = excelDataEncodeToJson[11].E;
    inputSample.inputs.sampleProperties.resTemp = excelDataEncodeToJson[12].E;
    inputSample.inputs.formationFator.f = excelDataEncodeToJson[19].B;
    inputSample.inputs.formationFator.fIndex = excelDataEncodeToJson[19].D;
    inputSample.inputs.porosityExponent.m = excelDataEncodeToJson[19].F;
    inputSample.inputs.porosityExponent.mIndex = excelDataEncodeToJson[19].G;
    inputSample.inputs.labValues.labRw = excelDataEncodeToJson[28].C;
    inputSample.inputs.labValues.labTemp = excelDataEncodeToJson[29].C;
    inputSample.inputs.saturationExponent.n = excelDataEncodeToJson[19].K;
    inputSample.inputs.saturationExponent.nIndex = excelDataEncodeToJson[19].L;
    inputSample.inputs.saturationExponent.qv = excelDataEncodeToJson[19].M;
    inputSample.inputs.resistivityIndex = [];
    excelDataEncodeToJson.forEach((element, index) => {
      if (index >= 20 && element.H && element.I && element.J) {
        inputSample.inputs.resistivityIndex.push({
          fractionVp: 0,
          riApparent: 0,
          riIndexApparent: 0
        });
      }
    });
    inputSample.inputs.resistivityIndex.forEach((el, index) => {
      el.fractionVp = excelDataEncodeToJson[index + 20].H;
      el.riApparent = excelDataEncodeToJson[index + 20].I;
      el.riIndexApparent = excelDataEncodeToJson[index + 20].J;
    });
  }
}
