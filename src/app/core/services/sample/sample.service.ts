import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import * as _ from 'lodash';
import { ISample } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class SampleService {
  private url = `{EPC_SERVICE}`;
  private wellData;
  private selectedItem;
  private sampleList = [];
  private wellList = [];
  private updatedSample;

  constructor(private httpClient: HttpClient) {}

  /**
   * utility method of get http call
   * @param url endpoint
   * @param criteria query parameters
   */
  private get(url, criteria = null) {
    if (!criteria) {
      return this.httpClient.get(url);
    }
    _.defaults(criteria, { page: 0, limit: 10 });
    const params = new URLSearchParams();
    _.forEach(criteria, (value, key) => {
      params.set(key, value);
    });
    return this.httpClient.get(url + '?' + params.toString());
  }

  /**
   * config option
   * @param options data options
   */
  private requestOptions(options?: any): any {
    if (!options) {
      options = {};
    }
    if (!options['Content-Type']) {
      options['Content-Type'] = 'application/json';
    }

    const extraOption: any = {
      headers: options
    };

    return extraOption;
  }

  /**
   * get error message from error response from server
   * @param error Error object
   */
  getErrorMessage(error) {
    const message = _.get(error, 'message', '');
    if (typeof message === 'object') {
      return JSON.stringify(message);
    } else if (typeof message === 'string') {
      return message;
    }
    return 'Network error.';
  }

  /**
   * catches the auth error
   * @param error the error response
   */
  catchNetworkError(error: Response): Observable<Response> {
    if ((error as any).error) {
      (error as any).error.message = this.getErrorMessage((error as any).error);
    }
    return throwError(_.get(error, 'error', {}));
  }

  /**
   * Performs a request with `post` http method.
   * @param url the url
   * @param body the body
   * @param options the request options
   */
  post(url: string, body: any, options?: any): Observable<any> {
    return this.httpClient
      .post(url, body, this.requestOptions(options))
      .pipe(catchError((err) => this.catchNetworkError(err)));
  }

  /**
   * Performs a request with `put` http method.
   * @param url the url
   * @param body the body
   * @param options the request options
   */
  put(url: string, body: any, options?: any): Observable<any> {
    return this.httpClient
      .put(url, body, this.requestOptions(options))
      .pipe(catchError((err) => this.catchNetworkError(err)));
  }

  /**
   * convert sample result from server to app sample object
   * @param sample sample object
   */
  convertSampleToUIData(item) {
    const series1: any = _.zip(item.input.RI, item.input.brineSaturation)
      .sort((a: any, b: any) => a[0] - b[0])
      .filter((ele, i, arr) => {
        if (i < arr.length - 1) {
          return ele[0] !== arr[i + 1][0];
        }
        return true;
      });

    return {
      id: item.id,
      sampleID: 'Sample # ' + item.input.sampleNumber,
      wellName: item.name,
      inputs: {
        sampleProperties: {
          sampleNumber: item.input.sampleNumber,
          proposity: item.input.porosityFraction,
          airPermeability: item.input.airPermeability,
          grainDensity: item.input.grainDensity,
          saturation: item.input.saturant,
          confiningStress: item.input.confiningStress,
          saturantResistivity: item.input.saturantResistivity,
          interceptY: item.input.interceptY,
          resRw: item.input.Rw,
          resTemp: item.input.T
        },
        formationFator: {
          f: item.input.Fa,
          fIndex: item.output.FStar
        },
        porosityExponent: {
          m: item.input.m,
          mIndex: item.input.mStar
        },
        labValues: {
          labRw: item.input.labRw,
          labTemp: item.input.labTemp
        },
        saturationExponent: {
          n: item.input.n,
          nIndex: item.input.nStar,
          qv: item.input.Qv
        },
        resistivityIndex: _.zip(item.input.brineSaturation, item.input.RI, item.input.RIStar).map((values) =>
          _.zipObject(['fractionVp', 'riApparent', 'riIndexApparent'], values)
        )
      },
      output: {
        currentReservoir: _.zip(
          item.output.Ires,
          item.output.Ires2,
          item.output.logSw,
          item.output.logIres,
          item.output.logIres2
        ).map((values) => _.zipObject(['iRes1', 'iRes2', 'logSw', 'logIRes1', 'logIRes2'], values)),
        visualizationGraph: {
          y: item.output.slope,
          rIndex: item.output.r,
          dataChart: {
            linear: [
              [series1[0][0], item.output.slope * series1[0][0]],
              [series1[series1.length - 1][0], item.output.slope * series1[series1.length - 1][0]]
            ],
            series1
          }
        },
        dataBottom: {
          longPhi: item.output.logPHI,
          longFRev: item.output.logFresv,
          mResv: item.output.mResv,
          labB: item.output.labB,
          resRw: item.input.Rw,
          resTemp: item.input.T
        }
      }
    };
  }

  /**
   * convert api response to fit model at frontend
   * @param res api response
   * @param sampleService this object
   */
  private sampleAdapter(res) {
    res.samples = res.results.map((item) => this.convertSampleToUIData(item));
    return res;
  }

  /**
   * convert 1 api response to fit model at frontend
   * @param res api response
   * @param sampleService this object
   */
  private oneSampleAdapter(res) {
    return this.convertSampleToUIData(res);
  }

  /**
   * search samples
   * @param criteria query parameters
   */
  public searchSamples(criteria): Observable<any> {
    return this.get(`${this.url}/services/epc/lookup/samples`, criteria);
  }

  /**
   * search wells
   * @param criteria query parameters
   */
  public searchWells(criteria): Observable<any> {
    return this.get(`${this.url}/services/epc/lookup/wells`, criteria);
  }

  /**
   * get details of sample/well
   * @param criteria query parameters
   */
  public getDetails(criteria): Observable<any> {
    _.defaults(criteria, {
      sortDirection: 'ASC'
    });
    return this.get(`${this.url}/services/epc/data`, criteria).pipe(
      map((res) => {
        return this.sampleAdapter(res);
      })
    );
  }

  /**
   * get detail of one sample
   * @param sampleId sample id
   */
  public getDetail(sampleId): Observable<any> {
    return this.get(`${this.url}/services/epc/data/${sampleId}`).pipe(
      map((res) => {
        return this.oneSampleAdapter(res);
      })
    );
  }

  /**
   * Convert input sample to body object
   * @param inputSample inputSample Object
   */
  private convertInputSampleToBody(inputSample: ISample) {
    if (!inputSample || !inputSample.inputs) {
      return null;
    }
    const body = {
      labRw: inputSample.inputs.labValues ? inputSample.inputs.labValues.labRw : null,
      labTemp: inputSample.inputs.labValues ? inputSample.inputs.labValues.labTemp : null,
      Fa: inputSample.inputs.formationFator ? inputSample.inputs.formationFator.f : null,
      B: 0,
      Qv: inputSample.inputs.saturationExponent ? inputSample.inputs.saturationExponent.qv : null,
      name: inputSample.sampleID,
      sampleNumber: inputSample.inputs.sampleProperties ? inputSample.inputs.sampleProperties.sampleNumber : null,
      brineSaturation: inputSample.inputs.resistivityIndex
        ? inputSample.inputs.resistivityIndex.map((value) => value.fractionVp)
        : null,
      RI: inputSample.inputs.resistivityIndex
        ? inputSample.inputs.resistivityIndex.map((value) => value.riApparent)
        : null,
      RIStar: inputSample.inputs.resistivityIndex
        ? inputSample.inputs.resistivityIndex.map((value) => value.riIndexApparent)
        : null,
      n: inputSample.inputs.saturationExponent ? inputSample.inputs.saturationExponent.n : null,
      nStar: inputSample.inputs.saturationExponent ? inputSample.inputs.saturationExponent.nIndex : null,
      porosityFraction: inputSample.inputs.sampleProperties ? inputSample.inputs.sampleProperties.proposity : null,
      airPermeability: inputSample.inputs.sampleProperties ? inputSample.inputs.sampleProperties.airPermeability : null,
      grainDensity: inputSample.inputs.sampleProperties ? inputSample.inputs.sampleProperties.grainDensity : null,
      saturant: inputSample.inputs.sampleProperties ? inputSample.inputs.sampleProperties.saturation : null,
      confiningStress: inputSample.inputs.sampleProperties ? inputSample.inputs.sampleProperties.confiningStress : null,
      saturantResistivity: inputSample.inputs.sampleProperties
        ? inputSample.inputs.sampleProperties.saturantResistivity
        : null,
      m: inputSample.inputs.porosityExponent ? inputSample.inputs.porosityExponent.m : null,
      mStar: inputSample.inputs.porosityExponent ? inputSample.inputs.porosityExponent.mIndex : null,
      interceptY: inputSample.inputs.sampleProperties ? inputSample.inputs.sampleProperties.interceptY : null,
      Rw: inputSample.inputs.sampleProperties ? inputSample.inputs.sampleProperties.resRw : null,
      T: inputSample.inputs.sampleProperties ? inputSample.inputs.sampleProperties.resTemp : null
    };
    return _.omitBy(body, _.isNil);
  }

  /**
   * create new sample
   * @param inputSample input sample data
   */
  public createNewSample(inputSample: ISample): Observable<any> {
    return this.post(`${this.url}/services/epc/data`, this.convertInputSampleToBody(inputSample));
  }

  /**
   * update sample
   * @param sampleId sample id
   * @param inputSample input sample data
   * @param inputSampleRaw input sample data raw without process
   */
  public updateSample(sampleId, inputSample: ISample, inputSampleRaw: ISample): Observable<any> {
    const body = this.convertInputSampleToBody(inputSample);
    if (!body) {
      return throwError({ message: 'Nothing change.' });
    }
    return this.put(`${this.url}/services/epc/data/${sampleId}`, body).pipe(
      map((res) => {
        this.updatedSample = inputSampleRaw;
        this.updatedSample.id = sampleId;
        return res;
      })
    );
  }

  /**
   * Get last updated sample
   * @param sampleId sample id
   */
  public getLastUpdatedSample(sampleId) {
    if (!sampleId) {
      return this.updatedSample;
    }
    if (this.updatedSample && sampleId === this.updatedSample.id) {
      return this.updatedSample;
    }
    return null;
  }

  /**
   * cache details of wells, will be displayed across pages
   * @param data wells data
   */
  public setWellData(data) {
    this.wellData = data;
  }

  /**
   * get wells details
   */
  public getWellData() {
    return this.wellData;
  }

  /**
   * cache selected sample/well when searching
   * @param item selected sample/well
   */
  public setSelectedItem(item) {
    this.selectedItem = item;
  }

  /**
   * get selected sample/well
   */
  public getSelectedItem() {
    return this.selectedItem;
  }

  /**
   * cache viewed sample list, will be displayed across pages
   * @param list sample list
   */
  public setSampleList(list) {
    this.sampleList = list;
  }

  /**
   * get sample list
   */
  public getSampleList() {
    return this.sampleList;
  }

  /**
   * cache viewed well list, will be displayed across pages
   * @param list well list
   */
  public setWellList(list) {
    this.wellList = list;
  }

  /**
   * get well list
   */
  public getWellList() {
    return this.wellList;
  }
}
