import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  constructor(private matSnackBar: MatSnackBar) {}

  /**
   * display a error toast widget
   * @param msg error message
   */
  public error(msg) {
    this.matSnackBar.open(msg, null, {
      duration: 2000,
      panelClass: ['toast-panel', 'error-message']
    });
  }

  /**
   * display a success toast widget
   * @param msg success message
   */
  public success(msg) {
    this.matSnackBar.open(msg, null, {
      duration: 2000,
      panelClass: ['toast-panel', 'success-message']
    });
  }
}
