export interface IWellsDetails {
  label: string;
  value: string | number;
  unit?: string;
}

export interface IOilProperties {
  pressure: number;
  liberates: number;
}

export interface ISchedule {
  stageFrom?: number;
  stageTo?: number;
  depthFrom?: number;
  depthTo?: number;
  minOrStand?: number;
  ftH?: number;
  stands?: number;
  time?: number;
}

export interface IUnitConverted {
  result: number[];
  created_at: string;
  from: string;
  to: string;
  amount: number[];
}

export interface ICTSData {
  total: number;
  page: number;
  pageSize: number;
  items: IItem[];
}

export interface IItem {
  createdAt: string;
  updatedAt: string;
  id: string;
  externalId: string;
  name: string;
  location: string;
  input: IInput;
  output: IOutput;
}

export interface IOutput {
  simulated_stands: ISimulatedstand[];
  trip_stages: ITripstage[];
}

export interface ITripstage {
  stage_index: number;
  depth_from_ft: number;
  depth_to_ft: number;
  velocity_min_per_stand: number;
  feet_per_h: number;
  num_stands: number;
  total_time_h: number;
  time_hr: number;
}

export interface ISimulatedstand {
  velocity_min_per_stand: number;
  mud_weight: number;
  core_top_depth_ft: number;
  bit_depth_ft: number;
  mud_pressure: number;
  pore_pressure: number;
  delta_p_thermal: number;
  delta_p: number;
  gas_volume1: number;
  gas_volume2: number;
  gas_extraction_speed: number;
  produced_gas_vol: number;
  gas_volume_corr: number;
  pressure_corr_psig: number;
  liberated_gor_perc: number;
}

export interface IInput {
  coring_depth_ft: number;
  core_run_ft: number;
  core_d_in: number;
  mud_weight_surf: number;
  mud_weight_bt: number;
  mud_temp_surf_f: number;
  mud_temp_bt_f: number;
  stand_ft: number;
  kg_md: number;
  diam_perc: number;
  sw_perc: number;
  viscosity_max: number;
  bubble_point_psi: number;
  liberated_gor_by_pressure: number[][];
  tensile_strength_psi: number;
}

export interface IOilWellsCalculator {
  id: number;
  name: string;
}

export interface IBlackOilPropertiesData {
  id: number;
  name: string;
  data: IBlackOilProperties[];
}

export interface IBlackOilProperties {
  inputs: IInputsBlackOilProperties;
  calculation: ICalculationBlackOilProperties;
}

export interface IInputsBlackOilProperties {
  sampleId: number;
  p: number;
  t: number;
  sga: number;
  sgg: number;
  pb: number;
  rs: number;
  rsb: number;
  tds: number;
  yH2S: number;
  yCO2: number;
  yN2: number;
}

export interface ICalculationBlackOilProperties {
  sampleId: number;
  zFactor: number;
  oilDensity: number;
  oilCompressibility: number;
  oilViscosity: number;
  gor: number;
  bo: number;
  bw: number;
  gwr: number;
  waterDesity: number;
  waterCompressibility: number;
  waterViscosity: number;
  gasViscosity: number;
}

export interface ICalculationResult {
  createdBy: string;
  createdAt: string;
  updatedAt: string;
  id: string;
  name: string;
  inputs: IInputs;
  results: IResult[];
  updatedBy: string;
}

export interface IResult {
  m: number;
  E: number;
  V: number;
  H: number;
  h: number;
  P: number;
}

export interface IInputs {
  obGradient: number;
  poissonSRatio: number;
  porePressureGradient: number;
  fractGradient: number;
  biotsAlpha: number;
  depths: number[];
}

export interface ICreateCalculationData {
  biotsAlpha: number;
  depths: number[];
  fractGradient: number;
  name: string;
  obGradient: number;
  poissonSRatio: number;
  porePressureGradient: number;
}

export interface IPVTSearchData {
  total: number;
  page: number;
  pageSize: number;
  items: IPVTItem[];
}

export interface IPVTItem {
  createdAt: string;
  updatedAt: string;
  id: string;
  externalId: string;
  name: string;
  input: IPVTInput[];
  output: IPVTOutput[];
}

export interface IPVTInput {
  sampleNumber: string;
  reservoirPressure: number;
  reservoirTemperature: number;
  oilGravity: number;
  specificGasGravity: number;
  oilBPPressure: number;
  solutionGOR: number;
  solutionGORPb: number;
  waterSalinity: number;
  h2sMoleFraction: number;
  co2MoleFraction: number;
  n2MoleFraction: number;
}

export interface IPVTOutput {
  oilDensity: number;
  oilCompressibility: number;
  gor: number;
  bo: number;
  bw: number;
  gwr: number;
  waterDensity: number;
  waterCompressibility: number;
  waterViscosity: number;
  oilViscosity: number;
  gasViscosity: number;
}

export interface IBrpAlgorithmInput {
  DTC: number;
  DTS: number;
  bulkDensity: number;
  depth: number;
  sampleNumber: string;
}

export interface IBrpAlgorithmOutput {
  E: number;
  K: number;
  Lambda: number;
  PWave: number;
  PR: number;
  Vc: number;
  Vs: number;
  u: number;
}

export interface IBrpData {
  createdAt: string;
  externalId: string;
  fileName: string;
  id: string;
  input: IBrpAlgorithmInput[];
  name: string;
  output: IBrpAlgorithmOutput;
  status: string;
  updatedAt: string;
}

export interface IBrpRequest {
  input: IBrpAlgorithmInput[];
  name: string;
  status: string;
}

export interface ISearchResult {
  items: IBrpData[];
  page: number;
  pageSize: number;
  total: number;
}

// Electric Models
export interface ISample {
  sampleID: string;
  id?: string;
  wellName: string;
  inputs: ISampleInputs;
  output: ISampleOutputs;
}

export interface ISampleInputs {
  sampleProperties: ISampleProperties;
  formationFator: IFormationFator;
  porosityExponent: IPorosityExponent;
  labValues: ILabValues;
  saturationExponent: ISaturationExponent;
  resistivityIndex: IResistivityIndex[];
}

export interface ISampleOutputs {
  currentReservoir: ICurrentReservoir[];
  visualizationGraph: IVisualizationGraph;
  dataBottom: ISampleDataBottom;
}

export interface ISampleProperties {
  sampleNumber: string;
  depth: number;
  proposity: number;
  airPermeability: number;
  grainDensity: number;
  saturation: number;
  confiningStress: number;
  saturantResistivity: number;
  porosityExponentM: number;
  porosityExponentMIndex: number;
  interceptY: number;
  resRw: number;
  resTemp: number;
}

export interface IFormationFator {
  f: number;
  fIndex: number;
}

export interface IPorosityExponent {
  m: number;
  mIndex: number;
}

export interface ILabValues {
  labRw: number;
  labTemp: number;
}

export interface ISaturationExponent {
  n: number;
  nIndex: number;
  qv: number;
}

export interface IResistivityIndex {
  fractionVp: number;
  riApparent: number;
  riIndexApparent: number;
}

export interface ICurrentReservoir {
  iRes1: number;
  iRes2: number;
  logSw: number;
  logIRes1: number;
  logIRes2: number;
}

export interface IVisualizationGraph {
  y: number;
  rIndex: number;
  dataChart: ISampleDataChart;
}

export interface ISampleDataChart {
  line?: Array<[number, number]>;
  linear?: Array<[number, number]>;
  series1?: Array<[number, number]>;
  scatter?: Array<[number, number]>;
}

export interface ISampleDataBottom {
  longPhi: number;
  longFRev: number;
  mResv: number;
  nResv: number;
  labB: number;
  reservoirB: number;
  resRw: number;
  resTemp: number;
}
