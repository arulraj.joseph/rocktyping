import { TestBed } from '@angular/core/testing';

import { CarbonByMaturityService } from './carbon-by-maturity.service';

describe('CarbonByMaturityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarbonByMaturityService = TestBed.get(CarbonByMaturityService);
    expect(service).toBeTruthy();
  });
});
