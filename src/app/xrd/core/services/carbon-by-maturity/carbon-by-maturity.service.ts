import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICarbonByMaturity } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class CarbonByMaturityService {
  constructor(private httpClient: HttpClient) {}

  public getData = (): Observable<ICarbonByMaturity[]> => {
    return this.httpClient.get<ICarbonByMaturity[]>('{XRD_SERVICE}/services/static/carbon/maturity');
  };
}
