import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  constructor(private snackBar: MatSnackBar) {}
  notify(message: string) {
    this.snackBar.open(message, null, { verticalPosition: 'top', duration: 3000 });
  }
}
