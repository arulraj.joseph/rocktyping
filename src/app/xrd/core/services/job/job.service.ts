import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isNumber, isString } from 'util';
import { Observable, BehaviorSubject } from 'rxjs';
import { IJobGroup, IJobRun, IPagedResult } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class JobService {
  private newRunDataSource: BehaviorSubject<Partial<IJobRun>> = new BehaviorSubject(null);

  newRunData$ = this.newRunDataSource.asObservable();

  constructor(private httpClient: HttpClient) {}

  setNewRunData = (data: Partial<IJobRun>): void => {
    this.newRunDataSource.next(data);
  };

  public getAllJobGroups(params?: {
    filter: string;
    pageNumber?: number;
    pageSize?: number;
    sort?: string;
  }): Observable<IPagedResult<IJobGroup>> {
    let httpParams = new HttpParams();

    if (params && isNumber(params.pageNumber)) {
      httpParams = httpParams.set('page', params.pageNumber.toString());
    }

    if (params && isNumber(params.pageSize)) {
      httpParams = httpParams.set('pageSize', params.pageSize.toString());
    }

    if (params && isString(params.sort)) {
      httpParams = httpParams.set('sort', params.sort);
    }

    return this.httpClient.get<IPagedResult<IJobGroup>>(`{XRD_SERVICE}/services/job/group/${params.filter}`, {
      params: httpParams
    });
  }

  public getJobGroup = (groupId: string): Observable<IJobGroup> =>
    this.httpClient.get<IJobGroup>(`{XRD_SERVICE}/services/job/group/${groupId}`);

  public createJobGroup = (data: Partial<IJobGroup>): Observable<IJobGroup> =>
    this.httpClient.post<IJobGroup>('{XRD_SERVICE}/services/job/group/create', data);

  public editJobGroup = (data: Partial<IJobGroup>): Observable<IJobGroup> =>
    this.httpClient.put<IJobGroup>(`{XRD_SERVICE}/services/job/group/save/${data.id}`, data);

  public getJobRuns = (groupId: string): Observable<IJobRun[]> =>
    this.httpClient.get<IJobRun[]>(`{XRD_SERVICE}/services/job/run/all/${groupId}`);

  public createJobRun = (data: Partial<IJobRun>): Observable<IJobRun> =>
    this.httpClient.post<IJobRun>('{XRD_SERVICE}/services/job/run/create', data);

  public launchJobRun = (data: Partial<IJobRun>): Observable<IJobRun> =>
    this.httpClient.post<IJobRun>(`{XRD_SERVICE}/services/job/run/launch`, data);

  public getJobRun = (runId: string): Observable<IJobRun> =>
    this.httpClient.get<IJobRun>(`{XRD_SERVICE}/services/job/run/${runId}`);

  public optimizeJobRun = (data: Partial<IJobRun>): Observable<IJobRun> =>
    this.httpClient.post<IJobRun>(`{XRD_SERVICE}/services/job/run/optimize`, data);

  public saveJobRun = (data: Partial<IJobRun>): Observable<IJobRun> =>
    this.httpClient.put<IJobRun>(`{XRD_SERVICE}/services/job/run/save/${data.id}`, data);

  public removeJobRun = (runId: string): Observable<IJobRun> =>
    this.httpClient.delete<IJobRun>(`{XRD_SERVICE}/services/job/run/${runId}`);

  public exportAlgorithmResult = (resultId): Observable<Blob> =>
    this.httpClient.get(`{XRD_SERVICE}/services/xrd/algorithm/download/${resultId}`, {
      observe: 'body',
      responseType: 'blob'
    });
}
