import { Injectable } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { IMineralData } from '../../models';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MineralsService {
  constructor(private httpClient: HttpClient) {}

  public getAllMinerals(): Observable<IMineralData[]> {
    return combineLatest(this.getMineralLibrary(), this.getMineralStaticData()).pipe(
      map(([lib, data]) => {
        lib.forEach((m) => {
          const staticData = data.find((s) => m.name === s.name);
          m.lowerBounds = m.lowerBounds || staticData.lowerBounds;
          m.upperBounds = m.upperBounds || staticData.upperBounds;
          m.color = m.color || staticData.color;
          m.startingDensities = m.startingDensities || staticData.startingDensities;
        });

        return lib;
      })
    );
  }

  private getMineralLibrary = (): Observable<IMineralData[]> => {
    return this.httpClient.get<IMineralData[]>('{XRD_SERVICE}/services/xrd/mineral/all');
  };

  private getMineralStaticData = (): Observable<IMineralData[]> => {
    return this.httpClient.get<IMineralData[]>('{XRD_SERVICE}/services/static/mineral/data');
  };
}
