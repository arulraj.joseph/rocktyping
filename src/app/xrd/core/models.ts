export interface IBaseDocument {
  createdBy: string;
  lastModifiedBy: string;
  createdDate: string;
  lastModifiedDate: string;
}

export interface IMineralValue {
  name: string;
  value: number;
}

export interface IMineralData {
  name: string;
  startingDensities?: number;
  finalDensities?: number;
  lowerBounds: number;
  upperBounds: number;
  color?: string;
}

export interface IJobGroupFile {
  name: string;
  data: Array<{ [key: string]: string }>;
  createdDate: string;
  lastModifiedDate: string;
}

export interface IXrdAlgorithmResult {
  number: number;
  depthFt: number;
  mineralValues: IMineralValue[];
  totalVolume: number;
  total: number;
  griDryBulkD: number;
  griGrainD: number;
  griPhit: number;
  apparentGrainD: number;
  deltaGrainD: number;
  averageDryClay: number;
  averageDryClayWithPyrite: number;
}

export interface IPagedResult<T> {
  count: number;
  items: T[];
}

export interface ICarbonByMaturity {
  ro: number;
  carbon: number;
}

export interface IJobGroupsSort {
  name: string;
  value: string;
}

export interface IJobGroupsSortParameters {
  filter?: string;
  sort?: string;
}

export type JobRunStatus = 'NEW' | 'RUNNING' | 'COMPLETE' | 'ARCHIVED';

export interface IJobGroup extends IBaseDocument {
  id: string;
  name: string;
  description: string;
  isPublic: boolean;
  jobGroupFile: IJobGroupFile;
}

export interface IJobRun extends IBaseDocument {
  id: string;
  name: string;
  description: string;
  jobGroupId: string;
  status: JobRunStatus;
  xrdAlgorithmResult: IXrdAlgorithmResult[];
  xrdAlgorithmId: string;
  public: boolean;
  kerogenWeightPercentage: number;
  startingDensities: { [key: string]: number };
  finalDensities: { [key: string]: number };
  lowerBounds: { [key: string]: number };
  upperBounds: { [key: string]: number };
}
