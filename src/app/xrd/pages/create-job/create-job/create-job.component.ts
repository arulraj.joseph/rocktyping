import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ErrorsNotiferService } from '@apc-ng/theme-material';
import { from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { IJobGroup } from '../../../core/models';
import { JobService } from '../../../core/services/job/job.service';
import { RawDataDialogComponent } from '../../../shared/raw-data-dialog/raw-data-dialog.component';
import { convertCsvToJson } from '../../../../utils/convert-csv-to-json';
import { readFile } from '../../../../utils/read-file';

@Component({
  selector: 'xrd-create-job',
  templateUrl: './create-job.component.html',
  styleUrls: ['./create-job.component.scss']
})
export class CreateJobComponent {
  readonly NameMaxLength = 100;
  readonly DescriptionMaxLength = 500;

  jobForm: FormGroup = this.fb.group({
    name: new FormControl('', [Validators.required, Validators.maxLength(this.NameMaxLength)]),
    description: new FormControl('', Validators.maxLength(this.DescriptionMaxLength)),
    jobGroupFile: new FormControl(null, Validators.required),
    isPublic: new FormControl(false)
  });

  fileDataControl: AbstractControl = this.jobForm.controls['jobGroupFile'];
  isInputValid = true;

  constructor(
    private errorsNotiferService: ErrorsNotiferService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private jobService: JobService,
    private router: Router
  ) {}

  public upload(file: File) {
    readFile(file)
      .pipe(switchMap(convertCsvToJson))
      .subscribe(
        (data) => {
          this.fileDataControl.setValue({ name: file.name, data });
          this.isInputValid = this.checkInputValid(data);
        },
        () => this.errorsNotiferService.notify('Invalid file')
      );
  }

  openDialog(): void {
    const fileValue = this.fileDataControl.value;

    const dialogRef = this.dialog.open(RawDataDialogComponent, {
      data: fileValue.data
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  onSubmit(): void {
    this.jobService
      .createJobGroup(this.jobForm.value)
      .pipe(
        map((data: IJobGroup) => data.id),
        switchMap((id) => {
          const jobRunUrl = `xrd/job/${id}/create-run/${this.jobForm.get('isPublic').value ? 'public' : 'private'}`;

          return from(this.router.navigateByUrl(jobRunUrl));
        })
      )
      .subscribe(undefined, () => this.errorsNotiferService.notify('Server error'));
  }

  private checkInputValid(data: Array<{ [key: string]: string }>): boolean {
    const hasRows = data.length > 0;

    return hasRows;
  }
}
