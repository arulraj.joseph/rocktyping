import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'xrd-raw-data-input',
  templateUrl: './raw-data-input.component.html',
  styleUrls: ['./raw-data-input.component.scss']
})
export class RawDataInputComponent {
  @Input() isInputValid;

  @Output() removeFile = new EventEmitter<void>();
  @Output() viewRawData = new EventEmitter<void>();
}
