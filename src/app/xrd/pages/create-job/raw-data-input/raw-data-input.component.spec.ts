import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawDataInputComponent } from './raw-data-input.component';

describe('RawDataInputComponent', () => {
  let component: RawDataInputComponent;
  let fixture: ComponentFixture<RawDataInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RawDataInputComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawDataInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
