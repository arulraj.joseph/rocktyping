import { CreateJobModule } from './create-job.module';

describe('CreateJobModule', () => {
  let createJobModule: CreateJobModule;

  beforeEach(() => {
    createJobModule = new CreateJobModule();
  });

  it('should create an instance', () => {
    expect(createJobModule).toBeTruthy();
  });
});
