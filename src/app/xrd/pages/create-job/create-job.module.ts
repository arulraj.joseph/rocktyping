import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatFormFieldModule, MatInputModule, MatRadioModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { CreateJobComponent } from './create-job/create-job.component';
import { RawDataInputComponent } from './raw-data-input/raw-data-input.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    MatInputModule,
    MatRadioModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [CreateJobComponent, RawDataInputComponent]
})
export class CreateJobModule {}
