import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaunchRunComponent } from './launch-run.component';

describe('LaunchRunComponent', () => {
  let component: LaunchRunComponent;
  let fixture: ComponentFixture<LaunchRunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LaunchRunComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchRunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
