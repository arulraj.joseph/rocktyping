import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { IJobRun } from '../../../core/models';
import { JobService } from '../../../core/services/job/job.service';

@Component({
  selector: 'xrd-launch-run',
  templateUrl: './launch-run.component.html',
  styleUrls: ['./launch-run.component.scss']
})
export class LaunchRunComponent implements OnInit {
  jobRun$: Observable<IJobRun>;

  constructor(private jobService: JobService, public dialog: MatDialog, private router: Router) {}

  ngOnInit() {
    this.jobService.newRunData$.subscribe((newRun) => {
      if (newRun) {
        this.jobRun$ = this.jobService.launchJobRun(newRun);
      } else {
        this.router.navigateByUrl(`xrd`).then();
      }
    });
  }
}
