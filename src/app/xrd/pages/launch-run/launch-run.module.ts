import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { XrdSharedModule } from '../../shared/xrd-shared.module';
import { LaunchRunComponent } from './launch-run/launch-run.component';

@NgModule({
  imports: [CommonModule, XrdSharedModule, SharedModule],
  declarations: [LaunchRunComponent],
  exports: [LaunchRunComponent]
})
export class LaunchRunModule {}
