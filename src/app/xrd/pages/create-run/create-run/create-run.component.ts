import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { IJobGroup, IMineralData, IJobRun } from '../../../core/models';
import { JobService } from '../../../core/services/job/job.service';
import { MineralsService } from '../../../core/services/minerals/minerals.service';

@Component({
  selector: 'xrd-create-run',
  templateUrl: './create-run.component.html',
  styleUrls: ['./create-run.component.scss']
})
export class CreateRunComponent implements OnInit {
  initialRun: boolean;

  jobGroupData$: Observable<IJobGroup>;
  mineralsData$: Observable<IMineralData[]>;
  data$: Observable<{ jobGroup: IJobGroup; minerals: IMineralData[] }>;

  constructor(
    private jobService: JobService,
    private mineralsService: MineralsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const jobGroupId = this.route.snapshot.paramMap.get('jobId');

    this.jobGroupData$ = this.jobService.getJobGroup(jobGroupId);
    this.mineralsData$ = this.getMineralData();

    this.data$ = combineLatest(
      this.jobGroupData$,
      this.mineralsData$,
      this.jobService.newRunData$,
      this.route.data
    ).pipe(
      map(([jobGroupData, mineralsData, draftJobRun, routeData]) => {
        this.initialRun = routeData && routeData.initial;

        const isPublic = routeData && routeData.public;
        const restoreDraft = routeData && routeData.restoreDraft;

        const emptyJobRun = { name: this.initialRun ? 'Initial Run' : '', public: isPublic };

        return {
          jobGroup: jobGroupData,
          minerals: restoreDraft && draftJobRun ? this.getMineralDataFromJobRun(draftJobRun) : mineralsData,
          jobRun: restoreDraft && draftJobRun ? draftJobRun : emptyJobRun
        };
      })
    );
  }

  private getMineralData(): Observable<IMineralData[]> {
    return combineLatest(this.mineralsService.getAllMinerals(), this.jobGroupData$).pipe(
      take(1),
      map((data: [IMineralData[], IJobGroup]) => {
        const requiredMinerals = data[1].jobGroupFile.data[0];
        const result = data[0].reduce((acc, item) => {
          const purifiedItemName = item.name.toLowerCase().trim();

          if (item.name && (requiredMinerals[item.name] !== undefined || purifiedItemName === 'kerogen')) {
            acc.push(item);
          }

          return acc;
        }, []);

        return result;
      })
    );
  }

  private getMineralDataFromJobRun(run: IJobRun | Partial<IJobRun>): IMineralData[] {
    return Object.keys(run.startingDensities).map((mineralName) => {
      return {
        name: mineralName,
        startingDensities: run.startingDensities[mineralName],
        lowerBounds: run.lowerBounds[mineralName],
        upperBounds: run.upperBounds[mineralName]
      };
    });
  }
}
