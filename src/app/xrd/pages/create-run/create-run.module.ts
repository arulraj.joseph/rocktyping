import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { XrdSharedModule } from '../../shared/xrd-shared.module';
import { CreateRunComponent } from './create-run/create-run.component';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule, XrdSharedModule],
  declarations: [CreateRunComponent],
  exports: [CreateRunComponent]
})
export class CreateRunModule {}
