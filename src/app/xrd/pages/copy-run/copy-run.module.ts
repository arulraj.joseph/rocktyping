import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';
import { XrdSharedModule } from '../../shared/xrd-shared.module';
import { CopyRunComponent } from './copy-run/copy-run.component';

@NgModule({
  imports: [CommonModule, SharedModule, XrdSharedModule, RouterModule],
  declarations: [CopyRunComponent],
  exports: [CopyRunComponent]
})
export class CopyRunModule {}
