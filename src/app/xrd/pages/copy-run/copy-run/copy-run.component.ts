import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, publishReplay, refCount, switchMap } from 'rxjs/operators';
import { IJobGroup, IJobRun, IMineralData } from '../../../core/models';
import { JobService } from '../../../core/services/job/job.service';

@Component({
  selector: 'xrd-copy-run',
  templateUrl: './copy-run.component.html',
  styleUrls: ['./copy-run.component.scss']
})
export class CopyRunComponent implements OnInit {
  jobRunData$: Observable<IJobRun>;
  jobGroupData$: Observable<IJobGroup>;
  mineralsData$: Observable<IMineralData[]>;
  data$: Observable<{ jobRun: IJobRun; jobGroup: IJobGroup; minerals: IMineralData[] }>;

  constructor(private jobService: JobService, private route: ActivatedRoute) {}

  ngOnInit() {
    const jobRunId = this.route.snapshot.paramMap.get('runId');
    this.jobRunData$ = this.jobService.getJobRun(jobRunId).pipe(
      publishReplay(),
      refCount()
    );
    this.jobGroupData$ = this.jobRunData$.pipe(
      switchMap((run: IJobRun) => this.jobService.getJobGroup(run.jobGroupId))
    );
    this.mineralsData$ = this.jobRunData$.pipe(map((run: IJobRun) => this.getMineralData(run)));
    this.data$ = combineLatest(this.jobRunData$, this.jobGroupData$, this.mineralsData$).pipe(
      map((data) => ({
        jobRun: data[0],
        jobGroup: data[1],
        minerals: data[2]
      }))
    );
  }

  private getMineralData(run: IJobRun): IMineralData[] {
    return Object.keys(run.startingDensities).map((mineralName) => {
      return {
        name: mineralName,
        startingDensities: run.startingDensities[mineralName],
        lowerBounds: run.lowerBounds[mineralName],
        upperBounds: run.upperBounds[mineralName]
      };
    });
  }
}
