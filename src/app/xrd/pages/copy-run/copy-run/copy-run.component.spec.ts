import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyRunComponent } from './copy-run.component';

describe('CopyRunComponent', () => {
  let component: CopyRunComponent;
  let fixture: ComponentFixture<CopyRunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CopyRunComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyRunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
