import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompareRunComponent } from './compare-run.component';

describe('CompareRunComponent', () => {
  let component: CompareRunComponent;
  let fixture: ComponentFixture<CompareRunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompareRunComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompareRunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
