import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';
import { IJobRun, IXrdAlgorithmResult } from '../../../core/models';
import { JobService } from '../../../core/services/job/job.service';
import { IPieChartData } from '../../../shared/pie-chart/pie-chart.component';
import { IScatterChartData } from '../../../shared/scatter-chart/scatter-chart.component';
import { IStackedBarChartData } from '../../../shared/stacked-bar-chart/stacked-bar-chart.component';
import { getPieChartData, getScatterChartData, getStackedBarChartData } from '../../../../utils/charts.helper';

@Component({
  selector: 'xrd-compare-run',
  templateUrl: './compare-run.component.html',
  styleUrls: ['./compare-run.component.scss']
})
export class CompareRunComponent implements OnInit {
  @Input() runId: string;

  selectedDepth: number;
  jobRun$: Observable<IJobRun>;

  minerals$: Observable<string[]>;
  selectedRow$: BehaviorSubject<IXrdAlgorithmResult> = new BehaviorSubject(null);

  pieChartData$: Observable<{ title: string; data: IPieChartData[]; depth?: number }>;
  scatterChartData$: Observable<IScatterChartData[]>;
  stackedBarChartData$: Observable<IStackedBarChartData[]>;

  constructor(private jobService: JobService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.jobRun$ = this.jobService.getJobRun(this.runId).pipe(
      publishReplay(1),
      refCount()
    );

    this.minerals$ = this.jobRun$.pipe(
      map((data: IJobRun) => data.xrdAlgorithmResult[0].mineralValues.map((mineral) => mineral.name))
    );

    this.pieChartData$ = combineLatest(this.jobRun$, this.selectedRow$).pipe(map(getPieChartData));

    this.scatterChartData$ = this.jobRun$.pipe(map((data: IJobRun) => getScatterChartData(data.xrdAlgorithmResult)));

    this.stackedBarChartData$ = this.jobRun$.pipe(
      map((data: IJobRun) => getStackedBarChartData(data.xrdAlgorithmResult))
    );
  }

  onSelectRow(selectedRow: IXrdAlgorithmResult) {
    if (selectedRow.depthFt === this.selectedDepth) {
      this.selectedDepth = null;
      this.selectedRow$.next(null);
    } else {
      this.selectedDepth = selectedRow.depthFt;
      this.selectedRow$.next(selectedRow);
    }
  }
}
