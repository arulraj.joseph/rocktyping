import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatIconModule, MatProgressSpinnerModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SharedModule } from '../../../shared/shared.module';
import { XrdSharedModule } from '../../shared/xrd-shared.module';
import { CompareRunComponent } from './compare-run/compare-run.component';
import { CompareRunsListComponent } from './compare-runs-list/compare-runs-list.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    XrdSharedModule,
    PerfectScrollbarModule,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule
  ],
  declarations: [CompareRunsListComponent, CompareRunComponent],
  exports: [CompareRunsListComponent]
})
export class CompareRunsModule {}
