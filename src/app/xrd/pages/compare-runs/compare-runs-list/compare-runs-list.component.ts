import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'xrd-compare-runs-list',
  templateUrl: './compare-runs-list.component.html',
  styleUrls: ['./compare-runs-list.component.scss']
})
export class CompareRunsListComponent implements OnInit {
  runsIds$: Observable<string[]> = this.route.queryParamMap.pipe(
    map((params) => JSON.parse(params.get('runId') || 'None'))
  );

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {}
}
