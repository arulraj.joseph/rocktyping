import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompareRunsListComponent } from './compare-runs-list.component';

describe('CompareRunsListComponent', () => {
  let component: CompareRunsListComponent;
  let fixture: ComponentFixture<CompareRunsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompareRunsListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompareRunsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
