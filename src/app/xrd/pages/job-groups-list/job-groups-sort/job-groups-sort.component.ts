import { Component, EventEmitter, Output, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material';
import { JobGroupsSortDialogComponent } from '../job-groups-sort-dialog/job-groups-sort-dialog.component';
import { IJobGroupsSort, IJobGroupsSortParameters } from '../../../core/models';

@Component({
  selector: 'xrd-job-groups-sort',
  templateUrl: './job-groups-sort.component.html',
  styleUrls: ['./job-groups-sort.component.scss']
})
export class JobGroupsSortComponent implements OnInit {
  @Input() currentJobGroupsSort: string;
  @Input() currentJobGroupsFilter: string;
  @Output() changeSort = new EventEmitter<IJobGroupsSortParameters>();
  @ViewChild('sortMenu', { read: ElementRef }) sortMenu: ElementRef;
  currentJobGroupsSortName: string;
  currentJobGroupsFilterName: string;
  jobGroupsSort: IJobGroupsSort[] = [
    {
      value: 'DESC',
      name: 'Recent'
    },
    {
      value: 'ASC',
      name: 'Oldest'
    }
  ];
  jobGroupsFilter: IJobGroupsSort[] = [
    {
      value: 'all',
      name: 'All'
    },
    {
      value: 'current/all',
      name: 'My Jobs'
    }
  ];

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.jobGroupsSort.forEach((item) => {
      if (item.value === this.currentJobGroupsSort) {
        this.currentJobGroupsSortName = item.name;
      }
    });

    this.jobGroupsFilter.forEach((item) => {
      if (item.value === this.currentJobGroupsFilter) {
        this.currentJobGroupsFilterName = item.name;
      }
    });
  }

  openDialog(currentChoice: string, parameters: IJobGroupsSort[]): void {
    const currentSortParameters = parameters;
    const rect = this.sortMenu.nativeElement.getBoundingClientRect();

    const dialogRef = this.dialog.open(JobGroupsSortDialogComponent, {
      data: { currentChoice, parameters },
      backdropClass: 'cdk-overlay-transparent-backdrop',
      panelClass: 'sort-pop-up',
      closeOnNavigation: true,
      position: { left: `${rect.left}px`, top: `${rect.bottom}px` },
      width: rect.width
    });

    dialogRef.afterClosed().subscribe((sortResult) => {
      currentSortParameters.forEach((item) => {
        if (sortResult === item.value) {
          if (currentSortParameters === this.jobGroupsSort) {
            this.currentJobGroupsSortName = item.name;
            this.changeSort.emit({ sort: sortResult });
          }

          if (currentSortParameters === this.jobGroupsFilter) {
            this.currentJobGroupsFilterName = item.name;
            this.changeSort.emit({ filter: sortResult });
          }
        }
      });
    });
  }
}
