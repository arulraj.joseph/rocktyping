/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { JobGroupsSortComponent } from './job-groups-sort.component';

describe('JobGroupsSortComponent', () => {
  let component: JobGroupsSortComponent;
  let fixture: ComponentFixture<JobGroupsSortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JobGroupsSortComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobGroupsSortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
