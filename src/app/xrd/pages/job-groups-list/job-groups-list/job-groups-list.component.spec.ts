import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobGroupsListComponent } from './job-groups-list.component';

describe('JobGroupsListComponent', () => {
  let component: JobGroupsListComponent;
  let fixture: ComponentFixture<JobGroupsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JobGroupsListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobGroupsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
