import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator } from '@angular/material';
import { of } from 'rxjs';
import { IJobGroup, IPagedResult, IJobGroupsSortParameters } from '../../../core/models';
import { JobService } from '../../../core/services/job/job.service';
import { RawDataDialogComponent } from '../../../shared/raw-data-dialog/raw-data-dialog.component';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'xrd-job-groups-list',
  templateUrl: './job-groups-list.component.html',
  styleUrls: ['./job-groups-list.component.scss']
})
export class JobGroupsListComponent implements OnInit {
  public readonly DefaultPageSize = 15;

  loading = true;
  showToolsForGroup: string;
  data: IPagedResult<IJobGroup>;
  jobGroupsSort: string = 'DESC';
  jobGroupsFilter: string = 'all';

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog, private jobService: JobService) {}

  ngOnInit() {
    this.loadData();

    this.paginator.page.subscribe(() => this.loadData());
  }

  public openRawData(e, data) {
    e.preventDefault();
    e.stopPropagation();
    const dialogRef = this.dialog.open(RawDataDialogComponent, {
      data
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  private loadData() {
    const pagingParams = {
      filter: this.jobGroupsFilter,
      pageNumber: (this.paginator && this.paginator.pageIndex) || 0,
      pageSize: (this.paginator && this.paginator.pageSize) || this.DefaultPageSize,
      sort: this.jobGroupsSort
    };

    this.loading = true;
    this.jobService
      .getAllJobGroups(pagingParams)
      .pipe(
        catchError(() => {
          return of({ count: 0, items: [] });
        })
      )
      .subscribe((data) => {
        this.data = data;
        this.loading = false;
      });
  }
  changeJobGroupName(name: string, jobGroup: IJobGroup) {
    const newJobGroupData = { ...jobGroup, name };
    this.jobService.editJobGroup(newJobGroupData).subscribe();
  }
  changeJobGroupDescription(description: string, jobGroup: IJobGroup) {
    const newJobGroupData = { ...jobGroup, description };
    this.jobService.editJobGroup(newJobGroupData).subscribe();
  }
  changeJobGroupsSort(event: IJobGroupsSortParameters) {
    if (!!event.sort) {
      this.jobGroupsSort = event.sort;
    }

    if (!!event.filter) {
      this.jobGroupsFilter = event.filter;
    }

    this.loadData();
  }
}
