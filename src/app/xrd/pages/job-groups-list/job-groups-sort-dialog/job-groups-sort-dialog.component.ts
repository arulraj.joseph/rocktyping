import { Component, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatRadioChange } from '@angular/material';
import { IJobGroupsSort } from '../../../core/models';

@Component({
  selector: 'xrd-job-groups-sort-dialog',
  templateUrl: './job-groups-sort-dialog.component.html',
  styleUrls: ['./job-groups-sort-dialog.component.scss']
})
export class JobGroupsSortDialogComponent {
  @Output() change: EventEmitter<MatRadioChange>;

  constructor(
    public dialogRef: MatDialogRef<JobGroupsSortDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { currentChoice: string; parameters: IJobGroupsSort[] }
  ) {}

  changeJobGroupsSort(event: MatRadioChange) {
    this.dialogRef.close(event.value);
  }
}
