import { Component, Input } from '@angular/core';
import { MatSlideToggleChange } from '@angular/material';
import { IJobRun } from '../../../core/models';
import { JobService } from '../../../core/services/job/job.service';

@Component({
  selector: 'xrd-run-public-field',
  templateUrl: './run-public-field.component.html',
  styleUrls: ['./run-public-field.component.scss']
})
export class RunPublicFieldComponent {
  _jobRun: IJobRun;
  @Input() showToggle = true;
  @Input() set jobRun(jobRun: IJobRun) {
    this._jobRun = jobRun;
  }
  constructor(private jobService: JobService) {}
  changeRunPrivacy(event: MatSlideToggleChange) {
    this._jobRun = { ...this._jobRun, public: event.checked };
    this.jobService.saveJobRun(this._jobRun).subscribe();
  }
}
