import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunPublicFieldComponent } from './run-public-field.component';

describe('RunPublicFieldComponent', () => {
  let component: RunPublicFieldComponent;
  let fixture: ComponentFixture<RunPublicFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RunPublicFieldComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunPublicFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
