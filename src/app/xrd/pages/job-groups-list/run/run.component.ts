import { Component, Input } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { IJobRun, IXrdAlgorithmResult } from '../../../core/models';
import { IPieChartData } from '../../../shared/pie-chart/pie-chart.component';
import { IScatterChartData } from '../../../shared/scatter-chart/scatter-chart.component';
import { IStackedBarChartData } from '../../../shared/stacked-bar-chart/stacked-bar-chart.component';
import { getPieChartData, getScatterChartData, getStackedBarChartData } from '../../../../utils/charts.helper';

enum ChartTypes {
  Pie,
  Scatter,
  StackedBar
}

@Component({
  selector: 'xrd-run',
  templateUrl: './run.component.html',
  styleUrls: ['./run.component.scss']
})
export class RunComponent {
  @Input() set jobRun(jobRun: IJobRun) {
    this.jobRun$.next(jobRun);
  }
  chartTypes = ChartTypes;
  selectedChart: ChartTypes = ChartTypes.Pie;

  selectedDepth: number;

  jobRun$: ReplaySubject<IJobRun> = new ReplaySubject(1);
  selectedRow$: BehaviorSubject<IXrdAlgorithmResult> = new BehaviorSubject(null);

  pieChartData$: Observable<{ title: string; data: IPieChartData[]; depth?: number }> = combineLatest(
    this.jobRun$,
    this.selectedRow$
  ).pipe(map(getPieChartData));

  scatterChartData$: Observable<IScatterChartData[]> = this.jobRun$.pipe(
    map((data: IJobRun) => getScatterChartData(data.xrdAlgorithmResult))
  );

  stackedBarChartData$: Observable<IStackedBarChartData[]> = this.jobRun$.pipe(
    map((data: IJobRun) => getStackedBarChartData(data.xrdAlgorithmResult))
  );

  onSelectRow(selectedRow: IXrdAlgorithmResult) {
    if (selectedRow.depthFt === this.selectedDepth) {
      this.selectedDepth = null;
      this.selectedRow$.next(null);
    } else {
      this.selectedDepth = selectedRow.depthFt;
      this.selectedRow$.next(selectedRow);
    }
  }
  showNextChart() {
    switch (this.selectedChart) {
      case ChartTypes.Pie:
        this.selectedChart = ChartTypes.Scatter;
        break;
      case ChartTypes.Scatter:
        this.selectedChart = ChartTypes.StackedBar;
        break;
      case ChartTypes.StackedBar:
        this.selectedChart = ChartTypes.Pie;
        break;
      default:
        this.selectedChart = ChartTypes.Pie;
    }
  }
}
