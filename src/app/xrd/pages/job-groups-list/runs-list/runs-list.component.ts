import { Component, Input, OnInit } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { publishReplay, refCount, switchMap } from 'rxjs/operators';
import { IJobRun } from '../../../core/models';
import { JobService } from '../../../core/services/job/job.service';
import { CurrentUserIdService } from '@apc-ng/core';

@Component({
  selector: 'xrd-runs-list',
  templateUrl: './runs-list.component.html',
  styleUrls: ['./runs-list.component.scss']
})
export class RunsListComponent implements OnInit {
  @Input() groupId: string;

  showToolsFroRun: string;
  currentUserId: string;

  private updateData$: ReplaySubject<void> = new ReplaySubject(1);
  public jobRunsList$: Observable<IJobRun[]> = this.updateData$.pipe(
    switchMap(() => this.jobService.getJobRuns(this.groupId)),
    publishReplay(1),
    refCount()
  );

  constructor(private currentUserService: CurrentUserIdService, private jobService: JobService) {}

  ngOnInit() {
    this.updateData$.next();

    this.currentUserService.getCurrentUserId().subscribe((currentUserId) => (this.currentUserId = currentUserId));
  }

  removeRun(runId) {
    this.jobService.removeJobRun(runId).subscribe(() => this.updateData$.next());
  }

  changeRunName(name: string, jobRun: IJobRun) {
    const newJobRunData = { ...jobRun, name };
    this.jobService.saveJobRun(newJobRunData).subscribe();
  }

  changeRunDescription(description: string, jobRun: IJobRun) {
    const newJobRunData = { ...jobRun, description };
    this.jobService.saveJobRun(newJobRunData).subscribe();
  }

  canEdit(run: IJobRun): boolean {
    return !!this.currentUserId && this.currentUserId === run.createdBy;
  }
}
