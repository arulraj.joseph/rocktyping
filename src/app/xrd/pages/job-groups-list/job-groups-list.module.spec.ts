import { JobGroupsListModule } from './job-groups-list.module';

describe('JobGroupsListModule', () => {
  let jobGroupsListModule: JobGroupsListModule;

  beforeEach(() => {
    jobGroupsListModule = new JobGroupsListModule();
  });

  it('should create an instance', () => {
    expect(jobGroupsListModule).toBeTruthy();
  });
});
