import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MatButtonModule,
  MatChipsModule,
  MatExpansionModule,
  MatIconModule,
  MatPaginatorModule,
  MatSlideToggleModule,
  MatTableModule,
  MatTooltipModule,
  MatRadioModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { MatDialogModule } from '@angular/material/dialog';
import { CompareModule } from '../../compare/compare.module';
import { SharedModule } from '../../../shared/shared.module';
import { XrdSharedModule } from '../../shared/xrd-shared.module';
import { JobGroupsListComponent } from './job-groups-list/job-groups-list.component';
import { RunComponent } from './run/run.component';
import { RunsListComponent } from './runs-list/runs-list.component';
import { RunPublicFieldComponent } from './run-public-field/run-public-field.component';
import { JobGroupsSortComponent } from './job-groups-sort/job-groups-sort.component';
import { JobGroupsSortDialogComponent } from './job-groups-sort-dialog/job-groups-sort-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatExpansionModule,
    MatChipsModule,
    MatButtonModule,
    MatPaginatorModule,
    FlexLayoutModule,
    MatTableModule,
    MatIconModule,
    MatTooltipModule,
    MatSlideToggleModule,
    MatRadioModule,
    SharedModule,
    XrdSharedModule,
    PerfectScrollbarModule,
    CompareModule,
    MatDialogModule
  ],
  declarations: [
    JobGroupsListComponent,
    RunsListComponent,
    RunComponent,
    RunPublicFieldComponent,
    JobGroupsSortComponent,
    JobGroupsSortDialogComponent
  ],
  entryComponents: [JobGroupsSortDialogComponent]
})
export class JobGroupsListModule {}
