import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { XrdSharedModule } from '../../shared/xrd-shared.module';
import { RunComponent } from './run/run.component';

@NgModule({
  imports: [CommonModule, XrdSharedModule, SharedModule],
  declarations: [RunComponent],
  exports: [RunComponent]
})
export class RunModule {}
