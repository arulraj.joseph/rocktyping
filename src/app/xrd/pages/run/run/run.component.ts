import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IJobRun } from '../../../core/models';
import { JobService } from '../../../core/services/job/job.service';

@Component({
  selector: 'xrd-run',
  templateUrl: './run.component.html',
  styleUrls: ['./run.component.scss']
})
export class RunComponent implements OnInit {
  jobRun$: Observable<IJobRun>;

  constructor(private jobService: JobService, private route: ActivatedRoute, public dialog: MatDialog) {}

  ngOnInit() {
    const runId = this.route.snapshot.paramMap.get('runId');
    this.jobRun$ = this.jobService.getJobRun(runId);
  }
}
