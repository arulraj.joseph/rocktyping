import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatBadgeModule, MatButtonModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SharedModule } from '../../shared/shared.module';
import { CompareBtnComponent } from './compare-btn/compare-btn.component';
import { NoRunsComponent } from './no-runs/no-runs.component';
import { RunsListDialogComponent } from './runs-list-dialog/runs-list-dialog.component';
import { RunsListItemComponent } from './runs-list-item/runs-list-item.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MatButtonModule,
    PerfectScrollbarModule,
    RouterModule,
    MatTooltipModule,
    MatBadgeModule,
    MatIconModule
  ],
  declarations: [CompareBtnComponent, RunsListDialogComponent, RunsListItemComponent, NoRunsComponent],
  exports: [CompareBtnComponent],
  entryComponents: [RunsListDialogComponent]
})
export class CompareModule {}
