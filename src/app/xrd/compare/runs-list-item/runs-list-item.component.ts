import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IJobRun } from '../../core/models';
import { JobService } from '../../core/services/job/job.service';
import { CompareService } from '../compare.service';

@Component({
  selector: 'xrd-runs-list-item',
  templateUrl: './runs-list-item.component.html',
  styleUrls: ['./runs-list-item.component.scss']
})
export class RunsListItemComponent implements OnInit {
  @Input() id: string;

  run$: Observable<IJobRun>;

  constructor(private jobService: JobService, public compareService: CompareService) {}

  ngOnInit(): void {
    this.run$ = this.jobService.getJobRun(this.id);
  }
}
