import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunsListItemComponent } from './runs-list-item.component';

describe('RunsListItemComponent', () => {
  let component: RunsListItemComponent;
  let fixture: ComponentFixture<RunsListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RunsListItemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
