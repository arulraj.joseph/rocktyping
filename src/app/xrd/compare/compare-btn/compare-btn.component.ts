import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { CompareService } from '../compare.service';
import { RunsListDialogComponent } from '../runs-list-dialog/runs-list-dialog.component';

@Component({
  selector: 'xrd-compare-btn',
  templateUrl: './compare-btn.component.html',
  styleUrls: ['./compare-btn.component.scss']
})
export class CompareBtnComponent implements OnInit {
  @ViewChild('button', { read: ElementRef }) button: ElementRef;

  constructor(public compareService: CompareService, public dialog: MatDialog) {}

  ngOnInit() {}

  openSelectedRunsList() {
    const matDialogConfig: MatDialogConfig = new MatDialogConfig();
    const rect = this.button.nativeElement.getBoundingClientRect();
    matDialogConfig.position = { left: `${rect.left}px`, top: `${rect.bottom}px` };
    matDialogConfig.backdropClass = 'cdk-overlay-transparent-backdrop';
    matDialogConfig.closeOnNavigation = true;
    this.dialog.open(RunsListDialogComponent, matDialogConfig);
  }
}
