import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompareBtnComponent } from './compare-btn.component';

describe('CompareBtnComponent', () => {
  let component: CompareBtnComponent;
  let fixture: ComponentFixture<CompareBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CompareBtnComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompareBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
