import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoRunsComponent } from './no-runs.component';

describe('NoRunsComponent', () => {
  let component: NoRunsComponent;
  let fixture: ComponentFixture<NoRunsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NoRunsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoRunsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
