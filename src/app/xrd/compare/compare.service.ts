import { Injectable } from '@angular/core';
import { NotificationService } from '../core/services/notification/notification.service';

@Injectable({
  providedIn: 'root'
})
export class CompareService {
  runsIds: string[] = [];
  constructor(private notificationService: NotificationService) {}

  addRun(runId: string) {
    if (this.runsIds.indexOf(runId) === -1) {
      this.runsIds.push(runId);
      this.notificationService.notify('Job Run successfully added to compare');
    } else {
      this.notificationService.notify('Job Run already added to compare');
    }
  }

  removeRun(runId) {
    if (this.runsIds.length) {
      this.runsIds = this.runsIds.filter((item) => item !== runId);
    }
  }
}
