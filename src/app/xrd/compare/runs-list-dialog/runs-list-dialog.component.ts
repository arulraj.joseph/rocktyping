import { Component } from '@angular/core';
import { CompareService } from '../compare.service';

@Component({
  selector: 'xrd-runs-list-dialog',
  templateUrl: './runs-list-dialog.component.html',
  styleUrls: ['./runs-list-dialog.component.scss']
})
export class RunsListDialogComponent {
  constructor(public compareService: CompareService) {}
  getRunsIds() {
    return JSON.stringify(this.compareService.runsIds);
  }
}
