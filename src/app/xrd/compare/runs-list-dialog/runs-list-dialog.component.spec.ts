import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunsListDialogComponent } from './runs-list-dialog.component';

describe('RunsListDialogComponent', () => {
  let component: RunsListDialogComponent;
  let fixture: ComponentFixture<RunsListDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RunsListDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunsListDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
