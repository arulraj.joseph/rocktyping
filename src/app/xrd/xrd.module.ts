import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CompareModule } from './compare/compare.module';
import { CompareRunsModule } from './pages/compare-runs/compare-runs.module';
import { CopyRunModule } from './pages/copy-run/copy-run.module';
import { CreateJobModule } from './pages/create-job/create-job.module';
import { CreateRunModule } from './pages/create-run/create-run.module';
import { JobGroupsListModule } from './pages/job-groups-list/job-groups-list.module';
import { LaunchRunModule } from './pages/launch-run/launch-run.module';
import { RunModule } from './pages/run/run.module';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CreateJobModule,
    JobGroupsListModule,
    CreateRunModule,
    LaunchRunModule,
    CompareRunsModule,
    CompareModule,
    CopyRunModule,
    RunModule
  ]
})
export class XrdModule {}
