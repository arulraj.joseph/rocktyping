import { Component, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
import { IMineralValue } from '../../core/models';
import { chartColors } from '../../../utils/charts.helper';

export interface IStackedBarChartData {
  key: string;
  minerals: IMineralValue[];
}

@Component({
  selector: 'xrd-stacked-bar-chart',
  templateUrl: './stacked-bar-chart.component.html',
  styleUrls: ['./stacked-bar-chart.component.scss']
})
export class StackedBarChartComponent {
  chart;

  Highcharts = Highcharts;
  chartConstructor = 'chart';
  chartOptions = {
    chart: {
      type: 'bar'
    },
    plotOptions: {
      series: {
        stacking: 'percent'
      }
    },
    xAxis: {
      categories: []
    },
    yAxis: {
      min: 0,
      title: {
        text: '<what do you want to see here?>'
      }
    },
    tooltip: {
      pointFormat: '<b>{series.name}</b><br/><b>{point.percentage:.2f}%</b>'
    },
    title: {
      text: null
    },
    series: [
      {
        data: null
      }
    ],
    colors: Object.keys(chartColors).map((x) => chartColors[x]),
    credits: {
      enabled: false
    }
  };
  updateFlag = false;
  oneToOneFlag = true;
  runOutsideAngularFlag = true;

  @Input()
  set title(title: string) {
    this.setChartTitle(title);
  }

  @Input()
  set data(data: IStackedBarChartData[]) {
    this.setChartOptions(data);
  }
  constructor() {}

  setChartOptions(data: IStackedBarChartData[]) {
    this.chartOptions = {
      ...this.chartOptions,
      series: this.getTransformedSeries(data),
      xAxis: {
        categories: this.getAllDepths(data)
      }
    };
  }

  setChartTitle(title: string) {
    this.chartOptions.title.text = title;
  }

  chartCallback = (chart) => (this.chart = chart); // optional function, defaults to null

  private getTransformedSeries(data: IStackedBarChartData[]) {
    if (!data || !data.length) {
      return [];
    }

    const transformedData: Array<{ name: string; data: number[] }> = [];

    data[0].minerals.forEach((mineral) =>
      transformedData.push({
        name: mineral.name,
        data: []
      })
    );

    data.forEach((row) => row.minerals.forEach((mineral, index) => transformedData[index].data.push(mineral.value)));

    return transformedData;
  }

  private getAllDepths(data: IStackedBarChartData[]): string[] {
    return data.map((x) => x.key);
  }
}
