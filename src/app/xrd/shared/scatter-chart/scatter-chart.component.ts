import { Component, Input } from '@angular/core';
import * as Highcharts from 'highcharts';

export interface IScatterChartData {
  name: string;
  color?: string;
  data: Array<[number, number]>;
}

const options = {
  chart: {
    type: 'scatter',
    zoomType: 'xy'
  },
  title: {
    text: ''
  },
  xAxis: {
    title: {
      text: 'XRD Grain Density'
    }
  },
  yAxis: {
    title: {
      text: 'GRI Grain Density'
    }
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'top',
    x: 0,
    y: 70,
    floating: false,
    backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
  },
  plotOptions: {
    scatter: {
      marker: {
        radius: 5,
        states: {
          hover: {
            enabled: true,
            lineColor: 'rgb(100,100,100)'
          }
        }
      },
      states: {
        hover: {
          marker: {
            enabled: false
          }
        }
      },
      tooltip: {
        headerFormat: '<b>{series.name}</b><br>',
        pointFormat: '{point.x:.4f}, {point.y:.4f}'
      }
    }
  },
  series: [],
  credits: {
    enabled: false
  }
};

@Component({
  selector: 'xrd-scatter-chart',
  templateUrl: './scatter-chart.component.html',
  styleUrls: ['./scatter-chart.component.scss']
})
export class ScatterChartComponent {
  chart;
  Highcharts = Highcharts;
  chartConstructor = 'chart';
  chartOptions = options;
  updateFlag = false;
  oneToOneFlag = true;
  runOutsideAngularFlag = true;

  @Input()
  set title(title: string) {
    this.setChartTitle(title);
  }

  @Input()
  set data(data) {
    this.setChartOptions(data);
  }

  constructor() {}

  setChartOptions(data: IScatterChartData[]) {
    this.chartOptions = {
      ...this.chartOptions,
      series: data
    };
  }

  setChartTitle(title: string) {
    this.chartOptions.title.text = title;
  }

  chartCallback = (chart) => (this.chart = chart);
}
