import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobRunToolsComponent } from './job-run-tools.component';

describe('JobRunToolsComponent', () => {
  let component: JobRunToolsComponent;
  let fixture: ComponentFixture<JobRunToolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JobRunToolsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobRunToolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
