import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmRemoveDialogComponent } from '../../../shared/components/confirm-remove-dialog/confirm-remove-dialog.component';
import { DownloadService } from '../../../shared/services/download.service';
import { CompareService } from '../../compare/compare.service';
import { IJobRun } from '../../core/models';
import { JobService } from '../../core/services/job/job.service';
import { NotificationService } from '../../core/services/notification/notification.service';
import { CurrentUserIdService } from '@apc-ng/core';

@Component({
  selector: 'xrd-job-run-tools',
  templateUrl: './job-run-tools.component.html',
  styleUrls: ['./job-run-tools.component.scss']
})
export class JobRunToolsComponent {
  @Input() viewAvailable: boolean;
  @Input() copyAvailable: boolean;
  @Input() deleteAvailable: boolean;
  @Input() compareAvailable: boolean;
  @Input() exportAvailable: boolean;

  @Input() run: IJobRun;
  @Output() remove = new EventEmitter<string>();

  currentUserId: string;

  constructor(
    private currentUserService: CurrentUserIdService,
    private compareService: CompareService,
    private notificationService: NotificationService,
    private jobService: JobService,
    public dialog: MatDialog,
    private downloadService: DownloadService
  ) {
    this.currentUserService.getCurrentUserId().subscribe((currentUserId) => (this.currentUserId = currentUserId));
  }

  addToCompare(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    if (this.run.status === 'COMPLETE') {
      this.compareService.addRun(this.run.id);
    } else {
      this.notificationService.notify('Only completed job runs can be compared');
    }
  }

  removeRun(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmRemoveDialogComponent, {
      data: { name: this.run.name }
    });

    dialogRef.afterClosed().subscribe((isConfirmed: boolean) => {
      if (isConfirmed) {
        this.remove.emit(this.run.id);
      }
    });
  }

  exportAlgorithmResult(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();
    const fileName = `${this.run.name}-algorithm-result.xlsx`;
    this.jobService.exportAlgorithmResult(this.run.id).subscribe((data) => {
      this.downloadService.downloadFile(data, fileName);
    });
  }

  canEdit(run: IJobRun): boolean {
    return !!this.currentUserId && this.currentUserId === run.createdBy;
  }
}
