import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunOutputComponent } from './run-output.component';

describe('RunOutputComponent', () => {
  let component: RunOutputComponent;
  let fixture: ComponentFixture<RunOutputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RunOutputComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
