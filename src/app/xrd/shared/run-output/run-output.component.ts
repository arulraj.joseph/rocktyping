import { Component, Input } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { BehaviorSubject, combineLatest, from, iif, Observable, ReplaySubject } from 'rxjs';
import { finalize, first, map, switchMap } from 'rxjs/operators';
import { IJobRun, IXrdAlgorithmResult } from '../../core/models';
import { JobService } from '../../core/services/job/job.service';
import { IPieChartData } from '../pie-chart/pie-chart.component';
import { RawDataDialogComponent } from '../raw-data-dialog/raw-data-dialog.component';
import { IScatterChartData } from '../scatter-chart/scatter-chart.component';
import { IStackedBarChartData } from '../stacked-bar-chart/stacked-bar-chart.component';
import { getPieChartData, getScatterChartData, getStackedBarChartData } from '../../../utils/charts.helper';

@Component({
  selector: 'xrd-run-output',
  templateUrl: './run-output.component.html',
  styleUrls: ['./run-output.component.scss']
})
export class RunOutputComponent {
  private readonly SnackbarDuration = 3000;

  @Input() set jobRun(jobRun: IJobRun) {
    this.jobRun$.next(jobRun);
  }
  selectedDepth: number;

  isOptimizing$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  isSaving$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  jobRun$: ReplaySubject<IJobRun> = new ReplaySubject(1);
  minerals$: Observable<string[]> = this.jobRun$.pipe(
    map((data: IJobRun) => data.xrdAlgorithmResult[0].mineralValues.map((mineral) => mineral.name))
  );
  selectedRow$: BehaviorSubject<IXrdAlgorithmResult> = new BehaviorSubject(null);
  scatterChartData$: Observable<IScatterChartData[]> = this.jobRun$.pipe(
    map((data: IJobRun) => getScatterChartData(data.xrdAlgorithmResult))
  );

  pieChartData$: Observable<{ title: string; data: IPieChartData[]; depth?: number }> = combineLatest(
    this.jobRun$,
    this.selectedRow$
  ).pipe(map(getPieChartData));

  stackedBarChartData$: Observable<IStackedBarChartData[]> = this.jobRun$.pipe(
    map((data: IJobRun) => getStackedBarChartData(data.xrdAlgorithmResult))
  );

  constructor(
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private jobService: JobService,
    private router: Router
  ) {}

  openRawData(): void {
    this.jobRun$
      .pipe(
        switchMap((jobRun) => this.jobService.getJobGroup(jobRun.jobGroupId)),
        map((jobGroup) => jobGroup.jobGroupFile)
      )
      .subscribe((file) => {
        const dialogRef = this.dialog.open(RawDataDialogComponent, {
          data: file.data
        });

        dialogRef.afterClosed().subscribe((result) => {
          console.log('The dialog was closed', result);
        });
      });
  }

  onSelectRow(selectedRow: IXrdAlgorithmResult) {
    if (selectedRow.depthFt === this.selectedDepth) {
      this.selectedDepth = null;
      this.selectedRow$.next(null);
    } else {
      this.selectedDepth = selectedRow.depthFt;
      this.selectedRow$.next(selectedRow);
    }
  }

  optimizeRun() {
    this.isOptimizing$.next(true);
    this.jobRun$
      .pipe(
        first(),
        switchMap((data) => this.jobService.optimizeJobRun(data))
      )
      .subscribe(
        (data) => {
          this.jobRun$.next(data);
          this.isOptimizing$.next(false);

          this.snackBar.open('Optimization completed', 'Ok', {
            duration: this.SnackbarDuration,
            panelClass: 'snackbar--success'
          });
        },
        () =>
          this.snackBar.open('Optimization failed', 'Ok', {
            duration: this.SnackbarDuration,
            panelClass: 'snackbar--error'
          })
      );
  }

  saveRun() {
    this.isSaving$.next(true);
    this.jobRun$
      .pipe(
        first(),
        switchMap((jobRun) =>
          iif(() => !!jobRun.id, this.jobService.saveJobRun(jobRun), this.jobService.createJobRun(jobRun))
        ),
        switchMap(() => from(this.router.navigateByUrl('/xrd/jobs-list'))),
        finalize(() => this.isSaving$.next(false))
      )
      .subscribe();
  }

  removeRun(runId) {
    this.jobService
      .removeJobRun(runId)
      .pipe(switchMap(() => from(this.router.navigateByUrl('/xrd/jobs-list'))))
      .subscribe();
  }

  canSave(jobRun: IJobRun): boolean {
    return !!jobRun && !jobRun.id;
  }
}
