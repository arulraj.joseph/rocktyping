import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DensitiesTableComponent } from './densities-table.component';

describe('DensitiesTableComponent', () => {
  let component: DensitiesTableComponent;
  let fixture: ComponentFixture<DensitiesTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DensitiesTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DensitiesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
