import { Component, Input } from '@angular/core';
import { IJobRun, IMineralValue } from '../../core/models';
import { isNumber } from 'util';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'xrd-densities-table',
  templateUrl: './densities-table.component.html',
  styleUrls: ['./densities-table.component.scss']
})
export class DensitiesTableComponent {
  displayedColumns: string[];
  densitiesData: Array<{ [key: string]: number | string }>;

  @Input() set data(data: IJobRun) {
    this.displayedColumns = this.getDisplayedColumns(data);
    this.densitiesData = [
      { Type: 'Starting Mineral Densities', ...data.startingDensities },
      { Type: 'Final Mineral Densities', ...data.finalDensities }
    ];
  }

  constructor(private numberFormatter: DecimalPipe) {}

  formatCell(value: any): string {
    if (value === 'NaN' || (typeof value === 'number' && isNaN(value))) {
      return '-';
    }

    return isNumber(value) ? this.numberFormatter.transform(value, '1.0-4') : value;
  }

  private getDisplayedColumns(data: IJobRun): string[] {
    const displayedColumns = data.xrdAlgorithmResult[0].mineralValues.map(
      (mineralData: IMineralValue) => mineralData.name
    );
    displayedColumns.unshift('Type');
    return displayedColumns;
  }
}
