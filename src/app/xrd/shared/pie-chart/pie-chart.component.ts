import { Component, Input } from '@angular/core';
import * as Highcharts from 'highcharts';
import { chartColors } from '../../../utils/charts.helper';

export interface IPieChartData {
  name: string;
  y: number;
}

@Component({
  selector: 'xrd-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent {
  chart;
  Highcharts = Highcharts;
  chartConstructor = 'chart';

  chartOptions = {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false
        },
        showInLegend: true
      }
    },
    tooltip: {
      pointFormat: '<b>{point.percentage:.2f}%</b> <br/> <b>{point.y:.4f}'
    },
    title: {
      text: null
    },
    series: [
      {
        data: null
      }
    ],
    colors: Object.keys(chartColors).map((x) => chartColors[x]),
    credits: {
      enabled: false
    }
  };

  updateFlag = false;
  oneToOneFlag = true;
  runOutsideAngularFlag = true;

  @Input()
  set title(title: string) {
    this.setChartTitle(title);
  }

  @Input()
  set data(data) {
    this.setChartOptions(data);
  }
  constructor() {}

  setChartOptions(data: IPieChartData[]) {
    const filteredData = data
      .filter((x) => x.y > 0)
      .map((x) => ({
        name: x.name,
        y: x.y,
        color: chartColors[x.name]
      }));

    this.chartOptions = {
      ...this.chartOptions,
      series: [
        {
          data: filteredData
        }
      ]
    };
  }
  setChartTitle(title: string) {
    this.chartOptions.title.text = title;
  }

  chartCallback = (chart) => (this.chart = chart);
}
