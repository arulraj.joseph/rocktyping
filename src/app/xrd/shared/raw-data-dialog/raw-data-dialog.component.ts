import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'xrd-raw-data-dialog',
  templateUrl: './raw-data-dialog.component.html',
  styleUrls: ['./raw-data-dialog.component.scss']
})
export class RawDataDialogComponent {
  displayedColumns: string[];

  constructor(
    public dialogRef: MatDialogRef<RawDataDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Array<{ [key: string]: string }>
  ) {
    this.displayedColumns = data && data.length > 0 ? Object.keys(data[0]) : undefined;
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }
}
