import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IJobGroup, IJobGroupFile, IJobRun, IMineralData } from '../../core/models';
import { JobService } from '../../core/services/job/job.service';
import { RawDataDialogComponent } from '../raw-data-dialog/raw-data-dialog.component';

interface IFormData {
  name: string;
  description: string;
  kerogenWt: number;
  kerogenMultiplier: number;
  isPublic: boolean;
  mineralsDensities: { [key: string]: IMineralsDensityFormData };
}

interface IMineralsDensityFormData {
  starting: string;
  lower: string;
  upper: string;
}

@Component({
  selector: 'xrd-run-input-form',
  templateUrl: './run-input-form.component.html',
  styleUrls: ['./run-input-form.component.scss']
})
export class RunInputFormComponent implements OnInit, OnDestroy {
  @Input() jobGroup: IJobGroup;
  @Input() jobRun: IJobRun;
  @Input() minerals: IMineralData[];

  initialRun: boolean;
  restoreDraft: boolean;
  public: boolean;

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  mineralsDensityFormGroup: FormGroup = new FormGroup({});
  runForm: FormGroup;

  constructor(
    public dialog: MatDialog,
    private fb: FormBuilder,
    private jobService: JobService,
    private router: Router,
    private location: Location,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const { name, description, kerogenWt, kerogenMultiplier, isPublic } = this.getInitialFormData();

    this.runForm = this.fb.group({
      name: [name, Validators.required],
      description: [description],
      kerogenWt: [kerogenWt, Validators.required],
      kerogenMultiplier: [kerogenMultiplier, Validators.required],
      isPublic: [isPublic],
      mineralsDensities: this.mineralsDensityFormGroup
    });

    this.setKerogenMultiplierControlValue();

    this.route.data.subscribe((data) => {
      this.initialRun = data && data.initial;
      this.public = data && data.public;
      this.restoreDraft = data && data.restoreDraft;
    });
  }

  private getInitialFormData(): Partial<IFormData> {
    if (this.jobRun) {
      return {
        name: this.jobRun.name,
        description: this.jobRun.description,
        kerogenWt: this.jobRun.kerogenWeightPercentage,
        kerogenMultiplier: this.computeKerogenMultiplierValue(this.jobRun.kerogenWeightPercentage),
        isPublic: this.jobRun.public
      };
    } else {
      return {
        name: '',
        description: '',
        kerogenWt: undefined,
        kerogenMultiplier: undefined,
        isPublic: false
      };
    }
  }

  private setKerogenMultiplierControlValue(): void {
    const kerogenWtControl = this.runForm.controls['kerogenWt'];
    const kerogenMultiplierControl = this.runForm.controls['kerogenMultiplier'];
    kerogenWtControl.valueChanges.pipe(takeUntil(this.destroyed$)).subscribe((value: string) => {
      kerogenMultiplierControl.setValue(this.computeKerogenMultiplierValue(parseFloat(value)));
    });
  }

  private computeKerogenMultiplierValue(kerogenWt: number): number {
    const computedValue = (1 / kerogenWt) * 100;
    return isNaN(computedValue) ? undefined : computedValue;
  }

  private getRunData(rawData: IFormData): Partial<IJobRun> {
    const { name, description, kerogenWt, isPublic, mineralsDensities } = rawData;
    const startingDensities = {};
    const lowerBounds = {};
    const upperBounds = {};

    Object.keys(mineralsDensities).forEach((key) => {
      startingDensities[key] = parseFloat(mineralsDensities[key].starting);
      lowerBounds[key] = parseFloat(mineralsDensities[key].lower);
      upperBounds[key] = parseFloat(mineralsDensities[key].upper);
    });

    return {
      name,
      description,
      startingDensities,
      lowerBounds,
      upperBounds,
      kerogenWeightPercentage: kerogenWt,
      public: isPublic,
      jobGroupId: this.jobGroup.id
    };
  }

  openRawData(file: IJobGroupFile): void {
    const dialogRef = this.dialog.open(RawDataDialogComponent, {
      data: file.data
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  onSubmit(): void {
    this.jobService.setNewRunData(this.getRunData(this.runForm.value));

    this.location.replaceState(`xrd/job/${this.jobGroup.id}/restore-draft${this.initialRun ? '/initial' : ''}`);
    this.router.navigateByUrl(`xrd/run/launch`).then();
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
