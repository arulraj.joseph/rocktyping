import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarbonByMaturityTableComponent } from './carbon-by-maturity-table.component';

describe('CarbonByMaturityTableComponent', () => {
  let component: CarbonByMaturityTableComponent;
  let fixture: ComponentFixture<CarbonByMaturityTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CarbonByMaturityTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarbonByMaturityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
