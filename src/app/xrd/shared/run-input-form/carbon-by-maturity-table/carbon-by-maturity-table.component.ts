import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ICarbonByMaturity } from '../../../core/models';
import { CarbonByMaturityService } from '../../../core/services/carbon-by-maturity/carbon-by-maturity.service';

@Component({
  selector: 'xrd-carbon-by-maturity-table',
  templateUrl: './carbon-by-maturity-table.component.html',
  styleUrls: ['./carbon-by-maturity-table.component.scss']
})
export class CarbonByMaturityTableComponent {
  data$: Observable<ICarbonByMaturity[]> = this.carbonByMaturityService.getData();
  constructor(public carbonByMaturityService: CarbonByMaturityService) {}
}
