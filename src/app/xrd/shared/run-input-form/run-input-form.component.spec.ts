import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunInputFormComponent } from './run-input-form.component';

describe('RunInputFormComponent', () => {
  let component: RunInputFormComponent;
  let fixture: ComponentFixture<RunInputFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RunInputFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunInputFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
