import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IMineralData } from '../../../core/models';
import { CrossFieldErrorMatcher } from '../../../../utils/errorStateMatcher';
import { mineralDensityValidator } from './minerals-density.validator';

@Component({
  selector: 'xrd-minerals-density-editor',
  templateUrl: './minerals-density-editor.component.html',
  styleUrls: ['./minerals-density-editor.component.scss']
})
export class MineralsDensityEditorComponent implements OnInit {
  @Input() minerals: IMineralData[];
  @Input() parentFormGroup: FormGroup;

  errorMatcher = {};

  constructor() {}

  ngOnInit() {
    this.minerals.forEach((item) => {
      const startingInitialValue = item.startingDensities ? item.startingDensities.toString() : '';
      const upperInitialValue = item.upperBounds ? item.upperBounds.toString() : '';
      const lowerInitialValue = item.lowerBounds ? item.lowerBounds.toString() : '';

      const formGroup = new FormGroup(
        {
          starting: new FormControl(startingInitialValue, [Validators.required, Validators.min(1)]),
          upper: new FormControl(upperInitialValue, [Validators.required, Validators.min(1)]),
          lower: new FormControl(lowerInitialValue, [Validators.required, Validators.min(1)])
        },
        { validators: mineralDensityValidator }
      );

      this.parentFormGroup.addControl(item.name, formGroup);

      this.errorMatcher[item.name] = new CrossFieldErrorMatcher(formGroup);
    });
  }
}
