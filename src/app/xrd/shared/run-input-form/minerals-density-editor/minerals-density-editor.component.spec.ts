import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MineralsDensityEditorComponent } from './minerals-density-editor.component';

describe('MineralsDensityEditorComponent', () => {
  let component: MineralsDensityEditorComponent;
  let fixture: ComponentFixture<MineralsDensityEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MineralsDensityEditorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MineralsDensityEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
