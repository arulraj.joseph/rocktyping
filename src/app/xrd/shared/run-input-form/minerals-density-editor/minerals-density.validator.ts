import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export const mineralDensityValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const starting = parseFloat(control.get('starting').value);
  const lower = parseFloat(control.get('lower').value);
  const upper = parseFloat(control.get('upper').value);

  const errors: ValidationErrors = {};
  if (starting < lower || starting > upper) {
    errors.starting = true;
  }

  if (lower > upper) {
    errors.lower = true;
    errors.upper = true;
  }

  return Object.keys(errors).length ? errors : null;
};
