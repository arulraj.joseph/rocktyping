import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatBadgeModule,
  MatButtonModule,
  MatChipsModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatTableModule,
  MatTooltipModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faFileExcel } from '@fortawesome/free-regular-svg-icons';
import { faBalanceScaleLeft } from '@fortawesome/free-solid-svg-icons';
import { HighchartsChartModule } from 'highcharts-angular';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SharedModule } from '../../shared/shared.module';
import { DensitiesTableComponent } from './densities-table/densities-table.component';
import { JobInputComponent } from './job-input/job-input.component';
import { JobRunToolsComponent } from './job-run-tools/job-run-tools.component';
import { MineralsPanelComponent } from './minerals-panel/minerals-panel.component';
import { MineralsTableComponent } from './minerals-table/minerals-table.component';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { RawDataDialogComponent } from './raw-data-dialog/raw-data-dialog.component';
import { CarbonByMaturityTableComponent } from './run-input-form/carbon-by-maturity-table/carbon-by-maturity-table.component';
import { MineralsDensityEditorComponent } from './run-input-form/minerals-density-editor/minerals-density-editor.component';
import { RunInputFormComponent } from './run-input-form/run-input-form.component';
import { RunOutputComponent } from './run-output/run-output.component';
import { ScatterChartComponent } from './scatter-chart/scatter-chart.component';
import { StackedBarChartComponent } from './stacked-bar-chart/stacked-bar-chart.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTableModule,
    HighchartsChartModule,
    MatButtonModule,
    PerfectScrollbarModule,
    MatChipsModule,
    MatIconModule,
    MatTooltipModule,
    MatBadgeModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatRadioModule,
    SharedModule
  ],
  declarations: [
    RawDataDialogComponent,
    MineralsTableComponent,
    DensitiesTableComponent,
    MineralsPanelComponent,
    JobRunToolsComponent,
    JobInputComponent,
    RunInputFormComponent,
    MineralsDensityEditorComponent,
    CarbonByMaturityTableComponent,
    RunOutputComponent,
    PieChartComponent,
    StackedBarChartComponent,
    ScatterChartComponent
  ],
  exports: [
    RawDataDialogComponent,
    MineralsTableComponent,
    DensitiesTableComponent,
    MineralsPanelComponent,
    JobRunToolsComponent,
    JobInputComponent,
    RunInputFormComponent,
    RunOutputComponent,
    PieChartComponent,
    StackedBarChartComponent,
    ScatterChartComponent
  ],
  entryComponents: [RawDataDialogComponent]
})
export class XrdSharedModule {
  constructor() {
    library.add(faFileExcel, faBalanceScaleLeft);
  }
}
