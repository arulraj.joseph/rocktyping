import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { IMineralValue, IXrdAlgorithmResult } from '../../core/models';
import { isNumber } from 'util';

@Component({
  selector: 'xrd-minerals-table',
  templateUrl: './minerals-table.component.html',
  styleUrls: ['./minerals-table.component.scss']
})
export class MineralsTableComponent {
  displayedColumns: string[];
  mappedData: Array<{ [key: string]: string }>;
  rawData: IXrdAlgorithmResult[];

  @Input()
  set data(data: IXrdAlgorithmResult[]) {
    this.rawData = data;
    this.mappedData = data.map((rawData) => this.mapMineralsData(rawData));
    this.displayedColumns = Object.keys(this.mappedData[0]);
  }

  @Output() selectRow = new EventEmitter<IXrdAlgorithmResult>();

  constructor(private numberFormatter: DecimalPipe) {}

  onRowClick(index: number) {
    this.selectRow.emit(this.rawData[index]);
  }

  mapMineralsData(data: IXrdAlgorithmResult): { [key: string]: string } {
    return data.mineralValues.reduce(
      (acc, mineralData: IMineralValue) => {
        acc[mineralData.name] = mineralData.value;
        return acc;
      },
      {
        Depth: `${data.depthFt} ft.`
      }
    );
  }

  formatCell(value: any): string {
    if (value === 'NaN' || (typeof value === 'number' && isNaN(value))) {
      return '-';
    }

    return isNumber(value) ? this.numberFormatter.transform(value, '1.0-4') : value;
  }
}
