import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MineralsTableComponent } from './minerals-table.component';

describe('MineralsTableComponent', () => {
  let component: MineralsTableComponent;
  let fixture: ComponentFixture<MineralsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MineralsTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MineralsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
