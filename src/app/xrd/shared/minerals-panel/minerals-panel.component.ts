import { Component, Input } from '@angular/core';

@Component({
  selector: 'xrd-minerals-panel',
  templateUrl: './minerals-panel.component.html',
  styleUrls: ['./minerals-panel.component.scss']
})
export class MineralsPanelComponent {
  visibleMinerals: string[];
  allMinerals: string[];
  @Input() set minerals(minerals: string[]) {
    this.visibleMinerals = minerals.slice(0, this.visibleItemsQuantity);
    this.allMinerals = minerals;
  }
  @Input() visibleItemsQuantity: number = 10;
  constructor() {}

  showAllMinerals() {
    this.visibleMinerals = this.allMinerals;
  }
}
