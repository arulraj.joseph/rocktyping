import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MineralsPanelComponent } from './minerals-panel.component';

describe('MineralsPanelComponent', () => {
  let component: MineralsPanelComponent;
  let fixture: ComponentFixture<MineralsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MineralsPanelComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MineralsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
