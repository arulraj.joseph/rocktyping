import { AuthGuard } from '../routing/auth.guard';
import { CompareRunsListComponent } from './pages/compare-runs/compare-runs-list/compare-runs-list.component';
import { CopyRunComponent } from './pages/copy-run/copy-run/copy-run.component';
import { CreateJobComponent } from './pages/create-job/create-job/create-job.component';
import { CreateRunComponent } from './pages/create-run/create-run/create-run.component';
import { JobGroupsListComponent } from './pages/job-groups-list/job-groups-list/job-groups-list.component';
import { LaunchRunComponent } from './pages/launch-run/launch-run/launch-run.component';
import { RunComponent } from './pages/run/run/run.component';
import { Routes } from '@angular/router';

export const XRD_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'jobs-list',
    pathMatch: 'full'
  },
  {
    path: 'jobs-list',
    component: JobGroupsListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'job',
    children: [
      {
        path: '',
        redirectTo: 'create',
        pathMatch: 'full'
      },
      {
        path: 'create',
        component: CreateJobComponent,
        canActivate: [AuthGuard]
      },
      {
        path: ':jobId/create-run/public',
        component: CreateRunComponent,
        canActivate: [AuthGuard],
        data: {
          public: true,
          initial: true
        }
      },
      {
        path: ':jobId/create-run/private',
        component: CreateRunComponent,
        canActivate: [AuthGuard],
        data: {
          public: false,
          initial: true
        }
      },
      {
        path: ':jobId/restore-draft/initial',
        component: CreateRunComponent,
        canActivate: [AuthGuard],
        data: {
          restoreDraft: true,
          initial: true
        }
      },
      {
        path: ':jobId/restore-draft',
        component: CreateRunComponent,
        canActivate: [AuthGuard],
        data: {
          restoreDraft: true
        }
      },
      {
        path: ':jobId/create-run',
        component: CreateRunComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'run',
    children: [
      {
        path: 'compare',
        component: CompareRunsListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: ':runId/copy',
        component: CopyRunComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'launch',
        component: LaunchRunComponent,
        canActivate: [AuthGuard]
      },
      {
        path: ':runId',
        component: RunComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];
