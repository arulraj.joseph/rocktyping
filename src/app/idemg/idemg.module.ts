import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatProgressBarModule,
  MatRadioModule,
  MatTabsModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { Ng5SliderModule } from 'ng5-slider';
import { FileDropModule } from 'ngx-file-drop';
import { TimeAgoPipe } from 'time-ago-pipe';
import { XrdSharedModule as SharedModule } from '../xrd/shared/xrd-shared.module';
import { CalculateComponent } from './pages/calculate-modal/calculate.component';
import { UpdateValuesComponent } from './pages/update-values-modal/update-values.component';
import { ResultsComponent } from './pages/results/results.component';
import { RangeSliderComponent } from './pages/range-slider/range-slider.component';
import { UploadFileComponent } from './pages/upload-file/upload-file.component';
import { UploadProgressComponent } from './pages/upload-progress/upload-progress.component';
import { UploadComponent } from './pages/upload/upload.component';
import { LineChartComponent } from './pages/line-chart/line-chart.component';
import { SharedModule as AppSharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FileDropModule,
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    MatTabsModule,
    MatMenuModule,
    MatDialogModule,
    MatRadioModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    AppSharedModule,
    SharedModule,
    Ng5SliderModule,
    FormsModule
  ],
  declarations: [
    UploadComponent,
    ResultsComponent,
    UploadFileComponent,
    UploadProgressComponent,
    CalculateComponent,
    UpdateValuesComponent,
    RangeSliderComponent,
    LineChartComponent,
    TimeAgoPipe
  ],
  entryComponents: [CalculateComponent, UpdateValuesComponent]
})
export class IdemgModule {}
