import { Routes } from '@angular/router';
import { ResultsComponent } from './pages/results/results.component';
import { UploadComponent } from './pages/upload/upload.component';

export const routes: Routes = [
  {
    path: 'idemg',
    children: [{ path: '', component: UploadComponent }, { path: 'results/:id', component: ResultsComponent }]
  }
];
