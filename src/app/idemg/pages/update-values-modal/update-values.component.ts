import { AfterViewInit, Component, EventEmitter, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'idemg-update-values',
  templateUrl: './update-values.component.html',
  styleUrls: ['./update-values.component.scss']
})
export class UpdateValuesComponent implements AfterViewInit {
  public onLoad = new EventEmitter<any>();

  constructor(public dialogRef: MatDialogRef<UpdateValuesComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}

  close(reason?) {
    this.dialogRef.close(reason);
  }

  ngAfterViewInit() {
    // after view init, delay and make the slider input re-calculate
    setTimeout(() => this.onLoad.emit(), 300);
  }
}
