import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'idemg-calculate',
  templateUrl: './calculate.component.html',
  styleUrls: ['./calculate.component.scss']
})
export class CalculateComponent {
  public value: any = {};

  constructor(public dialogRef: MatDialogRef<CalculateComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}

  close(reason?) {
    this.dialogRef.close(reason);
  }
}
