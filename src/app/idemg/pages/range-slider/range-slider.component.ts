import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Options } from 'ng5-slider';

@Component({
  selector: 'idemg-range-slider',
  templateUrl: './range-slider.component.html',
  styleUrls: ['./range-slider.component.scss']
})
export class RangeSliderComponent implements OnInit {
  @Input() min: number = 0;
  @Input() max: number = 0;
  @Input() minValue: number = 0;
  @Input() maxValue: number = 100;
  @Input() manualRefresh: EventEmitter<any>;

  public options: Options = {};

  @Output() minValueChange = new EventEmitter<any>();
  @Output() maxValueChange = new EventEmitter<any>();

  ngOnInit() {
    const ticksArray = [];
    const no = (this.max - this.min) / 10;

    for (let i = 0; i <= 10; i++) {
      ticksArray.push(this.min + no * i);
    }

    this.options = {
      animate: false,
      floor: this.min,
      ceil: this.max,
      step: 0.1,
      showTicks: true,
      ticksArray
    };
  }

  changeMin() {
    this.minValueChange.emit(this.minValue);
  }

  changeMax() {
    this.maxValueChange.emit(this.maxValue);
  }
}
