import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import * as d3 from 'd3';

const rotateTicks = (axis, rotate = -90, dyOffset = -8) => {
  axis
    .selectAll('text')
    .attr('y', 0)
    .attr('x', 9)
    .attr('dy', `${dyOffset}px`)
    .attr('transform', `rotate(${rotate})`)
    .style('text-anchor', 'start');
};

const classifyTicks = (axis, direction = 1, majorTickSize = 15) => {
  const ticks = axis.selectAll('.tick');

  ticks.each((d, i) => {
    const tick = d3.select(ticks.nodes()[i]);

    if (!i) {
      tick.select('text').remove();
      return;
    }

    const isMajor = !(i % 5);

    tick.classed('major', isMajor);

    if (!isMajor) {
      tick.select('line').attr('x2', direction * -5);
      tick.select('text').remove();
    } else {
      tick
        .select('line')
        .attr('x1', direction * -15)
        .attr('x2', direction * -15 - majorTickSize);
    }
  });
};

const limit = (scale, value) => {
  const [min, max] = scale.domain();
  return Math.max(min, Math.min(max, value));
};

@Component({
  selector: 'idemg-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnChanges, OnInit, AfterViewInit {
  @Input() data: any = {};
  @ViewChild('svgChart') svgElRef: ElementRef;

  @Output() legendClick = new EventEmitter<any>();

  get svgEl() {
    return this.svgElRef.nativeElement;
  }

  private margins = {
    top: 32,
    left: 21,
    right: 21
  };

  private d3svg; // main drawing area
  private xScale;
  private yScale;
  // chart axis
  private xAxis;
  private yAxis;
  private yAxisRight;

  private line = (xScale, yScale) =>
    d3
      .line()
      .x((d: any) => xScale(limit(xScale, d.value)))
      .y((d: any) => yScale(d.ref))
      .curve(d3.curveBasis); // apply smoothing to the line

  ngOnInit() {
    if (!this.d3svg) {
      this.drawChart();
    }
  }

  ngAfterViewInit() {
    // if parent container needs scrollbar, there won't be any resize event,
    // but we need to re-calculate styles. do it after 50ms to correct this behavior
    setTimeout(this.resizeChart.bind(this), 50);
  }

  /**
   * on changes
   */
  ngOnChanges() {
    this.drawChart();
  }

  initChart() {
    this.xScale = d3.scaleLinear();
    this.yScale = d3.scaleLinear();
    this.d3svg = d3
      .select(this.svgEl)
      .append('g')
      .attr('transform', 'translate(' + this.margins.left + ',' + this.margins.top + ')');

    this.xAxis = this.d3svg.append('g').attr('class', 'axis x-axis');
    console.log(this.xAxis);
    this.yAxis = this.d3svg.append('g').attr('class', 'axis y-axis');
    this.yAxisRight = this.d3svg.append('g').attr('class', 'axis y-axis right-axis');

    this.resizeChart();
  }

  /**
   * handles the resize chart
   */
  resizeChart() {
    const { width, height } = this.svgEl.getBoundingClientRect();

    this.xScale.range([0, width - this.margins.left - this.margins.right]);
    this.yScale.range([0, height - this.margins.top]);

    this.yAxisRight.attr('transform', `translate(${width - this.margins.left - this.margins.right}, 0)`);

    this.drawChart();
  }

  /**
   * renders a new svg chart in the specified div using the data passed in
   */
  drawChart() {
    if (!this.d3svg) {
      this.initChart();
    }

    if (!this.data.visible) {
      return;
    }

    const { data, meta } = this.data;
    const width = this.xScale.range()[1];
    if (meta.yRange) {
      this.yScale.domain([meta.yRange.min, meta.yRange.max]);
      this.yAxis.call(
        d3
          .axisLeft(this.yScale)
          .ticks(60)
          .tickSizeOuter(this.margins.left)
      );
      rotateTicks(this.yAxis);
      classifyTicks(this.yAxis, 1, -width - 15);
      this.yAxisRight.call(
        d3
          .axisRight(this.yScale)
          .ticks(60)
          .tickSizeOuter(this.margins.right)
      );
      rotateTicks(this.yAxisRight, -90, 15);
      classifyTicks(this.yAxisRight, -1);
      this.d3svg.selectAll('.sub-chart').remove();
      // charts count
      const count = this.data.visible.length;
      this.data.visible.forEach((name, i) => {
        const range = [(width / count) * i, (width / count) * (i + 1)];
        this.drawSubChart(data[name], meta[name], range);
      });
    }
  }

  drawSubChart(data, meta, xRange) {
    const svg = this.d3svg
      .append('g')
      .attr('class', 'sub-chart')
      .attr('style', 'min-width:200px');
    const dataset = data.sort((a, b) => a.ref - b.ref);

    const xScale = d3
      .scaleLinear()
      .range(xRange)
      .domain(meta.value ? [meta.value.min, meta.value.max] : d3.extent(dataset, (d: any) => d.value));

    const yRange = this.yScale.range();
    const yScale = d3
      .scaleLinear()
      .range(yRange)
      .domain(d3.extent(dataset, (d: any) => d.ref) as any);

    svg
      .append('g')
      .attr('class', 'grid y-gridlines')
      .call(
        d3
          .axisTop(xScale)
          .tickSize(-yRange[1])
          .tickFormat('' as any)
          .ticks(5)
      );

    // 9. Append the path, bind the data, and call the line generator
    svg
      .append('path')
      .datum(dataset) // 10. Binds data to the line
      .attr('class', 'line') // Assign a class for styling
      .attr('d', this.line(xScale, yScale)); // 11. Calls the line generator
  }

  /**
   * handles the windows resize
   * @param event the event
   */
  @HostListener('window:resize', ['$event']) onResize(event) {
    this.resizeChart();
  }
}
