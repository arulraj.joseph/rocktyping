import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver';
import { UpdateValuesComponent } from '../update-values-modal/update-values.component';
import { TemplatesService } from '../../../core/services/templates/templates.service';

@Component({
  selector: 'idemg-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {
  public filters: any = {};
  fileId = '';
  isLoading = false;

  chart = {
    visible: [],
    meta: {},
    data: {}
  };

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private templatesService: TemplatesService) {}

  ngOnInit() {
    this.fileId = this.route.snapshot.paramMap.get('id');
    this.getData();
  }

  getData() {
    // GET: Get BRP data according param id
    this.templatesService.readFileData(this.fileId).subscribe((res) => {
      const { input, output } = res;
      const meta = {
        yRange: {
          min: input[0]['depth'],
          max: input[1]['depth']
        },
        Vc: {
          value: {
            min: output[0]['Vc'] > output[1]['Vc'] ? output[1]['Vc'] : output[0]['Vc'],
            max: output[1]['Vc'] > output[0]['Vc'] ? output[1]['Vc'] : output[0]['Vc']
          },
          min: output[0]['Vc'] > output[1]['Vc'] ? output[1]['Vc'] : output[0]['Vc'],
          max: output[1]['Vc'] > output[0]['Vc'] ? output[1]['Vc'] : output[0]['Vc'],
          label: 'Vc',
          filterName: 'Vc'
        },
        Vs: {
          value: {
            min: output[0]['Vs'] > output[1]['Vs'] ? output[1]['Vs'] : output[0]['Vs'],
            max: output[1]['Vs'] > output[0]['Vs'] ? output[1]['Vs'] : output[0]['Vs']
          },
          min: output[0]['Vs'] > output[1]['Vs'] ? output[1]['Vs'] : output[0]['Vs'],
          max: output[1]['Vs'] > output[0]['Vs'] ? output[1]['Vs'] : output[0]['Vs'],
          label: 'Vs',
          filterName: 'Vs'
        },
        E: {
          value: {
            min: output[0]['E'] > output[1]['E'] ? output[1]['E'] : output[0]['E'],
            max: output[1]['E'] > output[0]['E'] ? output[1]['E'] : output[0]['E']
          },
          min: output[0]['E'] > output[1]['E'] ? output[1]['E'] : output[0]['E'],
          max: output[1]['E'] > output[0]['E'] ? output[1]['E'] : output[0]['E'],
          label: 'E',
          filterName: 'E'
        },
        u: {
          value: {
            min: output[0]['u'] > output[1]['u'] ? output[1]['u'] : output[0]['u'],
            max: output[1]['u'] > output[0]['u'] ? output[1]['u'] : output[0]['u']
          },
          min: output[0]['u'] > output[1]['u'] ? output[1]['u'] : output[0]['u'],
          max: output[1]['u'] > output[0]['u'] ? output[1]['u'] : output[0]['u'],
          label: 'u',
          filterName: 'u'
        },
        K: {
          value: {
            min: output[0]['K'] > output[1]['K'] ? output[1]['K'] : output[0]['K'],
            max: output[1]['K'] > output[0]['K'] ? output[1]['K'] : output[0]['K']
          },
          min: output[0]['K'] > output[1]['K'] ? output[1]['K'] : output[0]['K'],
          max: output[1]['K'] > output[0]['K'] ? output[1]['K'] : output[0]['K'],
          label: 'K',
          filterName: 'K'
        },
        PR: {
          value: {
            min: output[0]['PR'] > output[1]['PR'] ? output[1]['PR'] : output[0]['PR'],
            max: output[1]['PR'] > output[0]['PR'] ? output[1]['PR'] : output[0]['PR']
          },
          min: output[0]['PR'] > output[1]['PR'] ? output[1]['PR'] : output[0]['PR'],
          max: output[1]['PR'] > output[0]['PR'] ? output[1]['PR'] : output[0]['PR'],
          label: 'PR',
          filterName: 'PR'
        },
        'P-wave': {
          value: {
            min: output[0]['P-wave'] > output[1]['P-wave'] ? output[1]['P-wave'] : output[0]['P-wave'],
            max: output[1]['P-wave'] > output[0]['P-wave'] ? output[1]['P-wave'] : output[0]['P-wave']
          },
          min: output[0]['P-wave'] > output[1]['P-wave'] ? output[1]['P-wave'] : output[0]['P-wave'],
          max: output[1]['P-wave'] > output[0]['P-wave'] ? output[1]['P-wave'] : output[0]['P-wave'],
          label: 'P-wave',
          filterName: 'Pwave'
        },
        Lambda: {
          value: {
            min: output[0]['Lambda'] > output[1]['Lambda'] ? output[1]['Lambda'] : output[0]['Lambda'],
            max: output[1]['Lambda'] > output[0]['Lambda'] ? output[1]['Lambda'] : output[0]['Lambda']
          },
          min: output[0]['Lambda'] > output[1]['Lambda'] ? output[1]['Lambda'] : output[0]['Lambda'],
          max: output[1]['Lambda'] > output[0]['Lambda'] ? output[1]['Lambda'] : output[0]['Lambda'],
          label: 'Lambda',
          filterName: 'Lambda'
        }
      };
      const data = {
        Vc: [
          { ref: input[0]['depth'], value: output[0]['Vc'] > output[1]['Vc'] ? output[1]['Vc'] : output[0]['Vc'] },
          { ref: input[1]['depth'], value: output[1]['Vc'] > output[0]['Vc'] ? output[1]['Vc'] : output[0]['Vc'] }
        ],
        Vs: [
          { ref: input[0]['depth'], value: output[0]['Vs'] > output[1]['Vs'] ? output[1]['Vs'] : output[0]['Vs'] },
          { ref: input[1]['depth'], value: output[1]['Vs'] > output[0]['Vs'] ? output[1]['Vs'] : output[0]['Vs'] }
        ],
        E: [
          { ref: input[0]['depth'], value: output[0]['E'] > output[1]['E'] ? output[1]['E'] : output[0]['E'] },
          { ref: input[1]['depth'], value: output[1]['E'] > output[0]['E'] ? output[1]['E'] : output[0]['E'] }
        ],
        u: [
          { ref: input[0]['depth'], value: output[0]['u'] > output[1]['u'] ? output[1]['u'] : output[0]['u'] },
          { ref: input[1]['depth'], value: output[1]['u'] > output[0]['u'] ? output[1]['u'] : output[0]['u'] }
        ],
        K: [
          { ref: input[0]['depth'], value: output[0]['K'] > output[1]['K'] ? output[1]['K'] : output[0]['K'] },
          { ref: input[1]['depth'], value: output[1]['K'] > output[0]['K'] ? output[1]['K'] : output[0]['K'] }
        ],
        PR: [
          { ref: input[0]['depth'], value: output[0]['PR'] > output[1]['PR'] ? output[1]['PR'] : output[0]['PR'] },
          { ref: input[1]['depth'], value: output[1]['PR'] > output[0]['PR'] ? output[1]['PR'] : output[0]['PR'] }
        ],
        'P-wave': [
          {
            ref: input[0]['depth'],
            value: output[0]['P-wave'] > output[1]['P-wave'] ? output[1]['P-wave'] : output[0]['P-wave']
          },
          {
            ref: input[1]['depth'],
            value: output[1]['P-wave'] > output[0]['P-wave'] ? output[1]['P-wave'] : output[0]['P-wave']
          }
        ],
        Lambda: [
          {
            ref: input[0]['depth'],
            value: output[0]['Lambda'] > output[1]['Lambda'] ? output[1]['Lambda'] : output[0]['Lambda']
          },
          {
            ref: input[1]['depth'],
            value: output[1]['Lambda'] > output[0]['Lambda'] ? output[1]['Lambda'] : output[0]['Lambda']
          }
        ]
      };
      this.chart = { visible: [], meta, data };
      this.filters = this.templatesService.getFilters();
      this.onFiltersUpdate();
    });
  }

  // export function
  export2LAS() {
    this.templatesService.export2LAS(this.fileId).subscribe((res) => {
      // save res to las file
      const blob = new Blob([res], { type: 'text/plain;charset=utf-8' });
      saveAs(blob, 'export.las');
    });
  }

  openUpdateValuesDialog(chart) {
    const meta = this.chart.meta[chart];

    const dialogRef = this.dialog.open(UpdateValuesComponent, {
      width: '360px',
      data: {
        name: meta.label,
        minValue: meta.value.min,
        maxValue: meta.value.max,
        min: meta.min || 0,
        max: meta.max
      }
    });

    dialogRef.afterClosed().subscribe((data) => {
      if (!data) {
        return;
      }

      const { minValue, maxValue } = data;
      // use data to update charts
      meta.value.min = minValue;
      meta.value.max = maxValue;

      // update min/max for the selected chart
      this.chart = { ...this.chart };
    });
  }

  onFiltersUpdate() {
    const { meta, data } = this.chart;
    const visible = Object.keys(data).filter(
      (chartName) => !meta[chartName].filterName || this.filters[meta[chartName].filterName]
    );

    this.chart = { ...this.chart, visible };
  }
}
