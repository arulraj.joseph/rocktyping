import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { CalculateComponent } from '../calculate-modal/calculate.component';
import { UpdateValuesComponent } from '../update-values-modal/update-values.component';
import { TemplatesService } from '../../../core/services/templates/templates.service';

@Component({
  selector: 'idemg-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  public selected: {};
  public uploading = [];
  public files = [];
  public isLoading = false;
  chart = {
    // bulkDensity, DTC, DTS
    visible: [],
    meta: {},
    data: {}
  };

  constructor(public dialog: MatDialog, private templatesService: TemplatesService, private router: Router) {}

  ngOnInit() {
    this.getFiles();
  }

  // get files
  public getFiles() {
    this.templatesService
      .getFiles()
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((d) => (this.files = d.items));
  }

  // upload file
  public upload(file) {
    const fileUploadProgress = { progress: 0 };
    this.templatesService
      .uploadFile(file)
      .pipe(
        finalize(() => {
          this.getFiles();
        })
      )

      .subscribe((res) => {
        this.templatesService.uploadFileTimer().subscribe(
          (progress) => {
            // show upload progress
            fileUploadProgress.progress = progress;
          },
          () => {},
          () => {
            // when upload is ready, store the uploaded file and list in sidebar
            this.uploading = this.uploading.filter((f) => f !== fileUploadProgress);
          }
        );
        this.uploading = this.uploading.concat(fileUploadProgress);
      });
  }

  // toggle reject
  public toggleReject(file, e) {
    event.stopPropagation();
    this.isLoading = true;
    const status = file.status === 'Uploaded' ? 'Rejected' : 'Uploaded';
    // file.rejected = !file.rejected;
    this.templatesService
      .toggleReject(file.id, { ...file, status })
      .pipe(
        finalize(() => {
          this.getFiles();
          this.isLoading = false;
        })
      )
      .subscribe((res) => {
        if (e.target.innerText === 'Reject/Re-Import Data') {
          this.selected = res;
        }
      });
  }

  openCalculateDialog(file) {
    const dialogRef = this.dialog.open(CalculateComponent, {
      width: '380px',
      data: this.selected
    });

    dialogRef.afterClosed().subscribe((data) => {
      if (!data) {
        return;
      }

      this.templatesService.applyFilters(data);
      this.router.navigate(['/idemg/results/' + file.id]);
    });
  }

  openUpdateValuesDialog(chart) {
    const meta = this.chart.meta[chart];

    const dialogRef = this.dialog.open(UpdateValuesComponent, {
      width: '360px',
      data: {
        name: meta.label,
        minValue: meta.value.min,
        maxValue: meta.value.max,
        min: meta.min || 0,
        max: meta.max
      }
    });

    dialogRef.afterClosed().subscribe((data) => {
      if (!data) {
        return;
      }

      const { minValue, maxValue } = data;
      // use data to update charts
      meta.value.min = minValue;
      meta.value.max = maxValue;

      // update min/max for the selected chart
      this.chart = { ...this.chart };
    });
  }

  public select(file) {
    this.isLoading = true;
    this.templatesService
      .readFileData(file.id)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((res) => {
        const { input } = res;
        // Structure the data
        const meta = {
          yRange: {
            min: input[0]['depth'],
            max: input[1]['depth']
          },
          bulkDensity: {
            value: {
              min:
                input[0]['bulkDensity'] > input[1]['bulkDensity'] ? input[1]['bulkDensity'] : input[0]['bulkDensity'],
              max: input[1]['bulkDensity'] > input[0]['bulkDensity'] ? input[1]['bulkDensity'] : input[0]['bulkDensity']
            },
            min: input[0]['bulkDensity'] > input[1]['bulkDensity'] ? input[1]['bulkDensity'] : input[0]['bulkDensity'],
            max: input[1]['bulkDensity'] > input[0]['bulkDensity'] ? input[1]['bulkDensity'] : input[0]['bulkDensity'],
            label: 'bulkDensity'
          },
          DTC: {
            value: {
              min: input[0]['DTC'] > input[1]['DTC'] ? input[1]['DTC'] : input[0]['DTC'],
              max: input[1]['DTC'] > input[0]['DTC'] ? input[1]['DTC'] : input[0]['DTC']
            },
            min: input[0]['DTC'] > input[1]['DTC'] ? input[1]['DTC'] : input[0]['DTC'],
            max: input[1]['DTC'] > input[0]['DTC'] ? input[1]['DTC'] : input[0]['DTC'],
            label: 'DTC'
          },
          DTS: {
            value: {
              min: input[0]['DTS'] > input[1]['DTS'] ? input[1]['DTS'] : input[0]['DTS'],
              max: input[1]['DTS'] > input[0]['DTS'] ? input[1]['DTS'] : input[0]['DTS']
            },
            min: input[0]['DTS'] > input[1]['DTS'] ? input[1]['DTS'] : input[0]['DTS'],
            max: input[1]['DTS'] > input[0]['DTS'] ? input[1]['DTS'] : input[0]['DTS'],
            label: 'DTS'
          }
        };

        const visible = ['bulkDensity', 'DTC', 'DTS'];
        const data = {
          bulkDensity: [
            {
              ref: input[0]['depth'],
              value:
                input[0]['bulkDensity'] > input[1]['bulkDensity'] ? input[1]['bulkDensity'] : input[0]['bulkDensity']
            },
            {
              ref: input[1]['depth'],
              value:
                input[1]['bulkDensity'] > input[0]['bulkDensity'] ? input[1]['bulkDensity'] : input[0]['bulkDensity']
            }
          ],
          DTC: [
            { ref: input[0]['depth'], value: input[0]['DTC'] > input[1]['DTC'] ? input[1]['DTC'] : input[0]['DTC'] },
            { ref: input[1]['depth'], value: input[1]['DTC'] > input[0]['DTC'] ? input[1]['DTC'] : input[0]['DTC'] }
          ],
          DTS: [
            { ref: input[0]['depth'], value: input[0]['DTS'] > input[1]['DTS'] ? input[1]['DTS'] : input[0]['DTS'] },
            { ref: input[1]['depth'], value: input[1]['DTS'] > input[0]['DTS'] ? input[1]['DTS'] : input[0]['DTS'] }
          ]
        };
        this.chart = { visible, meta, data };
        this.selected = file;
      });
  }
}
