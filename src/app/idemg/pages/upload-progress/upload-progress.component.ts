import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'idemg-upload-progress',
  templateUrl: './upload-progress.component.html',
  styleUrls: ['./upload-progress.component.scss']
})
export class UploadProgressComponent implements OnInit {
  @Input() progress = 0;

  constructor() {}
  ngOnInit() {}
}
