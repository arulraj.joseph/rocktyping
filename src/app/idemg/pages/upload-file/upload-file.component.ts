import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FileSystemFileEntry, UploadEvent, UploadFile } from 'ngx-file-drop';

@Component({
  selector: 'idemg-idemg-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent {
  @Input() full = false;
  @Output() fileUploaded = new EventEmitter<File>();

  public dropped(event: UploadEvent) {
    event.files
      .filter((file: UploadFile) => file.fileEntry.isFile && file.fileEntry.name.match(/.las$/i))
      .forEach((file: UploadFile) => {
        const fileEntry = file.fileEntry as FileSystemFileEntry;
        fileEntry.file((dataFile: File) => this.fileUploaded.emit(dataFile));
      });
  }
}
