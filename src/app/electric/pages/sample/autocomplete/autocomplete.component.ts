import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { forkJoin, of } from 'rxjs';
import 'rxjs/add/operator/catch';
import { map } from 'rxjs/operators';
import { ProgressBarService } from '../../../../core/services/progress-bar/progress-bar.service';
import { SampleService } from '../../../../core/services/sample/sample.service';
import { ToastService } from '../../../../core/services/toast/toast.service';

@Component({
  selector: 'electric-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent {
  public data: any[] = [];
  public querySearch = '';
  public showListSamples = false;
  @Input() placeholder: string;
  @Input() type: string;
  @Output() callback = new EventEmitter();
  constructor(
    private sampleSvc: SampleService,
    private progressBar: ProgressBarService,
    private toastSvc: ToastService,
    private eRef: ElementRef
  ) {}

  /**
   * handles the select value item
   * @param item the selected item
   */
  selectValueFunc(item) {
    this.showListSamples = false;
    this.callback.emit({ event: 'select', type: this.type, data: item });
  }

  /**
   * highlight text search
   * @param text the text
   * @param search the search text
   */
  highlightText = (text, search) => {
    let pattern = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    pattern = pattern
      .split(' ')
      .filter((t) => t.length > 0)
      .join('|');
    const regex = new RegExp(pattern, 'gi');
    return search
      ? text.replace(regex, (match) => {
          return `<span class='highlight'>${match}</span>`;
        })
      : text;
  };

  /**
   * search data
   */
  search() {
    this.progressBar.show();
    if (this.type === 'well') {
      this.sampleSvc
        .searchWells({ name: this.querySearch })
        .pipe(map(this.processor('well')))
        .catch(this.errorHandler('well'))
        .subscribe((res) => {
          this.showList(res);
        });
    } else if (this.type === 'sample') {
      this.sampleSvc
        .searchSamples({ name: this.querySearch })
        .pipe(map(this.processor('sample')))
        .catch(this.errorHandler('sample'))
        .subscribe((res) => {
          this.showList(res);
        });
    } else {
      forkJoin(
        this.sampleSvc
          .searchSamples({ name: this.querySearch })
          .pipe(map(this.processor('sample')))
          .catch(this.errorHandler('sample')),
        this.sampleSvc
          .searchWells({ name: this.querySearch })
          .pipe(map(this.processor('well')))
          .catch(this.errorHandler('well'))
      ).subscribe((res) => {
        this.showList(res[0].concat(res[1]));
      });
    }
  }

  /**
   * lookup results processor
   * @param type type of lookup data
   */
  processor(type) {
    return (res: any) => {
      return res.results.map((name) => ({ name, type }));
    };
  }

  /**
   * common api error handler
   * @param type type of api to call
   */
  errorHandler(type) {
    return () => {
      this.toastSvc.error(`Failed to search ${type}`);
      return of([]);
    };
  }

  /**
   * show auto-complete option list
   * @param data sample/well list
   */
  showList(data) {
    this.data = data;
    this.progressBar.hide();
    this.showListSamples = true;
  }

  /**
   * handles the click body
   * @param event the event
   */
  @HostListener('document:click', ['$event'])
  clickBody(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.showListSamples = false;
    }
  }
}
