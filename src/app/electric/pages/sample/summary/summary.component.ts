import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { pick } from 'lodash';
import { SampleService } from '../../../../core/services/sample/sample.service';
import { ModalExportComponent } from '../modal-export/modal-export.component';

@Component({
  selector: 'electric-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  public listSummary = [];
  public isLoading = false;
  public sampleList: any[] = [];
  public wellList: any[] = [];

  constructor(private sampleSvc: SampleService, private dialog: MatDialog) {}

  ngOnInit() {
    this.sampleList = this.sampleSvc.getSampleList();
    this.wellList = this.sampleSvc.getWellList();
    const map = this.sampleSvc.getWellData();
    if (map) {
      map.forEach((v) => {
        this.listSummary = this.listSummary.concat(
          v.map((s) => {
            return {
              sampleID: s.sampleID,
              wellName: s.wellName,
              currentReservoir: s.output.currentReservoir.map((c) => pick(c, ['iRes1', 'iRes2'])),
              mResv: [
                {
                  value: s.output.dataBottom.mResv
                },
                {
                  value: s.output.dataBottom.mResv
                }
              ],
              formationFactor: [
                {
                  label: 'F',
                  value: s.inputs.formationFator.f
                },
                {
                  label: 'F*',
                  value: s.inputs.formationFator.fIndex
                }
              ],
              porosityExponent: [
                {
                  label: 'm',
                  value: s.inputs.porosityExponent.m
                },
                {
                  label: 'm*',
                  value: s.inputs.porosityExponent.mIndex
                }
              ]
            };
          })
        );
      });
    }
  }

  /**
   * export all
   */
  openDialogExport(item) {
    const dialogRef = this.dialog.open(ModalExportComponent, {
      panelClass: 'modal-export',
      data: {
        type: 'export',
        item
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }
}
