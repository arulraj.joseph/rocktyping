import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'electric-modal-export',
  templateUrl: './modal-export.component.html',
  styleUrls: ['./modal-export.component.scss']
})
export class ModalExportComponent implements OnInit {
  public isCheckInput = true;
  public isCheckOutput = true;
  public nameFileExport = '';
  constructor(public dialogRef: MatDialogRef<ModalExportComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    if (this.data.summary && this.data.summary.length > 0) {
      this.data.summary.forEach((element) => {
        element.isCheck = true;
      });
    }
  }

  /**
   * select all export
   */
  selectAllExport() {
    this.data.summary.forEach((element) => {
      element.isCheck = true;
    });
  }

  /**
   * unselect all export
   */
  unSelectAllExport() {
    this.data.summary.forEach((element) => {
      element.isCheck = false;
    });
  }

  /**
   * close dialog
   */
  close() {
    this.dialogRef.close();
  }

  /**
   * export
   */
  export() {
    this.dialogRef.close();
  }
}
