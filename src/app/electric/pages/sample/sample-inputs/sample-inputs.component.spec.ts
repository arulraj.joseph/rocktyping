import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleInputsComponent } from './sample-inputs.component';

describe('SampleInputsComponent', () => {
  let component: SampleInputsComponent;
  let fixture: ComponentFixture<SampleInputsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SampleInputsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleInputsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
