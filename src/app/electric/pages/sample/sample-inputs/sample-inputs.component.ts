import { Component, Input, OnInit } from '@angular/core';
import * as _ from 'lodash';

import { ISample } from '../../../../core/models';
import { SampleService } from '../../../../core/services/sample/sample.service';

@Component({
  selector: 'electric-sample-inputs',
  templateUrl: './sample-inputs.component.html',
  styleUrls: ['./sample-inputs.component.scss']
})
export class SampleInputsComponent implements OnInit {
  @Input() data: ISample;
  @Input() public changeValue: any = {};

  constructor(private sampleSvc: SampleService) {}

  ngOnInit(): void {
    this.changeValue = this.sampleSvc.getLastUpdatedSample(this.data.id);
  }

  /**
   * Check if field is change
   * @param field' field Formula string
   */
  isChangeField(fieldFormula) {
    if (!this.isChangeValue()) {
      return false;
    }

    const changeFieldValue = _.get(this.changeValue, fieldFormula);
    if (changeFieldValue !== null && changeFieldValue !== undefined) {
      return true;
    }
    return false;
  }

  /**
   * Check if this input changed value
   */
  isChangeValue() {
    return !_.isEmpty(this.changeValue);
  }
}
