import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ModalExportComponent } from '../modal-export/modal-export.component';

@Component({
  selector: 'electric-sidebar-left',
  templateUrl: './sidebar-left.component.html',
  styleUrls: ['./sidebar-left.component.scss']
})
export class SidebarLeftComponent implements OnInit {
  @Input() type: string;
  @Input() inputSamples = [];
  @Input() wells = [];
  @Input() selected = {
    name: ''
  };
  public type_name: string;

  @Output() reset = new EventEmitter();
  @Output() summary = new EventEmitter();
  @Output() callback = new EventEmitter();
  constructor(private dialog: MatDialog, private router: Router) {}

  ngOnInit() {
    this.type_name = this.type === 'add new' ? 'New Sample' : 'Edit Inputs';
  }

  /**
   * callback autocomplete
   * @param result result data autocomplete
   */
  callbackAutocomplete(result) {
    this.callback.emit(result);
  }

  /**
   *
   * @param type type of item
   * @param name name of item
   * @param item full item object
   */
  select(type, name, item) {
    this.callback.emit({ data: { type, name, item } });
    if (this.type === 'edit inputs' && type === 'sample') {
      this.router.navigate(['/electric/sample/edit', name]);
    }
  }

  /**
   * export all
   */
  openDialogExport() {
    const dialogRef = this.dialog.open(ModalExportComponent, {
      panelClass: 'modal-export',
      data: {
        type: 'export-all',
        summary: this.wells
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }

  /**
   * reset action
   */
  onReset() {
    this.reset.emit();
  }

  /**
   * summary action
   */
  onSummary() {
    this.summary.emit();
  }
}
