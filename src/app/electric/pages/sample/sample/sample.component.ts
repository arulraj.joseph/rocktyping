import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { findIndex, isEqual, pick, get } from 'lodash';
import { SampleService } from '../../../../core/services/sample/sample.service';

@Component({
  selector: 'electric-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit, OnDestroy {
  public samples: any[] = [];
  public isLoading = false;
  public selectedItem;
  public sampleList: any[] = [];
  public wellList: any[] = [];
  public wellMap = new Map();
  private sub: any;

  constructor(private sampleSvc: SampleService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.sampleList = this.sampleSvc.getSampleList();
    this.wellList = this.sampleSvc.getWellList();
    this.selectedItem = this.sampleSvc.getSelectedItem();
    if (this.selectedItem) {
      const theLastestUpdateItem = this.sampleSvc.getLastUpdatedSample(null);
      if (theLastestUpdateItem) {
        const newSampleNumber = get(theLastestUpdateItem, 'inputs.sampleProperties.sampleNumber');
        if (newSampleNumber) {
          this.selectedItem.name = newSampleNumber;
        }
      }
      this.getDetails(this.selectedItem);
    }

    this.sub = this.route.queryParams.subscribe((params) => {
      if (params.sampleId) {
        this.loadDefaultSample(params.sampleId);
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  /**
   * select sample/well event handler
   * @param event event object
   */
  selectItem(event) {
    const data = event.data;
    if (isEqual(data, this.selectedItem)) {
      return;
    }
    if (data.item && data.item.id) {
      this.loadDefaultSample(data.item.id);
    } else {
      this.selectedItem = data;
      this.sampleSvc.setSelectedItem(data);
      this.getDetails(data);
    }
  }

  /**
   * Load default sample
   * @param sampleId sample id
   */
  loadDefaultSample(sampleId) {
    this.isLoading = true;
    this.sampleSvc
      .getDetail(sampleId)
      /*.catch(() => {
        this.toastSvc.error(`failed to load ${sampleId} detail`);
        return of(null);
      })*/
      .subscribe((res) => {
        this.isLoading = false;
        if (!res) {
          return;
        }
        res.name = get(res, 'inputs.sampleProperties.sampleNumber');
        res.type = 'sample';
        this.selectedItem = res;
        this.sampleSvc.setSelectedItem(res);
        this.processItemAfterGetFromServer(res, [res]);
      });
  }

  /**
   * get details of sample/well
   * @param item sample/well
   */
  getDetails(item) {
    this.isLoading = true;
    let criteria;
    if (item.type === 'sample') {
      criteria = {
        sampleNumber: get(item, 'input.sampleNumber', item.name),
        sortBy: 'input.sampleNumber'
      };
    } else if (item.type === 'well') {
      criteria = {
        name: item.name,
        sortBy: 'name'
      };
    }
    this.sampleSvc
      .getDetails(criteria)
      /*.catch(() => {
        this.toastSvc.error(`failed to load ${item.type} details`);
        return of(null);
      })*/
      .subscribe((res) => {
        this.isLoading = false;
        if (!res) {
          return;
        }
        this.processItemAfterGetFromServer(item, res.samples);
      });
  }

  /**
   * process item and sample list after get from server
   * @param item Sample list
   * @param sampleList Sample list
   */
  processItemAfterGetFromServer(item, sampleList) {
    this.samples = sampleList;
    if (item.type === 'sample') {
      const ix = findIndex(this.sampleList, (sample) => sample.sampleID === 'Sample # ' + item.name);
      if (this.samples.length > 0) {
        let wellName = '';
        if (item.wellName) {
          wellName = item.wellName;
        } else if (item.item && item.item.wellName) {
          wellName = item.item.wellName;
        } else {
          wellName = this.samples[0].wellName;
        }
        const sample = {
          wellName,
          sampleID: this.samples[0].sampleID,
          sampleNumber: item.name,
          id: item.id
        };
        if (ix >= 0) {
          this.sampleList[ix] = sample;
        } else {
          this.sampleList.push(sample);
        }
        this.sampleSvc.setSampleList(this.sampleList);
      }
    } else if (item.type === 'well') {
      const ix = findIndex(this.wellList, (w) => w.wellName === item.name);
      const well = {
        wellName: item.name,
        data: this.samples.map((s) => pick(s, ['wellName', 'sampleID']))
      };
      if (ix >= 0) {
        this.wellList[ix] = well;
      } else {
        this.wellList.push(well);
      }
      this.sampleSvc.setWellList(this.wellList);
      this.wellMap.set(item.name, this.samples);
    }
  }

  /**
   * reset event handler
   */
  reset() {
    this.wellList = [];
    this.sampleList = [];
    this.selectedItem = null;
    this.samples = [];
    this.sampleSvc.setSampleList([]);
    this.sampleSvc.setWellList([]);
  }

  /**
   * go to summary page
   */
  goToSummary() {
    this.sampleSvc.setWellData(this.wellMap);
    this.router.navigate(['/electric/sample/summary']);
  }
}
