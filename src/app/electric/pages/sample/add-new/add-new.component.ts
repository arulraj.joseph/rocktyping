import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';

import { SampleService } from '../../../../core/services/sample/sample.service';
import { ExcelService } from '../../../../core/services/excel/excel.service';
import { ToastService } from '../../../../core/services/toast/toast.service';
import { ISample } from '../../../../core/models';

@Component({
  selector: 'electric-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.scss']
})
export class AddNewComponent implements OnInit, OnDestroy {
  public inputSample: ISample = {
    sampleID: '',
    wellName: '',
    inputs: {
      sampleProperties: {
        sampleNumber: '7-132H',
        depth: 0,
        proposity: 0,
        airPermeability: 0,
        grainDensity: 0,
        saturation: 0,
        confiningStress: 0,
        saturantResistivity: 0,
        porosityExponentM: 0,
        porosityExponentMIndex: 0,
        interceptY: 0,
        resRw: 0,
        resTemp: 0
      },
      formationFator: {
        f: 0,
        fIndex: 0
      },
      porosityExponent: {
        m: 0,
        mIndex: 0
      },
      labValues: {
        labRw: 0,
        labTemp: 0
      },
      saturationExponent: {
        n: 0,
        nIndex: 0,
        qv: 0
      },
      resistivityIndex: [
        {
          fractionVp: 0,
          riApparent: 0,
          riIndexApparent: 0
        }
      ]
    },
    output: {
      currentReservoir: [
        {
          iRes1: 0,
          iRes2: 1,
          logSw: 0,
          logIRes1: 0,
          logIRes2: 0
        },
        {
          iRes1: 0,
          iRes2: 1,
          logSw: 0,
          logIRes1: 0,
          logIRes2: 0
        },
        {
          iRes1: 0,
          iRes2: 1,
          logSw: 0,
          logIRes1: 0,
          logIRes2: 0
        },
        {
          iRes1: 1.11,
          iRes2: 1.11614865,
          logSw: -0.02457438,
          logIRes1: 0.04141975,
          logIRes2: 0.04741975
        },
        {
          iRes1: 1.44,
          iRes2: 1.462285696,
          logSw: -0.08650536,
          logIRes1: 0.15310409,
          logIRes2: 0.16310409
        },
        {
          iRes1: 2.31,
          iRes2: 2.446131973,
          logSw: -0.20141975,
          logIRes1: 0.36557438,
          logIRes2: 0.38557438
        },
        {
          iRes1: 2.97,
          iRes2: 3.127865185,
          logSw: -0.26310409,
          logIRes1: 0.47141975,
          logIRes2: 0.49141975
        },
        {
          iRes1: 3.97,
          iRes2: 4.11614865,
          logSw: -0.33557438,
          logIRes1: 0.59650536,
          logIRes2: 0.62650536
        },
        {
          iRes1: 4.75,
          iRes2: 4.462285696,
          logSw: -0.38141975,
          logIRes1: 0.67141975,
          logIRes2: 0.69141975
        },
        {
          iRes1: 5.64,
          iRes2: 5.446131973,
          logSw: -0.42650536,
          logIRes1: 0.75310409,
          logIRes2: 0.76310409
        },
        {
          iRes1: 7.27,
          iRes2: 7.127865185,
          logSw: -0.49141975,
          logIRes1: 0.86557438,
          logIRes2: 0.87557438
        },
        {
          iRes1: 8.76,
          iRes2: 9.11614865,
          logSw: -0.54310409,
          logIRes1: 0.94141975,
          logIRes2: 0.96141975
        },
        {
          iRes1: 10.51,
          iRes2: 11.462285696,
          logSw: -0.59557438,
          logIRes1: 1.02650536,
          logIRes2: 1.04650536
        },
        {
          iRes1: 13.36,
          iRes2: 13.446131973,
          logSw: -0.65141975,
          logIRes1: 1.12141975,
          logIRes2: 1.14141975
        },
        {
          iRes1: 15.75,
          iRes2: 15.127865185,
          logSw: -0.70650536,
          logIRes1: 1.19310409,
          logIRes2: 1.19310409
        },
        {
          iRes1: 18.2,
          iRes2: 17.11614865,
          logSw: -0.74141975,
          logIRes1: 1.26141975,
          logIRes2: 1.23141975
        },
        {
          iRes1: 24.35,
          iRes2: 19.462285696,
          logSw: -0.83310409,
          logIRes1: 1.38310409,
          logIRes2: 1.29310409
        }
      ],
      visualizationGraph: {
        y: -1.7107,
        rIndex: 0.9896,
        dataChart: {
          linear: [[0, 0], [-0.7, 1.4]],
          series1: [
            [-0.25, 0.55],
            [-0.35, 0.62],
            [-0.37, 0.67],
            [-0.39, 0.72],
            [-0.42, 0.82],
            [-0.55, 1.0],
            [-0.6, 1.1],
            [-0.62, 1.18],
            [-0.68, 1.2],
            [-0.72, 1.25],
            [-0.81, 1.33]
          ]
        }
      },
      dataBottom: {
        longPhi: -0.6478175,
        longFRev: 1.19062789,
        mResv: 1.8379063,
        nResv: -1.7107,
        labB: 3.61986193,
        reservoirB: 14.7983358,
        resRw: 0.19,
        resTemp: 96
      }
    }
  };
  public fileData: any;
  public objWaterSample: any;
  public isLoading = false;
  private sub: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sampleSvc: SampleService,
    private excelSvc: ExcelService,
    private toastSvc: ToastService
  ) {}

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe((params) => {
      this.inputSample.wellName = '';
      this.inputSample.sampleID = params['name'];
    });

    this.objWaterSample = {
      fractionVp: '',
      riApparent: '',
      riIndexApparent: ''
    };
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  /**
   * handles delete row water sample
   */
  delete(index) {
    this.inputSample.inputs.resistivityIndex.splice(index, 1);
  }

  /**
   * handles add row water sample
   */
  addRowFunc() {
    if (
      this.objWaterSample.fractionVp !== '' &&
      this.objWaterSample.riApparent !== '' &&
      this.objWaterSample.riIndexApparent !== ''
    ) {
      this.inputSample.inputs.resistivityIndex.push(this.objWaterSample);
      this.objWaterSample = {
        fractionVp: '',
        riApparent: '',
        riIndexApparent: ''
      };
    }
  }

  /**
   * Submit form
   */
  submitForm() {
    this.isLoading = true;
    this.sampleSvc.createNewSample(this.inputSample).subscribe(
      (res) => {
        this.isLoading = false;
        this.toastSvc.success('Create new sample success.');
        this.router.navigate(['/electric/sample/sample-inputs'], {
          queryParams: {
            sampleId: res.id
          }
        });
      },
      (err) => {
        this.isLoading = false;
        this.toastSvc.error(_.get(err, 'message', ''));
      }
    );
  }

  uploadFile(event) {
    console.log(event.target.files[0]);
    const reader: FileReader = new FileReader();
    this.readerExcel(reader);
    reader.readAsArrayBuffer(event.target.files[0]);
  }

  readerExcel(reader) {
    /* reset array */
    reader.onload = (e: any) => {
      const data: string = e.target.result;
      this.excelSvc.updateInputSampleWithExcelData(data, this.inputSample);
      this.fileData = null;
    };
  }
}
