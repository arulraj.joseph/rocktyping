import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizationGraphComponent } from './visualization-graph.component';

describe('VisualizationGraphComponent', () => {
  let component: VisualizationGraphComponent;
  let fixture: ComponentFixture<VisualizationGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VisualizationGraphComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizationGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
