import { Component, Input } from '@angular/core';
import * as Highcharts from 'highcharts';

const options = {
  xAxis: {
    lineWidth: 2,
    gridLineWidth: 1,
    gridLineDashStyle: 'longdash',
    gridLineColor: 'rgba(151, 151, 151, 0.5)',
    tickInterval: 0.2,
    lineColor: '#A3A3A3',
    labels: {
      style: {
        color: '#000000',
        fontSize: '12px',
        opacity: 0.5
      }
    }
  },
  yAxis: {
    opposite: true,
    lineWidth: 2,
    lineColor: '#A3A3A3',
    gridLineWidth: 1,
    gridLineDashStyle: 'longdash',
    gridLineColor: 'rgba(151, 151, 151, 0.5)',
    tickInterval: 0.2,
    title: {
      text: ''
    },
    labels: {
      style: {
        color: '#000000',
        fontSize: '12px',
        opacity: 0.5
      }
    }
  },
  title: {
    text: ''
  },
  chart: {
    width: 330,
    height: 525,
    zoomType: 'xy'
  },
  credits: {
    enabled: false
  },
  plotOptions: {
    scatter: {
      marker: {
        radius: 5,
        states: {
          hover: {
            enabled: true,
            lineColor: 'rgb(100,100,100)'
          }
        }
      },
      states: {
        hover: {
          marker: {
            enabled: false
          }
        }
      },
      tooltip: {
        headerFormat: '<b>{series.name}</b><br>',
        pointFormat: '{point.x}, {point.y}'
      }
    }
  },
  series: [
    {
      type: 'scatter',
      name: 'Series 1',
      data: [],
      color: '#205CB9',
      marker: {
        radius: 3.5,
        symbol: 'circle'
      }
    },
    {
      type: 'line',
      name: 'Linear (Series 1)',
      data: [],
      color: '#205CB9',
      lineWidth: 1,
      marker: {
        enabled: false
      },
      states: {
        hover: {
          lineWidth: 0
        }
      },
      enableMouseTracking: false
    }
  ]
};

@Component({
  selector: 'electric-visualization-graph',
  templateUrl: './visualization-graph.component.html',
  styleUrls: ['./visualization-graph.component.scss']
})
export class VisualizationGraphComponent {
  public chart;
  public Highcharts = Highcharts; // required
  public chartConstructor = 'chart'; // optional string, defaults to 'chart'
  public chartOptions = options; // required
  public updateFlag = false; // optional boolean
  public oneToOneFlag = true; // optional boolean, defaults to false
  public runOutsideAngularFlag = false; // optional boolean, defaults to false
  @Input()
  set data(data) {
    this.setChartOptions(data);
  }

  /**
   * set chart options when data changes
   * @param data chart data
   */
  setChartOptions(data) {
    this.chartOptions = {
      ...this.chartOptions,
      series: [
        {
          ...this.chartOptions.series[0],
          data: data.series1
        },
        {
          ...this.chartOptions.series[1],
          data: data.linear
        }
      ]
    };
  }

  /**
   * callback
   * @param chart highcharts instance
   */
  chartCallback(chart) {
    this.chart = chart;
  }
}
