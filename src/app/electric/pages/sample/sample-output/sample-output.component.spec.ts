import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SampleOutputComponent } from './sample-output.component';

describe('SampleOutputComponent', () => {
  let component: SampleOutputComponent;
  let fixture: ComponentFixture<SampleOutputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SampleOutputComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SampleOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
