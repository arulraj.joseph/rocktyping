import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ISample } from '../../../../core/models';
import { ModalExportComponent } from '../modal-export/modal-export.component';

@Component({
  selector: 'electric-sample-output',
  templateUrl: './sample-output.component.html',
  styleUrls: ['./sample-output.component.scss']
})
export class SampleOutputComponent {
  @Input() data: ISample;
  constructor(private dialog: MatDialog) {}

  /**
   * export all
   */
  openDialogExport() {
    const dialogRef = this.dialog.open(ModalExportComponent, {
      panelClass: 'modal-export',
      data: {
        type: 'input-output'
      }
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
    });
  }
}
