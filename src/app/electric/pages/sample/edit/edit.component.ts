import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';

import { ExcelService } from '../../../../core/services/excel/excel.service';
import { SampleService } from '../../../../core/services/sample/sample.service';
import { ToastService } from '../../../../core/services/toast/toast.service';

@Component({
  selector: 'electric-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit, OnDestroy {
  public inputSample: any;
  public inputSampleOriginal: any;
  public objWaterSample: any;
  public isLoading = false;
  public paramsSub: any;
  public sampleList: any[] = [];
  public wellList: any[] = [];
  public fileData: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private sampleSvc: SampleService,
    private excelSvc: ExcelService,
    private toastSvc: ToastService
  ) {}

  ngOnInit() {
    this.sampleList = this.sampleSvc.getSampleList();
    this.wellList = this.sampleSvc.getWellList();

    this.paramsSub = this.route.params.subscribe((params) => {
      this.isLoading = true;
      this.sampleSvc.getDetail(params.sampleID).subscribe((res) => {
        this.isLoading = false;
        if (!res) {
          return;
        }
        this.inputSampleOriginal = _.cloneDeep(res);
        this.inputSample = res;
      });
    });
    this.objWaterSample = {
      fractionVp: '',
      riApparent: '',
      riIndexApparent: ''
    };
  }

  /**
   * Deep diff between two object, using lodash
   * @param  {Object} object Object compared
   * @param  {Object} base   Object to compare with
   * @return {Object}        Return a new object who represent the diff
   */
  private difference(object, base) {
    function changes(obj, bs) {
      return _.transform(obj, (result, value, key) => {
        if (!_.isEqual(value, bs[key])) {
          result[key] = _.isObject(value) && _.isObject(bs[key]) ? changes(value, bs[key]) : value;
        }
      });
    }
    return changes(object, base);
  }

  /**
   * Submit form
   */
  submitForm() {
    this.isLoading = true;
    const inputSampleDifference: any = this.difference(this.inputSample, this.inputSampleOriginal);
    const inputSampleDifferenceRaw = _.cloneDeep(inputSampleDifference);
    if (inputSampleDifference.inputs && inputSampleDifference.inputs.resistivityIndex) {
      inputSampleDifference.inputs.resistivityIndex = _.cloneDeep(this.inputSample.inputs.resistivityIndex);
      const differenceWith = _.differenceWith(
        this.inputSample.inputs.resistivityIndex,
        this.inputSampleOriginal.inputs.resistivityIndex,
        _.isEqual
      );
      if (!differenceWith || differenceWith.length === 0) {
        inputSampleDifferenceRaw.inputs.resistivityIndex = [];
      }
    }
    this.sampleSvc.updateSample(this.inputSample.id, inputSampleDifference, inputSampleDifferenceRaw).subscribe(
      () => {
        this.isLoading = false;
        this.toastSvc.success('Update sample success.');
        this.router.navigate(['/electric/sample/sample-inputs']);
      },
      (err) => {
        this.isLoading = false;
        this.toastSvc.error(_.get(err, 'message', ''));
      }
    );
  }

  ngOnDestroy() {
    this.paramsSub.unsubscribe();
  }

  /**
   * handles delete row water sample
   */
  delete(index) {
    this.inputSample.inputs.resistivityIndex.splice(index, 1);
  }

  /**
   * handles add row water sample
   */
  addRowFunc() {
    if (
      this.objWaterSample.fractionVp !== '' &&
      this.objWaterSample.riApparent !== '' &&
      this.objWaterSample.riIndexApparent !== ''
    ) {
      this.inputSample.inputs.resistivityIndex.push(this.objWaterSample);
      this.objWaterSample = {
        fractionVp: '',
        riApparent: '',
        riIndexApparent: ''
      };
    }
  }

  uploadFile(event) {
    console.log(event.target.files[0]);
    const reader: FileReader = new FileReader();
    this.readerExcel(reader);
    reader.readAsArrayBuffer(event.target.files[0]);
  }

  readerExcel(reader) {
    /* reset array */
    reader.onload = (e: any) => {
      const data: string = e.target.result;
      this.excelSvc.updateInputSampleWithExcelData(data, this.inputSample);
      this.fileData = null;
    };
  }
}
