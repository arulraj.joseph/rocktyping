import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SampleService } from '../../../../core/services/sample/sample.service';

@Component({
  selector: 'electric-search-core',
  templateUrl: './search-core.component.html',
  styleUrls: ['./search-core.component.scss']
})
export class SearchCoreComponent implements OnInit {
  public newSampleName: string;
  public isError = false;

  constructor(private router: Router, private sampleSvc: SampleService) {}

  ngOnInit() {
    this.sampleSvc.setSampleList([]);
    this.sampleSvc.setWellList([]);
  }

  /**
   * search data
   */
  callbackAutocomplete(result) {
    this.sampleSvc.setSelectedItem(result.data);
    this.router.navigate(['/electric/sample/sample-inputs']);
  }

  /**
   * Open new sample page
   */
  newSample() {
    if (!this.newSampleName) {
      this.isError = true;
      return;
    }
    this.router.navigate(['/electric/sample/add-new'], {
      queryParams: {
        name: this.newSampleName
      }
    });
  }
}
