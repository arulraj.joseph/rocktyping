import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatListModule,
  MatRadioModule,
  MatDialogModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule
} from 'ngx-perfect-scrollbar';
import { HighchartsChartModule } from 'highcharts-angular';

import { AddNewComponent } from './pages/sample/add-new/add-new.component';
import { EditComponent } from './pages/sample/edit/edit.component';
import { SampleComponent } from './pages/sample/sample/sample.component';
import { SearchCoreComponent } from './pages/sample/search-core/search-core.component';
import { SummaryComponent } from './pages/sample/summary/summary.component';

import { AutocompleteComponent } from './pages/sample/autocomplete/autocomplete.component';
import { ModalExportComponent } from './pages/sample/modal-export/modal-export.component';
import { SampleInputsComponent } from './pages/sample/sample-inputs/sample-inputs.component';
import { SampleOutputComponent } from './pages/sample/sample-output/sample-output.component';
import { SidebarLeftComponent } from './pages/sample/sidebar-left/sidebar-left.component';
import { VisualizationGraphComponent } from './pages/sample/visualization-graph/visualization-graph.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: false
};

import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,

    MatRadioModule,
    RouterModule,
    PerfectScrollbarModule,
    FormsModule,
    HighchartsChartModule,
    MatInputModule,
    MatListModule,
    MatButtonModule,
    MatDialogModule,
    MatCheckboxModule,
    MatProgressSpinnerModule
  ],
  declarations: [
    SampleComponent,
    EditComponent,
    SummaryComponent,
    SearchCoreComponent,
    AddNewComponent,
    VisualizationGraphComponent,
    ModalExportComponent,
    SampleInputsComponent,
    SampleOutputComponent,
    SidebarLeftComponent,
    AutocompleteComponent
  ],
  entryComponents: [ModalExportComponent],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})
export class ElectricModule {}
