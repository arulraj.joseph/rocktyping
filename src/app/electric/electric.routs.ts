import { AuthGuard } from '../routing/auth.guard';
import { AddNewComponent } from './pages/sample/add-new/add-new.component';
import { EditComponent } from './pages/sample/edit/edit.component';
import { SampleComponent } from './pages/sample/sample/sample.component';
import { SearchCoreComponent } from './pages/sample/search-core/search-core.component';
import { SummaryComponent } from './pages/sample/summary/summary.component';

export const ELECTRIC_ROUTES = [
  {
    path: '',
    redirectTo: 'sample',
    pathMatch: 'full'
  },
  {
    path: 'sample',
    children: [
      {
        path: '',
        redirectTo: 'search-core',
        pathMatch: 'full'
      },
      {
        path: 'search-core',
        component: SearchCoreComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'sample-inputs',
        component: SampleComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'add-new',
        component: AddNewComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'edit/:sampleID',
        component: EditComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'summary',
        component: SummaryComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];
