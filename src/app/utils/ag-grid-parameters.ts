export const getNumbers = (params, numbersAfterComma: number): string => {
  const currentValue = Number(params.value);

  if (isNaN(currentValue) || currentValue < 0) {
    return undefined;
  }

  return String(parseFloat(currentValue.toFixed(numbersAfterComma)));
};

export const getTwoNumbersAfterComma = (params) => {
  return getNumbers(params, 2);
};

export const getSixNumbersAfterComma = (params) => {
  return getNumbers(params, 6);
};

export const numberParser = (params) => {
  if (!Number(params.newValue)) {
    return undefined;
  }

  return Number(params.newValue);
};
