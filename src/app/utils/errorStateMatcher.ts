import { FormControl, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

export class CrossFieldErrorMatcher implements ErrorStateMatcher {
  private formGroup: FormGroup;

  constructor(formGroup: FormGroup) {
    this.formGroup = formGroup;
  }

  isErrorState(control: FormControl | null): boolean {
    return control.dirty && this.formGroup.invalid;
  }
}
