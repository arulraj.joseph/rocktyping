import { IJobRun, IMineralValue, IXrdAlgorithmResult } from '../xrd/core/models';
import { IPieChartData } from '../xrd/shared/pie-chart/pie-chart.component';
import { IScatterChartData } from '../xrd/shared/scatter-chart/scatter-chart.component';
import { IStackedBarChartData } from '../xrd/shared/stacked-bar-chart/stacked-bar-chart.component';

export const calculateAverageAloritmResult = (data: IXrdAlgorithmResult[]): IPieChartData[] => {
  if (!data || !data.length) {
    return [];
  }

  const mineralKeys = data[0].mineralValues.map((x) => x.name);

  return mineralKeys.map((key) => {
    const filteredData = data
      .map((x) => x.mineralValues.find((m) => m.name === key))
      .filter((x) => x && x.value !== undefined && x.value !== null && !isNaN(x.value))
      .map((x) => x.value);

    const avg = filteredData.reduce((sum, current) => sum + current, 0) / filteredData.length;

    return {
      name: key,
      y: avg
    };
  });
};

export const getPieChartData = (
  data: [IJobRun, IXrdAlgorithmResult]
): { title: string; data: IPieChartData[]; depth?: number } => {
  const [jobRun, selectedRow] = data;
  if (!selectedRow) {
    return {
      title: 'Average Mineral Distribution',
      data: calculateAverageAloritmResult(jobRun.xrdAlgorithmResult)
    };
  } else {
    const chartData = selectedRow.mineralValues.map(
      (mineralData: IMineralValue): IPieChartData => ({
        y: mineralData.value,
        name: mineralData.name
      })
    );

    return {
      title: `Minerals Distribution for depth ${selectedRow.depthFt}ft`,
      data: chartData,
      depth: selectedRow.depthFt
    };
  }
};

export const chartColors = {
  Calcite: '#96000F',
  Dolomite: '#FF8800',
  Siderite: '#FFD000',
  Quartz: '#FFCFC9',
  Kspar: '#FF001D',
  Plag: '#65FF00',
  Pyrite: '#3C9600',
  Apatite: '#17BEC1',
  Marcasite: '#00FFDD',
  Chlorite: '#00C7FF',
  Kaolinite: '#0072FF',
  Illite: '#9D00FF',
  'Mx I/S*': '#D000FF',
  Anhydrite: '#9E6B9B',
  Barite: '#9B8976',
  Other: '#60995D',
  Kerogen: '#587699'
};

export const getStackedBarChartData = (data: IXrdAlgorithmResult[]): IStackedBarChartData[] => {
  return data.map((x) => {
    return {
      key: `${x.depthFt} ft.`,
      minerals: x.mineralValues
    };
  });
};

export const getScatterChartData = (data: IXrdAlgorithmResult[]): IScatterChartData[] => {
  const apparentGrainDChartData: IScatterChartData = {
    name: 'Apparent Grain D',
    color: '#C0504D',
    data: []
  };
  const griGrainDChartData: IScatterChartData = {
    name: 'GRI Grain D',
    color: '#9BBB59',
    data: []
  };
  const versusChartData: IScatterChartData = {
    name: 'Versus',
    color: '#4F81BD',
    data: []
  };

  data.forEach((item: IXrdAlgorithmResult) => {
    const { apparentGrainD, griGrainD } = item;
    if (apparentGrainD && griGrainD) {
      versusChartData.data.push([apparentGrainD, griGrainD]);
    }

    if (apparentGrainD) {
      apparentGrainDChartData.data.push([apparentGrainD, apparentGrainD]);
    }

    if (griGrainD) {
      griGrainDChartData.data.push([griGrainD, griGrainD]);
    }
  });

  return [versusChartData, apparentGrainDChartData, griGrainDChartData];
};
