import { Observable } from 'rxjs';

export const readFile = (file: File): Observable<string> => {
  const reader: FileReader = new FileReader();

  const readerObservable = Observable.create((observer) => {
    reader.onload = (e: ProgressEvent) => {
      let stringResult = '';
      const binaryResult = (e.target as any).result;
      const bytes = new Uint8Array(binaryResult);

      for (let i = 0; i < bytes.byteLength; i++) {
        stringResult += String.fromCharCode(bytes[i]);
      }

      observer.next(stringResult);
      observer.complete();
    };

    reader.onerror = (e: ProgressEvent) => observer.error((e.target as any).result);
  });

  reader.readAsArrayBuffer(file);

  return readerObservable;
};
