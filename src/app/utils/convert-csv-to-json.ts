import * as csv from 'csvtojson';
import { Observable } from 'rxjs';

export const convertCsvToJson = (data: string): Observable<Array<{ [key: string]: string }>> => {
  return Observable.create((observer) => {
    csv()
      .fromString(data)
      .then(
        (json) => {
          observer.next(json);
          observer.complete();
        },
        (error) => observer.error(error)
      );
  });
};
