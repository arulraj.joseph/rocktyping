import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@NgModule({
  declarations: [],
  imports: [CommonModule]
})
export class IconsModule {
  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    this.registerSvgIcon('brooks-corey');
    this.registerSvgIcon('litho-tracker');
    this.registerSvgIcon('unit-converter');
    this.registerSvgIcon('xrd');
    this.registerSvgIcon('anadarko-logo');
    this.registerSvgIcon('anadarko-logo-circle');
  }

  private registerSvgIcon(name: string, path?: string) {
    path = path || `assets/icons/${name}.svg`;

    this.matIconRegistry.addSvgIcon(name, this.domSanitizer.bypassSecurityTrustResourceUrl(path));
  }
}
