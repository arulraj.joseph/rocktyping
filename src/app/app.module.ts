import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  ApcAuthAcceleratorModule,
  apcAuthInterceptorFactory,
  ApcAuthInterceptorRequestMatcherService,
  ApcAuthTokenService
} from '@apc-ng/auth/accelerators';
import { SpringCloudConfigModule } from '@apc-ng/config';
import { AuthHttpClient } from '@apc-ng/core';
import { UserModule } from '@apc-ng/user';
import { ApcThemeMaterialModule, LayoutModule } from '@apc-ng/theme-material';
import { AppComponent } from './app.component';
import { BrooksCoreyModule } from './brooks-corey/brooks-corey.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { CoreInventoryModule } from './core-inventory/core-inventory.module';
import { IconsModule } from './icons/icons.module';
import { AppRoutingModule } from './routing/routing.module';
import { UnitsConverterModule } from './units-converter/units-converter.module';
import { XrdModule } from './xrd/xrd.module';
import { KlinkenbergModule } from './klinkenberg/klinkenberg.module';
import { CalculatorsModule } from './calculators/calculators.module';
import { LicenseManager } from 'ag-grid-enterprise';
import { CtsModule } from './cts/cts.module';
import { ElectricModule } from './electric/electric.module';
import { IdemgModule } from './idemg/idemg.module';
import { PvtModule } from './pvt/pvt.module';
import { NcsModule } from './ncs/ncs.module';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';

LicenseManager.setLicenseKey(
  'Anadarko_Petroleum_Corporation_MultiApp_15Devs14_November_2019__MTU3MzY4OTYwMDAwMA==799ba40e10f9cb4746320cae62ce4b7b'
);

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    // Service workers are only available to "secure origins" (HTTPS sites, basically)
    // ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    IconsModule,
    AppRoutingModule,
    UserModule,
    SpringCloudConfigModule.forRoot({
      appPropertiesConfig: { appName: 'gng-ui' }
    }),
    ApcAuthAcceleratorModule.forRoot(),
    ApcThemeMaterialModule.forRoot({
      environmentBadge: {
        enabled: true
      },
      layout: {
        avatar: {
          enabled: true,
          showUserName: true
        },
        footer: {
          enabled: true
        },
        sidebarLeft: {
          enabled: true,
          type: 'compact'
        }
      },
      menu: {
        enabled: true,
        type: 'vertical'
      },
      support: {
        enabled: false,
        url: 'https://material.angular.io/'
      },
      settings: {
        enabled: false,
        theming: {
          enabled: false
        }
      },
      theme: 'light'
    }),
    UnitsConverterModule,
    CoreInventoryModule,
    XrdModule,
    KlinkenbergModule,
    CalculatorsModule,
    CtsModule,
    BrooksCoreyModule,
    LayoutModule,
    DashboardModule,
    IdemgModule,
    ElectricModule,
    PvtModule,
    NcsModule,
    MatIconModule,
    MatProgressBarModule
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  providers: [
    {
      // This replaces AuthHttpClient with HttpClient. It is required until all modules that use AuthHttpClient are refactored.
      provide: AuthHttpClient,
      useExisting: HttpClient
    },
    {
      provide: HTTP_INTERCEPTORS,
      useFactory: apcAuthInterceptorFactory,
      deps: [ApcAuthTokenService, ApcAuthInterceptorRequestMatcherService],
      multi: true
    },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ]
})
export class AppModule {}
