import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatRadioModule,
  MatTooltipModule,
  MatSelectModule
} from '@angular/material';
import { SharedModule } from '../shared/shared.module';

import { NetConfiningStressComponent } from './pages/ncs/net-confining-stress/net-confining-stress.component';
import { TableNetConfiningStressComponent } from './pages/ncs/table-net-confining-stress/table-net-confining-stress.component';
import { InputRangeComponent } from './pages/ncs/input-range/input-range.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatRadioModule,
    MatTooltipModule,
    MatSelectModule,
    SharedModule
  ],
  declarations: [NetConfiningStressComponent, TableNetConfiningStressComponent, InputRangeComponent],
  exports: [NetConfiningStressComponent, TableNetConfiningStressComponent, InputRangeComponent]
})
export class NcsModule {}
