import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ncs-input-range',
  templateUrl: './input-range.component.html',
  styleUrls: ['./input-range.component.scss']
})
export class InputRangeComponent implements OnInit {
  @Input() incrementBy: number;
  @Input() value;
  @Input() unit: string;
  @Input() showError: boolean;
  @Output() callback = new EventEmitter();
  constructor() {}

  ngOnInit() {
    if (this.value && this.value !== 0) {
      this.value = parseFloat(this.value).toFixed(2);
    }
  }

  /**
   * change value
   */
  changeValueFunc(event) {
    this.value = parseFloat(event.target.value);
    if (event.target.value === '') {
      this.value = 0;
    }
    this.callback.emit(this.value);
  }

  /**
   * click increment up
   */
  incrementUpFunc() {
    if (typeof this.value === 'string') {
      this.value = parseFloat(this.value);
    }
    if (this.incrementBy === undefined) {
      this.value++;
    } else {
      this.value += this.incrementBy;
    }
    this.callback.emit(this.value);
  }

  /**
   * click increment down
   */
  incrementDownFunc() {
    if (typeof this.value === 'string') {
      this.value = parseFloat(this.value);
    }
    if (this.value > 0 || this.value - this.incrementBy > 0) {
      if (this.incrementBy === undefined) {
        this.value--;
      } else {
        this.value -= this.incrementBy;
      }
    }
    this.callback.emit(this.value);
  }
}
