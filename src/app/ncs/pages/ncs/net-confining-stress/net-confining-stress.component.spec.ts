import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NetConfiningStressComponent } from './net-confining-stress.component';

describe('NetConfiningStressComponent', () => {
  let component: NetConfiningStressComponent;
  let fixture: ComponentFixture<NetConfiningStressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NetConfiningStressComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NetConfiningStressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
