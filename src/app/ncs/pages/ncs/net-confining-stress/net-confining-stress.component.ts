import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { ngxCsv } from 'ngx-csv';
import { finalize } from 'rxjs/operators';
import { ProgressBarService } from '../../../../core/services/progress-bar/progress-bar.service';
import { ICreateCalculationData } from '../../../../core/models';
import { CalculatorsService } from '../../../../core/services/calculators/calculators.service';
import { DropdownComponent } from '../../../../shared/components/dropdown/dropdown.component';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'ncs-net-confining-stress',
  templateUrl: './net-confining-stress.component.html',
  styleUrls: ['./net-confining-stress.component.scss']
})
export class NetConfiningStressComponent implements OnInit {
  // options select or type
  optionsWellName: any[] = [];
  storedOptionsWellName: any[] = [];
  selectedWellName = null;
  // options select increment
  optionsIncrementBy = [];
  selectedIncrementBy = 1;
  // range depth
  startDepth = 0;
  endDepth = 0;
  dataCalculator: any = {};
  showRange = false;
  showTableCalculator = false;
  showBtnCalculator = false;
  dataInputs: any = {};
  querySearch = '';
  txtUnit = 'ft';
  showError = false;
  selectedDepths: any[] = [];
  isUpdate = false;
  selectedResult: any = {};
  isLoading = false;
  @Output() callback = new EventEmitter();
  @Input() index: number;

  @ViewChild(DropdownComponent) dropdownComponent: DropdownComponent;

  constructor(private calculatorsSvc: CalculatorsService, private progressBar: ProgressBarService) {}

  /**
   * on init
   */
  ngOnInit() {
    this.calculatorsSvc.getAllIncrementBy().subscribe((res) => {
      this.optionsIncrementBy = res;
    });
    if (!this.dataInputs.depths) {
      if (!this.showRange) {
        this.dataInputs.depths = [
          {
            depth: 0,
            obGradient: 0,
            poissonSRatio: 0,
            porePressureGradient: 0,
            fractGradient: 0,
            biotsAlpha: 0
          }
        ];
      } else {
        this.dataInputs.depths = [
          {
            from: 0,
            to: 0,
            obGradient: 0,
            poissonSRatio: 0,
            porePressureGradient: 0,
            fractGradient: 0,
            biotsAlpha: 0
          }
        ];
      }
    }
  }

  /**
   * callback dropdown
   * @param result the result
   */
  callbackDropdown(result) {
    this.querySearch = result.query;
    if (result.event === 'search') {
      this.isUpdate = false;
      if (this.querySearch.trim() === '') {
        this.showTableCalculator = false;
        this.showBtnCalculator = false;
        this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
      } else {
        this.showBtnCalculator = true;
        this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
      }
      this.fetch();
      this.showBtnCalculator = true;
    } else if (result.event === 'select') {
      this.progressBar.show();
      setTimeout(() => {
        this.progressBar.hide();
        this.selectedResult = { ...result.item };
        this.isUpdate = true;
        const arrayDepths = [];
        const arrayTable = [];
        this.showTableCalculator = false;
        this.showBtnCalculator = true;
        this.dataCalculator = { ...result.item };
        this.dataCalculator.results.forEach((element, index) => {
          arrayTable.push({ depth: this.dataCalculator.inputs.depths[index], ...element });
        });
        this.dataCalculator.results = arrayTable;
        this.dataInputs = {
          name: result.item.name,
          ...this.dataCalculator.inputs
        };
        if (!this.showRange) {
          this.dataInputs.depths.forEach((element) => {
            if (element.from) {
              arrayDepths.push({
                depth: element.from,
                obGradient: this.dataInputs.obGradient,
                poissonSRatio: this.dataInputs.poissonSRatio,
                porePressureGradient: this.dataInputs.porePressureGradient,
                fractGradient: this.dataInputs.fractGradient,
                biotsAlpha: this.dataInputs.biotsAlpha
              });
            } else {
              arrayDepths.push({
                depth: element,
                obGradient: this.dataInputs.obGradient,
                poissonSRatio: this.dataInputs.poissonSRatio,
                porePressureGradient: this.dataInputs.porePressureGradient,
                fractGradient: this.dataInputs.fractGradient,
                biotsAlpha: this.dataInputs.biotsAlpha
              });
            }
          });
        } else {
          this.dataInputs.depths.forEach((element) => {
            arrayDepths.push({
              from: element,
              to: element,
              obGradient: this.dataInputs.obGradient,
              poissonSRatio: this.dataInputs.poissonSRatio,
              porePressureGradient: this.dataInputs.porePressureGradient,
              fractGradient: this.dataInputs.fractGradient,
              biotsAlpha: this.dataInputs.biotsAlpha
            });
          });
        }
        this.dataInputs.depths.splice(0, this.dataInputs.depths.length - 1);
        this.dataInputs.depths = arrayDepths;
        this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
      }, 1000);
    }
  }

  /**
   * change value unit conversion from feet to meter & vice-versa
   * @param event event change value
   */
  changeValueFeetToMeter(event) {
    this.txtUnit = event.value;
    const amount = [];
    this.dataInputs.depths.forEach((element) => {
      amount.push(parseFloat(element.depth ? element.depth : 0));
    });
    const data = {
      from: event.value === 'm' ? 'feet' : 'meter',
      to: event.value === 'ft' ? 'feet' : 'meter',
      amount
    };
    this.isLoading = true;
    this.calculatorsSvc
      .convertUnit(data)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((res) => {
        // check if input depth is present
        if (this.dataInputs.depths.length > 0) {
          let index = 0;
          // iterate over depth array and convert from api result
          this.dataInputs.depths.forEach((element, i) => {
            if (element) {
              this.dataInputs.depths[i].depth = res.result[index];
            }
            index++;
          });
        }
      });
  }

  /**
   * change value range checkbox
   */
  changeValueRange(event) {
    this.showRange = event.checked;
    const array = [];
    if (!this.showRange) {
      this.dataInputs.depths.forEach((element) => {
        array.push({
          depth: element.from,
          obGradient: this.dataInputs.obGradient,
          poissonSRatio: this.dataInputs.poissonSRatio,
          porePressureGradient: this.dataInputs.porePressureGradient,
          fractGradient: this.dataInputs.fractGradient,
          biotsAlpha: this.dataInputs.biotsAlpha
        });
      });
    } else {
      this.dataInputs.depths.forEach((element) => {
        array.push({
          from: element.depth,
          to: element.depth,
          obGradient: this.dataInputs.obGradient,
          poissonSRatio: this.dataInputs.poissonSRatio,
          porePressureGradient: this.dataInputs.porePressureGradient,
          fractGradient: this.dataInputs.fractGradient,
          biotsAlpha: this.dataInputs.biotsAlpha
        });
      });
    }
    this.dataInputs.depths.splice(0, this.dataInputs.depths.length - 1);
    this.dataInputs.depths = array;
  }

  /**
   * callback range start
   * @param event event change input range
   * @param index index change input range
   */
  callbackRangeStart(event, index) {
    if (!this.showRange) {
      this.dataInputs.depths[index].depth = event;
    } else {
      this.dataInputs.depths[index].from = event;
    }
  }

  /**
   * callback range end
   * @param event event change input range
   */
  callbackRangeEnd(event, index) {
    if (!this.showRange) {
      this.dataInputs.depths[index].depth = event;
    } else {
      this.dataInputs.depths[index].to = event;
    }
  }

  /**
   * show calculator
   */
  showCalculator() {
    this.showError = false;
    if (
      !this.dataInputs.obGradient ||
      !this.dataInputs.poissonSRatio ||
      !this.dataInputs.porePressureGradient ||
      !this.dataInputs.fractGradient ||
      !this.dataInputs.biotsAlpha
    ) {
      this.showError = true;
    }
    let depths = [];
    if (this.showRange) {
      this.dataInputs.depths.forEach((element) => {
        if (!this.showRange && element === 0) {
          this.showError = true;
        } else if (this.showRange && (element.from === 0 || !element.from) && (element.to === 0 || !element.to)) {
          this.showError = true;
        }
        for (let i = element.from; i < element.to; i += this.selectedIncrementBy) {
          depths.push(i);
        }
        depths.push(element.to);
      });
    } else {
      depths = this.dataInputs.depths;
    }
    if (!this.showError) {
      const dataDepths = [];
      depths.forEach((element) => {
        dataDepths.push(element.depth);
      });
      depths = dataDepths;
      const data: ICreateCalculationData = {
        biotsAlpha: this.dataInputs.biotsAlpha,
        fractGradient: this.dataInputs.fractGradient,
        obGradient: this.dataInputs.obGradient,
        name: this.isUpdate ? this.selectedResult.name : this.dropdownComponent.selectOption,
        poissonSRatio: this.dataInputs.poissonSRatio,
        porePressureGradient: this.dataInputs.porePressureGradient,
        depths
      };
      if (this.txtUnit === 'm') {
        console.log(this.txtUnit);
        this.txtUnit = 'ft';
        const amount = [];
        this.dataInputs.depths.forEach((element) => {
          amount.push(parseFloat(element.depth ? element.depth : 0));
        });
        const dataUnit = {
          from: this.txtUnit === 'm' ? 'feet' : 'meter',
          to: this.txtUnit === 'ft' ? 'feet' : 'meter',
          amount
        };
        this.calculatorsSvc
          .convertUnit(dataUnit)
          .pipe(finalize(() => (this.isLoading = false)))
          .subscribe((res) => {
            // check if input depth is present
            if (this.dataInputs.depths.length > 0) {
              let index = 0;
              // iterate over depth array and convert from api result
              this.dataInputs.depths.forEach((element, i) => {
                if (element) {
                  this.dataInputs.depths[i].depth = res.result[index];
                }
                index++;
              });
            }
            this.isLoading = true;
            if (this.isUpdate) {
              this.calculatorsSvc
                .updateCalculation(this.selectedResult.id, data)
                .pipe(finalize(() => (this.isLoading = false)))
                .subscribe((resp) => {
                  const result = _.each(resp.results, (item: any, index) => {
                    item.depth = depths[index];
                  });
                  this.dataCalculator.results = result;
                  this.showTableCalculator = true;
                  this.showBtnCalculator = true;
                  this.selectedDepths = depths;
                  this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
                });
            } else {
              this.calculatorsSvc
                .createCalculation(data)
                .pipe(finalize(() => (this.isLoading = false)))
                .subscribe((resp) => {
                  const result = _.each(resp.results, (item: any, index) => {
                    item.depth = depths[index];
                  });
                  this.dataCalculator.results = result;
                  this.showTableCalculator = true;
                  this.showBtnCalculator = true;
                  this.selectedDepths = depths;
                  this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
                  this.selectedResult = resp;
                  this.isUpdate = true;
                });
            }
          });
      } else {
        console.log(this.txtUnit);
        this.isLoading = true;
        if (this.isUpdate) {
          this.calculatorsSvc
            .updateCalculation(this.selectedResult.id, data)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((resp) => {
              const result = _.each(resp.results, (item: any, index) => {
                item.depth = depths[index];
              });
              this.dataCalculator.results = result;
              this.showTableCalculator = true;
              this.showBtnCalculator = true;
              this.selectedDepths = depths;
              this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
            });
        } else {
          this.calculatorsSvc
            .createCalculation(data)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((resp) => {
              const result = _.each(resp.results, (item: any, index) => {
                item.depth = depths[index];
              });
              this.dataCalculator.results = result;
              this.showTableCalculator = true;
              this.showBtnCalculator = true;
              this.selectedDepths = depths;
              this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
              this.selectedResult = resp;
              this.isUpdate = true;
            });
        }
      }
    }
  }

  /**
   * add depth
   */
  addDepthFunc() {
    if (!this.showRange) {
      this.dataInputs.depths.push({
        depth: 0,
        obGradient: 0,
        poissonSRatio: 0,
        porePressureGradient: 0,
        fractGradient: 0,
        biotsAlpha: 0
      });
    } else {
      this.dataInputs.depths.push({
        from: 0,
        to: 0,
        obGradient: 0,
        poissonSRatio: 0,
        porePressureGradient: 0,
        fractGradient: 0,
        biotsAlpha: 0
      });
    }
  }

  /**
   * remove depth
   */
  removeDepthFunc() {
    this.dataInputs.depths.pop();
  }

  /**
   * remove Depth Row
   */
  removeDepthRowFunc(i) {
    this.dataInputs.depths.splice(i, 1);
  }

  /**
   * export file to csv
   */
  downloadCsv() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Net Confining Stress',
      useBom: true,
      noDownload: false,
      headers: [
        'Well Name',
        'OB Gradient',
        'Poisson’s Ratio',
        'Pore Pressure Gradient',
        'Fract. Gradient',
        'Depth',
        'Vertical Stress (σV)',
        'Maximum Horizontal stress (σH)',
        'Minimum Horizontal Stress (σh)',
        'Pore Pressure (Pp)',
        'Net Effective Stress (σE)',
        'Teeuw`s min NES (σm)'
      ]
    };
    const result = [];
    _.each(this.dataCalculator.results, (item, index) => {
      result[index] = {};
      result[index].wellName = this.dataInputs.name;
      result[index].obGradient = this.dataInputs.obGradient;
      result[index].poissonSRatio = this.dataInputs.poissonSRatio;
      result[index].porePressureGradient = this.dataInputs.porePressureGradient;
      result[index].fractGradient = this.dataInputs.fractGradient;
      result[index].biotsAlpha = this.dataInputs.biotsAlpha;
      result[index].depth = item.depth;
      result[index]['σV'] = item.V;
      result[index]['H'] = item.H;
      result[index]['h'] = item.h;
      result[index]['P'] = item.P;
      result[index]['E'] = item.E;
      result[index]['m'] = item.m;
    });
    const exportFile = new ngxCsv(result, 'Calculator-Net-Confining-Stress', options);
    return exportFile;
  }

  /**
   * handles the pdf download click
   */
  downloadPdf() {
    const doc = new jsPDF('l', 'pt', 'letter');

    // the titles
    const col = [
      { title: 'Depth' },
      { title: 'Depth Vertical Stress (σV)' },
      { title: 'Maximum Horizontal stress (σH)' },
      { title: 'Minimum Horizontal Stress (σh)' },
      { title: 'Pore Pressure (PP)' },
      { title: 'Net Effective Stress (σ*E)' },
      { title: 'Teeuw`s min NES(σm)' }
    ];
    const rows = [];

    this.dataCalculator.results.forEach((element) => {
      const dataArray = [];
      dataArray.push(element.depth.toFixed(2));
      dataArray.push(element.V.toFixed(2));
      dataArray.push(element.H.toFixed(2));
      dataArray.push(element.h.toFixed(2));
      dataArray.push(element.P.toFixed(2));
      dataArray.push(element.E.toFixed(2));
      dataArray.push(element.m.toFixed(2));
      rows.push(dataArray);
    });

    doc.autoTable(col, rows);
    doc.save(this.getDownloadFileName());
  }

  /**
   * gets the download file name
   * @returns the file name
   */
  getDownloadFileName() {
    const date = new Date();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? `0${month}` : month;
    return `${this.dropdownComponent.selectOption.trim()}${month}${date.getDate()}`;
  }

  /**
   * fetches the well data
   */
  fetch() {
    this.progressBar.show();
    this.calculatorsSvc
      .getWellData(this.dropdownComponent.selectOption.trim())
      .pipe(finalize(() => this.progressBar.hide()))
      .subscribe((res: any) => {
        this.optionsWellName = res.items;
        this.storedOptionsWellName = _.cloneDeep(this.optionsWellName);
      });
  }
}
