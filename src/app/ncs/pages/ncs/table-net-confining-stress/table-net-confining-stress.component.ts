import { Component, Input } from '@angular/core';

@Component({
  selector: 'ncs-table-net-confining-stress',
  templateUrl: './table-net-confining-stress.component.html',
  styleUrls: ['./table-net-confining-stress.component.scss']
})
export class TableNetConfiningStressComponent {
  @Input() data: any[] = [];
  @Input() unit: string;

  // the header columns
  public columns = [
    {
      label: 'Depth',
      tooltip: ''
    },
    {
      label: 'Vertical Stress (σV)',
      tooltip: 'σV = Depth x OB Gradient'
    },
    {
      label: 'Maximum Horizontal stress (σH)',
      tooltip: 'σH = Depth x ((OB Gradient + Fract Gradient)/2)'
    },
    {
      label: 'Minimum Horizontal Stress (σh)',
      tooltip: 'σh = Depth x Fract Gradient'
    },
    {
      label: 'Pore Pressure (PP)',
      tooltip: 'PP = Depth x Pore Pressure Gradient'
    },
    {
      label: 'Net Effective Stress (σ*E)',
      tooltip: 'σE = [(σV+σH+σh)/3] - αPP '
    },
    {
      label: 'Teeuw`s min NES(σm)',
      tooltip: 'σm = [(Pob-Pres)/3]*[(1+n)/(1-n)]'
    }
  ];
}
