import { XrdAlgorithmResult } from '../../core/models';
import { PieChartData } from '../components/pie-chart/pie-chart.component';

export const calculateAverageAloritmResult = (data: XrdAlgorithmResult[]): PieChartData[] => {
  if (!data || !data.length) {
    return [];
  }

  const mineralKeys = data[0].mineralValues.map((x) => x.name);

  return mineralKeys.map((key) => {
    const filteredData = data
      .map((x) => x.mineralValues.find((m) => m.name === key))
      .filter((x) => x && x.value !== undefined && x.value !== null && x.value !== 'NaN')
      .map((x) => parseFloat(x.value));

    const avg = filteredData.reduce((sum, current) => sum + current, 0) / filteredData.length;

    return {
      name: key,
      y: avg
    };
  });
};
