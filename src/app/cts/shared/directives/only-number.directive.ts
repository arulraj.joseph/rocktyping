import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[xrdOnlyNumber]'
})
export class OnlyNumberDirective implements OnInit {
  private regex: RegExp = new RegExp(/^\d*\.?\d{0,99}$/g);
  private specialKeys = ['Backspace', 'Tab', 'End', 'Home', 'ArrowRight', 'ArrowLeft', '-', 'Ctrl'];
  @Input() type: string;
  @Input() ngModel: any;
  constructor(private el: ElementRef) {}

  ngOnInit() {
    if (this.type !== 'normal' && (this.el.nativeElement.value !== '' || this.el.nativeElement.value !== '0')) {
      this.convertNumber();
    }
  }

  /**
   * convert number
   */
  convertNumber() {
    if (this.el.nativeElement.value !== '' || this.el.nativeElement.value !== '0') {
      let valueBeforeDot;
      let valueAfterDot;
      let format = false;
      let indexDots;
      this.el.nativeElement.value.split('').forEach((element, index) => {
        if (element === '.') {
          format = true;
          indexDots = index;
        }
      });
      if (format) {
        valueBeforeDot = this.el.nativeElement.value.slice(0, indexDots);
        valueAfterDot = parseFloat(this.el.nativeElement.value.slice(indexDots, this.el.nativeElement.value.length))
          .toFixed(2)
          .toString();
        valueAfterDot = valueAfterDot.substr(1);
        this.el.nativeElement.value = valueBeforeDot + valueAfterDot;
      }
    }
  }

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if ((event.metaKey && event.key === 'v') || (event.ctrlKey && event.key === 'v')) {
      return true;
    } else if ((event.metaKey && event.key === 'c') || (event.ctrlKey && event.key === 'c')) {
      return true;
    } else if ((event.metaKey && event.key === 'x') || (event.ctrlKey && event.key === 'x')) {
      return true;
    } else if ((event.metaKey && event.key === 'a') || (event.ctrlKey && event.key === 'a')) {
      return true;
    }
    if (this.type === 'normal') {
      console.log(event);
      if (event.which === 64 || event.which === 16) {
        // numbers
        return false;
      }
      if ([8, 13, 27, 37, 38, 39, 40, 110].indexOf(event.which) > -1) {
        // backspace, enter, escape, arrows
        return true;
      } else if (event.which >= 48 && event.which <= 57) {
        // numbers
        return true;
      } else if (event.which >= 96 && event.which <= 105) {
        // numpad number
        return true;
      } else if ([46, 110, 190].indexOf(event.which) > -1) {
        // dot and numpad dot
        return true;
      } else {
        event.preventDefault();
        return false;
      }
    } else {
      // Allow Backspace, tab, end, and home keys
      if (this.specialKeys.indexOf(event.key) !== -1) {
        return;
      }
      const current: string = this.el.nativeElement.value;
      const next: string = current.concat(event.key);
      if (next && !String(next).match(this.regex)) {
        event.preventDefault();
      }
    }
  }

  /**
   * focus input
   */
  @HostListener('focus', ['$event'])
  onFocus(event) {
    if (this.type !== 'normal' && (this.el.nativeElement.value !== '' && this.el.nativeElement.value !== '0')) {
      this.convertNumber();
    }
  }

  /**
   * blur input
   */
  @HostListener('blur', ['$event'])
  onBlur(event) {
    if (this.type !== 'normal' && (this.el.nativeElement.value !== '' && this.el.nativeElement.value !== '0')) {
      this.convertNumber();
    }
  }

  /**
   * change model input
   * @param value value input change
   */
  @HostListener('ngModelChange', ['$event'])
  onNgModelChange(value) {
    this.el.nativeElement.value =
      value.length > 1 && value.charAt(0) === '0' && !(value.charAt(1) === '.') ? value.substr(1) : value;
  }
}
