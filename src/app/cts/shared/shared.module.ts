import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatBadgeModule,
  MatButtonModule,
  MatChipsModule,
  MatDialogModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { HighchartsChartModule } from 'highcharts-angular';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { NanToDashPipe } from './pipes/nan-to-dash.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTableModule,
    HighchartsChartModule,
    MatButtonModule,
    PerfectScrollbarModule,
    MatChipsModule,
    MatIconModule,
    MatTooltipModule,
    MatBadgeModule
  ],
  declarations: [
    SpinnerComponent,
    NanToDashPipe,
    LineChartComponent,
    OnlyNumberDirective
  ],
  exports: [
    SpinnerComponent,
    NanToDashPipe,
    LineChartComponent,
    MatSelectModule,
    MatTabsModule,
    OnlyNumberDirective
  ],
  entryComponents: []
})
export class SharedModule { }
