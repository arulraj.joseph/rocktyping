import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nanToDash'
})
export class NanToDashPipe implements PipeTransform {

  transform (value: string | number) {
    if (typeof value === 'string') {
      return value === 'NaN' ? '-' : value;
    } else {
      return isNaN(value) ? '-' : value;
    }
  }
}
