import { Component, ElementRef, HostListener, Input, OnChanges, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import { ISchedule } from '../../../core/models';

@Component({
  selector: 'xrd-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnChanges {
  @Input() schedules: ISchedule[];
  public dataArea = [];
  public dataChart: any[];
  @ViewChild('svgChart') svgChart: ElementRef;
  constructor(private _el: ElementRef) {}

  /**
   * on changes
   */
  ngOnChanges() {
    this.resizeChart();
  }

  /**
   * handles the resize chart
   */
  resizeChart() {
    const widthContent = this._el.nativeElement.offsetWidth;
    if (this.schedules && this.schedules.length > 0 && widthContent > 0) {
      this.drawChart();
    }
  }

  /**
   * renders a new svg chart in the specified div using the data passed in
   */
  drawChart() {
    this.dataArea = [];
    this.dataChart = [...this.schedules];
    const widthContent = this._el.nativeElement.offsetWidth;
    const heightContent = this._el.nativeElement.offsetHeight - 60;
    // remove svg
    d3.select(this.svgChart.nativeElement.querySelector('*')).remove();
    // setup dimensions based on size of container element
    const margin = { top: 100, right: 45, bottom: 67, left: 86 };
    const width = widthContent - margin.left - margin.right;
    const height = heightContent - margin.top - margin.bottom;
    const svg = d3
      .select(this.svgChart.nativeElement)
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .style('margin', '0 auto')
      .style('display', 'block');
    const g = svg.append('g').attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
    const x = d3.scaleLinear().range([0, width]);
    const y = d3.scaleLinear().range([height, 0]);
    // vertical and horizontal axis
    const xAxis = d3
      .axisBottom(x)
      .tickSizeInner(0)
      .tickSizeOuter(0)
      .ticks(24);
    const yAxis = d3
      .axisLeft(y)
      .ticks(5)
      .tickSizeOuter(0)
      .tickSizeInner(-width)
      .tickFormat(d3.format('~s'));
    let timeTemp = 0;
    let lastItem: any;
    this.dataChart.forEach((d, i) => {
      d.stages = d.stageFrom;
      d.from = d.depthFrom;
      if (i === 0) {
        d.value = 0;
      } else {
        timeTemp += this.schedules[i - 1].time;
        d.value = timeTemp;
      }
      if (i === this.schedules.length - 1) {
        timeTemp += this.schedules[i].time;
        lastItem = {
          stages: d.stageTo,
          from: d.depthTo,
          value: timeTemp
        };
      }
      const array = [];
      if (d.ftH === 0 && i + 1 <= this.schedules.length + 1) {
        array.push(d);
        array.push(this.schedules[i + 1]);
        this.dataArea.push(array);
      }
      return d;
    });
    this.dataChart.push(lastItem);
    console.log(this.dataChart);
    // setup domain axis
    x.domain(d3.extent(this.dataChart, (d) => d.value)).nice();
    y.domain([d3.max(this.dataChart, (d) => d.from) + 2000, 0]);
    // add the x axis
    g.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, -27)')
      .call(customXAxis)
      .selectAll('text')
      .style('text-anchor', 'middle')
      .style('font-family', 'Arial')
      .style('fill', '#00162A')
      .style('font-size', '12px')
      .style('font-weight', '300')
      .style('opacity', 0.5)
      .attr('x', 0)
      .attr('y', 18)
      .attr('dy', 0)
      .attr('transform', 'rotate(0)');
    // add the y axis
    g.append('g')
      .attr('class', 'y axis')
      .call(customYAxis)
      .append('text')
      .attr('x', -(height / 2 - 30))
      .attr('y', -60)
      .attr('dx', 0)
      .attr('dy', 0)
      .style('font-size', '12px')
      .style('font-family', 'Arial')
      .style('fill', '#00162A')
      .style('transform', 'rotate(-90deg)')
      .style('text-anchor', 'center')
      .style('opacity', '0.5')
      .text('Depth, feet');
    // define the line
    const line: any = d3
      .line()
      .x((d: any) => x(d.value))
      .y((d: any) => y(d.from));
    // define the text duration
    const textDuration: any = d3
      .line()
      .x((d: any, i: number) => {
        let xTransition = 0;
        if (i + 1 <= this.dataChart.length - 1) {
          xTransition = (x(d.value) + x(this.dataChart[i + 1].value)) / 2;
        }
        return xTransition - 17.5;
      })
      .y((d: any, i: number) => {
        let yTransition = 0;
        if (i + 1 <= this.dataChart.length - 1) {
          yTransition = (y(d.from) + y(this.dataChart[i + 1].from)) / 2;
        }
        return yTransition;
      });
    // define the area
    const area: any = d3
      .area()
      .x((d: any) => x(d.value))
      .y0(0)
      .y1((d: any) => y(d.from));
    // define the group element
    const gLine = g
      .selectAll('.path')
      .data(this.dataChart)
      .enter()
      .append('g')
      .attr('class', 'group-dot-line');
    // add all the line
    gLine
      .append('line')
      .attr('class', 'line-stage')
      .attr('y1', y(0))
      .attr('y2', (d: any) => y(d.from))
      .attr('transform', (d) => 'translate(' + x(d.value) + ', 0)')
      .style('stroke', 'rgba(175, 189, 209, 0.5)')
      .style('stroke-width', 2);
    if (this.dataArea.length > 0) {
      // add the area
      g.selectAll('area-statges')
        .data(this.dataArea)
        .enter()
        .append('path')
        .attr('class', 'area')
        .style('fill', 'rgba(175, 189, 209, 0.2)')
        .attr('d', area);
    }
    // add the line path
    g.append('path')
      .datum(this.dataChart)
      .attr('d', line)
      .attr('class', 'line')
      .attr('x', 0)
      .attr('stroke-width', 2)
      .attr('stroke', '#006ED2')
      .attr('fill', 'none')
      .attr('height', (d: any) => y(d.from));
    // add all the dots rectangle
    gLine
      .append('rect')
      .attr('class', 'dot')
      .attr('transform', 'translate(-3, -3.5)')
      .attr('fill', '#006ED2')
      .attr('width', '7px')
      .attr('height', '6px')
      .attr('x', line.x())
      .attr('y', line.y());
    // add all text stage
    gLine
      .append('text')
      .attr('class', 'txt-stage')
      .attr('x', line.x())
      .attr('y', -50)
      .attr('transform', 'translate(-4.5, 0)')
      .style('font-family', 'Arial')
      .style('font-size', '16px')
      .style('fill', '#00162A')
      .style('stroke', 'none')
      .text((d) => d.stages);
    // add all the text duration
    gLine
      .append('text')
      .attr('class', 'txt-duration')
      .attr('x', textDuration.x())
      .attr('y', textDuration.y())
      .attr('transform', 'translate(10, 24)')
      .style('font-family', 'Arial')
      .style('font-size', '12px')
      .style('fill', '#00162A')
      .style('stroke', 'none')
      .text((d, i) => {
        let hour = 0;
        if (i + 1 <= this.dataChart.length - 1) {
          hour = this.dataChart[i + 1].value - d.value;
          return hour.toFixed(1) + 'h';
        }
      });
    // add all the images
    gLine
      .append('svg:image')
      .attr('class', 'images')
      .attr('xlink:href', 'assets/images/icon-derrick.svg')
      .attr('transform', 'translate(-10, -46)')
      .attr('x', line.x())
      .attr('y', 0)
      .attr('width', '20px')
      .attr('height', '20px');

    /**
     * custom style x axis
     * @param gTemp the gTemp
     */
    function customXAxis(gTemp: any) {
      gTemp.call(xAxis);
      gTemp.selectAll('.axis path').attr('stroke', '#AFBDD1');
    }

    /**
     * custom style y axis
     * @param gTemp the gTemp
     */
    function customYAxis(gTemp: any) {
      gTemp.call(yAxis);
      gTemp.selectAll('.axis .tick line').attr('stroke', '#D8D8D8');
      gTemp
        .selectAll('.axis .tick text')
        .attr('x', -10)
        .style('font-family', 'Arial')
        .attr('font-size', '12px')
        .attr('fill', '#00162A')
        .attr('display', 'block');
      gTemp.selectAll('.axis path').attr('display', 'none');
    }
  }

  /**
   * handles the windows resize
   * @param event the event
   */
  @HostListener('window:resize', ['$event']) onResize(event) {
    this.resizeChart();
  }
}
