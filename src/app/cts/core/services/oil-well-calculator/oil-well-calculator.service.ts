import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { ICTSData, IUnitConverted, IOilProperties, IOilWellsCalculator, ISchedule, IWellsDetails } from '../../models';

const TIMEOUT = 500;

@Injectable({
  providedIn: 'root'
})
export class OilWellCalculatorService {
  private url = 'assets/data';
  constructor(private httpClient: HttpClient) {}

  public getAllWellsCalculator(searchText: string = ''): Observable<IOilWellsCalculator[]> {
    //  TODO: change mock data to real service
    return this.httpClient
      .get(`/${this.url}/OilWellCalculator.json`)
      .pipe(delay(TIMEOUT))
      .pipe(
        map((ret: any) => {
          return ret.filter((item) => {
            if (!searchText) {
              return item;
            }
            return item.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1;
          });
        })
      );
  }

  public getDataTripSchedule(): Observable<ISchedule[]> {
    //  TODO: change mock data to real service
    return this.httpClient.get<ISchedule[]>(`/${this.url}/TripSchedule.json`).pipe(delay(TIMEOUT));
  }

  public getDataColumnTripSchedule(): Observable<any[]> {
    //  TODO: change mock data to real service
    return this.httpClient.get<any[]>(`/${this.url}/ColumnTableTripSchedule.json`).pipe(delay(TIMEOUT));
  }

  public getAllDataDropdown(): Observable<any> {
    //  TODO: change mock data to real service
    return this.httpClient.get(`/${this.url}/DataDropdown.json`).pipe(delay(TIMEOUT));
  }

  public getWellDetails(): Observable<IWellsDetails[]> {
    //  TODO: change mock data to real service
    return this.httpClient.get<IWellsDetails[]>(`/${this.url}/WellDetails.json`);
  }

  public getOilProperties(): Observable<IOilProperties[]> {
    //  TODO: change mock data to real service
    return this.httpClient.get<IOilProperties[]>(`/${this.url}/OilProperties.json`);
  }

  /**
   * gets the well data
   * @returns The observable for the HTTP request.
   */
  public getWellData() {
    return this.httpClient.get<string[]>(`{CTS_SERVICE}/services/cts/lookups/wells`);
  }

  /**
   * creates the query string
   * @param options the criteria
   */
  private createQueryString(criteria) {
    const params = new URLSearchParams();
    for (const key in criteria) {
      if (criteria[key]) {
        params.set(key, criteria[key]);
      }
    }
    return params.toString();
  }

  /**
   * gets the cts data
   * @param criteria the criteria
   * @returns The observable for the HTTP request.
   */
  public getCTSData(criteria) {
    return this.httpClient.get<ICTSData>(`{CTS_SERVICE}/services/cts/data?${this.createQueryString(criteria)}`);
  }

  /**
   * gets the location data
   * @returns The observable for the HTTP request.
   */
  public getLocationData() {
    return this.httpClient.get<string[]>(`{CTS_SERVICE}/services/cts/lookups/locations`);
  }

  /**
   * converts the unit
   * @param data the request data
   * @returns The observable for the HTTP request.
   */
  public convertUnit(data) {
    return this.httpClient.post<IUnitConverted>(`{UNIT_CONVERTER_SERVICE}/services/convert`, data);
  }

  /**
   * creates the cts data
   * @param data the data
   * @returns The observable for the HTTP request.
   */
  createCTSData(data) {
    return this.httpClient.post(`{CTS_SERVICE}/services/cts/data`, data);
  }

  /**
   * updates the cts data
   * @param id the id
   * @param data the data
   * @returns The observable for the HTTP request.
   */
  updateCTSData(id, data) {
    return this.httpClient.put(`{CTS_SERVICE}/services/cts/data/${id}`, data);
  }
}
