import { TestBed } from '@angular/core/testing';

import { OilWellCalculatorService } from './oil-well-calculator.service';

describe('OilWellCalculatorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OilWellCalculatorService = TestBed.get(OilWellCalculatorService);
    expect(service).toBeTruthy();
  });
});
