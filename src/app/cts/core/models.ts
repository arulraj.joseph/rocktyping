export interface IWellsDetails {
  label: string;
  value: string | number;
  unit?: string;
  realValue?: string;
}

export interface IOilProperties {
  pressure: number;
  liberates: number;
}

export interface ISchedule {
  stageFrom?: number;
  stageTo?: number;
  depthFrom?: number;
  depthTo?: number;
  minOrStand?: number;
  ftH?: number;
  stands?: number;
  time?: number;
}

export interface IUnitConverted {
  result: number[];
  created_at: string;
  from: string;
  to: string;
  amount: number[];
}

export interface ICTSData {
  total: number;
  page: number;
  pageSize: number;
  items: IScheduleItem[];
}

export interface IScheduleItem {
  createdAt: string;
  updatedAt: string;
  id: string;
  externalId: string;
  name: string;
  location: string;
  input: IWellDetailsInput;
  output: IOutput;
}

export interface IOutput {
  simulated_stands: ISimulatedStand[];
  trip_stages: ITripStage[];
}

export interface ITripStage {
  stage_index: number;
  depth_from_ft: number;
  depth_to_ft: number;
  velocity_min_per_stand: number;
  feet_per_h: number;
  num_stands: number;
  total_time_h: number;
  time_hr: number;
}

export interface ISimulatedStand {
  velocity_min_per_stand: number;
  mud_weight: number;
  core_top_depth_ft: number;
  bit_depth_ft: number;
  mud_pressure: number;
  pore_pressure: number;
  delta_p_thermal: number;
  delta_p: number;
  gas_volume1: number;
  gas_volume2: number;
  gas_extraction_speed: number;
  produced_gas_vol: number;
  gas_volume_corr: number;
  pressure_corr_psig: number;
  liberated_gor_perc: number;
}

export interface IWellDetailsInput {
  coring_depth_ft: number;
  core_run_ft: number;
  core_d_in: number;
  mud_weight_surf: number;
  mud_weight_bt: number;
  mud_temp_surf_f: number;
  mud_temp_bt_f: number;
  stand_ft: number;
  kg_md: number;
  diam_perc: number;
  sw_perc: number;
  viscosity_max: number;
  bubble_point_psi: number;
  liberated_gor_by_pressure: number[][];
  tensile_strength_psi: number;
}

export interface IOilWellsCalculator {
  id: number;
  name: string;
}
