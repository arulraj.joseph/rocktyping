import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule, MatRadioModule, MatTabsModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CURRENCY_MASK_CONFIG, CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';
import {
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarModule
} from 'ngx-perfect-scrollbar';
import { AutocompleteWellsNameComponent } from './pages/cts/autocomplete-wells-name/autocomplete-wells-name.component';
import { CtsComponent } from './pages/cts/cts/cts.component';
import { DropdownComponent } from './pages/cts/dropdown/dropdown.component';
import { OilPropertiesComponent } from './pages/cts/oil-properties/oil-properties.component';
import { RowPropertiesComponent } from './pages/cts/row-properties/row-properties.component';
import { TripScheduleComponent } from './pages/cts/trip-schedule/trip-schedule.component';
import { WellsDetailsComponent } from './pages/cts/wells-details/wells-details.component';
import { SharedModule } from '../shared/shared.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'left',
  allowNegative: false,
  decimal: '.',
  precision: 1,
  prefix: '',
  suffix: '',
  thousands: ','
};

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    MatTabsModule,
    MatButtonModule,
    PerfectScrollbarModule,
    MatInputModule,
    MatRadioModule,
    CurrencyMaskModule,
    SharedModule
  ],
  declarations: [
    CtsComponent,
    WellsDetailsComponent,
    OilPropertiesComponent,
    TripScheduleComponent,
    AutocompleteWellsNameComponent,
    DropdownComponent,
    RowPropertiesComponent
  ],
  exports: [
    CtsComponent,
    WellsDetailsComponent,
    OilPropertiesComponent,
    TripScheduleComponent,
    AutocompleteWellsNameComponent,
    DropdownComponent,
    RowPropertiesComponent
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: CURRENCY_MASK_CONFIG,
      useValue: CustomCurrencyMaskConfig
    }
  ]
})
export class CtsModule {}
