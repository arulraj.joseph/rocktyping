import { Component, Input, OnChanges } from '@angular/core';
import { ISchedule } from '../../../../core/models';

@Component({
  selector: 'cts-trip-schedule',
  templateUrl: './trip-schedule.component.html',
  styleUrls: ['./trip-schedule.component.scss']
})
export class TripScheduleComponent implements OnChanges {
  @Input() schedules: ISchedule[];
  @Input() columns: any[];
  public totalTime: number = 0;

  ngOnChanges() {
    if (this.schedules) {
      this.totalTime = 0;
      this.schedules.forEach((item) => {
        this.totalTime += item.time;
      });
    }
  }
}
