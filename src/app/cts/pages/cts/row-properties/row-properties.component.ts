import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'cts-row-properties',
  templateUrl: './row-properties.component.html',
  styleUrls: ['./row-properties.component.scss']
})
export class RowPropertiesComponent {
  @Input() item: any = {};
  @Input() index: number;
  public isEdit = false;
  public liberates: number;
  public pressure: number;
  @Output() callback = new EventEmitter();

  /**
   * on key input
   */
  onKeydown(event) {
    if (
      // Allow: Delete, Backspace, Tab
      [46, 8, 9].indexOf(event.keyCode) !== -1 ||
      (event.keyCode === 65 && event.ctrlKey === true) || // Allow: Ctrl+A
      (event.keyCode === 67 && event.ctrlKey === true) || // Allow: Ctrl+C
      (event.keyCode === 86 && event.ctrlKey === true) || // Allow: Ctrl+V
      (event.keyCode === 88 && event.ctrlKey === true) || // Allow: Ctrl+X
      (event.keyCode === 65 && event.metaKey === true) || // Cmd+A (Mac)
      (event.keyCode === 67 && event.metaKey === true) || // Cmd+C (Mac)
      (event.keyCode === 86 && event.metaKey === true) || // Cmd+V (Mac)
      (event.keyCode === 88 && event.metaKey === true) || // Cmd+X (Mac)
      (event.keyCode >= 35 && event.keyCode <= 39) // Home, End, Left, Right
    ) {
      return; // let it happen, don't do anything
    }
    // Ensure that it is a number and stop the keypress
    if ((event.shiftKey || (event.keyCode < 48 || event.keyCode > 57)) && (event.keyCode < 96 || event.keyCode > 105)) {
      event.preventDefault();
    }

    if (event.key === 'Enter') {
      this.save();
    } else if (event.key === 'Escape') {
      this.item.pressure = this.pressure;
      this.item.liberates = this.liberates;
      this.isEdit = false;
    }
  }

  /**
   * edit row
   */
  edit() {
    this.isEdit = true;
    this.liberates = this.item.liberates;
    this.pressure = this.item.pressure;
  }

  /**
   * save
   */
  save() {
    if (this.item.pressure === '') {
      this.item.pressure = 0;
    }
    if (this.item.liberates === '') {
      this.item.liberates = 0;
    }
    this.isEdit = false;
    this.callback.emit({ event: 'change', index: this.index, item: this.item });
  }

  /**
   * delete
   */
  delete() {
    this.callback.emit({ event: 'delete', index: this.index });
  }
}
