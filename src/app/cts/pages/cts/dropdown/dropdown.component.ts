import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Component({
  selector: 'cts-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent {
  @Input() data: any[] = [];
  @Input() selectOption = 'Select';
  @Input() type: string;
  @Output() callback = new EventEmitter();
  constructor(private el: ElementRef) {}

  // show dropdown
  showDropdownFunc(event: any) {
    if (this.data.length > 0) {
      if (event.currentTarget.parentElement.classList.contains('show-dropdown')) {
        event.currentTarget.parentElement.classList.remove('show-dropdown');
      } else {
        event.currentTarget.parentElement.classList.add('show-dropdown');
      }
    }
  }

  /**
   * select item dropdown
   */
  selectItem(item): void {
    this.el.nativeElement.querySelector('.group-dropdown').classList.remove('show-dropdown');
    this.selectOption = item.name;
    this.callback.emit(item);
  }

  /**
   * reset dropdown
   */
  resetDropdown(val) {
    this.selectOption = val;
  }

  // click body
  @HostListener('document:click', ['$event.target'])
  onClick(targetElement): void {
    if (!this.el.nativeElement.contains(targetElement)) {
      this.el.nativeElement.querySelector('.group-dropdown').classList.remove('show-dropdown');
    }
  }
}
