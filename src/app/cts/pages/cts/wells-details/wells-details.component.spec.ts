import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WellsDetailsComponent } from './wells-details.component';

describe('WellsDetailsComponent', () => {
  let component: WellsDetailsComponent;
  let fixture: ComponentFixture<WellsDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WellsDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WellsDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
