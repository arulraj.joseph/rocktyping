import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { IWellsDetails } from '../../../../core/models';
import { OilWellCalculatorService } from '../../../../core/services/oil-well-calculator/oil-well-calculator.service';

@Component({
  selector: 'cts-wells-details',
  templateUrl: './wells-details.component.html',
  styleUrls: ['./wells-details.component.scss']
})
export class WellsDetailsComponent implements OnChanges {
  public form: FormGroup;
  public fluid = [{ name: 'Oil' }];
  public submitted = false;
  public querySearch = '';
  public txtUnit = 'feet';
  public isLoading = false;
  @Input() listWellsDetails: IWellsDetails[];
  @Output() callback = new EventEmitter();
  @Output() selectedValues = new EventEmitter();

  constructor(private oilWellCalSvc: OilWellCalculatorService, private fb: FormBuilder) {}

  /**
   * on changes
   */
  ngOnChanges() {
    this.form = this.fb.group({
      data: this.fb.array([])
    });
    const control = this.form.get('data') as FormArray;
    if (this.listWellsDetails && this.listWellsDetails.length > 0) {
      this.listWellsDetails.forEach((element) => {
        control.push(
          this.fb.group({
            label: [element.label],
            value: [element.value, [Validators.required]],
            unit: [element.unit ? element.unit : this.txtUnit]
          })
        );
      });
    }
  }

  /**
   * change value unit conversion from feet to meter & vice-versa
   * @param event event change value unit
   */
  changeValueFeetToMeter(event) {
    this.isLoading = true;
    const control = this.form.get('data') as FormArray;
    const amount = [];
    // get all values to be converted
    _.each(control.controls, (item: any, index) => {
      if (
        (index !== 6 && item.controls.label.value === 'Coring Depth (MD)') ||
        item.controls.label.value === 'Core Run' ||
        item.controls.label.value === 'Core Diameter' ||
        item.controls.label.value === 'RIG STAND LENGTH'
      ) {
        amount.push(parseFloat(item.controls.value.value));
      }
    });
    const body = {
      from: event.value === 'meter' ? 'feet' : 'meter',
      to: event.value === 'meter' ? 'meter' : 'feet',
      amount
    };
    this.oilWellCalSvc
      .convertUnit(body)
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((res) => {
        const amountControl = _.filter(
          control.controls,
          (i) =>
            i.value.label === 'Coring Depth (MD)' ||
            i.value.label === 'Core Run' ||
            i.value.label === 'Core Diameter' ||
            i.value.label === 'RIG STAND LENGTH'
        );
        // convert the values according to the result
        amountControl.forEach((element: any, i) => {
          element.controls.unit.setValue(event.value);
          element.controls.value.value = element.controls.value.value.toString();
          element.controls.value.value = parseFloat(res.result[i].toString().replace(/,/g, ''));
          element.controls.value.setValue(res.result[i].toFixed(2));
        });
        this.selectedValues.emit(_.each(res.result, (i: any) => (i = i.toFixed(2))));
      });
  }

  /**
   * callback dropdown
   * @param event the event
   * @param item the item
   */
  callbackDropdown(event, item) {
    item.controls.value.setValue(event.name);
  }

  /**
   * handles the submit schedule
   */
  submit() {
    this.submitted = true;
    this.form.value.data.forEach((element) => {
      if (element.value === '') {
        element.value = 0;
      }
      // convert string to number value
      element.value = parseFloat(element.value.toString().replace(',', ''));
      if (element.label === 'Coring Depth (MD)' && element.value !== 0 && element.value !== '') {
        this.submitted = false;
      }
    });
    if (!this.submitted) {
      this.listWellsDetails = [...this.form.value.data];
      this.callback.emit({ data: this.listWellsDetails });
    }
  }

  /**
   * handles the reset click
   */
  reset() {
    this.querySearch = '';
  }
}
