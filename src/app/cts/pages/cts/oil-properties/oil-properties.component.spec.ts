import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OilPropertiesComponent } from './oil-properties.component';

describe('OilPropertiesComponent', () => {
  let component: OilPropertiesComponent;
  let fixture: ComponentFixture<OilPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OilPropertiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OilPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
