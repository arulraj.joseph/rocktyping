import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IOilProperties } from '../../../../core/models';

@Component({
  selector: 'cts-oil-properties',
  templateUrl: './oil-properties.component.html',
  styleUrls: ['./oil-properties.component.scss']
})
export class OilPropertiesComponent {
  @Input() data: IOilProperties[] = [];
  @Input() total = 0;
  @Output() callback = new EventEmitter();

  /**
   * callback row properties
   * @param result result row properties
   */
  callbackRowProperties(result) {
    this.callback.emit(result);
  }

  /**
   * add row function
   */
  addRowFunc() {
    this.callback.emit({ event: 'add' });
  }
}
