import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatInputModule, MatRadioModule, MatTabsModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CURRENCY_MASK_CONFIG, CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';
import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SharedModule } from '../../shared/shared.module';
import { AutocompleteWellsNameComponent } from './autocomplete-wells-name/autocomplete-wells-name.component';
import { CtsComponent } from './cts/cts.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { OilPropertiesComponent } from './oil-properties/oil-properties.component';
import { RowPropertiesComponent } from './row-properties/row-properties.component';
import { TripScheduleComponent } from './trip-schedule/trip-schedule.component';
import { WellsDetailsComponent } from './wells-details/wells-details.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'left',
  allowNegative: false,
  decimal: '.',
  precision: 1,
  prefix: '',
  suffix: '',
  thousands: ','
};

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    RouterModule,
    MatTabsModule,
    MatButtonModule,
    PerfectScrollbarModule,
    MatInputModule,
    MatRadioModule,
    CurrencyMaskModule
  ],
  declarations: [
    CtsComponent,
    WellsDetailsComponent,
    OilPropertiesComponent,
    TripScheduleComponent,
    AutocompleteWellsNameComponent,
    DropdownComponent,
    RowPropertiesComponent
  ],
  exports: [
    CtsComponent,
    WellsDetailsComponent,
    OilPropertiesComponent,
    TripScheduleComponent,
    AutocompleteWellsNameComponent,
    DropdownComponent,
    RowPropertiesComponent
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: CURRENCY_MASK_CONFIG,
      useValue: CustomCurrencyMaskConfig
    }
  ]
})
export class CtsPageModule { }
