import { Component, OnInit, ViewChild } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import * as _ from 'lodash';
import { ngxCsv } from 'ngx-csv';
import { finalize } from 'rxjs/operators';
import { AppConstants } from '../../../../app.constants';
import { ProgressBarService } from '../../../../core/services/progress-bar/progress-bar.service';
import { IItem, IOilProperties, ISchedule, IWellsDetails } from '../../../../core/models';
import { OilWellCalculatorService } from '../../../../core/services/oil-well-calculator/oil-well-calculator.service';
import { LineChartComponent } from '../../../../shared/components/line-chart/line-chart.component';
import { AutocompleteWellsNameComponent } from '../autocomplete-wells-name/autocomplete-wells-name.component';
import { WellsDetailsComponent } from '../wells-details/wells-details.component';

@Component({
  selector: 'cts-cts',
  templateUrl: './cts.component.html',
  styleUrls: ['./cts.component.scss']
})
export class CtsComponent implements OnInit {
  public listLocation: any[] = [];
  public listOilProperties: IOilProperties[];
  public listWellsDetails: IWellsDetails[];
  public schedules: ISchedule[];
  public columns = AppConstants.CTSDataColumns;
  public isLoading: boolean;
  public labelWellSearch: string;
  public totalPressure = 0;
  public txtTab = 'Wells Details';
  public searchQueryString: string = '';
  public dataOilProperties = [];
  public wellData: any[] = [];
  public storedData: any[] = [];
  public selectedLocation: string = '';
  public isUpdate = true;
  public selectedItem = null;
  @ViewChild('wellsDetails') wellsDetailsEl: WellsDetailsComponent;
  @ViewChild('lineChart') lineChartEl: LineChartComponent;
  @ViewChild(AutocompleteWellsNameComponent) autocompleteWellsNameComponent: AutocompleteWellsNameComponent;
  constructor(private oilWellCalSvc: OilWellCalculatorService, private progressBar: ProgressBarService) {}

  /**
   * on init
   */
  ngOnInit() {
    const wellsDetails: IWellsDetails[] = new Array<IWellsDetails>();
    // add text box predefined values
    wellsDetails.push({ label: 'Coring Depth (MD)', value: 0 });
    wellsDetails.push({ label: 'Core Run', value: 0 });
    wellsDetails.push({ label: 'Core Diameter', value: 0, unit: 'inches' });
    wellsDetails.push({ label: 'MW (SURF)', value: 0 });
    wellsDetails.push({ label: 'MUD SURFACE TEMPERATURE', value: 0 });
    wellsDetails.push({ label: 'MW (BOTTOM)', value: 0 });
    wellsDetails.push({ label: 'FLUID', value: 0 });
    wellsDetails.push({ label: 'RIG STAND LENGTH', value: 0 });
    wellsDetails.push({ label: 'KG', value: 0 });
    wellsDetails.push({ label: 'Ø', value: 0 });
    wellsDetails.push({ label: 'SW', value: 0 });
    wellsDetails.push({ label: 'BUBBLE POINT', value: 0 });
    wellsDetails.push({ label: 'GOR', value: 0 });
    wellsDetails.push({ label: 'TENSILE STRENGTH', value: 0 });
    wellsDetails.push({ label: 'VISCOSITY', value: 0 });
    wellsDetails.push({ label: 'Mud Tem', value: 0 });
    this.listWellsDetails = wellsDetails;
    this.listOilProperties = [];
    this.initData();
  }

  /**
   * initialize the data
   */
  initData() {
    this.schedules = undefined;
  }

  /**
   * callback oil properties component
   */
  callbackOilProperties(result) {
    this.totalPressure = 0;
    if (result.event === 'delete') {
      this.listOilProperties.splice(result.index, 1);
    } else if (result.event === 'add') {
      this.listOilProperties.push({
        pressure: 0,
        liberates: 0
      });
    } else if (result.event === 'change') {
      result.item.liberates = parseFloat(result.item.liberates);
      this.listOilProperties[result.index] = result.item;
    }
    this.listOilProperties.forEach((element) => {
      this.totalPressure += element.liberates;
    });
  }

  /**
   * selected tab change
   * @param event the event
   */
  selectedTabChange(event) {
    this.txtTab = event.tab.textLabel;
  }

  /**
   * handles the reset button click
   */
  reset() {
    // reset tab wells details
    this.wellsDetailsEl.reset();
    this.listWellsDetails = [...this.listWellsDetails];
    this.listWellsDetails = _.cloneDeep(this.listWellsDetails);
    // reset tab oil properties
    this.totalPressure = 0;
    this.listOilProperties = [...this.dataOilProperties];
    this.listOilProperties.forEach((element) => {
      this.totalPressure += element.liberates;
    });
    this.dataOilProperties = _.cloneDeep(this.listOilProperties);
  }

  /**
   * callback wells details
   */
  callbackWellDetails(event) {
    this.listWellsDetails = [...event.data];
  }

  /**
   * load data schedule
   */
  loadDataSchedule() {
    let check = false;
    this.wellsDetailsEl.submit();
    this.listWellsDetails.forEach((element: any) => {
      if (element.label === 'Coring Depth (MD)' && element.value !== 0 && element.value !== '') {
        check = true;
      }
      if (element.label !== 'FLUID') {
        element.value = parseFloat(element.value);
      }
    });
    if (check) {
      this.isLoading = true;
      const liberated_gor_by_pressure = [];
      _.each(this.listOilProperties, (item) => {
        const arrayData = [];
        arrayData.push(item.pressure);
        arrayData.push(item.liberates);
        liberated_gor_by_pressure.push(arrayData);
      });
      if (this.autocompleteWellsNameComponent.querySearch.trim() === '') {
        window.alert('Please provide name');
        return;
      }
      this.isLoading = true;
      const body = {
        bubble_point_psi: this.getLabelValue('BUBBLE POINT'),
        core_d_in: this.getLabelValue('Core Diameter'),
        core_run_ft: this.getLabelValue('Core Run'),
        coring_depth_ft: this.getLabelValue('Coring Depth (MD)'),
        diam_perc: this.getLabelValue('Ø'),
        kg_md: this.getLabelValue('KG'),
        liberated_gor_by_pressure,
        location: this.selectedLocation,
        mud_temp_bt_f: this.getLabelValue('Mud Tem'),
        mud_temp_surf_f: this.getLabelValue('MUD SURFACE TEMPERATURE'),
        mud_weight_bt: this.getLabelValue('MW (BOTTOM)'),
        mud_weight_surf: this.getLabelValue('MW (SURF)'),
        name: this.autocompleteWellsNameComponent.querySearch.trim(),
        stand_ft: this.getLabelValue('RIG STAND LENGTH'),
        sw_perc: this.getLabelValue('SW'),
        tensile_strength_psi: this.getLabelValue('TENSILE STRENGTH'),
        viscosity_max: this.getLabelValue('VISCOSITY')
      };
      if (this.isUpdate) {
        this.oilWellCalSvc
          .updateCTSData(this.selectedItem.id, body)
          .pipe(finalize(() => (this.isLoading = false)))
          .subscribe((res: any) => {
            this.isUpdate = true;
            this.createTableDataResult(res.output.trip_stages);
          });
      } else {
        this.oilWellCalSvc
          .createCTSData(body)
          .pipe(finalize(() => (this.isLoading = false)))
          .subscribe((res: any) => {
            this.createTableDataResult(res.output.trip_stages);
          });
      }
    }
  }

  /**
   * creates the table data result
   * @param result the result
   */
  createTableDataResult(result) {
    const data = _.map(result, (i) => {
      return {
        depthFrom: i.depth_from_ft,
        depthTo: i.depth_to_ft,
        ftH: i.feet_per_h,
        minOrStand: i.velocity_min_per_stand,
        stageFrom: i.stage_index,
        stageTo: i.stage_index + 1,
        stands: i.num_stands,
        time: i.time_hr
      };
    });
    this.schedules = data;
  }

  /**
   * gets the value filtering by label
   * @param label the label
   */
  getLabelValue(label) {
    return this.listWellsDetails.find((x) => x.label === label).value;
  }

  /**
   * handles the values change
   * @param data the data
   */
  getSelectedValues(data) {
    this.storedData = data;
  }

  /**
   * on change tab
   * @param tab the selected tab
   */
  onChangeRightTab(tab) {
    if (tab.index === 1 && this.lineChartEl) {
      this.lineChartEl.resizeChart();
    }
  }

  /**
   * on select Well Name
   * @param item the selected well
   */
  onSelectWellName(item: IItem) {
    this.isUpdate = true;
    this.selectedItem = item;
    // reset tab wells details
    this.wellsDetailsEl.reset();
    this.wellsDetailsEl.txtUnit = 'feet';
    this.selectedLocation = item.location;
    const result: IOilProperties[] = new Array<IOilProperties>();
    item.input.liberated_gor_by_pressure.forEach((element) => {
      const newData: IOilProperties = {
        pressure: element[0],
        liberates: element[1]
      };
      result.push(newData);
    });
    this.listOilProperties = result;
    this.listOilProperties.forEach((element) => {
      this.totalPressure += element.liberates;
    });
    this.dataOilProperties = _.cloneDeep(result);

    const wellsDetails: IWellsDetails[] = new Array<IWellsDetails>();
    // add text box predefined values
    wellsDetails.push({ label: 'Coring Depth (MD)', value: item.input.coring_depth_ft });
    wellsDetails.push({ label: 'Core Run', value: item.input.core_run_ft });
    wellsDetails.push({ label: 'Core Diameter', value: item.input.core_d_in });
    wellsDetails.push({ label: 'MW (SURF)', value: item.input.mud_weight_surf });
    wellsDetails.push({ label: 'MUD SURFACE TEMPERATURE', value: item.input.mud_temp_surf_f });
    wellsDetails.push({ label: 'MW (BOTTOM)', value: item.input.mud_weight_bt });
    wellsDetails.push({ label: 'FLUID', value: '' });
    wellsDetails.push({ label: 'RIG STAND LENGTH', value: item.input.stand_ft });
    wellsDetails.push({ label: 'KG', value: item.input.kg_md });
    wellsDetails.push({ label: 'Ø', value: item.input.diam_perc });
    wellsDetails.push({ label: 'SW', value: item.input.sw_perc });
    wellsDetails.push({ label: 'BUBBLE POINT', value: item.input.bubble_point_psi });
    wellsDetails.push({ label: 'GOR', value: '' });
    wellsDetails.push({ label: 'TENSILE STRENGTH', value: item.input.tensile_strength_psi });
    wellsDetails.push({ label: 'VISCOSITY', value: item.input.viscosity_max });
    wellsDetails.push({ label: 'Mud Tem', value: item.input.mud_temp_bt_f });
    this.listWellsDetails = wellsDetails;
  }

  /**
   * handles the well search
   * @param searchText the search text
   */
  searchWell(searchText) {
    this.isUpdate = false;
    this.wellData = [];
    this.searchQueryString = searchText;
    this.schedules = undefined;
    this.autocompleteWellsNameComponent.querySearch.trim() !== ''
      ? (this.autocompleteWellsNameComponent.showListWells = true)
      : (this.autocompleteWellsNameComponent.showListWells = false);

    // get first selected well data
    if (this.autocompleteWellsNameComponent.querySearch.trim() !== '') {
      this.progressBar.show();
      this.oilWellCalSvc
        .getCTSData({ name: searchText })
        .pipe(finalize(() => this.progressBar.hide()))
        .subscribe((data) => {
          this.wellData = data.items;
        });
    }
  }

  /**
   * handles the location change
   * @param item the selected item
   */
  selectLocation(item) {
    this.selectedLocation = item.name;
  }

  /**
   * handles the csv download
   */
  downloadCsv() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Net Confining Stress',
      useBom: true,
      noDownload: false,
      headers: ['STAGES', 'FROM (ft)', 'TO (ft)', '(MIN/STAND)', '(ft/h)', '# STANDS', 'TIME (hr)']
    };
    const data: any = _.cloneDeep(this.schedules);
    const csvData = _.map(data, (i) => {
      return {
        stages: `${i.stageFrom}-${i.stageTo}`,
        depthFrom: i.depthFrom,
        depthTo: i.depthTo,
        minOrStand: i.minOrStand,
        ftH: i.ftH,
        stands: i.stands,
        time: parseFloat(i.time).toFixed(2)
      };
    });
    // keep empty values for column gap
    const totalTimeRow: any = {
      label: 'TOTAL TRIP TIME',
      label1: '',
      label2: '',
      label3: '',
      label4: '',
      label5: '',
      total: this.getTotalTime()
    };
    csvData.push(totalTimeRow);
    const exportFile = new ngxCsv(csvData, this.getDownloadFileName(), options);
    return exportFile;
  }

  /**
   * handles the pdf download click
   */
  downloadPdf() {
    const doc = new jsPDF();

    // the titles
    const col = [
      { title: 'STAGES' },
      { title: 'FROM (ft)' },
      { title: 'TO (ft)' },
      { title: '(MIN/STAND)' },
      { title: '(ft/h)' },
      { title: '# STANDS' },
      { title: 'TIME (hr)' }
    ];
    const rows = [];

    this.schedules.forEach((element) => {
      const dataArray = [];
      dataArray.push(`${element.stageFrom}-${element.stageTo}`);
      dataArray.push(element.depthFrom);
      dataArray.push(element.depthTo);
      dataArray.push(element.minOrStand);
      dataArray.push(element.ftH);
      dataArray.push(element.stands);
      dataArray.push(element.time.toFixed(2));
      rows.push(dataArray);
    });
    const totalArray = [];
    // add last total row
    totalArray.push('TOTAL TRIP TIME');
    totalArray.push('');
    totalArray.push('');
    totalArray.push('');
    totalArray.push('');
    totalArray.push('');
    totalArray.push(this.getTotalTime());
    rows.push(totalArray);

    doc.autoTable(col, rows);
    doc.save(this.getDownloadFileName());
  }

  /**
   * gets the download file name
   * @returns the file name
   */
  getDownloadFileName() {
    const date = new Date();
    let month: any = date.getMonth() + 1;
    month = month < 10 ? `0${month}` : month;
    return `${this.autocompleteWellsNameComponent.querySearch.trim()}${month}${date.getDate()}`;
  }

  /**
   * gets the total time
   * @returns the total time
   */
  getTotalTime() {
    let totalTime = 0;
    this.schedules.forEach((item) => {
      totalTime += item.time;
    });
    return totalTime.toFixed(2);
  }
}
