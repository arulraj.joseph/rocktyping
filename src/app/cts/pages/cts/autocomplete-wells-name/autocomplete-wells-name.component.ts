import { Component, EventEmitter, HostListener, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'cts-autocomplete-wells-name',
  templateUrl: './autocomplete-wells-name.component.html',
  styleUrls: ['./autocomplete-wells-name.component.scss']
})
export class AutocompleteWellsNameComponent implements OnChanges {
  public querySearch = '';
  public listWellsName: string[];
  public showListWells = false;

  @Input() data: string[] = [];
  @Input() searchQueryString: string = '';
  @Output() callback = new EventEmitter();
  @Output() searchWell = new EventEmitter();

  /**
   * on changes
   * @param changes the changes
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.data && changes.data.currentValue) {
      this.data = changes.data.currentValue;
      this.querySearch = this.searchQueryString;
      this.listWellsName = _.cloneDeep(this.data);
    }
  }

  /**
   * handles the search
   */
  searchQuery() {
    this.search();
  }

  /**
   * handles the select value item
   * @param item the selected item
   */
  selectValueFunc(item) {
    this.querySearch = item.name;
    this.showListWells = false;
    this.callback.emit(item);
  }

  /**
   * highlight text search
   * @param text the text
   * @param search the search text
   */
  highlightText = (text, search) => {
    let pattern = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    pattern = pattern
      .split(' ')
      .filter((t) => {
        return t.length > 0;
      })
      .join('|');
    const regex = new RegExp(pattern, 'gi');
    return search
      ? text.replace(regex, (match) => {
          return `<span class='highlight'>${match}</span>`;
        })
      : text;
  };

  /**
   * search data
   */
  search() {
    this.searchWell.emit(this.querySearch);
  }

  /**
   * handles the click body
   * @param event the event
   */
  @HostListener('document:click', ['$event'])
  clickBody(event) {
    this.showListWells = false;
  }
}
