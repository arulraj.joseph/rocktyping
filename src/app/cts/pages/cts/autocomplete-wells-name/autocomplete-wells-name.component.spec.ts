import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteWellsNameComponent } from './autocomplete-wells-name.component';

describe('AutocompleteWellsNameComponent', () => {
  let component: AutocompleteWellsNameComponent;
  let fixture: ComponentFixture<AutocompleteWellsNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteWellsNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteWellsNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
