import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ConfigService } from '@apc-ng/core';
import { of } from 'rxjs';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  let configService: ConfigService;
  const mockConfigService = {
    setAppVersion: (version) => {},
    setAppName: (name) => {},
    getAppProperties: () => {
      return of({});
    }
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: ConfigService, useValue: mockConfigService }],
      declarations: [AppComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    configService = fixture.debugElement.injector.get(ConfigService);
  });

  it('should create the app', async(() => {
    expect(component).toBeTruthy();
  }));
});
