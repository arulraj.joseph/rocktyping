import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthenticationService } from '@apc-ng/core';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authenticationService: AuthenticationService) {}

  canActivate(): Observable<boolean> {
    return this.authenticationService.getAuthEvents().pipe(
      map((event) => this.isEventValid(event)),
      filter((event) => event === true),
      take(1)
    );
  }

  private isEventValid(event) {
    return !event.expired && !event.failed;
  }
}
