import { APP_BASE_HREF } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Menu } from '@apc-ng/theme-material';
import { BROOKS_COREY_ROUTES } from '../brooks-corey/brooks-corey.routs';
import { CORE_INVENTORY_ROUTES } from '../core-inventory/core-inventory.routs';
import { UNITS_CONVERTER_ROUTES } from '../units-converter/units-converter.routs';
import { KLINKENBERG_ROUTES } from '../klinkenberg/klinkenberg.routs';
import { XRD_ROUTES } from '../xrd/xrd.routs';
import { AuthGuard } from './auth.guard';
import { WellSelectedGuard } from './well-selected.guard';
import { RoutingPreloader } from './routing-preloader';
import { CalculatorsComponent } from '../calculators/pages/calculators/calculators/calculators.component';
import { DashboardComponent } from '../dashboard/dashboard/dashboard.component';
import { BlackOilPropertiesComponent } from '../pvt/pages/pvt/black-oil-properties/black-oil-properties.component';
import { NetConfiningStressComponent } from '../ncs/pages/ncs/net-confining-stress/net-confining-stress.component';
import { routes as IdemgRoutes } from '../idemg/idemg.routes';
import { CtsComponent } from '../cts/pages/cts/cts/cts.component';
import { ELECTRIC_ROUTES } from '../electric/electric.routs';

export const navItems: Menu[] = [
  // Icons can be found here: https://materialdesignicons.com/
  {
    title: 'Select a Well',
    type: 'link',
    svgIcon: 'anadarko-logo',
    path: '/',
    data: {
      code: 'dashboard'
    }
  },
  {
    title: 'XRD',
    type: 'link',
    svgIcon: 'xrd',
    path: 'xrd',
    data: {
      code: 'xrd'
    }
  },
  {
    title: 'Oilfield Unit Converter',
    type: 'link',
    svgIcon: 'unit-converter',
    path: 'units-converter',
    data: {
      code: 'unitconverter'
    }
  },
  {
    title: 'Litho Tracker',
    type: 'link',
    svgIcon: 'litho-tracker',
    path: 'litho-tracker',
    data: {
      code: 'coreinventory'
    }
  },
  {
    title: 'Klinkenberg',
    type: 'link',
    iconClass: 'material-icons',
    iconCode: 'bubble_chart',
    path: 'klinkenberg',
    data: {
      code: 'klinkenberg'
    }
  },
  {
    title: 'Calculators',
    type: 'link',
    iconClass: 'material-icons',
    iconCode: 'exposure',
    path: 'calculators',
    data: {
      code: 'calculators'
    }
  },
  {
    title: 'Brooks Corey',
    type: 'link',
    svgIcon: 'brooks-corey',
    path: 'brooks-corey',
    data: {
      code: 'brookscorey'
    }
  },
  {
    title: 'PVT Calculator',
    type: 'link',
    iconClass: 'icons icon-pvt',
    iconCode: '',
    path: 'pvt',
    data: {
      code: 'pvt'
    }
  },
  {
    title: 'NCS Calculator',
    type: 'link',
    iconClass: 'material-icons',
    iconCode: 'exposure',
    path: 'ncs',
    data: {
      code: 'ncs'
    }
  },
  {
    title: 'Core Trip Schedule Stimulator',
    type: 'link',
    iconClass: 'material-icons',
    iconCode: 'blur_linear',
    path: 'cts',
    data: {
      code: 'cts'
    }
  },
  {
    title: 'Electric',
    type: 'link',
    iconClass: 'material-icons',
    iconCode: 'flash_on',
    path: 'electric',
    data: {
      code: 'electric'
    }
  },
  {
    title: 'IDEMG',
    type: 'link',
    iconClass: 'material-icons',
    iconCode: 'blur_linear',
    path: 'idemg',
    data: {
      code: 'idemg'
    }
  }
];

export const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
  {
    path: 'xrd',
    children: XRD_ROUTES
  },
  {
    path: 'units-converter',
    children: UNITS_CONVERTER_ROUTES
  },
  {
    path: 'litho-tracker',
    children: CORE_INVENTORY_ROUTES,
    canActivate: [WellSelectedGuard]
  },
  {
    path: 'klinkenberg',
    children: KLINKENBERG_ROUTES,
    canActivate: []
  },
  {
    path: 'cts',
    component: CtsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'calculators',
    component: CalculatorsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'calculators/:tabId',
    component: CalculatorsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'brooks-corey',
    children: BROOKS_COREY_ROUTES
  },
  {
    path: 'cts',
    component: CtsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'pvt',
    component: BlackOilPropertiesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'ncs',
    component: NetConfiningStressComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'electric',
    children: ELECTRIC_ROUTES
  },
  ...IdemgRoutes
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: false,
      enableTracing: false,
      preloadingStrategy: RoutingPreloader
    })
  ],
  providers: [AuthGuard, WellSelectedGuard, { provide: APP_BASE_HREF, useValue: '/' }, RoutingPreloader],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {}
