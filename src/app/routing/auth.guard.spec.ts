import { async, getTestBed, TestBed } from '@angular/core/testing';
import { AuthenticationService } from '@apc-ng/core';
import { of } from 'rxjs';
import { AuthGuard } from './auth.guard';

describe('Service: AuthGuard', () => {
  let service: AuthGuard;
  let authenticationService: AuthenticationService;
  const mockAuthService = {
    getAuthEvents: of({})
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard, { provide: AuthenticationService, useValue: mockAuthService }]
    });
    service = getTestBed().get(AuthGuard);
    authenticationService = getTestBed().get(AuthenticationService);
  }));

  it('should create the service', () => {
    expect(service).toBeTruthy();
  });

  it('should return false when expired', async(() => {
    spyOn(authenticationService, 'getAuthEvents').and.returnValue(of({ expired: true, failed: false }));
    service.canActivate().subscribe((result) => {
      expect(result).toBe(false);
    });
  }));

  it('should return false when failed', async(() => {
    spyOn(authenticationService, 'getAuthEvents').and.returnValue(of({ expired: false, failed: true }));
    service.canActivate().subscribe((result) => {
      expect(result).toBe(false);
    });
  }));

  it('should return true when not expired and not failed', async(() => {
    spyOn(authenticationService, 'getAuthEvents').and.returnValue(of({ expired: false, failed: false }));
    service.canActivate().subscribe((result) => {
      expect(result).toBe(true);
    });
  }));
});
