import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CurrentWellService } from '../shared/services/current-well.service';

@Injectable()
export class WellSelectedGuard implements CanActivate {
  constructor(private router: Router, private currentWellService: CurrentWellService) {}

  canActivate(): Observable<boolean> {
    return this.currentWellService.currentWell.pipe(
      map((data) => {
        if (data) {
          return true;
        }

        this.router.navigate(['/']);

        return false;
      })
    );
  }
}
