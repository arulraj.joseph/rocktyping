import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IFile, ISample } from '../models';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  constructor(private httpClient: HttpClient) {}

  getAllFiles = (): Observable<IFile[]> =>
    this.httpClient.get<IFile[]>(`{CORE_INVENTORY_SERVICE}/services/database/all`);

  uploadFile = (file: File): Observable<IFile> => {
    const body = new FormData();
    body.append('file', file);
    return this.httpClient.post<IFile>(`{CORE_INVENTORY_SERVICE}/services/database/save`, body);
  };
  removeFile = (id: string): Observable<IFile[]> =>
    this.httpClient.delete<IFile[]>(`{CORE_INVENTORY_SERVICE}/services/database/${id}`);

  downloadFile = (id: string): Observable<Blob> =>
    this.httpClient.get(`{CORE_INVENTORY_SERVICE}/services/database/download/${id}`, { responseType: 'blob' });

  getSamplesFromFile = (fileId: string): Observable<{ databaseFileName: string; samples: ISample[] }> =>
    this.httpClient.get<{ databaseFileName: string; samples: ISample[] }>(
      `{CORE_INVENTORY_SERVICE}/services/sample/${fileId}`
    );

  updateSample = (sample: ISample): Observable<ISample> =>
    this.httpClient.put<ISample>(`{CORE_INVENTORY_SERVICE}/services/sample/${sample.id}`, sample);
}
