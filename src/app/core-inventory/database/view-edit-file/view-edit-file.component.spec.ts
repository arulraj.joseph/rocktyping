import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEditFileComponent } from './view-edit-file.component';

describe('ViewEditFileComponent', () => {
  let component: ViewEditFileComponent;
  let fixture: ComponentFixture<ViewEditFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ViewEditFileComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEditFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
