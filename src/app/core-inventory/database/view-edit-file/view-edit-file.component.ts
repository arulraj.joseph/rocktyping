import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { first } from 'rxjs/internal/operators/first';
import { finalize } from 'rxjs/operators';
import { ConfirmRemoveDialogComponent } from '../../../shared/components/confirm-remove-dialog/confirm-remove-dialog.component';
import { DownloadService } from '../../../shared/services/download.service';
import { ISample } from '../../models';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'ci-view-edit-file',
  templateUrl: './view-edit-file.component.html',
  styleUrls: ['./view-edit-file.component.scss']
})
export class ViewEditFileComponent implements OnInit {
  fileName: string;
  fileId: string;
  columnDefs = [
    { headerName: 'Well Number', field: 'wellNumber', editable: true },
    { headerName: 'UWI', field: 'uwi', editable: true },
    { headerName: 'Lab', field: 'labName', editable: true },
    { headerName: 'Location', field: 'labLocation', editable: true },
    { headerName: 'Sample Title', field: 'title', editable: true },
    { headerName: 'Sample Type', field: 'type', editable: true }
  ];

  samples: ISample[];

  editedRows: { [key: string]: ISample } = {};
  editedRowsQuantity: number = 0;
  loading: boolean;

  messageMapping: { [k: string]: string } = { '=1': 'record edited', other: 'records edited' };

  constructor(
    private route: ActivatedRoute,
    private databaseService: DatabaseService,
    private dialog: MatDialog,
    private downloadService: DownloadService,
    private router: Router
  ) {}

  ngOnInit() {
    this.fileId = this.route.snapshot.paramMap.get('fileId');
    this.databaseService.getSamplesFromFile(this.fileId).subscribe((data) => {
      this.fileName = data.databaseFileName;
      this.samples = data.samples;
    });
  }
  onCellEditingStopped(event) {
    if (event.oldValue !== event.newValue) {
      this.editedRows[event.data.id] = event.data;
      this.editedRowsQuantity = Object.keys(this.editedRows).length;
    }
  }
  updateSamples() {
    this.loading = true;
    combineLatest(
      Object.keys(this.editedRows).map((sampleId) => this.databaseService.updateSample(this.editedRows[sampleId]))
    )
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe(() => this.router.navigateByUrl('/litho-tracker/database'));
  }
  removeFile() {
    event.preventDefault();
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmRemoveDialogComponent, {
      data: { name: this.fileName }
    });

    dialogRef.afterClosed().subscribe((isConfirmed: boolean) => {
      if (isConfirmed) {
        this.loading = true;
        this.databaseService
          .removeFile(this.fileId)
          .pipe(
            first(),
            finalize(() => (this.loading = false))
          )
          .subscribe(() => this.router.navigateByUrl('/litho-tracker/database'));
      }
    });
  }
  downloadFile() {
    event.preventDefault();
    event.stopPropagation();
    this.databaseService
      .downloadFile(this.fileId)
      .pipe(
        first(),
        finalize(() => (this.loading = false))
      )
      .subscribe((data) => {
        this.downloadService.downloadFile(data, this.fileName);
      });
  }
}
