import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable, ReplaySubject } from 'rxjs';
import { finalize, switchMap, tap } from 'rxjs/operators';
import { ConfirmRemoveDialogComponent } from '../../../shared/components/confirm-remove-dialog/confirm-remove-dialog.component';
import { DownloadService } from '../../../shared/services/download.service';
import { IFile } from '../../models';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'ci-update-database',
  templateUrl: './update-database.component.html',
  styleUrls: ['./update-database.component.scss']
})
export class UpdateDatabaseComponent implements OnInit {
  loadFiles$: ReplaySubject<void> = new ReplaySubject(1);
  files$: Observable<IFile[]> = this.loadFiles$.pipe(
    tap(() => (this.loading = true)),
    switchMap(() => this.databaseService.getAllFiles()),
    tap(() => (this.loading = false)),
    finalize(() => (this.loading = false))
  );
  loading: boolean;
  displayedColumns: string[] = ['filename', 'numberOfRecords', 'lastModifiedDate', 'view', 'download', 'delete'];
  constructor(
    private databaseService: DatabaseService,
    public dialog: MatDialog,
    private downloadService: DownloadService
  ) {}

  ngOnInit(): void {
    this.loadFiles();
  }

  public upload(file: File) {
    this.loading = true;
    this.databaseService.uploadFile(file).subscribe(() => this.loadFiles$.next());
  }
  loadFiles() {
    this.loadFiles$.next();
  }
  removeFile(file: IFile) {
    event.preventDefault();
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmRemoveDialogComponent, {
      data: { name: file.filename }
    });

    dialogRef.afterClosed().subscribe((isConfirmed: boolean) => {
      if (isConfirmed) {
        this.loading = true;
        this.databaseService.removeFile(file.id).subscribe(() => this.loadFiles$.next());
      }
    });
  }
  downloadFile(file: IFile) {
    event.preventDefault();
    event.stopPropagation();
    this.databaseService.downloadFile(file.id).subscribe((data) => {
      this.downloadService.downloadFile(data, file.filename);
    });
  }
}
