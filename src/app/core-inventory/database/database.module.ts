import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { AgGridModule } from 'ag-grid-angular';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatTableModule,
  MatTooltipModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SharedModule } from '../../shared/shared.module';
import { DatabasePageComponent } from './database-page/database-page.component';
import { UpdateDatabaseComponent } from './update-database/update-database.component';
import { ViewEditFileComponent } from './view-edit-file/view-edit-file.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AgGridModule.withComponents(null),
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatMenuModule,
    PerfectScrollbarModule,
    MatIconModule,
    MatTooltipModule,
    FontAwesomeModule,
    SharedModule
  ],
  declarations: [DatabasePageComponent, UpdateDatabaseComponent, ViewEditFileComponent],
  exports: [DatabasePageComponent, ViewEditFileComponent]
})
export class DatabaseModule {
  constructor() {
    library.add(faDownload);
  }
}
