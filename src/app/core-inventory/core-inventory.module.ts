import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatabaseModule } from './database/database.module';
import { SearchModule } from './search/search.module';

@NgModule({
  imports: [CommonModule, SearchModule, DatabaseModule]
})
export class CoreInventoryModule {}
