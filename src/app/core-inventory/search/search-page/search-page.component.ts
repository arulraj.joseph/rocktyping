import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'ci-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent {
  searchParamsIsExist$: Observable<boolean> = this.route.queryParams.pipe(
    map((queryParams) => !!queryParams.wellNumber || !!queryParams.UWI)
  );

  constructor(private route: ActivatedRoute) {}
}
