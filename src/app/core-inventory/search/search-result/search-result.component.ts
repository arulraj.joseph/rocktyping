import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/internal/operators/first';
import { switchMap, tap } from 'rxjs/operators';
import { DownloadService } from '../../../shared/services/download.service';
import { ISample } from '../../models';
import { SearchService } from '../search.service';

@Component({
  selector: 'ci-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent {
  loading: boolean = false;
  fileName: string;

  searchResult$: Observable<ISample[]> = this.route.queryParams.pipe(
    tap(() => setTimeout(() => (this.loading = true))),
    switchMap(({ wellNumber, UWI }) => this.searchService.getSearchResult(wellNumber, UWI)),
    tap(() => (this.loading = false))
  );
  displayedColumns: string[] = ['wellNumber', 'uwi', 'labName', 'labLocation', 'title', 'type'];

  constructor(
    private searchService: SearchService,
    private route: ActivatedRoute,
    private downloadService: DownloadService
  ) {}

  exportSearchResult() {
    this.route.queryParams
      .pipe(
        first(),
        tap(({ wellNumber, UWI }) => (this.fileName = `wellNumber=${wellNumber}_UWI=${UWI}.xlsx`)),
        switchMap(({ wellNumber, UWI }) => this.searchService.exportSearchResult(wellNumber, UWI))
      )
      .subscribe((data) => {
        this.downloadService.downloadFile(data, this.fileName);
      });
  }
}
