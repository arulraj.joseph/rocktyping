import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IPagedResult, ISearch, ISample } from '../models';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  constructor(private httpClient: HttpClient) {}

  getRecentSearches = (params: { pageNumber: number; pageSize: number }): Observable<IPagedResult<ISearch>> =>
    this.httpClient.get<IPagedResult<ISearch>>(
      `{CORE_INVENTORY_SERVICE}/services/search/recent?page=${params.pageNumber}&pageSize=${params.pageSize}`
    );

  getSearchResult = (wellNumber: string, uwi: string): Observable<ISample[]> =>
    this.httpClient.post<ISample[]>(`{CORE_INVENTORY_SERVICE}/services/sample`, { uwi, wellNumber });

  exportSearchResult = (wellNumber: string, uwi: string): Observable<Blob> =>
    this.httpClient.post(
      `{CORE_INVENTORY_SERVICE}/services/sample/export`,
      { uwi, wellNumber },
      { observe: 'body', responseType: 'blob' }
    );
}
