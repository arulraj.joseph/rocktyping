import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ci-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit, OnDestroy {
  showClearBtn: boolean = false;
  searchForm: FormGroup = this.fb.group({
    wellNumber: new FormControl(''),
    UWI: new FormControl('')
  });

  routeSubscription: Subscription;

  constructor(private fb: FormBuilder, private route: ActivatedRoute) {}

  ngOnInit() {
    this.routeSubscription = this.route.queryParams.subscribe(({ wellNumber, UWI }) => {
      this.showClearBtn = !!wellNumber || !!UWI;
      this.searchForm.setValue({
        wellNumber: wellNumber || '',
        UWI: UWI || ''
      });
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }
}
