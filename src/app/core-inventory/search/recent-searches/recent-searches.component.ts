import { Component } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, map, switchMap, scan, publishReplay, refCount } from 'rxjs/operators';
import { IPagedResult, ISearch } from '../../models';
import { SearchService } from '../search.service';

const PAGE_SIZE = 10;

@Component({
  selector: 'ci-recent-searches',
  templateUrl: './recent-searches.component.html',
  styleUrls: ['./recent-searches.component.scss']
})
export class RecentSearchesComponent {
  loading: boolean = false;
  pageNumber$: BehaviorSubject<number> = new BehaviorSubject(0);

  data$: Observable<IPagedResult<ISearch>> = this.pageNumber$.pipe(
    tap(() => (this.loading = true)),
    switchMap((pageNumber) => this.searchService.getRecentSearches({ pageNumber, pageSize: PAGE_SIZE })),
    tap(() => (this.loading = false)),
    publishReplay(1),
    refCount()
  );

  recentSearches$: Observable<ISearch[]> = this.data$.pipe(
    map((resp) => resp.results),
    scan((acc, curr) => [...acc, ...curr], [])
  );

  showMoreBtn$: Observable<boolean> = this.data$.pipe(
    map((resp) => Boolean(resp.total > (this.pageNumber$.value + 1) * PAGE_SIZE))
  );

  displayedColumns: string[] = ['wellNumber', 'uwi', 'labNames', 'totalSamples', 'createdDate', 'details'];

  constructor(private searchService: SearchService) {}

  showMore() {
    this.pageNumber$.next(this.pageNumber$.value + 1);
  }
}
