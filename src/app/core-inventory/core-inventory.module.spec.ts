import { CoreInventoryModule } from './core-inventory.module';

describe('CoreInventoryModule', () => {
  let coreInventoryModule: CoreInventoryModule;

  beforeEach(() => {
    coreInventoryModule = new CoreInventoryModule();
  });

  it('should create an instance', () => {
    expect(coreInventoryModule).toBeTruthy();
  });
});
