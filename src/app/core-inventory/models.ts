export interface IPagedResult<T> {
  currentPage: number;
  perPage: number;
  total: number;
  totalPages: number;
  results: T[];
}

export interface ISearch {
  createdBy: string;
  lastModifiedBy: string;
  createdDate: string;
  lastModifiedDate: string;
  wellNumber: string;
  uwi: string;
  labNames: string[];
  totalSamples: number;
}
export interface ISample {
  id: string;
  createdBy: string;
  lastModifiedBy: string;
  createdDate: string;
  lastModifiedDate: string;
  wellNumber: string;
  uwi: string;
  databaseFileId: string;
  labName: string;
  labLocation: string;
  title: string;
  type: string;
}
export interface IFile {
  id: string;
  createdBy: string;
  lastModifiedBy: string;
  createdDate: string;
  lastModifiedDate: string;
  filename: string;
  numberOfRecords: number;
}
