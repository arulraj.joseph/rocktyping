import { AuthGuard } from '../routing/auth.guard';
import { DatabasePageComponent } from './database/database-page/database-page.component';
import { ViewEditFileComponent } from './database/view-edit-file/view-edit-file.component';
import { SearchPageComponent } from './search/search-page/search-page.component';

export const CORE_INVENTORY_ROUTES = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full'
  },
  {
    path: 'search',
    component: SearchPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'database',
    component: DatabasePageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'database',
    children: [
      {
        path: '',
        component: DatabasePageComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'file/:fileId',
        component: ViewEditFileComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];
