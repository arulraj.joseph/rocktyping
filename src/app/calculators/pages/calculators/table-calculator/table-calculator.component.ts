import { Component, Input } from '@angular/core';

@Component({
  selector: 'xrd-table-calculator',
  templateUrl: './table-calculator.component.html',
  styleUrls: ['./table-calculator.component.scss']
})
// class of TableCalculatorComponent
export class TableCalculatorComponent {
  @Input() data: any[] = [];
}
