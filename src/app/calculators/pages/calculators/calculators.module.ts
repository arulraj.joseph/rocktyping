import {CommonModule, DecimalPipe} from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatRadioModule, MatTooltipModule } from '@angular/material';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CURRENCY_MASK_CONFIG, CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';
import { SharedModule } from '../../shared/shared.module';
import { BlackOilPropertiesComponent } from './black-oil-properties/black-oil-properties.component';
import { CalculatorsComponent } from './calculators/calculators.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { InputRangeComponent } from './input-range/input-range.component';
import { NetConfiningStressComponent } from './net-confining-stress/net-confining-stress.component';
import { RowBlackOilPropertiesComponent } from './row-black-oil-properties/row-black-oil-properties.component';
import { TableCalculatorComponent } from './table-calculator/table-calculator.component';
import { TableNetConfiningStressComponent } from './table-net-confining-stress/table-net-confining-stress.component';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  decimal: '.',
  precision: 0,
  prefix: '',
  suffix: '',
  thousands: ','
};

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatRadioModule,
    MatTooltipModule,
    CurrencyMaskModule
  ],
  declarations: [
    CalculatorsComponent,
    TableCalculatorComponent,
    RowBlackOilPropertiesComponent,
    DropdownComponent,
    BlackOilPropertiesComponent,
    NetConfiningStressComponent,
    InputRangeComponent,
    TableNetConfiningStressComponent
  ],
  providers: [
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
    DecimalPipe
  ]
})
export class CalculatorsPageModule { }
