import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackOilPropertiesComponent } from './black-oil-properties.component';

describe('BlackOilPropertiesComponent', () => {
  let component: BlackOilPropertiesComponent;
  let fixture: ComponentFixture<BlackOilPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackOilPropertiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackOilPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
