import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { finalize } from 'rxjs/operators';
import { CalculatorsService } from '../../../core/services/calculators/calculators.service';
import { DropdownComponent } from '../dropdown/dropdown.component';

@Component({
  selector: 'xrd-black-oil-properties',
  templateUrl: './black-oil-properties.component.html',
  styleUrls: ['./black-oil-properties.component.scss']
})
export class BlackOilPropertiesComponent implements OnInit {
  public optionsWellName: any[] = [];
  public selectedWellName = null;
  public dataTableCalculators: any[];
  public showTableCalculator = false;
  public showBtnCalculator = false;
  public querySearch = '';
  public isLoading = false;
  public form: FormGroup;
  public submitted = false;
  public isUpdate = false;
  public selectedData = null;
  @Output() callback = new EventEmitter();

  @ViewChild(DropdownComponent) dropdownComponent: DropdownComponent;

  constructor(private calculatorsService: CalculatorsService, private fb: FormBuilder) {}

  /**
   * on init
   */
  ngOnInit() {
    this.form = this.fb.group({
      data: this.fb.array([])
    });
  }

  /**
   * gets the array data
   */
  get arrayData() {
    return this.form.get('data') as FormArray;
  }

  /**
   * handles callback dropdown
   * @param result the result
   */
  callbackDropdown(result) {
    if (result.event === 'search') {
      if (result.query.trim() !== '') {
        this.selectedData = null;
      }
      this.isUpdate = result.query.trim() === '' ? true : false;
      this.querySearch = result.query;
      this.fetch();
    } else if (result.event === 'select') {
      this.showTableCalculator = false;
      this.showBtnCalculator = false;
      this.isUpdate = true;
      this.isLoading = true;
      this.calculatorsService
        .searchPVTData(result.item.name)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe((res) => {
          this.selectedData = res.items[0];
          this.clearFormArray(this.arrayData);
          res.items[0].input.forEach((element) => {
            this.arrayData.push(
              this.fb.group({
                inputs: this.fb.group({
                  sampleId: [element.sampleNumber, Validators.required],
                  p: [element.reservoirPressure, Validators.required],
                  t: [element.reservoirTemperature, Validators.required],
                  sga: [element.oilGravity, Validators.required],
                  sgg: [element.specificGasGravity, Validators.required],
                  pb: [element.oilBPPressure, Validators.required],
                  rs: [element.solutionGOR, Validators.required],
                  rsb: [element.solutionGORPb, Validators.required],
                  tds: [element.waterSalinity, Validators.required],
                  yH2S: [element.h2sMoleFraction, Validators.required],
                  yCO2: [element.co2MoleFraction, Validators.required],
                  yN2: [element.n2MoleFraction, Validators.required]
                })
              })
            );
          });
          this.showTableCalculator = true;
          this.dataTableCalculators = res.items[0].output;

          if (this.form.invalid) {
            return;
          }
          this.showBtnCalculator = true;
          this.callback.emit({ showBtn: this.showBtnCalculator });
        });
    }
  }

  /**
   * clear form array
   * @param formArray the form array
   */
  clearFormArray(formArray: FormArray) {
    while (formArray.length !== 0) {
      formArray.removeAt(0);
    }
  }

  /**
   * handles callback back row black oil properties
   * @param event the event
   */
  callbackRowBlackOilProperties(event) {
    if (this.form.invalid) {
      return;
    }
    this.showBtnCalculator = event;
    this.callback.emit({ showBtn: this.showBtnCalculator });
  }

  /**
   * add sample id
   */
  addSampleID() {
    this.arrayData.push(
      this.fb.group({
        inputs: this.fb.group({
          sampleId: [null, Validators.required],
          p: [0, Validators.required],
          t: [0, Validators.required],
          sga: [0, Validators.required],
          sgg: [0, Validators.required],
          pb: [0, Validators.required],
          rs: [0, Validators.required],
          rsb: [0, Validators.required],
          tds: [0, Validators.required],
          yH2S: [0, Validators.required],
          yCO2: [0, Validators.required],
          yN2: [0, Validators.required]
        })
      })
    );
  }

  /**
   * show calculator
   */
  showCalculator() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }

    this.dataTableCalculators = [];
    this.showTableCalculator = true;
    const data = _.cloneDeep(this.form.value);
    const requestData = [];
    _.each(data.data, (element) => {
      const elementData = {
        sampleNumber: element.inputs.sampleId,
        reservoirPressure: element.inputs.p,
        reservoirTemperature: element.inputs.t,
        oilGravity: element.inputs.sga,
        specificGasGravity: element.inputs.sgg,
        oilBPPressure: element.inputs.pb,
        solutionGOR: element.inputs.rs,
        solutionGORPb: element.inputs.rsb,
        waterSalinity: element.inputs.tds,
        h2sMoleFraction: element.inputs.yH2S,
        co2MoleFraction: element.inputs.yCO2,
        n2MoleFraction: element.inputs.yN2
      };
      requestData.push(elementData);
    });
    const body = {
      input: requestData,
      name: this.isUpdate ? this.selectedData.name : this.dropdownComponent.selectOption
    };
    this.isLoading = true;

    // if its already selected well then update
    if (this.isUpdate) {
      this.calculatorsService
        .updatePVT(this.selectedData.id, body)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe((res: any) => {
          this.dataTableCalculators = res.output;
        });
    } else {
      this.calculatorsService
        .createPVT(body)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe((res: any) => {
          this.dataTableCalculators = res.output;
        });
    }
  }

  /**
   * export file to csv
   */
  exportFileCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Black Oil Properties',
      useBom: true,
      noDownload: false,
      headers: [
        'Sample ID',
        'z-factor',
        'Oil Density',
        'Oil Compressibility',
        'Oil Viscosity',
        'GOR',
        'Bo',
        'Bw',
        'GWR',
        'Water Density',
        'Water Compressibility',
        'Water Viscosity',
        'Gas Viscosity'
      ]
    };
    const array = [];
    this.dataTableCalculators.forEach((element: any) => {
      const data: any = {};
      data.sampleId = element.sampleNumber;
      data.zFactor = element.zFactor;
      data.density = element.oilDensity;
      data.oilCompressibility = element.oilCompressibility;
      data.oilViscosity = element.oilViscosity;
      data.gor = element.gor;
      data.bo = element.bo;
      data.bw = element.bw;
      data.gwr = element.gwr;
      data.waterDensity = element.waterDensity;
      data.waterCompressibility = element.waterCompressibility;
      data.viscosity = element.waterViscosity;
      data.gasViscosity = element.gasViscosity;
      array.push(data);
    });
    const exportFile = new ngxCsv(array, 'Calculator-Black-Oil-Properties', options);
    return exportFile;
  }

  /**
   * fetches the well data
   */
  fetch() {
    this.isLoading = true;
    this.calculatorsService
      .getWellLookupData(this.dropdownComponent.selectOption.trim())
      .pipe(finalize(() => (this.isLoading = false)))
      .subscribe((res) => {
        this.optionsWellName = _.map(res, (i) => {
          return {
            name: i
          };
        });
      });
  }

  /**
   * Remove current properties row
   */
  removePropertiesRow(index: number) {
    (this.form.get('data') as FormArray).removeAt(index);
  }
}
