import { DecimalPipe } from '@angular/common';
import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { ngxCsv } from 'ngx-csv';
import { finalize } from 'rxjs/operators';
import { ProgressBarService } from '../../../../shared/services/progress-bar/progress-bar.service';
import { ICreateCalculationData } from '../../../core/models';
import { CalculatorsService } from '../../../core/services/calculators/calculators.service';
import { DropdownComponent } from '../dropdown/dropdown.component';
import { TableNetConfiningStressComponent } from '../table-net-confining-stress/table-net-confining-stress.component';

@Component({
  selector: 'xrd-net-confining-stress',
  templateUrl: './net-confining-stress.component.html',
  styleUrls: ['./net-confining-stress.component.scss']
})
export class NetConfiningStressComponent implements OnInit {
  // options select or type
  optionsWellName: any[] = [];
  storedOptionsWellName: any[] = [];
  selectedWellName = null;
  // options select increment
  optionsIncrementBy = [];
  selectedIncrementBy = 1;
  // range depth
  startDepth = 0;
  endDepth = 0;
  dataCalculator: any = {};
  showRange = false;
  showTableCalculator = false;
  showBtnCalculator = false;
  dataInputs: any = {};
  querySearch = '';
  txtUnit = 'ft';
  showError = false;
  selectedDepths: any[] = [];
  isUpdate = false;
  selectedResult: any = {};
  @Output() callback = new EventEmitter();
  isLoading = false;

  @ViewChild(DropdownComponent) dropdownComponent: DropdownComponent;
  @ViewChild('inputList') inputList: ElementRef;
  @ViewChild(TableNetConfiningStressComponent) ncsTable: TableNetConfiningStressComponent;

  constructor(
    private calculatorsSvc: CalculatorsService,
    private progressBar: ProgressBarService,
    private decimalPipe: DecimalPipe
  ) {}

  /**
   * on init
   */
  ngOnInit() {
    this.calculatorsSvc.getAllIncrementBy().subscribe((res) => {
      this.optionsIncrementBy = res;
    });
    if (!this.dataInputs.depths) {
      if (!this.showRange) {
        this.dataInputs.depths = [0];
      } else {
        this.dataInputs.depths = [
          {
            from: 0,
            to: 0
          }
        ];
      }
    }
  }

  /**
   * callback dropdown
   * @param result the result
   */
  callbackDropdown(result) {
    this.querySearch = result.query;
    if (result.event === 'search') {
      this.isUpdate = false;
      if (this.querySearch.trim() === '') {
        this.showTableCalculator = false;
        this.showBtnCalculator = false;
        this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
      } else {
        this.showBtnCalculator = true;
        this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
      }
      this.fetch();
      this.showBtnCalculator = true;
    } else if (result.event === 'select') {
      this.progressBar.show();
      setTimeout(() => {
        this.progressBar.hide();
        this.selectedResult = { ...result.item };
        this.isUpdate = true;
        const arrayDepths = [];
        const arrayTable = [];
        this.showTableCalculator = false;
        this.showBtnCalculator = true;
        this.dataCalculator = { ...result.item };
        this.dataCalculator.results.forEach((element, index) => {
          arrayTable.push({ depth: this.dataCalculator.inputs.depths[index], ...element });
        });
        this.dataCalculator.results = arrayTable;
        this.dataInputs = {
          name: result.item.name,
          ...this.dataCalculator.inputs
        };
        if (!this.showRange) {
          this.dataInputs.depths.forEach((element) => {
            if (element.from) {
              arrayDepths.push(element.from);
            } else {
              arrayDepths.push(element);
            }
          });
        } else {
          this.dataInputs.depths.forEach((element) => {
            arrayDepths.push({
              from: element,
              to: element,
              increment: 1
            });
          });
        }
        this.dataInputs.depths.splice(0, this.dataInputs.depths.length - 1);
        this.dataInputs.depths = arrayDepths;
        this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
      }, 1000);
    }
  }

  /**
   * change value unit conversion from feet to meter & vice-versa
   * @param event event change value
   */
  changeValueFeetToMeter(event) {
    this.txtUnit = event.value;
    if (
      this.dataInputs.obGradient ||
      this.dataInputs.poissonSRatio ||
      this.dataInputs.porePressureGradient ||
      this.dataInputs.fractGradient
    ) {
      const amount = [];
      this.dataInputs.depths.forEach((element) => {
        if (element.from) {
          amount.push(parseFloat(element.from));
        }
        if (element.to) {
          amount.push(parseFloat(element.to));
        }
        if (!element.from && !element.to) {
          amount.push(parseFloat(element ? element : 0));
        }
      });
      const data = {
        from: event.value === 'm' ? 'feet' : 'meter',
        to: event.value === 'ft' ? 'feet' : 'meter',
        amount
      };
      this.isLoading = true;
      this.calculatorsSvc
        .convertUnit(data)
        .pipe(finalize(() => (this.isLoading = false)))
        .subscribe((res) => {
          // check if input depth is present
          if (this.dataInputs.depths.length > 0) {
            let index = 0;
            // iterate over depth array and convert from api result
            if (this.showRange) {
              // tslint:disable-next-line: prefer-for-of
              for (let rangeIndex = 0; rangeIndex < this.dataInputs.depths.length; rangeIndex++) {
                const element = this.dataInputs.depths[rangeIndex];
                element.from = res.result[index].toFixed(2);
                index++;
                element.to = res.result[index].toFixed(2);
                index++;
              }
            } else {
              this.dataInputs.depths.forEach((element, i) => {
                if (element) {
                  this.dataInputs.depths[i] = res.result[i].toFixed(2);
                }
              });
            }
          }
        });
    }
  }

  /**
   * change value range checkbox
   */
  changeValueRange(event) {
    this.showRange = event.checked;
    const array = [];
    if (!this.showRange) {
      this.dataInputs.depths.forEach((element) => {
        array.push(element.from);
      });
    } else {
      this.dataInputs.depths.forEach((element) => {
        array.push({
          from: element,
          to: element,
          increment: 1
        });
      });
    }
    this.dataInputs.depths.splice(0, this.dataInputs.depths.length - 1);
    this.dataInputs.depths = array;
  }

  /**
   * callback range start
   * @param event event change input range
   * @param index index change input range
   */
  callbackRangeStart(event, index) {
    if (!this.showRange) {
      this.dataInputs.depths[index] = event;
    } else {
      this.dataInputs.depths[index].from = event;
    }
  }

  /**
   * callback range end
   * @param event event change input range
   */
  callbackRangeEnd(event, index) {
    if (!this.showRange) {
      this.dataInputs.depths[index] = event;
    } else {
      this.dataInputs.depths[index].to = event;
    }
  }

  /**
   * show calculator
   */
  showCalculator() {
    this.showError = false;
    if (
      !this.dataInputs.obGradient ||
      !this.dataInputs.poissonSRatio ||
      !this.dataInputs.porePressureGradient ||
      !this.dataInputs.fractGradient ||
      !this.dataInputs.biotsAlpha
    ) {
      this.showError = true;
    }
    let depths = [];
    if (this.showRange) {
      this.dataInputs.depths.forEach((element) => {
        if (!this.showRange && element === 0) {
          this.showError = true;
        } else if (this.showRange && (element.from === 0 || !element.from) && (element.to === 0 || !element.to)) {
          this.showError = true;
        }
        for (let i = element.from; i < element.to; i += element.increment) {
          depths.push(i);
        }
        depths.push(element.to);
      });
    } else {
      depths = this.dataInputs.depths;
    }
    if (!this.showError) {
      const data: ICreateCalculationData = {
        biotsAlpha: this.dataInputs.biotsAlpha,
        fractGradient: this.dataInputs.fractGradient,
        obGradient: this.dataInputs.obGradient,
        name: this.isUpdate ? this.selectedResult.name : this.dropdownComponent.selectOption,
        poissonSRatio: this.dataInputs.poissonSRatio,
        porePressureGradient: this.dataInputs.porePressureGradient,
        depths
      };
      this.isLoading = true;
      if (this.isUpdate) {
        if (this.txtUnit === 'm') {
          depths = _.map(depths, _.ary(parseFloat, 1));
          this.calculatorsSvc
            .convertUnit({ amount: depths, from: 'meter', to: 'feet' })
            .subscribe((convertData: any) => {
              data.depths = convertData.result;
              this.calculatorsSvc
                .updateCalculation(this.selectedResult.id, data)
                .pipe(finalize(() => (this.isLoading = false)))
                .subscribe((res) => {
                  const result = _.each(res.results, (item: any, index) => {
                    item.depth = depths[index];
                  });
                  this.dataCalculator.results = result;
                  this.showTableCalculator = true;
                  this.showBtnCalculator = true;
                  this.selectedDepths = depths;
                  this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
                });
            });
        } else {
          this.calculatorsSvc
            .updateCalculation(this.selectedResult.id, data)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((res) => {
              const result = _.each(res.results, (item: any, index) => {
                item.depth = depths[index];
              });
              this.dataCalculator.results = result;
              this.showTableCalculator = true;
              this.showBtnCalculator = true;
              this.selectedDepths = depths;
              this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
            });
        }
      } else {
        if (this.txtUnit === 'm') {
          depths = _.map(depths, _.ary(parseFloat, 1));
          this.calculatorsSvc
            .convertUnit({ amount: depths, from: 'meter', to: 'feet' })
            .subscribe((convertData: any) => {
              data.depths = convertData.result;
              this.calculatorsSvc
                .createCalculation(data)
                .pipe(finalize(() => (this.isLoading = false)))
                .subscribe((res) => {
                  const result = _.each(res.results, (item: any, index) => {
                    item.depth = depths[index];
                  });
                  this.dataCalculator.results = result;
                  this.showTableCalculator = true;
                  this.showBtnCalculator = true;
                  this.selectedDepths = depths;
                  this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
                  this.selectedResult = res;
                  this.isUpdate = true;
                });
            });
        } else {
          this.calculatorsSvc
            .createCalculation(data)
            .pipe(finalize(() => (this.isLoading = false)))
            .subscribe((res) => {
              const result = _.each(res.results, (item: any, index) => {
                item.depth = depths[index];
              });
              this.dataCalculator.results = result;
              this.showTableCalculator = true;
              this.showBtnCalculator = true;
              this.selectedDepths = depths;
              this.callback.emit({ showTable: this.showTableCalculator, showBtn: this.showBtnCalculator });
              this.selectedResult = res;
              this.isUpdate = true;
            });
        }
      }
    }
  }

  /**
   * add depth
   */
  addDepthFunc() {
    if (!this.showRange) {
      this.dataInputs.depths.push(0);
    } else {
      this.dataInputs.depths.push({
        from: 0,
        to: 0,
        increment: 1
      });
    }
    setTimeout(() => {
      const inputListEl = this.inputList.nativeElement;
      if (inputListEl) {
        const uls = inputListEl.querySelectorAll('ul');
        const lastUl = uls[uls.length - 1];
        const input = lastUl.querySelector('input');
        input.focus();
      }
    }, 0);
  }

  /**
   * remove depth
   */
  removeDepthFunc() {
    this.dataInputs.depths.pop();
  }
  /**
   * remove Depth Row
   */
  removeDepthRowFunc(i) {
    this.dataInputs.depths.splice(i, 1);
  }

  /**
   * export file to csv
   */
  exportFileCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      title: 'Net Confining Stress',
      useBom: true,
      noDownload: false,
      headers: ['Depth', 'σV', 'σH', 'σh', 'Pp', 'σE', 'σm']
    };
    const result = [];
    _.each(this.dataCalculator.results, (item, index) => {
      result[index] = {};
      result[index].depth = item.depth;
      result[index]['σV'] = this.decimalPipe.transform(item.V, '1.2-2');
      result[index]['H'] = this.decimalPipe.transform(item.H, '1.2-2');
      result[index]['h'] = this.decimalPipe.transform(item.h, '1.2-2');
      result[index]['P'] = this.decimalPipe.transform(item.P, '1.2-2');
      result[index]['E'] = this.decimalPipe.transform(item.E, '0.2-2');
      result[index]['m'] = this.decimalPipe.transform(item.m, '1.2-2');
    });
    const exportFile = new ngxCsv(result, 'Calculator-Net-Confining-Stress', options);
    return exportFile;
  }

  /**
   * fetches the well data
   */
  fetch() {
    this.progressBar.show();
    this.calculatorsSvc
      .getWellData(undefined)
      .pipe(finalize(() => this.progressBar.hide()))
      .subscribe((res: any) => {
        this.optionsWellName = res.items;
        this.storedOptionsWellName = _.cloneDeep(this.optionsWellName);
      });
  }
}
