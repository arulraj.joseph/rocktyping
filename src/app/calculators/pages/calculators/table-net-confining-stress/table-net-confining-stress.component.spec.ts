import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableNetConfiningStressComponent } from './table-net-confining-stress.component';

describe('TableNetConfiningStressComponent', () => {
  let component: TableNetConfiningStressComponent;
  let fixture: ComponentFixture<TableNetConfiningStressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableNetConfiningStressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableNetConfiningStressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
