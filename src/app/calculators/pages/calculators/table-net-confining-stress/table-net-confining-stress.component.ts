import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'xrd-table-net-confining-stress',
  templateUrl: './table-net-confining-stress.component.html',
  styleUrls: ['./table-net-confining-stress.component.scss']
})
export class TableNetConfiningStressComponent implements OnInit {
  @Input() data: any[] = [];
  @Input() unit: string;
  tableFixed: boolean = false;
  top: number = 0;

  @ViewChild('tableHeader') tableHeader: ElementRef;

  // the header columns
  public columns = [
    {
      label: 'Depth',
      tooltip: ''
    },
    {
      label: 'σV',
      tooltip: 'σV = Depth x OB Gradient'
    },
    {
      label: 'σH',
      tooltip: 'σH = Depth x ((OB Gradient + Fract Gradient)/2)'
    },
    {
      label: 'σh',
      tooltip: 'σh = Depth x Fract Gradient'
    },
    {
      label: 'Pp',
      tooltip: 'PP = Depth x Pore Pressure Gradient'
    },
    {
      label: 'σE',
      tooltip: 'σE = [(σV+σH+σh)/3] - αPP '
    },
    {
      label: 'σm',
      tooltip: 'σm = [(Pob-Pres)/3]*[(1+n)/(1-n)]'
    }
  ];

  ngOnInit(): void {
    const tableHeaderEl = this.tableHeader.nativeElement;
    const container = document.querySelector('.cover-content');
    const top = tableHeaderEl.getBoundingClientRect().top + container.scrollTop;
    const containerTop = container.getBoundingClientRect().top;
    const distance = top - containerTop;
    container.addEventListener('scroll', () => {
      const containerScrollTop = container.scrollTop;
      this.tableFixed = containerScrollTop > distance;
      tableHeaderEl.style.top = containerScrollTop - top + 130 + 'px';
    });
  }
}
