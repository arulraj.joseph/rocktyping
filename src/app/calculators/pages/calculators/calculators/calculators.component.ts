import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlackOilPropertiesComponent } from '../black-oil-properties/black-oil-properties.component';
import { NetConfiningStressComponent } from '../net-confining-stress/net-confining-stress.component';

@Component({
  selector: 'xrd-calculators',
  templateUrl: './calculators.component.html',
  styleUrls: ['./calculators.component.scss']
})
export class CalculatorsComponent implements OnInit {

  options = [
    {
      label: 'Net Confining Stress',
      id: 'net-confining-stress'
    },
    {
      label: 'Black Oil Properties',
      id: 'black-oil-properties'
    }
  ];
  selected = null;
  public showBtnCalculator = false;
  public showTableCalculator = false;
  public txtUnit = 'ft';
  @ViewChild('blackOilProperties') blackOilProperties: BlackOilPropertiesComponent;
  @ViewChild('netConfiningStress') netConfiningStress: NetConfiningStressComponent;
  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  /**
   * on init
   */
  ngOnInit() {
    this.selected = this.route.snapshot.paramMap.get('tabId');
  }

  /**
   * export file csv
   */
  exportFileCSV(tab) {
    if (tab === 'black-oil-properties') {
      this.blackOilProperties.exportFileCSV();
    } else {
      this.netConfiningStress.exportFileCSV();
    }
  }

  /**
   * show calculator
   * @param tab text tab change
   */
  showCalculator(tab) {
    if (tab === 'black-oil-properties') {
      this.blackOilProperties.showCalculator();
      this.showTableCalculator = this.blackOilProperties.showTableCalculator;
    } else {
      this.netConfiningStress.showCalculator();
      this.showTableCalculator = this.netConfiningStress.showTableCalculator;
    }
  }

  /**
   * change value unit feet to meter
   * @param event event change value
   */
  changeValueFeetToMeter(event) {
    this.netConfiningStress.changeValueFeetToMeter(event);
  }

  /**
   * callback black oil properties component
   * @param result result callback component
   */
  callbackBlackOilProperties(result) {
    this.showBtnCalculator = result.showBtn;
    this.showTableCalculator = result.showTable;
  }


  /**
   * callback net confining stress component
   * @param result result callback component
   */
  callbackNetConfiningStress(result) {
    this.showBtnCalculator = result.showBtn;
    this.showTableCalculator = result.showTable;
  }

  /**
   * get selected index
   */
  getSelectedIndex() {
    const ids = this.options.map((option) => option.id);
    const index = ids.indexOf(this.selected);
    return index === -1 ? null : index;
  }

  /**
   * handles the navigate to select option click
   * @param $event the event
   */
  navigateTo($event) {
    this.router.navigateByUrl(`/calculators/${$event.value}`);
    this.showBtnCalculator = false;
    this.showTableCalculator = false;
  }
}
