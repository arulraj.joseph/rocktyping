export interface IUnitConverted {
  result: number[];
  created_at: string;
  from: string;
  to: string;
  amount: number[];
}

export interface IBlackOilPropertiesData {
  id: number;
  name: string;
  data: IBlackOilProperties[];
}

export interface IBlackOilProperties {
  inputs: IBlackOilPropertiesInputs;
  calculation: ICalculationBlackOilProperties;
}

export interface IBlackOilPropertiesInputs {
  sampleId: number;
  p: number;
  t: number;
  sga: number;
  sgg: number;
  pb: number;
  rs: number;
  rsb: number;
  tds: number;
  yH2S: number;
  yCO2: number;
  yN2: number;
}

export interface ICalculationBlackOilProperties {
  sampleId: number;
  zFactor: number;
  oilDensity: number;
  oilCompressibility: number;
  oilViscosity: number;
  gor: number;
  bo: number;
  bw: number;
  gwr: number;
  waterDesity: number;
  waterCompressibility: number;
  waterViscosity: number;
  gasViscosity: number;
}

export interface ICalculationResult {
  createdBy: string;
  createdAt: string;
  updatedAt: string;
  id: string;
  name: string;
  inputs: INCSInputs;
  results: IResult[];
  updatedBy: string;
}

export interface IResult {
  m: number;
  E: number;
  V: number;
  H: number;
  h: number;
  P: number;
}

export interface INCSInputs {
  obGradient: number;
  poissonSRatio: number;
  porePressureGradient: number;
  fractGradient: number;
  biotsAlpha: number;
  depths: number[];
}

export interface ICreateCalculationData {
  biotsAlpha: number;
  depths: number[];
  fractGradient: number;
  name: string;
  obGradient: number;
  poissonSRatio: number;
  porePressureGradient: number;
}

export interface IPVTSearchData {
  total: number;
  page: number;
  pageSize: number;
  items: IPVTItem[];
}

export interface IPVTItem {
  createdAt: string;
  updatedAt: string;
  id: string;
  externalId: string;
  name: string;
  input: IPVTInput[];
  output: IPVTOutput[];
}

export interface IPVTInput {
  sampleNumber: string;
  reservoirPressure: number;
  reservoirTemperature: number;
  oilGravity: number;
  specificGasGravity: number;
  oilBPPressure: number;
  solutionGOR: number;
  solutionGORPb: number;
  waterSalinity: number;
  h2sMoleFraction: number;
  co2MoleFraction: number;
  n2MoleFraction: number;
}

export interface IPVTOutput {
  oilDensity: number;
  oilCompressibility: number;
  gor: number;
  bo: number;
  bw: number;
  gwr: number;
  waterDensity: number;
  waterCompressibility: number;
  waterViscosity: number;
  oilViscosity: number;
  gasViscosity: number;
}
