import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatBadgeModule,
  MatButtonModule,
  MatChipsModule,
  MatDialogModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { NanToDashPipe } from './pipes/nan-to-dash.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatTableModule,
    MatButtonModule,
    PerfectScrollbarModule,
    MatChipsModule,
    MatIconModule,
    MatTooltipModule,
    MatBadgeModule
  ],
  declarations: [
    SpinnerComponent,
    NanToDashPipe,
    OnlyNumberDirective
  ],
  exports: [
    SpinnerComponent,
    NanToDashPipe,
    MatSelectModule,
    MatTabsModule,
    OnlyNumberDirective
  ],
  entryComponents: []
})
export class SharedModule { }
