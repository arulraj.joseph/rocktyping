import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService {
  /**
   * Progress bar observable
   *
   * @memberof ProgressBarService
   */
  public progressBarObservable = new Subject<boolean>();

  /**
   * To show progress bar
   *
   * @memberof ProgressBarService
   */
  show() {
    this.progressBarObservable.next(true);
  }
  /**
   * To hide progress bar
   *
   * @memberof ProgressBarService
   */
  hide() {
    this.progressBarObservable.next(false);
  }
}
