import { TestBed } from '@angular/core/testing';

import { CurrentWellService } from './current-well.service';

describe('CurrentWellService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CurrentWellService = TestBed.get(CurrentWellService);
    expect(service).toBeTruthy();
  });
});
