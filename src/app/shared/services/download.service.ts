import { Inject, Injectable } from '@angular/core';
import { WINDOW } from 'ngx-window-token';

@Injectable({
  providedIn: 'root'
})
export class DownloadService {
  constructor(@Inject(WINDOW) private _window: Window) {}

  downloadFile(data, fileName) {
    const blob = new Blob([data], { type: 'application/octet-stream' });
    if (this._window.navigator && this._window.navigator.msSaveOrOpenBlob) {
      this._window.navigator.msSaveOrOpenBlob(blob, fileName);
      return;
    }
    const a = document.createElement('a');
    a.href = URL.createObjectURL(blob);
    a.download = fileName;
    this._window.document.body.appendChild(a);
    a.click();
    this._window.document.body.removeChild(a);
  }
}
