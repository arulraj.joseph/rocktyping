import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IWellData } from '../entities/well-data';

@Injectable({
  providedIn: 'root'
})
export class CurrentWellService {
  private readonly defaultUwiValue = '';

  private currentWell$: BehaviorSubject<IWellData> = new BehaviorSubject(undefined);
  private currentUWI$: BehaviorSubject<string> = new BehaviorSubject(this.defaultUwiValue);

  get currentWell() {
    return this.currentWell$;
  }

  get currentUWI() {
    return this.currentUWI$;
  }

  constructor() {}

  selectWell(well: IWellData): void {
    this.currentWell$.next(well);
    this.currentUWI$.next(well ? well.apiUwiNo : this.defaultUwiValue);
  }
}
