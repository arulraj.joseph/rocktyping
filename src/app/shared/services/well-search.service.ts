import { Injectable } from '@angular/core';
import { IWellData } from '../entities';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

export interface IWellSearchRequest {
  assetGroupId?: string;
  wellNo?: string;
  wellName?: string;
  status?: string;
  reservour?: string;
  afeNo?: string;
  operatorName?: string;
  operatorType?: string;
  apiUwiNo: string;
  countryName?: string;
  stateName?: string;
  countyName?: string;
  fieldName?: string;

  pageNo: number;
  pageSize: number;
}

export interface IWellSearchResponse {
  productionWellList: IWellData[];
}

@Injectable({
  providedIn: 'root'
})
export class WellSearchService {
  private readonly MaxPageSize = 250;

  constructor(private http: HttpClient) {}

  public searchWells(searchTerms: string | IWellSearchRequest): Observable<IWellData[]> {
    if (typeof searchTerms === 'string') {
      return this.searchWellsByFreetext(searchTerms as string);
    } else {
      return this.searchWellsByParameters(searchTerms as IWellSearchRequest);
    }
  }

  private searchWellsByFreetext(query: string): Observable<IWellData[]> {
    const parameters: IWellSearchRequest = {
      apiUwiNo: query,
      pageNo: 1,
      pageSize: this.MaxPageSize
    };

    return this.http
      .post<IWellSearchResponse>(`{ANORM_SERVICE}/services/well/production/search`, parameters)
      .pipe(map((data) => data.productionWellList));
  }

  private searchWellsByParameters(parameters: IWellSearchRequest): Observable<IWellData[]> {
    return this.http
      .post<IWellSearchResponse>(`{ANORM_SERVICE}/services/well/production/search`, parameters)
      .pipe(map((data) => data.productionWellList));
  }
}
