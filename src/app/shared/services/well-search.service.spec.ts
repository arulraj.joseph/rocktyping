import { TestBed } from '@angular/core/testing';

import { WellSearchService } from './well-search.service';

describe('WellSearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WellSearchService = TestBed.get(WellSearchService);
    expect(service).toBeTruthy();
  });
});
