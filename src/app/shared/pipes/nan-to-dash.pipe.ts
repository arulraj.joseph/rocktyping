import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nanToDash'
})
export class NanToDashPipe implements PipeTransform {
  transform(value: number): string | number {
    return isNaN(value) ? '-' : value;
  }
}
