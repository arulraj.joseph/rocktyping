export interface IWellData {
  wellName?: string;
  wellNo?: string;
  cmplNo?: string;
  cmplName?: string;
  slLatitude?: number;
  slLongitude?: number;
  apiUwiNo?: string;
  reservoir?: string;
  afeNo?: string;
  operatorName?: string;
  operatorType?: string;
  countryName?: string;
  stateName?: string;
  countyName?: string;
  fieldName?: string;

  isCustomMetadata?: boolean;
}
