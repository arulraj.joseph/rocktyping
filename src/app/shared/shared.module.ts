import { CommonModule, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatBadgeModule,
  MatChipsModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';
import { FileDropModule } from 'ngx-file-drop';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ConfirmRemoveDialogComponent } from './components/confirm-remove-dialog/confirm-remove-dialog.component';
import { InlineEditComponent } from './components/inline-edit/inline-edit.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { NanToDashPipe } from './pipes/nan-to-dash.pipe';
import { GoBackComponent } from './components/go-back/go-back.component';
import { NoCommaPipe } from './pipes/no-comma.pipe';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faFilePdf, faFileCsv, faMinusCircle, faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { HighchartsChartModule } from 'highcharts-angular';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { NumericEditorComponent } from './components/numeric-editor/numeric-editor.component';

@NgModule({
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatButtonModule,
    PerfectScrollbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    FileDropModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatTableModule,
    HighchartsChartModule,
    MatChipsModule,
    MatTooltipModule,
    MatBadgeModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    SpinnerComponent,
    NanToDashPipe,
    ConfirmRemoveDialogComponent,
    InlineEditComponent,
    UploadFileComponent,
    GoBackComponent,
    NoCommaPipe,
    BreadcrumbsComponent,
    LineChartComponent,
    DropdownComponent,
    OnlyNumberDirective,
    NumericEditorComponent
  ],
  providers: [DecimalPipe],
  exports: [
    SpinnerComponent,
    NanToDashPipe,
    InlineEditComponent,
    UploadFileComponent,
    GoBackComponent,
    NoCommaPipe,
    BreadcrumbsComponent,
    FontAwesomeModule,
    LineChartComponent,
    MatSelectModule,
    MatTabsModule,
    OnlyNumberDirective,
    DropdownComponent
  ],
  entryComponents: [ConfirmRemoveDialogComponent, NumericEditorComponent]
})
export class SharedModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
    library.add(faFilePdf, faFileCsv, faMinusCircle, faPlusCircle);
  }
}
