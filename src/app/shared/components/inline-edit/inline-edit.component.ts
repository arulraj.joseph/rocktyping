import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'gng-inline-edit',
  templateUrl: './inline-edit.component.html',
  styleUrls: ['./inline-edit.component.scss']
})
export class InlineEditComponent {
  @Input() text = '';
  @Input() placeholder = '';
  @Input() showEditIcon = true;
  @Output() changeText = new EventEmitter<string>();
  textForm: FormGroup;
  @HostListener('document:click', ['$event'])
  onClick(event) {
    if (!this.eref.nativeElement.contains(event.target)) {
      this.closeEditMode();
    }
  }

  constructor(private fb: FormBuilder, private eref: ElementRef) {}

  openEditMode(event) {
    event.stopPropagation();
    event.preventDefault();
    this.initForm();
  }

  closeEditMode() {
    this.textForm = null;
  }

  onSubmit(event) {
    event.stopPropagation();
    event.preventDefault();
    if (this.textForm.valid) {
      this.text = this.textForm.controls.text.value;
      this.changeText.next(this.text);
      this.closeEditMode();
    }
  }
  private initForm() {
    this.textForm = this.fb.group({
      text: [this.text, Validators.required]
    });
  }
}
