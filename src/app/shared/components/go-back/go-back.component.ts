import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'gng-go-back',
  templateUrl: './go-back.component.html',
  styleUrls: ['./go-back.component.scss']
})
export class GoBackComponent implements OnInit {
  @Input() content: string;

  constructor(private location: Location) {}

  ngOnInit() {
    this.content = this.content || '';
  }

  goBack() {
    this.location.back();
  }
}
