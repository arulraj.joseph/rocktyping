import { Component, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Component({
  selector: 'pvt-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent {
  @Input() data: any[] = [];
  @Input() selectOption = '';
  @Input() hasFiltering = false;
  @Output() callback = new EventEmitter();
  constructor(private el: ElementRef) {}

  /**
   * handles the search
   * @param event the event
   */
  searchQuery(event) {
    event.currentTarget.parentElement.parentElement.classList.add('show-dropdown');
    if (this.hasFiltering) {
      this.callback.emit({ event: 'search', query: this.selectOption });
    } else {
      if (this.selectOption !== '') {
        this.callback.emit({ event: 'search', query: this.selectOption });
      } else {
        this.callback.emit({ event: 'search', query: '' });
      }
    }
  }

  /**
   * handles the show dropdown
   * @param event the event
   */
  showDropdownFunc(event) {
    if (event.currentTarget.parentElement.parentElement.classList.contains('show-dropdown')) {
      event.currentTarget.parentElement.parentElement.classList.remove('show-dropdown');
    } else {
      event.currentTarget.parentElement.parentElement.classList.add('show-dropdown');
    }
    this.callback.emit({ event: 'search', query: '' });
  }

  /**
   * handles select item dropdown
   * @param item the selected item
   */
  selectItem(item): void {
    this.el.nativeElement.querySelector('.group-dropdown').classList.remove('show-dropdown');
    this.selectOption = item.name;
    this.callback.emit({ event: 'select', item });
  }

  /**
   * focus text box
   * @param event event focus text box
   */
  focus(event) {
    event.currentTarget.parentElement.parentElement.classList.add('show-dropdown');
    this.callback.emit({ event: 'search', query: '' });
  }

  /**
   * reset dropdown
   */
  resetDropdown(val) {
    this.selectOption = val;
  }

  /**
   * handles the click body
   * @param targetElement the target element
   */
  @HostListener('document:click', ['$event.target'])
  onClick(targetElement): void {
    if (!this.el.nativeElement.contains(targetElement)) {
      this.el.nativeElement.querySelector('.group-dropdown').classList.remove('show-dropdown');
    }
  }
}
