import { Component, OnInit, Input } from '@angular/core';
import { CurrentWellService } from '../../services/current-well.service';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'gng-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {
  @Input()
  additionalParts: string[] = [];

  parts: string[] = [];

  constructor(private currentWellService: CurrentWellService) {}

  ngOnInit() {
    combineLatest(this.currentWellService.currentWell).subscribe((data) => {
      const parts = [];

      const selectedWell = data[0];

      if (selectedWell && selectedWell.wellName) {
        parts.push(selectedWell.wellName);
      }

      parts.push(...(this.additionalParts || []));

      this.parts = parts;
    });
  }
}
