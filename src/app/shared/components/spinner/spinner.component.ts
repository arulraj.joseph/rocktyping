import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'gng-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  @Input() diameter: number = 100;
  constructor() {}

  ngOnInit() {}
}
