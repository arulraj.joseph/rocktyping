import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

export interface IConfirmRemoveDialogData {
  name: string;
}

@Component({
  selector: 'gng-confirm-remove-dialog',
  templateUrl: './confirm-remove-dialog.component.html',
  styleUrls: ['./confirm-remove-dialog.component.scss']
})
export class ConfirmRemoveDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: IConfirmRemoveDialogData) {}
}
