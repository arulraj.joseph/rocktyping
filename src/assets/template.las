# LAS WORX (tm) v18.02.19 Header Hash (5373) Data Hash (17433) DO NOT REMOVE THIS LINE
~VERSION INFORMATION SECTION
VERS. 2.0 : CWLS LOG ASCII STANDARD-VERSION 2.0
WRAP. NO  : ONE LINE PER DEPTH STEP
#
#      MNEM        UNIT      VALUE/NAME            DESCRIPTION    
#----------------- ---- ---------------------   ------------------
~WELL INFORMATION SECTION
STRT             .F               1340.00000 : TOP DEPTH
STOP             .F               8515.00000 : BOTTOM DEPTH
STEP             .F                  0.50000 : DEPTH INCREMENT
NULL             .                  -999.250 : NULL VALUE
APC_ORIGINAL_WELL.                 SACK 23-6 : ORIGINAL WELL NAME
API              .            05001095780000 : API NUMBER
CNTY             .                     ADAMS : COUNTY
COMP             .      MERIT ENERGY COMPANY : OPERATOR
COUN             .                     ADAMS : RIG NAME
CTRY             .             UNITED STATES : COUNTRY
DATE             .                2007/06/09 : RUN DATE
EGL              .                      5050 : GROUND LEVEL
EKB              .                      5067 : KB ELEVATION
EPD              .                       0.0 : ELEV PERM DATUM
FLD              .                   SPINDLE : FIELD NAME
GDAT             .                     NAD27 : DATUM
LAT              .               39.99544590 : LATITUDE
LMF              .                        KB : LOGS MEAS FROM
LON              .               -104.933457 : LONGITUDE
LOC              .               aaaaaaaaaaA : LOCATION
PDAT             .                       MSL : PERM DATUM
PROV             .                  COLORADO : STATE OR_PROVINCE
RANG             .                       67W : RANGE
SECT             .                         6 : SECTION
SPUD             .                2007/06/06 : SPUD DATE
STAT             .                  COLORADO : STATE
TOWN             .                        1S : TOWNSHIP
UWI              .            05001095780000 : UNIQUE WELL ID
WELL             .                 SACK 23-6 : WELL NAME
#
#MNEM  UNIT    API CODES                    DESCRIPTION                
#---- ------ -------------   ------------------------------------------
# =========                                                                                                                                    ========    == ===========
~CURVE INFORMATION SECTION
DEPT.F       00 001 00 00 : 1  DEPTH
CILD.MMHO-M            01 : 2  DEEP INDUCTION CONDUCTIVITY
DCAL.IN                01 : 3  DIFFERENTIAL CALIPER
DPOR.%                 01 : 4  DENSITY POROSITY
GR  .GAPI              01 : 5  GAMMA RAY
LSD .CPS               01 : 6  LONG SPACED DISCRIMINATOR LEVEL SETTING
NPOR.%                 01 : 7  NEUTRON POROSITY
RHOB.G/CC              01 : 8  BULK DENSITY
RHOC.G/CC              01 : 9  BHC CORR. APPARENT RESISTIVITY
RILD.OHMM              01 : 10  DEEP RESISTIVITY
RILM.OHMM              01 : 11  MEDIUM RESISTIVITY
DTS.OHMM              01 : 12  RAW LL COUNT RATE
DTC  .MV                01 : 13  SPONTANEOUS POTENTIAL
#
#MNEM UNIT            VALUE/NAME                  DESCRIPTION    
#---- ---- ---------------------------------   ------------------
~PARAMETERS INFORMATION SECTION
LNAM.                           DIL/CDS/CDN : NAME
LTYP.                                   RAW : LOG TYPE
LACT.                               MAINLOG : LOG ACTIVITY
LVSN.                                     1 : LOG VERSION
BHIS.                                    RC : BRIEF HISTORY
CLNT.                          SCHLUMBERGER : CLIENT
FHIS.                          RAW-LAS/COPY : FULL HISTORY
CN  .                  MERIT ENERGY COMPANY : OPERATOR
DATE.                         SAT JUN 09 18 : RUN DATE
SRVC.                   PHOENIX SURVEYS INC : LOGGING CONTRACTOR
TFLD.                            WATTENBERG : TAPE FIELD NAME
DFOF.                                  OFDB : DUPLICATE STATUS
SFN .      0500109578_1340_8515_4825_13.LAS : SOURCE FILE NAME
CASE.                                    NO : CASEDHOLE FLAG
DSRC.                                LOGGED : DIGIT SOURCE
GTOL.                          TRIPLE COMBO : GENERIC TOOLSTRING
PLVL.                                 CLEAN : PROCESSING LEVEL
#
#   DEPT      CILD    DCAL    DPOR       GR       LSD      NPOR      RHOB      RHOC       RILD       RILM     DTS      DTC    
#---------- -------- ------ --------- -------- --------- --------- --------- --------- ----------- -------- -------- -------- 
~ASCII LOG DATA
1340.00000    .0000 2.8091 -999.250   97.7755 -999.250  -999.250  0.7  		 -999.250  100000.0000   4.8671   101  		200 
1340.50000 155.5300 2.8081 -999.250   94.1707 -999.250  -999.250  0.7 		 -999.250       6.4296   3.0905   201  		100 
