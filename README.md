# APC NG STARTER
A quick start for APC AAET Web Application Development.
---
## Cloning the project
Clone the project from http://tfs.anadarko.com:8080/tfs/DefaultCollection/apc-ng/_git/apc-ng-starter
```
git clone http://tfs.anadarko.com:8080/tfs/DefaultCollection/apc-ng/_git/apc-ng-starter
npm install
```
---
## Running the project locally
* Clone from the apc-ng-starter Repository 
* NPM Start to initiate the app on http://localhost:4200
```
npm start
```
* Navigate to http://localhost:4200  
* _See the Troubleshooting CLI Version issues section below if the app doesn't run_

### What npm start does and alternative start process  
The _npm start_ command above runs the following code:  
`ng serve --app=dev`  
  
This starts the app locally using the dev environment.  
  
To run start the app with another environment:  
`ng serve --env=test --app=web` 
---
## Troubleshooting CLI Version issues  
You should have a version of the CLI installed equal to or greater than the version
used in the project.  
>See [the angular-cli page](https://github.com/angular/angular-cli) for the steps to update the CLI. Search for 'Updating angular-cli' on the page - it's near the bottom of the page.  

---
## Angular CLI commands  
### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `www/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/). 
Before running the tests make sure you are serving the app via `ng serve`.

<!--
### Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.
-->

### Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
