import { DashboardPage } from './dashboard.po';

describe('Dashboard', () => {
  let page: DashboardPage;

  beforeEach(() => {
    page = new DashboardPage();
  });

  it('should display app title', async () => {
    await page.navigateToDashboard();
    expect(page.getAppTitleText()).toEqual('Welcome to your new app!');
  });
});
