import { browser, by, element } from 'protractor';

export class DashboardPage {
  navigateToDashboard() {
    return browser.get('/dashboard');
  }

  getAppTitleText() {
    return element(by.css('.card-title')).getText();
  }
}
